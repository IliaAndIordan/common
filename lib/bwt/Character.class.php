<?php 
/******************************** HEAD_BEG ************************************
*
* Project                	: ams
* Module                        : ams
* Responsible for module 	: IordIord
*
* Filename               	: Character.class.php
*
* Database System        	: MySQL
* Created from                  : IordIord
* Date Creation			: 21.03.2016
*------------------------------------------------------------------------------
*                        Description
*------------------------------------------------------------------------------
* @TODO Insert some description.
*
*------------------------------------------------------------------------------
*                        History
*------------------------------------------------------------------------------
* HISTORY:
* <br>--- $Log: Character.class.php,v $
* <br>---
* <br>---
*
********************************* HEAD_END ************************************
*/
require_once("GVDataTable.class.php");

// <editor-fold defaultstate="collapsed" desc="Character Class">

/**
 * Description of Character class
 *
 * @author IordIord
 */
class Character {
/**
 * ***************************************************************************
 * Methods Declarations
 * ***************************************************************************
 */
    
  // <editor-fold defaultstate="collapsed" desc="Update Data">

    public function save() {
        $mn = "Character->save()";
        BwtLogger::logBegin($mn);

        BwtLogger::log($mn, "is_object(this)=" . is_object($this));
        
        if (is_object($this)) {
            try {
                BwtLogger::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {
                    BwtLogger::log($mn,  "Insert ");
                    $strSQL = "INSERT INTO " . Character::TABLE_NAME . " (".
                    Character::COL_NAME_CRTR_NAME . ", ".
                    Character::COL_NAME_CRTR_NIKNAME . ", ".
                    Character::COL_NAME_CRTR_URL . ", ".
                    Character::COL_NAME_BOOK_ID . ", ".
                    Character::COL_NAME_CRTR_DESC . ", ".
                    Character::COL_NAME_CRTR_ORDER . ", ".
                    Character::COL_NAME_CRTR_IMAGE_URL . ") ".
                    " VALUES(?, ?, ?, ?, ?, ?, ?)";

                    $bound_params_r = ["sssisis",
                        (($this->crtr_name == null) ? null : $this->crtr_name),
                        (($this->crtr_nikname == null) ? null : $this->crtr_nikname),
                        (($this->crtr_url == null) ? null : $this->crtr_url),
                        (($this->book_id == null) ? null : $this->book_id),
                        (($this->crtr_desc == null) ? null : $this->crtr_desc),
                        (($this->crtr_order == null) ? null : $this->crtr_order),
                        (($this->crtr_image_url == null) ? null : $this->crtr_image_url),
                    ];
                    $conn = BwtConnection::dbConnect();
                    $logModel = BwtLogger::currLogger()->getModule($mn);
                    $this->crtr_id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
                
                    BwtLogger::log("$mn", "crtr_id=" . $this->crtr_id);
                    $this->LoadById($this->crtr_id);
                } else {
                    BwtLogger::log($mn,  "Update ");

                    $strSQL = "UPDATE " . Character::TABLE_NAME;
                    $strSQL .=" SET " . Character::COL_NAME_CRTR_NAME . "=?, ";
                    $strSQL .=Character::COL_NAME_CRTR_NIKNAME . "=?, ";
                    $strSQL .=Character::COL_NAME_CRTR_URL . "=?, ";
                    $strSQL .=Character::COL_NAME_BOOK_ID . "=?, ";
                    $strSQL .=Character::COL_NAME_CRTR_DESC . "=?, ";
                    $strSQL .=Character::COL_NAME_CRTR_ORDER . "=?, ";
                    $strSQL .=Character::COL_NAME_CRTR_IMAGE_URL . "=? ";
                    
                    $strSQL .=" WHERE " . Character::COL_NAME_ID . " = ? ";
                 
                    $bound_params_r = ["sssisisi",
                        (($this->crtr_name == null) ? null : $this->crtr_name),
                        (($this->crtr_nikname == null) ? null : $this->crtr_nikname),
                        (($this->crtr_url == null) ? null : $this->crtr_url),
                        (($this->book_id == null) ? null : $this->book_id),
                        (($this->crtr_desc == null) ? null : $this->crtr_desc),
                        (($this->crtr_order == null) ? null : $this->crtr_order),
                        (($this->crtr_image_url == null) ? null : $this->crtr_image_url),
                        $this->getId()
                    ];

                    $conn = BwtConnection::dbConnect();
                    $logModel = BwtLogger::currLogger()->getModule($mn);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    BwtLogger::log($mn, "affectedRows=" . $affectedRows);
                    
                }
            } catch (Exception $ex) {
                BwtLogger::log($mn, "Error id " . $this->getId());
                BwtLogger::logError($mn, $ex);
            }
        }
        BwtLogger::logEnd($mn);
        return $this;
    }
    
    public static function deleteById($id) {
        $mn = "Character.deleteById()";
        BwtLogger::logBegin($mn);
        BwtLogger::log($mn, " id = ".$id);
        //$this->setId($id);
        $sql = "DELETE ".
                " FROM ".Character::TABLE_NAME." ".
                " WHERE ".Character::COL_NAME_ID."=?";
        $bound_params_r = ["i",$id];
         $conn = BwtConnection::dbConnect();
        $logModel = BwtLogger::currLogger()->getModule($mn);
        $deleted_id = $conn->preparedDelete($sql, $bound_params_r, $logModel);
        BwtLogger::logEnd($mn);
    }

// </editor-fold>
    
    
  // <editor-fold defaultstate="collapsed" desc="Load Data">
    
    
    public function loadById($id)
    {
        $mn = "ams:Character.loadById()";
        BwtLogger::logBegin($mn);
        BwtLogger::log($mn, "id = ".$id);
        $this->setId($id);
        $sql = "SELECT ".Character::getAllColumnsSQL().
                " FROM ".Character::TABLE_NAME." ".
                " WHERE ".Character::COL_NAME_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = BwtConnection::dbConnect();
        $logModel = BwtLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        BwtLogger::log($mn, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //BwtLogger::log($mn, "count(data)=".count($data));
        }
        //BwtLogger::log("$mn", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        BwtLogger::log($mn, "Airline is ".$this->toString());

        BwtLogger::logEnd($mn);
    }
    
    public function loadFromArray($result) {
        $mn = "Character.loadFromArray()";
         BwtLogger::logBegin($mn);
        try{
            if(!$result==null) {
                BwtLogger::log($mn, "result: ".implode(",", $result));
               
                $this->crtr_id = ($result[Character::COL_NAME_ID]);
                $this->crtr_name = ($result[Character::COL_NAME_CRTR_NAME]);
                $this->crtr_nikname = ($result[Character::COL_NAME_CRTR_NIKNAME]);
                $this->crtr_url = ($result[Character::COL_NAME_CRTR_URL]);
                $this->book_id = ($result[Character::COL_NAME_BOOK_ID]);
                $this->crtr_desc = ($result[Character::COL_NAME_CRTR_DESC]);
                $this->crtr_order = ($result[Character::COL_NAME_CRTR_ORDER]);
                $this->crtr_image_url = ($result[Character::COL_NAME_CRTR_IMAGE_URL]);
            }
        }
        catch(Exception $ex){BwtLogger::log($mn, "Error id ".$this->getId());BwtLogger::logError($mn, $ex);}
        BwtLogger::logEnd($mn);
    }

    public function toString()
    {
        $retValue = $this->toJSON();
        return $retValue;
    }
    
    public function toJSON() {
        return json_encode($this);
    }
    
     // </editor-fold>
    
  
   /****************************************************************************
   * Parameters Declarations
   * ***************************************************************************
   */
   
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">
    
    public function getId() {
        return $this->crtr_id;
    }
    
    public function setId($id) {
        $this->crtr_id = $id;
    }

    public $crtr_id;
    public $crtr_name;
    public $crtr_nikname;
    public $crtr_url;
    public $book_id;
    public $crtr_desc;
    public $crtr_order;
    public $crtr_image_url;
   
// </editor-fold>
    
    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME            = "iordanov_bwt.bwt_character";
    
    const COL_NAME_ID           = "crtr_id";
    const COL_NAME_CRTR_NAME    = "crtr_name";
    const COL_NAME_CRTR_NIKNAME = "crtr_nikname";
    const COL_NAME_CRTR_URL     = "crtr_url";
    const COL_NAME_BOOK_ID      = "book_id";
    const COL_NAME_CRTR_DESC    = "crtr_desc";
    const COL_NAME_CRTR_ORDER   = "crtr_order";
    const COL_NAME_CRTR_IMAGE_URL   = "crtr_image_url";
    
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . Character::COL_NAME_ID . ", " .
                Character::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " .  Character::COL_NAME_CRTR_NAME . ", " .
                Character::COL_NAME_CRTR_NIKNAME . ", " .
                Character::COL_NAME_CRTR_URL . ", " .
                Character::COL_NAME_BOOK_ID . ", " .
                Character::COL_NAME_CRTR_DESC . ", " .
                Character::COL_NAME_CRTR_ORDER . ", " .
                 Character::COL_NAME_CRTR_IMAGE_URL;
    }

    // </editor-fold>
}


// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Character GVDT">

// </editor-fold>

