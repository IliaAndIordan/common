<?php


/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Sep 27, 2018 
 *  Filename          : BwtChapterModel.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

require_once 'Book.class.php';

/**
 * Description of BwtChapterModel
 *
 * @author IZIordanov
 */
class BwtChapterModel {

    public $id;
    public $title;
    public $subtitle;
    public $type;
    public $notes;
    public $bookId;
    public $content;
    public $order;
    public $adate;
    public $udate;

    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($id) {
        $mn = "BwtChapterModel::LoadById(" . $id . ")";
        BwtLogger::logBegin($mn);
        $response = new BwtChapterModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $objArrJ = BwtChapterModel::SelectJson($id, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = null;
        }
        BwtLogger::logEnd($mn);
        return $response;
    }

    public static function Save($data) {
        $mn = "BwtChapterModel::Save()";
        BwtLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //BwtLogger::log($mn, " flightName = " . $dataJson->flightName);
        $response = new BwtChapterModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $id = null;
            if (isset($dataJson->id)) {
                //BwtLogger::log($mn, "Update  book_id =" . $dataJson->id);
                $id = $dataJson->id;
                BwtChapterModel::Update($dataJson, $conn, $mn, $logModel);
            } else {
                //BwtLogger::log($mn, "Create CfgCharterModel");
                $id = BwtChapterModel::Create($dataJson, $conn, $mn, $logModel);
            }

            //BwtLogger::log($mn, " id =" . $id);
            if (isset($id)) {
                $objArrJ = BwtChapterModel::SelectJson($id, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = json_decode(json_encode($objArrJ[0]));
                }
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
        }

        //BwtLogger::log($mn, " response = " . json_encode($response));
        BwtLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = "SELECT ch.bch_id as id,
                    ch.bch_title as title,
                    ch.bch_subtitle as subtitle,
                    IF(b.book_type_id = 6,2,1) as type,
                    ch.bch_notes as notes,
                    ch.book_id as bookId,
                    ch.bch_content as content,
                    ch.bch_order as 'order',
                    ch.adate, ch.udate,
                    chartcount, wordcount, pagescount, 
                    b.user_id as autorId
                FROM iordanov_bwt.bwt_book_chapter ch
                join iordanov_bwt.bwt_book b on ch.book_id = b.book_id
                WHERE ch.bch_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function Create($dataJson, $conn, $mn, $logModel) {

        $strSQL = "INSERT INTO iordanov_bwt.bwt_book_chapter 
            ( bch_title, bch_subtitle, bch_notes, book_id,
            bch_content, bch_order)
            VALUES(?, ?, ?, ?, ?, ?)";

        $bound_params_r = ["sssisi",
            ((!isset($dataJson->title)) ? null : $dataJson->title),
            ((!isset($dataJson->subtitle)) ? null : $dataJson->subtitle),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->bookId)) ? null : $dataJson->bookId),
            ((!isset($dataJson->content)) ? null : $dataJson->content),
            ((!isset($dataJson->order)) ? null : $dataJson->order),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log("$mn", "id=" . $id);

        return $id;
    }

    static function Update($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE iordanov_bwt.bwt_book_chapter
                    SET bch_title=?,    bch_subtitle=?, 
                    bch_notes=?,        book_id=?, 
                    bch_content=?,      bch_order=?,
                    chartcount=?,       wordcount=?, pagescount=? 
                    WHERE bch_id = ? ";

        $bound_params_r = ["sssisiiiii",
            ((!isset($dataJson->title)) ? null : $dataJson->title),
            ((!isset($dataJson->subtitle)) ? null : $dataJson->subtitle),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->bookId)) ? null : $dataJson->bookId),
            ((!isset($dataJson->content)) ? null : $dataJson->content),
            ((!isset($dataJson->order)) ? null : $dataJson->order),
            ((!isset($dataJson->chartcount)) ? null : $dataJson->chartcount),
            ((!isset($dataJson->wordcount)) ? null : $dataJson->wordcount),
            ((!isset($dataJson->pagescount)) ? null : $dataJson->pagescount),
            ((!isset($dataJson->id)) ? null : $dataJson->id),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->id;
    }

    static function Delete($id, $conn, $mn, $logModel) {

        $strSQL = "DELETE FROM iordanov_bwt.bwt_book_chapter
                   WHERE bch_id = ? ";

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        BwtLogger::log($mn, "deleted bch_id =" . $id);

        return $id;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Table Methods">

    public static function ChapterTable($params, $actorId) {
        $mn = "BwtChapterModel::ChapterTable()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT ch.bch_id as id,
                        ch.bch_title as title,
                        ch.bch_subtitle as subtitle,
                        IF(b.book_type_id = 6,2,1) as type,
                        ch.bch_notes as notes,
                        ch.book_id as bookId,
                        ch.bch_content as content,
                        ch.bch_order as 'order',
                        ch.adate, ch.udate,
                        ch.chartcount, ch.wordcount, ch.pagescount, 
                        b.user_id as autorId
                    FROM iordanov_bwt.bwt_book_chapter ch
                    join iordanov_bwt.bwt_book b on ch.book_id = b.book_id";

            $sqlWhere = "";
            if (isset($params->bookId) && strlen($params->bookId) > 0) {
                $sqlWhere = " WHERE b.book_id = " . $params->bookId . " ";
            } else {
                $sqlWhere = " WHERE b.user_id = " . $actorId . " ";
            }

            if (isset($params->filter) && strlen($params->filter) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND ( ifnull(ch.bch_title, ch.bch_subtitle) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or ifnull(ch.bch_subtitle, ch.bch_title) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or ch.bch_notes like '%" . $params->filter . "%' )";
                } else {
                    $sqlWhere .= " WHERE ( ifnull(ch.bch_title, ch.bch_subtitle) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or ifnull(ch.bch_subtitle, ch.bch_title) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or ch.bch_notes like '%" . $params->filter . "%' )";
                }
            }
            $sqlOrder = "";
            if (isset($params->sortCol)) {
                if($params->sortCol == 'order'){
                    $sqlOrder .= " order by ch.bch_order " . ($params->sortDesc ? "desc" : " asc");
                } else{
                    $sqlOrder .= " order by " . $params->sortCol . " " . ($params->sortDesc ? "desc" : " asc");
                }
                
            } else {
                $sqlOrder .= " order by ch.bch_order asc ";
            }
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder) > 1 ? $sqlOrder : "");
            $sql .= " LIMIT ? OFFSET ? ";
            BwtLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->limit, $params->offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("chapters", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_bwt.bwt_book_chapter ch
                    join iordanov_bwt.bwt_book b on ch.book_id = b.book_id " .
                    (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $obj->totalRows);
            //$response->addData("rowsCount", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
}
