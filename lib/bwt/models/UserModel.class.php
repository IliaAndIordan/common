<?php
/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Sep 27, 2018 
 *  Filename            : UserModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2024 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */


/**
 * Description of UserModel
 *
 * @author IZIordanov
 */
class BwtUser {
    
   // <editor-fold defaultstate="collapsed" desc="Fields">

    public $id;
    public $name;
    public $email;
    public $password;
    public $role = 1; //2 predefined roles 1-user, 3 admin, 2 editor
    
    public $isReceiveEMails;
    public $ipAddress;
    public $adate;
    public $udate;
    
    public function toJSON() {
        return json_encode($this);
    }

    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">

    public static function Login($email, $pwd) {
        $mn = "BwtUser::Login()";
        BwtLogger::logBegin($mn);
        //BwtLogger::log($mn, " eMail: " . $email.", password".$pwd); 
        $response = null;
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            
            $objArrJ = BwtUser::LoginJson($email, $pwd, $conn, $mn, $logModel);
            //BwtLogger::log($mn, " objArrJ = " . json_encode($objArrJ));
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = null;
        }
        BwtLogger::logEnd($mn);
        return $response;
    }
    
    public static function LoadById($user_id) {
        $mn = "BwtUser::LoadById(".$user_id.")";
        BwtLogger::logBegin($mn);
        $response = new BwtUser();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $objArrJ = BwtUser::SelectJson($user_id, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = null;
        }
        BwtLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "BwtUser::Save()";
        BwtLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        BwtLogger::log($mn, " dataJson: " . json_encode($dataJson));
        $response = null;
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $user_id = null;
            if(isset($dataJson->id)){
               BwtLogger::log($mn, "Update  id =" . $dataJson->id);
               $user_id = $dataJson->id;
               BwtUser::Update($dataJson, $conn, $mn, $logModel);

            } else{
                BwtLogger::log($mn, "Create user");
                $user_id = BwtUser::Create($dataJson, $conn, $mn, $logModel);
            }
            
            BwtLogger::log($mn, " user_id =" . $user_id);
            $response = BwtUser::LoadById($user_id);
            
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
        }

        BwtLogger::log($mn, " response = " . json_encode($response));
        BwtLogger::logEnd($mn);
        return $response;
    }
    
    public static function ChangePassword($data) {
        $mn = "BwtUser::ChangePassword()";
        BwtLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        BwtLogger::log($mn, " dataJson: " . json_encode($dataJson));
        $response = null;
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $user_id = null;
            if(isset($dataJson->id) && isset($dataJson->password)){
               BwtLogger::log($mn, "Update  id =" . $dataJson->id);
               $user_id = $dataJson->id;
               BwtUser::UpdatePassword($dataJson, $conn, $mn, $logModel);

            }
            BwtLogger::log($mn, " user_id =" . $user_id);
            $response = BwtUser::LoadById($user_id);
            
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
        }

        BwtLogger::log($mn, " response = " . json_encode($response));
        BwtLogger::logEnd($mn);
        return $response;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_bwt.bwt_user
            (user_name, e_mail, password,
            user_role, is_receive_emails, ip_address)
            VALUES(?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["sssiis",
             ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->email)) ? null : $dataJson->email),
            ((!isset($dataJson->password)) ? null : $dataJson->password),
            ((!isset($dataJson->role)) ? 1 : $dataJson->role),
            ((!isset($dataJson->isReceiveEmails)) ? 0 : $dataJson->isReceiveEmails),
            ((!isset($dataJson->ipAddress)) ? null : json_encode($dataJson->ipAddress)),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        BwtLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
            
        $strSQL = "UPDATE iordanov_bwt.bwt_user
                    SET user_name=?, e_mail=?, 
                    user_role=?, 
                    is_receive_emails=?, 
                    ip_address=?
                    WHERE user_id = ? " ;

        $bound_params_r = ["ssiisi",
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->email)) ? null : $dataJson->email),
            ((!isset($dataJson->role)) ? 3 : $dataJson->role),
            ((!isset($dataJson->isReceiveEmails)) ? 0 : $dataJson->isReceiveEmails),
            ((!isset($dataJson->ipAddress)) ? null : json_encode($dataJson->ipAddress)),
            ($dataJson->id),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        BwtLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->id;
    }
    
    static function UpdatePassword($dataJson, $conn, $mn, $logModel){
            
        $strSQL = "UPDATE iordanov_bwt.bwt_user
                    SET password=?, ip_address=?
                    WHERE user_id = ? " ;

        $bound_params_r = ["ssi",
            ((!isset($dataJson->password)) ? null : $dataJson->password),
            ((!isset($dataJson->ipAddress)) ? null : json_encode($dataJson->ipAddress)),
            ($dataJson->id),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        BwtLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->id;
    }
            
    static function SelectJson($uaer_id, $conn, $mn, $logModel){
        
        $sql = "SELECT u.user_id    as id, 
                    u.user_name     as name,
                    u.e_mail        as email, 
                    u.user_role     as role, 
                    u.is_receive_emails as isReceiveEMails, 
                    u.ip_address    as ipAddress, 
                    u.adate, u.udate
                    FROM iordanov_bwt.bwt_user u
                    WHERE u.user_id = ? " ;

        $bound_params_r = ["i",$uaer_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
            
    static function LoginJson($email, $pwd, $conn, $mn, $logModel){
        
        $sql = "SELECT u.user_id    as id, 
                    u.user_name     as name,
                    u.e_mail        as email, 
                    u.user_role     as role, 
                    u.is_receive_emails as isReceiveEMails, 
                    u.ip_address    as ipAddress, 
                    u.adate, u.udate
                    FROM iordanov_bwt.bwt_user u
                    WHERE u.e_mail = ? and u.password =?" ;

        $bound_params_r = ["ss",$email, $pwd];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function CheckEmailJson($email, $conn, $mn, $logModel){
        
        $sql = "SELECT count(*) as rowCount, '".$email."' as email
                    FROM iordanov_bwt.bwt_user u
                    WHERE u.e_mail = ? 
                    GROUP By u.e_mail ";

        $bound_params_r = ["s",$email];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
            
    public static function UsersTable($params) {
        $mn = "BwtUser::UsersTable()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT u.user_id    as id, 
                    u.user_name         as name,
                    u.e_mail            as email, 
                    u.user_role         as role, 
                    u.is_receive_emails as isReceiveEMails, 
                    u.ip_address        as ipAddress, 
                    u.adate, u.udate
                    FROM iordanov_bwt.bwt_user u
                     ";
            
            
            $sqlWhere="";
            if(isset($params->role) && strlen($params->role)>0){
                $sqlWhere = " WHERE u.user_role = ".$params->role." ";
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (u.e_mail like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or u.user_name like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere = " WHERE (u.e_mail like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or u.user_name like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = "";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by u.".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by u.e_mail, u.user_name, u.user_id ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            BwtLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("usersList", $ret_json_data);
            
            $sql = "SELECT count(*) as total_rows
                    FROM iordanov_bwt.bwt_user u ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("totals", $ret_json_data);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
}


/**
 * Description of UserModel
 *
 * @author IZIordanov
 */
class UserModel {
    //put your code here
    
    function __construct($amsUser) {
        $this->id = $amsUser->getId();
        $this->eMail = $amsUser->getEMail();
        $this->name = $amsUser->getName();
        $this->role = $amsUser->getRole();
        $this->roleName = $amsUser->getRoleName();
        $this->isReceiveEMails = $amsUser->getIsReceiveEMails();
        $this->ipAddress = $amsUser->getIpAddress();
        $this->lastLogged = $amsUser->getLastLogged();
        $this->updated = $amsUser->getUpdated();
    }
}
