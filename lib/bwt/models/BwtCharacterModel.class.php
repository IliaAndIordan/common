<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Jen 27, 2023 
 *  Filename            : BwtCharacterModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2024 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

require_once 'Book.class.php';

/**
 * Description of BwtCharacterModel
 *
 * @author IZIordanov
 */
class BwtCharacterModel {

    public $id;
    public $fullname;
    public $nickname;
    public $sortOrder;
    public $notes;
    public $bookId;
    public $bgImageUrl;
    public $imageUrl;

    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($id) {
        $mn = "BwtCharacterModel::LoadById(" . $id . ")";
        BwtLogger::logBegin($mn);
        $response = new BwtCharacterModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $objArrJ = BwtCharacterModel::SelectJson($id, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = null;
        }
        BwtLogger::logEnd($mn);
        return $response;
    }

    public static function Save($data) {
        $mn = "BwtCharacterModel::Save()";
        BwtLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //BwtLogger::log($mn, " flightName = " . $dataJson->flightName);
        $response = new BwtCharacterModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $id = null;
            if (isset($dataJson->id)) {
                //BwtLogger::log($mn, "Update  book_id =" . $dataJson->id);
                $id = $dataJson->id;
                BwtCharacterModel::Update($dataJson, $conn, $mn, $logModel);
            } else {
                //BwtLogger::log($mn, "Create CfgCharterModel");
                $id = BwtCharacterModel::Create($dataJson, $conn, $mn, $logModel);
            }

            //BwtLogger::log($mn, " id =" . $id);
            if (isset($id)) {
                $objArrJ = BwtCharacterModel::SelectJson($id, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = json_decode(json_encode($objArrJ[0]));
                }
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
        }

        //BwtLogger::log($mn, " response = " . json_encode($response));
        BwtLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = "SELECT crtr_id as id,
                    crtr_name as fullname,
                    crtr_nikname as nickname,
                    crtr_order as sortOrder,
                    crtr_desc as notes,
                    book_id as bookId,
                    crtr_url as bgImageUrl,
                    crtr_image_url as imageUrl
                FROM iordanov_bwt.bwt_character
                WHERE crtr_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function Create($dataJson, $conn, $mn, $logModel) {

        $strSQL = "INSERT INTO iordanov_bwt.bwt_character 
            ( crtr_name, crtr_nikname, crtr_order, crtr_desc, 
            book_id, crtr_url, crtr_image_url)
            VALUES(?, ?, ?, ?, ?, ?, ?)";

        $bound_params_r = ["ssisiss",
            ((!isset($dataJson->fullname)) ? null : $dataJson->fullname),
            ((!isset($dataJson->nickname)) ? null : $dataJson->nickname),
            ((!isset($dataJson->sortOrder)) ? null : $dataJson->sortOrder),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->bookId)) ? null : $dataJson->bookId),
            ((!isset($dataJson->bgImageUrl)) ? null : $dataJson->bgImageUrl),
            ((!isset($dataJson->imageUrl)) ? null : $dataJson->imageUrl),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log("$mn", "id=" . $id);

        return $id;
    }

    static function Update($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE iordanov_bwt.bwt_character
                    SET crtr_name=?,    crtr_nikname=?, 
                    crtr_order=?,        crtr_desc=?, 
                    book_id=?,      crtr_url=?,
                    crtr_image_url=?
                    WHERE crtr_id = ? ";

        $bound_params_r = ["ssisissi",
            ((!isset($dataJson->fullname)) ? null : $dataJson->fullname),
            ((!isset($dataJson->nickname)) ? null : $dataJson->nickname),
            ((!isset($dataJson->sortOrder)) ? null : $dataJson->sortOrder),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->bookId)) ? null : $dataJson->bookId),
            ((!isset($dataJson->bgImageUrl)) ? null : $dataJson->bgImageUrl),
            ((!isset($dataJson->imageUrl)) ? null : $dataJson->imageUrl),
            ((!isset($dataJson->id)) ? null : $dataJson->id),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->id;
    }

    static function Delete($id, $conn, $mn, $logModel) {

        $strSQL = "DELETE FROM iordanov_bwt.bwt_character
                   WHERE crtr_id = ? ";

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        BwtLogger::log($mn, "deleted crtr_id =" . $id);

        return $id;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Table Methods">

    public static function CharacterTable($params, $actorId) {
        $mn = "BwtCharacterModel::CharacterTable()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT cr.crtr_id as id,
                        cr.crtr_name as fullname,
                        cr.crtr_nikname as nickname,
                        cr.crtr_order as sortOrder,
                        cr.crtr_desc as notes,
                        cr.book_id as bookId,
                        cr.crtr_url as bgImageUrl,
                        cr.crtr_image_url as imageUrl
                    FROM iordanov_bwt.bwt_character cr
                    left join iordanov_bwt.bwt_book_chapter_character ch on ch.crtr_id = cr.crtr_id
                    join iordanov_bwt.bwt_book b on cr.book_id = b.book_id ";

            $sqlWhere = "";
            if (isset($params->bookId) && strlen($params->bookId) > 0) {
                $sqlWhere = " WHERE b.book_id = " . $params->bookId . " ";
            } else {
                $sqlWhere = " WHERE b.user_id = " . $actorId . " ";
            }

            if (isset($params->chapterId) && strlen($params->chapterId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and ch.bch_id = " . $params->chapterId . " ";
                } else {
                    $sqlWhere = " WHERE ch.bch_id = " . $params->chapterId . " ";
                }
            }

            if (isset($params->filter) && strlen($params->filter) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND ( ifnull(cr.crtr_name, cr.crtr_nikname) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or ifnull(cr.crtr_nikname, cr.crtr_name) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or cr.crtr_desc like '%" . $params->filter . "%' )";
                } else {
                    $sqlWhere .= " WHERE ( ifnull(cr.crtr_name, cr.crtr_nikname) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or ifnull(cr.crtr_nikname, cr.crtr_name) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or cr.crtr_desc like '%" . $params->filter . "%' )";
                }
            }
            $sqlOrder = "";
            if (isset($params->sortCol)) {
                if ($params->sortCol == 'order') {
                    $sqlOrder .= " order by ch.crtr_order " . ($params->sortDesc ? "desc" : " asc");
                } else {
                    $sqlOrder .= " order by " . $params->sortCol . " " . ($params->sortDesc ? "desc" : " asc");
                }
            } else {
                $sqlOrder .= " order by ch.crtr_order asc ";
            }
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder) > 1 ? $sqlOrder : "");
            $sql .= " LIMIT ? OFFSET ? ";
            BwtLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->limit, $params->offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("characters", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_bwt.bwt_character cr
                    left join iordanov_bwt.bwt_book_chapter_character ch on ch.crtr_id = cr.crtr_id
                    join iordanov_bwt.bwt_book b on cr.book_id = b.book_id " .
                    (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $obj->totalRows);
            //$response->addData("rowsCount", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        return $response;
    }
}
