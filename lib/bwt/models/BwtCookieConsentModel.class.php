<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Sep 27, 2018 
 *  Filename          : BwtCookieConsentModel.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 **/

/**
 * Description of BwtCookieConsentModel
 *
 * @author IZIordanov
 **/
class BwtCookieConsentModel {

    public $id;
    public $ipaddress;
    public $userId;
    public $consentUseLocalStore;
    public $consentUseStatistics;
    public $consentAdvertisement;
    public $adate;
    public $udate;

    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($id) {
        $mn = "BwtCookieConsentModel::LoadById(" . $id . ")";
        BwtLogger::logBegin($mn);
        $response = new BwtCookieConsentModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $objArrJ = BwtCookieConsentModel::SelectJson($id, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = null;
        }
        BwtLogger::logEnd($mn);
        return $response;
    }

    public static function Save($data) {
        $mn = "BwtCookieConsentModel::Save()";
        BwtLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //BwtLogger::log($mn, " flightName = " . $dataJson->flightName);
        $response = new BwtCookieConsentModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $id = null;
            if (isset($dataJson->id)) {
                //BwtLogger::log($mn, "Update  book_id =" . $dataJson->id);
                $id = $dataJson->id;
                BwtCookieConsentModel::Update($dataJson, $conn, $mn, $logModel);
            } else {
                //BwtLogger::log($mn, "Create CfgCharterModel");
                $id = BwtCookieConsentModel::Create($dataJson, $conn, $mn, $logModel);
            }

            //BwtLogger::log($mn, " id =" . $id);
            if (isset($id)) {
                $objArrJ = BwtCookieConsentModel::SelectJson($id, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = json_decode(json_encode($objArrJ[0]));
                }
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
        }

        //BwtLogger::log($mn, " response = " . json_encode($response));
        BwtLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = "select consent_id as id, ipaddress, user_id as userId, 
                consent_local_store as consentUseLocalStore, 
                consent_statistics as consentUseStatistics, 
                consent_advertisement as consentAdvertisement,
                adate, udate
                FROM iordanov_bwt.bwt_cookies_consent
                WHERE consent_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function Create($dataJson, $conn, $mn, $logModel) {

        $strSQL = "INSERT INTO iordanov_bwt.bwt_cookies_consent 
            ( ipaddress, user_id, consent_local_store, consent_statistics, consent_advertisement)
            VALUES(?, ?, ?, ?, ?)";

        $bound_params_r = ["siiii",
            ((!isset($dataJson->ipaddress)) ? null : $dataJson->ipaddress),
            ((!isset($dataJson->userId)) ? null : $dataJson->userId),
            ((!isset($dataJson->consentUseLocalStore)) ? null : ($dataJson->consentUseLocalStore)),
            ((!isset($dataJson->consentUseStatistics)) ? null : ($dataJson->consentUseStatistics)),
            ((!isset($dataJson->consentAdvertisement)) ? null : $dataJson->consentAdvertisement),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log("$mn", "id=" . $id);

        return $id;
    }

    static function Update($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE iordanov_bwt.bwt_cookies_consent
                    SET ipaddress=?,        user_id=?, 
                    consent_local_store=?,  consent_statistics=?,
                    consent_advertisement=?
                    WHERE consent_id = ? ";

        $bound_params_r = ["siiiii",
            ((!isset($dataJson->ipaddress)) ? null : $dataJson->ipaddress),
            ((!isset($dataJson->userId)) ? null : $dataJson->userId),
            ((!isset($dataJson->consentUseLocalStore)) ? null : $dataJson->consentUseLocalStore),
            ((!isset($dataJson->consentUseStatistics)) ? null : $dataJson->consentUseStatistics),
            ((!isset($dataJson->consentAdvertisement)) ? null : $dataJson->consentAdvertisement),
            ((!isset($dataJson->id)) ? null : $dataJson->id),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->id;
    }

    static function Delete($id, $conn, $mn, $logModel) {

        $strSQL = "DELETE FROM iordanov_bwt.bwt_cookies_consent
                   WHERE consent_id = ? ";

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        BwtLogger::log($mn, "deleted consent_id =" . $id);

        return $id;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Table Methods">

    public static function ConsenTable($params, $actorId) {
        $mn = "BwtCookieConsentModel::ConsenTable()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT select c.consent_id as id, c.ipaddress, c.user_id as userId, 
                    c.consent_local_store as consentUseLocalStore, 
                    c.consent_statistics as consentUseStatistics, 
                    c.consent_advertisement as consentAdvertisement,
                    c.adate, c.udate,
                    u.user_name userName, u.e_mail as email
                    FROM iordanov_bwt.bwt_cookies_consent c
                    left join iordanov_bwt.bwt_user u on u.user_id = c.user_id";

            $sqlWhere = "";
            if (isset($params->consentId) && strlen($params->consentId) > 0) {
                $sqlWhere = " WHERE c.consent_id = " . $params->consentId . " ";
            }
            $sqlOrder = " order by c.adate desc ";
           
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder) > 1 ? $sqlOrder : "");
            $sql .= " LIMIT ? OFFSET ? ";
            BwtLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->limit, $params->offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("consents", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_bwt.bwt_cookies_consent c
                    left join iordanov_bwt.bwt_user u on u.user_id = c.user_id " .
                    (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $obj->totalRows);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
}