<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Sep 27, 2018 
 *  Filename          : BookModel.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

require_once 'Book.class.php';
/**
 * Description of BookModel
 *
 * @author IZIordanov
 */
class BookModel {
    //put your code here
    public $id;
    public $title;
    public $subtitle;
    public $coverpageUrl;
    public $userId;
    public $typeId;
    public $desc;
    public $adate;
    public $udate;
    
    function __construct($book) {
        $this->id = $book->getId();
        $this->title = $book->getTitle();
        $this->subtitle = $book->getSubtitle();
        $this->coverpageUrl = $book->getCoverPageUrl();
        $this->userId = $book->getUserId();
        $this->typeId = $book->getTypeId();
        $this->desc = $book->getDescription();
        $this->adate = $book->getIpAddress();
        $this->udate = $book->getLastLogged();
    }
    
    // <editor-fold defaultstate="collapsed" desc="Table Methods">
    
    public static function BooksTable($params, $actorId) {
        $mn = "BookModel::BooksTable()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT b.book_id as id,
                b.book_title as title,
                b.book_subtitle as subtitle,
                b.book_coverpage_url as coverpageUrl,
                b.book_type_id as typeId,
                b.book_desc as description,
                b.adate, b.udate, 
                b.user_id as userId,
                b.user_id as autorId,
                u.user_name as autorName,
                u.e_mail as autorEmail
                FROM iordanov_bwt.bwt_book b
                join iordanov_bwt.bwt_user u on u.user_id = b.user_id";

            $sqlWhere = "";
            if (isset($params->userId) && strlen($params->userId) > 0) {
                $sqlWhere = " WHERE b.user_id = " . $params->userId . " ";
            } else{
                $sqlWhere = " WHERE b.user_id = " . $actorId . " ";
            }

            if (isset($params->typeId) && strlen($params->typeId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and b.book_type_id = " . $params->typeId . " ";
                } else {
                    $sqlWhere = " WHERE b.book_type_id = " . $params->typeId . " ";
                }
            }

            if (isset($params->filter) && strlen($params->filter) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND ( ifnull(b.book_title, b.book_subtitle) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or ifnull(b.book_subtitle, b.book_title) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or b.book_desc like '%" . $params->filter . "%' )";
                } else {
                    $sqlWhere .= " WHERE ( ifnull(b.book_title, b.book_subtitle) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or ifnull(b.book_subtitle, b.book_title) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or b.book_desc like '%" . $params->filter . "%' )";
                }
            }
            $sqlOrder = "";
            if (isset($params->sortCol)) {
                $sqlOrder .= " order by " . $params->sortCol . " " . ($params->sortDesc ? "desc" : " asc");
            } else {
                $sqlOrder .= " order by udate desc, title, id desc ";
            }
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder) > 1 ? $sqlOrder : "");
            $sql .= " LIMIT ? OFFSET ? ";
            BwtLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->limit, $params->offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("books", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_bwt.bwt_book b " . 
                    (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $obj->totalRows);
            //$response->addData("rowsCount", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
}
    class BwtBookModel {
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($id) {
        $mn = "BwtBookModel::LoadById(".$id.")";
        BwtLogger::logBegin($mn);
        $response = new BwtBookModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $objArrJ = BwtBookModel::SelectJson($id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = null;
        }
        BwtLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "BwtBookModel::Save()";
        BwtLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //BwtLogger::log($mn, " flightName = " . $dataJson->flightName);
        $response = new BwtBookModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $id = null;
            if(isset($dataJson->id)){
               //BwtLogger::log($mn, "Update  book_id =" . $dataJson->id);
               $id = $dataJson->id;
               BwtBookModel::Update($dataJson, $conn, $mn, $logModel);

            } else{
                //BwtLogger::log($mn, "Create CfgCharterModel");
                $id = BwtBookModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            //BwtLogger::log($mn, " id =" . $id);
           if(isset($id)){
                $objArrJ = BwtBookModel::SelectJson($id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
        }

        //BwtLogger::log($mn, " response = " . json_encode($response));
        BwtLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    static function SelectJson($id, $conn, $mn, $logModel){
        
        $sql = "SELECT b.book_id as id,
                b.book_title as title,
                b.book_subtitle as subtitle,
                b.book_coverpage_url as coverpageUrl,
                b.book_type_id as typeId,
                b.book_desc as description,
                b.adate, b.udate, 
                b.user_id as userId,
                b.user_id as autorId,
                u.user_name as autorName,
                u.e_mail as autorEmail
                FROM iordanov_bwt.bwt_book b
                join iordanov_bwt.bwt_user u on u.user_id = b.user_id
                WHERE b.user_id = ? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_bwt.bwt_book 
            ( book_title, book_subtitle, book_coverpage_url, user_id,
            book_type_id, book_desc)
            VALUES(?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["sssiis",
             ((!isset($dataJson->title)) ? null : $dataJson->title),
            ((!isset($dataJson->subtitle)) ? null : $dataJson->subtitle),
            ((!isset($dataJson->coverpageUrl)) ? null : $dataJson->coverpageUrl),
            ((!isset($dataJson->userId)) ? null : $dataJson->userId),
            ((!isset($dataJson->typeId)) ? null : $dataJson->typeId),
            ((!isset($dataJson->description)) ? null : $dataJson->description),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_bwt.bwt_book
                    SET book_title=?,   book_subtitle=?, 
                    book_coverpage_url=?,  user_id=?, 
                    book_type_id=?,  book_desc=?
                    WHERE book_id = ? " ;

        $bound_params_r = ["sssiisi",
             ((!isset($dataJson->title)) ? null : $dataJson->title),
            ((!isset($dataJson->subtitle)) ? null : $dataJson->subtitle),
            ((!isset($dataJson->coverpageUrl)) ? null : $dataJson->coverpageUrl),
            ((!isset($dataJson->userId)) ? null : $dataJson->userId),
            ((!isset($dataJson->typeId)) ? null : $dataJson->typeId),
            ((!isset($dataJson->description)) ? null : $dataJson->description),
            ((!isset($dataJson->id)) ? null : $dataJson->id),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->id;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM iordanov_bwt.bwt_book
                   WHERE book_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        BwtLogger::log($mn, "deleted book_id =" . $id);
                    
        return $id;
    }
    
    
    // </editor-fold>
}
