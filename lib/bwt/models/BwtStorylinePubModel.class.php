<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Sep 27, 2018 
 *  Filename          : BwtStorylinePubModel.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

require_once 'Book.class.php';

/**
 * Description of BwtStorylinePubModel
 *
 * @author IZIordanov
 */
class BwtStorylinePubModel {
    public $id;
    public $guid;
    public $storylineId;
    public $userId;
    public $linkname;
    public $adate;

    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($id) {
        $mn = "BwtStorylinePubModel::LoadById(" . $id . ")";
        BwtLogger::logBegin($mn);
        $response = new BwtStorylinePubModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $objArrJ = BwtStorylinePubModel::SelectJson($id, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = null;
        }
        BwtLogger::logEnd($mn);
        return $response;
    }
    
    public static function LoadByStlId($id) {
        $mn = "BwtStorylinePubModel::LoadByStlId(" . $id . ")";
        BwtLogger::logBegin($mn);
        $response = new BwtStorylinePubModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $objArrJ = BwtStorylinePubModel::SelectJsonStlId($id, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = null;
        }
        BwtLogger::logEnd($mn);
        return $response;
    }
    
     public static function LoadByGuid($id) {
        $mn = "BwtStorylinePubModel::LoadByGuid(" . $id . ")";
        BwtLogger::logBegin($mn);
        $response = new BwtStorylinePubModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $objArrJ = BwtStorylinePubModel::SelectGuidJson($id, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = null;
        }
        BwtLogger::logEnd($mn);
        return $response;
    }

    public static function Save($data) {
        $mn = "BwtStorylinePubModel::Save()";
        BwtLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //BwtLogger::log($mn, " flightName = " . $dataJson->flightName);
        $response = new BwtStorylinePubModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
             $id = null;
            if (isset($dataJson->id)) {
                //BwtLogger::log($mn, "Update  book_id =" . $dataJson->id);
                $id = $dataJson->id;
                BwtStorylinePubModel::Update($dataJson, $conn, $mn, $logModel);
            } else {
                //BwtLogger::log($mn, "Create CfgCharterModel");
                $id = BwtStorylinePubModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            //BwtLogger::log($mn, " id =" . $id);
            if (isset($id)) {
                $objArrJ = BwtStorylinePubModel::SelectJson($id, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = json_decode(json_encode($objArrJ[0]));
                }
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
        }

        //BwtLogger::log($mn, " response = " . json_encode($response));
        BwtLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = "SELECT p.id as id, p.guid as guid, 
                p.stl_id as storylineId,  
                p.user_id as userId,
                p.linkname, p.adate, count(pv.id) viewsCount
                FROM iordanov_bwt.bwt_storyline_pub p
                left join iordanov_bwt.bwt_storyline_pub_view pv on pv.stlpub_id = p.id
                where p.id = ? 
                group by p.id";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }
    
    static function SelectJsonStlId($id, $conn, $mn, $logModel) {

        $sql = "SELECT p.id as id, p.guid as guid, 
                p.stl_id as storylineId,  
                p.user_id as userId,
                p.linkname, p.adate, count(pv.id) viewsCount
                FROM iordanov_bwt.bwt_storyline_pub p
                left join iordanov_bwt.bwt_storyline_pub_view pv on pv.stlpub_id = p.id
                where p.stl_id = ? 
                group by p.id";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }
    
     static function SelectGuidJson($id, $conn, $mn, $logModel) {

        $sql = "SELECT p.id as id, p.guid as guid, 
                p.stl_id as storylineId,  
                p.user_id as userId,
                p.linkname, p.adate, count(pv.id) viewsCount
                FROM iordanov_bwt.bwt_storyline_pub p
                left join iordanov_bwt.bwt_storyline_pub_view pv on pv.stlpub_id = p.id
                where p.guid = ? 
                group by p.id ";

        $bound_params_r = ["s", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function Create($dataJson, $conn, $mn, $logModel) {
        
        if(isset($dataJson->storylineId)){
            BwtStorylinePubModel::DeleteStotyline($dataJson->storylineId, $conn, $mn, $logModel);
        }
        $strSQL = "INSERT INTO iordanov_bwt.bwt_storyline_pub 
            ( stl_id, user_id, linkname)
            VALUES(?, ?, ?)";

        $bound_params_r = ["iis",
            ((!isset($dataJson->storylineId)) ? null : $dataJson->storylineId),
            ((!isset($dataJson->userId)) ? null : $dataJson->userId),
            ((!isset($dataJson->linkname)) ? null : $dataJson->linkname)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        BwtLogger::log("$mn", "id=" . $id);

        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE iordanov_bwt.bwt_storyline_pub
                    SET linkname=?
                    WHERE id = ? ";

        $bound_params_r = ["si",
            ((!isset($dataJson->linkname)) ? null : $dataJson->linkname),
            ((!isset($dataJson->id)) ? null : $dataJson->id),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->id;
    }

    static function Delete($id, $conn, $mn, $logModel) {

        $strSQL = "DELETE FROM iordanov_bwt.bwt_storyline_pub
                   WHERE guid = ? ";

        $bound_params_r = ["s", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        BwtLogger::log($mn, "deleted guid =" . $id);

        return $id;
    }
    
    static function DeleteStotyline($id, $conn, $mn, $logModel) {

        $strSQL = "DELETE FROM iordanov_bwt.bwt_storyline_pub
                   WHERE stl_id = ? ";

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        BwtLogger::log($mn, "deleted guid =" . $id);

        return $id;
    }

    // </editor-fold>

}

class BwtStorylinePubViewModel {

    public $id;
    public $stlpubId;
    public $storylineId;
    public $username;
    public $ipaddress;
    public $adate;

    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($id) {
        $mn = "BwtStorylinePubViewModel::LoadById(" . $id . ")";
        BwtLogger::logBegin($mn);
        $response = new BwtStorylinePubViewModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $objArrJ = BwtStorylinePubViewModel::SelectJson($id, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = null;
        }
        BwtLogger::logEnd($mn);
        return $response;
    }

    public static function Save($data) {
        $mn = "BwtStorylinePubViewModel::Save()";
        BwtLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //BwtLogger::log($mn, " flightName = " . $dataJson->flightName);
        $response = new BwtStorylinePubViewModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $id = BwtStorylinePubViewModel::Create($dataJson, $conn, $mn, $logModel);
            //BwtLogger::log($mn, " id =" . $id);
            if (isset($id)) {
                $objArrJ = BwtStorylinePubViewModel::SelectJson($id, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = json_decode(json_encode($objArrJ[0]));
                }
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
        }

        //BwtLogger::log($mn, " response = " . json_encode($response));
        BwtLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = "SELECT id, stlpub_id as stlpubId, 
                    stl_id as storylineId,
                    username, ipaddress, adate
                    FROM iordanov_bwt.bwt_storyline_pub_view
                    where id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function Create($dataJson, $conn, $mn, $logModel) {

        $strSQL = "INSERT INTO iordanov_bwt.bwt_storyline_pub_view 
            ( stlpub_id, stl_id, username, ipaddress)
            VALUES(?, ?, ?, ?)";

        $bound_params_r = ["iiss",
            ((!isset($dataJson->stlpubId)) ? null : $dataJson->stlpubId),
            ((!isset($dataJson->storylineId)) ? null : $dataJson->storylineId),
            ((!isset($dataJson->username)) ? null : $dataJson->username),
            ((!isset($dataJson->ipaddress)) ? null : $dataJson->ipaddress)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log("$mn", "id=" . $id);

        return $id;
    }

    static function Delete($id, $conn, $mn, $logModel) {

        $strSQL = "DELETE FROM iordanov_bwt.bwt_storyline_pub_view
                   WHERE id = ? ";

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log($mn, "deleted id =" . $id);

        return $id;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Table Methods">

    public static function StorylinePubViewTable($params) {
        $mn = "BwtStorylinePubViewModel::StorylinePubViewTable()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT id, stlpub_id as stlpubId, 
                    stl_id as storylineId,
                    username, ipaddress, adate
                    FROM iordanov_bwt.bwt_storyline_pub_view ";

            $sqlWhere = " where stl_id = ? ";
            
            $sqlOrder = " order by adate desc";
           
            if (isset($params->filter) && strlen($params->filter) > 1) {
                $sqlWhere .= " AND (  ipaddress like '%" . $params->filter . "%' ";
                $sqlWhere .= " or username like '%" . $params->filter . "%' )";
            }
            
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder) > 1 ? $sqlOrder : "");
            $sql .= " LIMIT ? OFFSET ? ";
            BwtLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["iii", $params->storylineId, $params->limit, $params->offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("storylinepubviews", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_bwt.bwt_storyline_pub_view " .$sqlWhere;
            $bound_params_r = ["i", $params->storylineId];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $obj->totalRows);
            //$response->addData("rowsCount", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        return $response;
    }
    
     // </editor-fold>

}

