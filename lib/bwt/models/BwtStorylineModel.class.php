<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Sep 27, 2018 
 *  Filename          : BwtStorylineModel.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

require_once 'Book.class.php';

/**
 * Description of BwtStorylineModel
 *
 * @author IZIordanov
 */
class BwtStorylineModel {

    public $id;
    public $title;
    public $statusId;
    public $imageUrl;
    public $notes;
    public $sortOrder;
    public $parentId;
    public $bookId;
    public $chapterId;
    public $autorId;
    public $pubguid;
    public $nextStlId;
    public $prevStlId;
    	

    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($id) {
        $mn = "BwtStorylineModel::LoadById(" . $id . ")";
        BwtLogger::logBegin($mn);
        $response = new BwtStorylineModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $objArrJ = BwtStorylineModel::SelectJson($id, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = null;
        }
        BwtLogger::logEnd($mn);
        return $response;
    }

    public static function Save($data) {
        $mn = "BwtStorylineModel::Save()";
        BwtLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //BwtLogger::log($mn, " flightName = " . $dataJson->flightName);
        $response = new BwtStorylineModel();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $id = null;
            if (isset($dataJson->id)) {
                //BwtLogger::log($mn, "Update  book_id =" . $dataJson->id);
                $id = $dataJson->id;
                BwtStorylineModel::Update($dataJson, $conn, $mn, $logModel);
            } else {
                //BwtLogger::log($mn, "Create CfgCharterModel");
                $id = BwtStorylineModel::Create($dataJson, $conn, $mn, $logModel);
            }

            //BwtLogger::log($mn, " id =" . $id);
            if (isset($id)) {
                $objArrJ = BwtStorylineModel::SelectJson($id, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = json_decode(json_encode($objArrJ[0]));
                }
            }
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
        }

        //BwtLogger::log($mn, " response = " . json_encode($response));
        BwtLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = "SELECT sl.stl_id as id,
                sl.stl_title as title,
                sl.stl_status_id as statusId,
                sl.stl_image_url as imageUrl,
                sl.stl_notes as notes,
                sl.stl_order as sortOrder,
                sl.parent_stl_id as parentId,
                sl.book_id as bookId,
                sl.chapter_id as chapterId,
                sl.next_stl_id as nextStlId,
                sl.prev_stl_id as prevStlId,
                b.user_id as autorId,
                pub.guid as pubguid
                FROM iordanov_bwt.bwt_storyline sl
                join iordanov_bwt.bwt_book b on b.book_id = sl.book_id
                left join iordanov_bwt.bwt_storyline_pub pub on pub.stl_id = sl.stl_id
                where sl.stl_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function Create($dataJson, $conn, $mn, $logModel) {

        $strSQL = "INSERT INTO iordanov_bwt.bwt_storyline 
            ( stl_title, stl_status_id, stl_image_url, stl_notes,
            stl_order, parent_stl_id, book_id, chapter_id, next_stl_id, prev_stl_id)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $bound_params_r = ["sissiiiiii",
            ((!isset($dataJson->title)) ? null : $dataJson->title),
            ((!isset($dataJson->statusId)) ? null : $dataJson->statusId),
            ((!isset($dataJson->imageUrl)) ? null : $dataJson->imageUrl),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->sortOrder)) ? null : $dataJson->sortOrder),
            ((!isset($dataJson->parentId)) ? null : $dataJson->parentId),
            ((!isset($dataJson->bookId)) ? null : $dataJson->bookId),
            ((!isset($dataJson->chapterId)) ? null : $dataJson->chapterId),
            ((!isset($dataJson->nextStlId)) ? null : $dataJson->nextStlId),
            ((!isset($dataJson->prevStlId)) ? null : $dataJson->prevStlId),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log("$mn", "id=" . $id);

        return $id;
    }

    static function Update($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE iordanov_bwt.bwt_storyline
                    SET stl_title=?,    stl_status_id=?, 
                    stl_image_url=?,    stl_notes=?, 
                    stl_order=?,        parent_stl_id=?,
                    book_id=?,          chapter_id=? ,
                    next_stl_id=?,     prev_stl_id=? 
                    WHERE stl_id = ? ";

        $bound_params_r = ["sissiiiiiii",
            ((!isset($dataJson->title)) ? null : $dataJson->title),
            ((!isset($dataJson->statusId)) ? null : $dataJson->statusId),
            ((!isset($dataJson->imageUrl)) ? null : $dataJson->imageUrl),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->sortOrder)) ? null : $dataJson->sortOrder),
            ((!isset($dataJson->parentId)) ? null : $dataJson->parentId),
            ((!isset($dataJson->bookId)) ? null : $dataJson->bookId),
            ((!isset($dataJson->chapterId)) ? null : $dataJson->chapterId),
            ((!isset($dataJson->nextStlId)) ? null : $dataJson->nextStlId),
            ((!isset($dataJson->prevStlId)) ? null : $dataJson->prevStlId),
            ((!isset($dataJson->id)) ? null : $dataJson->id),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->id;
    }

    static function Delete($id, $conn, $mn, $logModel) {

        $strSQL = "DELETE FROM iordanov_bwt.bwt_storyline
                   WHERE stl_id = ? ";

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        BwtLogger::log($mn, "deleted stl_id =" . $id);

        return $id;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Table Methods">

    public static function StorylineTable($params, $actorId) {
        $mn = "BwtStorylineModel::ChapterTable()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT sl.stl_id as id,
                        sl.stl_title as title,
                        sl.stl_status_id as statusId,
                        sl.stl_image_url as imageUrl,
                        sl.stl_notes as notes,
                        sl.stl_order as sortOrder,
                        sl.parent_stl_id as parentId,
                        sl.book_id as bookId,
                        sl.chapter_id as chapterId,
                        sl.next_stl_id as nextStlId,
                        sl.prev_stl_id as prevStlId,
                        b.user_id as autorId,
                        ch.bch_title as chapterTitle,
                        pub.guid as pubguid
                    FROM iordanov_bwt.bwt_storyline sl
                    join iordanov_bwt.bwt_book b on b.book_id = sl.book_id
                    left join iordanov_bwt.bwt_storyline_pub pub on pub.stl_id = sl.stl_id
                    left join iordanov_bwt.bwt_book_chapter ch on ch.bch_id = sl.chapter_id "; 

            $sqlWhere = "";
            if (isset($params->bookId) && strlen($params->bookId) > 0) {
                $sqlWhere = " WHERE b.book_id = " . $params->bookId . " ";
            } else {
                $sqlWhere = " WHERE b.user_id = " . $actorId . " ";
            }
            
            if (isset($params->chapterId) && strlen($params->chapterId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and sl.chapter_id = " . $params->chapterId . " ";
                } else {
                    $sqlWhere = " WHERE sl.chapter_id = " . $params->chapterId . " ";
                }
            }
            
            if (isset($params->parentId) && strlen($params->parentId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and sl.parent_stl_id = " . $params->parentId . " ";
                } else {
                    $sqlWhere = " WHERE sl.parent_stl_id = " . $params->parentId . " ";
                }
            }
            
            if (isset($params->statusId) && strlen($params->statusId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and sl.stl_status_id = " . $params->statusId . " ";
                } else {
                    $sqlWhere = " WHERE sl.stl_status_id = " . $params->statusId . " ";
                }
            }

            if (isset($params->filter) && strlen($params->filter) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND ( ifnull(ch.bch_title, ch.bch_subtitle) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or ifnull(ch.bch_subtitle, ch.bch_title) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or sl.stl_title like '%" . $params->filter . "%' )";
                } else {
                    $sqlWhere .= " WHERE ( ifnull(ch.bch_title, ch.bch_subtitle) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or ifnull(ch.bch_subtitle, ch.bch_title) like '%" . $params->filter . "%' ";
                    $sqlWhere .= " or sl.stl_title like '%" . $params->filter . "%' )";
                }
            }
            $sqlOrder = "";
            if (isset($params->sortCol)) {
                if($params->sortCol == 'sortOrder'){
                    $sqlOrder .= " order by sl.stl_order " . ($params->sortDesc ? "desc" : " asc");
                } else{
                    $sqlOrder .= " order by " . $params->sortCol . " " . ($params->sortDesc ? "desc" : " asc");
                }
                
            } else {
                $sqlOrder .= " Order by sortOrder, parentId, title ";
            }
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder) > 1 ? $sqlOrder : "");
            $sql .= " LIMIT ? OFFSET ? ";
            BwtLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->limit, $params->offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("storylines", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_bwt.bwt_storyline sl
                    join iordanov_bwt.bwt_book b on b.book_id = sl.book_id
                    left join iordanov_bwt.bwt_book_chapter ch on ch.bch_id = sl.chapter_id " .
                    (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $obj->totalRows);
            //$response->addData("rowsCount", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        return $response;
    }
    
    public static function PublicRoadmapGet($storylineId) {
        $mn = "BwtStorylineModel::RoadmapGet()";
        BwtLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT sl.stl_id as id,
                        sl.stl_title as title,
                        sl.stl_status_id as statusId,
                        sl.stl_image_url as imageUrl,
                        sl.stl_notes as notes,
                        sl.stl_order as sortOrder,
                        sl.parent_stl_id as parentId,
                        sl.book_id as bookId,
                        sl.chapter_id as chapterId,
                        sl.next_stl_id as nextStlId,
                        sl.prev_stl_id as prevStlId,
                        b.user_id as autorId,
                        ch.bch_title as chapterTitle
                FROM iordanov_bwt.bwt_storyline sl
                join iordanov_bwt.bwt_book b on b.book_id = sl.book_id
                left join iordanov_bwt.bwt_book_chapter ch on ch.bch_id = sl.chapter_id
                where sl.stl_id = ?
                union all
                SELECT sl.stl_id as id,
                        sl.stl_title as title,
                        sl.stl_status_id as statusId,
                        sl.stl_image_url as imageUrl,
                        sl.stl_notes as notes,
                        sl.stl_order as sortOrder,
                        sl.parent_stl_id as parentId,
                        sl.book_id as bookId,
                        sl.chapter_id as chapterId,
                        sl.next_stl_id as nextStlId,
                        sl.prev_stl_id as prevStlId,
                        b.user_id as autorId,
                        ch.bch_title as chapterTitle
                FROM (select * from (select * from iordanov_bwt.bwt_storyline order by stl_order, parent_stl_id, stl_id) stl, 
                 (select @pv := ?) initialisation 
                 where find_in_set(parent_stl_id, @pv) > 0 and @pv := concat(@pv, ',', stl_id ) ) sl
                join iordanov_bwt.bwt_book b on b.book_id = sl.book_id
                left join iordanov_bwt.bwt_book_chapter ch on ch.bch_id = sl.chapter_id ";

            $sql = "call iordanov_bwt.roadmapgetids(?)";
            BwtLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["i", $storylineId];
            $ret_json_data = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            $response->addData("storylines", $ret_json_data);

            $sql = "SELECT b.book_id as id,
                        b.book_title as title,
                        b.book_subtitle as subtitle,
                        b.book_coverpage_url as coverpageUrl,
                        b.book_type_id as typeId,
                        b.book_desc as description,
                        b.adate, b.udate, 
                        b.user_id as userId,
                        b.user_id as autorId,
                        u.user_name as autorName,
                        u.e_mail as autorEmail
                        FROM iordanov_bwt.bwt_book b
                        join iordanov_bwt.bwt_user u on u.user_id = b.user_id
                        join iordanov_bwt.bwt_storyline sl on sl.book_id = b.book_id
                       WHERE sl.stl_id = ? ";
            $bound_params_r = ["i", $storylineId];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = json_decode(json_encode($ret_json_data[0]));
            $response->addData("book", $obj);
            //$response->addData("rowsCount", $obj);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //BwtLogger::log($mn, " response = " . $response->toJSON());
        BwtLogger::logEnd($mn);
        return $response;
    }
}
