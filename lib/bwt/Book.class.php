<?php 
/******************************** HEAD_BEG ************************************
*
* Project                	: isgt
* Module                        : user
* Responsible for module 	: IordIord
*
* Filename               	: Book.class.php
*
* Database System        	: ORCL, MySQL
* Created from                  : IordIord
* Date Creation			: 19.12.2018
*------------------------------------------------------------------------------
*                        Description
*------------------------------------------------------------------------------
* @TODO Insert some description.
*
*------------------------------------------------------------------------------
*                        History
*------------------------------------------------------------------------------
* HISTORY:
* <br>--- $Log: Book.class.php,v $
* <br>---
* <br>---
*
********************************* HEAD_END ************************************
*/

/**
 * Description of Book class
 *
 * @author IordIord
 */
class Book {

/**
 * ***************************************************************************
 * Methods Declarations
 * ***************************************************************************
 */
    /**
     *
     * @param <type> $id 
     */
    public function loadById($id) {
        $mn = "bwt:Book.loadById()";
         BwtLogger::logBegin($mn);
        BwtLogger::log($mn, "id = ".$id);
        $this->setId($id);
        $sql = "SELECT ".Book::getAllColumnsSQL(). ", " . Book::COL_NAME_UDATE.
                " FROM ".Book::TABLE_NAME." ".
                " WHERE ".Book::COL_NAME_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = BwtConnection::dbConnect();
        $logModel = BwtLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        BwtLogger::log($mn, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //BwtLogger::log($mn, "count(data)=".count($data));
        }
        //BwtLogger::log("$mn", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        BwtLogger::log($mn, "Book is ".$this->toString());

        BwtLogger::logEnd($mn);
    }
    
    public static function loadByUserId ($userId)
    {
        $mn="bwt:Book.loadByUserId()";
         BwtLogger::logBegin($mn);
        $out = null;

        $sqlStr ="SELECT ".Book::getAllColumnsSQL()." FROM ".Book::TABLE_NAME.
                    " WHERE ".Book::COL_NAME_ID."=? ";
        $bound_params_r = array('i', $userId);

        $conn = BwtConnection::dbConnect();
        $logModel = BwtLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        
        $retArray = array();
        for ($index = 0, $max_count = sizeof( $result_r ); $index < $max_count; $index++)
        {
           $result = $result_r[$index];
           $item = new Book();
           $item->loadFromPosArray($result);
           $retArray[] = $item;
           BwtLogger::log($mn, "Add Item =".$item->toString());
        }
        
        BwtLogger::logEnd($mn);
        return $retArray;
    }

    public static function loadAll()
    {
        $mn = "bwt:Book.loadAll()";
        $st = BwtLogger::logBegin($mn);


        $sql = "SELECT ".Book::getAllColumnsSQL() . ", " . Book::COL_NAME_UDATE.
                " FROM ".Book::TABLE_NAME." ";

        $conn = BwtConnection::dbConnect();
        $logModel = BwtLogger::currLogger()->getModule($mn);
        $data = $conn->dbExecuteSQL($sql, $logModel);
        BwtLogger::log($mn, "count(data)=".count($data));
        $retArray = array();
        for ($index = 0, $max_count = sizeof( $data ); $index < $max_count; $index++)
        {
           $result = $data[$index];
           $item = new Book();
           $item->loadFromPosArray($result);
           $retArray[] = $item;
           BwtLogger::log($mn, "Add Item =".$item->toString());
        }

        BwtLogger::log($mn, "count(retArray)=".count($retArray));
        BwtLogger::logEnd($mn);
        return $retArray;
    }
    
    /**
     *
     * @param <type> $id
     */
    public static function deleteById($id) {
        $mn = "bwt:Book.deleteById()";
         BwtLogger::logBegin($mn);
        BwtLogger::log($mn, "id = ".$id);
        //$this->setId($id);
        $sql = "DELETE ".
                " FROM ".Book::TABLE_NAME." ".
                " WHERE ".Book::COL_NAME_ID."=?";
        $bound_params_r = array("i",$id);
        $conn = BwtConnection::dbConnect();
        $logModel = BwtLogger::currLogger()->getModule($mn);
        $id = $conn->preparedDelete($sql, $bound_params_r, $logModel);

        BwtLogger::logEnd($mn);
    }
    
    public static function CreateRowData($bookModel) {
        $mn = "bwt:Book.CreateRowData()";
         BwtLogger::logBegin($mn);

        $rowData = new Book();
        
        $rowData->setTitle($bookModel->title);
        $rowData->setSubtitle($bookModel->subtitle);
        $rowData->setCoverPageUrl($bookModel->coverpageUrl);
        $rowData->setUserId($bookModel->userId);
        $rowData->setTypeId($bookModel->typeId);
        $rowData->setDescription($bookModel->description);
        
        $rowData->save();
        BwtLogger::log($mn, "after save.");
        BwtLogger::logEnd($mn);
        return $rowData;
    }

    public function save() {
        $mn = "bwt:Book.save()";
        $st = BwtLogger::logBegin($mn);

        BwtLogger::log($mn, "is_object(this)=" . is_object($this));
        BwtLogger::log($mn, "ID=" . $this->getId());
        try {
            if (is_object($this)) {
                BwtLogger::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {

                    BwtLogger::log($mn, "Insert ");
                    $strSQL = "INSERT INTO " . Book::TABLE_NAME . " (" . Book::getAllColumnsNoIdSQL() . ") ";
                    $strSQL .=" VALUES( ?, ?, ?, ?, ?, ?, ?, ?)";

                    $bound_params_r = array("sssiisss",
                       (($this->getTitle() == null) ? null : $this->getTitle()),
                        (($this->getSubtitle() == null) ? null : $this->getSubtitle()),
                        (($this->getCoverPageUrl() == null) ? null : $this->getCoverPageUrl()),
                        ($this->getUserId()),
                        (($this->getTypeId() == null) ? 1 : $this->getTypeId()),
                        (($this->getDescription() == null) ? null : $this->getDescription()),
                        (($this->getADate() == null) ? null : $this->getADate()->format("Y-m-d H:i:s")),
                        (($this->getUDate() == null) ? null : $this->getUDate()->format("Y-m-d H:i:s")),
                    );
                    $conn = BwtConnection::dbConnect();
                    $logModel = BwtLogger::currLogger()->getModule($mn);
                    $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);

                    BwtLogger::log("$mn", "id=" . $id);
                    $this->LoadById($id);
                } else {
                    BwtLogger::log($mn, "Update ");
                    //$lngID = $this->getId();
                    $strSQL = "UPDATE " . Book::TABLE_NAME;
                    $strSQL .=" SET " . Book::COL_NAME_TITLE . "=?, ";
                    $strSQL .=Book::COL_NAME_SUBTITLE . "=?, ";
                    $strSQL .=Book::COL_NAME_COVER_PAGE . "=?, ";
                    $strSQL .=Book::COL_NAME_USER_ID . "=?, ";
                    $strSQL .=Book::COL_NAME_TYPE_ID . "=?, ";
                    $strSQL .=Book::COL_NAME_DESCRIPTION . "=? ";

                    $strSQL .=" WHERE " . Book::COL_NAME_ID . "=? ";

                    $bound_params_r = array("sssiisi",
                        (($this->getTitle() == null) ? null : $this->getTitle()),
                        (($this->getSubtitle() == null) ? null : $this->getSubtitle()),
                        (($this->getCoverPageUrl() == null) ? null : $this->getCoverPageUrl()),
                        ($this->getUserId()),
                        (($this->getTypeId() == null) ? 1 : $this->getTypeId()),
                        (($this->getDescription() == null) ? null : $this->getDescription()),
                        $this->getId()
                    );


                    $conn = BwtConnection::dbConnect();
                    $logModel = BwtLogger::currLogger()->getModule($mn);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    BwtLogger::log($mn, "affectedRows=" . $affectedRows);

                    //$this->LoadById($this->getId());
                }
            }
        } catch (Exception $ex) {
            BwtLogger::log($mn, "Error id " . $this->getId());
            BwtLogger::logError($mn, $ex);
        }
        BwtLogger::logEnd($mn);
        return $this;
    }

    /**
     *
     * @param <type> $result 
     */
    public function loadFromArray($result) {
        $mn = "bwt:Book.loadFromArray()";
         BwtLogger::logBegin($mn);
        try{
            
             if(isset($result) && count($result)>0) {
                 
                $this->setId($result[Book::COL_NAME_ID]);
                $this->setTitle($result[Book::COL_NAME_TITLE]);
                $this->setSubtitle($result[Book::COL_NAME_SUBTITLE]);
                $this->setCoverPageUrl($result[Book::COL_NAME_COVER_PAGE]);
                $this->setUserId($result[Book::COL_NAME_USER_ID]);
                
                $this->setTypeId($result[Book::COL_NAME_TYPE_ID]);
                $this->setDescription($result[Book::COL_NAME_DESCRIPTION]);
                $this->setADate($result[Book::COL_NAME_ADATE]);
                $this->setUDate($result[Book::COL_NAME_UDATE]);
                
                BwtLogger::log($mn, "id ".$this->getId());
            }
            
        }
        catch(Exception $ex){BwtLogger::log($mn, "Error id ".$this->getId());BwtLogger::logError($mn, $ex);}
        BwtLogger::logEnd($mn);

    }

    public function loadFromPosArray($result) {
        $mn = "bwt:Book.loadFromPosArray()";
         BwtLogger::logBegin($mn);
        if(isset($result) && count($result)>0){
            $this->setId($result[Book::COL_IXD_ID]);
            $this->setTitle($result[Book::COL_IDX_TITLE]);
            $this->setSubtitle($result[Book::COL_IDX_SUBTITLE]);
            $this->setCoverPageUrl($result[Book::COL_IDX_COVER_PAGE]);
            $this->setUserId($result[Book::COL_IDX_USER_ID]);
            
            $this->setTypeId($result[Book::COL_IDX_TYPE_ID]);
            $this->setDescription($result[Book::COL_IDX_DESCRIPTION]);
            $this->setADate($result[Book::COL_IDX_ADATE]);
            $this->setUDate($result[Book::COL_IDX_UDATE]);
        }
        BwtLogger::logEnd($mn);

    }

    public function toString()
    {
         $retValue = $this->toJSON();
         
        return $retValue;
    }
    
    public function toJSON() {
        return json_encode($this);
    }
    /**
     * ***************************************************************************
     * Getters and Setters Declarations
     * ***************************************************************************
     */
    
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters  Declarations">
    

    public function getId() {
        return $this->id;
    }
    
    public function setId($id) {
        $this->id = $id;
    }

    public function getTitle() {
        $retValue = $this->title;
        return $retValue;
    }
    
    public function setTitle($title) {
        $this->title = $title;
    }
    
    public function getSubtitle() {
        $retValue = $this->subtitle;
        return $retValue;
    }
    
    public function setSubtitle($subtitle) {
        $this->subtitle = $subtitle;
    }
    public function getCoverPageUrl() {
        return $this->coverpageUrl;
    }

    public function setCoverPageUrl($coverpageUrl) {
        $this->coverpageUrl = $coverpageUrl;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
    }

    public function getTypeId() {
        return $this->typeId;
    }
    
    public function setTypeId($typeId) {
        $this->typeId = $typeId;
    }

    public function getDescription() {
        return $this->description;
    }
    public function setDescription($description) {
        $this->description = $description;
    }
    
    public function getADate() {
        $retValue = null;
        if (isset($this->adate) && $this->adate != null) {
            if (!is_object($this->adate))
                $retValue = new DateTime($this->adate);
            else
                $retValue = $this->adate;

            BwtLogger::log("getADate()", "retValue=", getDateStr($retValue));
        }
        else
            $retValue = new DateTime();
        return $retValue;
    }
    
    public function setADate($adate) {
         if (isset($adate) && $adate != null) {
            if (is_object($adate))
                $this->adate = $adate;
            else
                $this->adate = new DateTime($adate);
        }
        
    }
    
    public function getUDate() {
        $retValue = null;
        if (isset($this->udate) && $this->udate != null) {
            if (!is_object($this->udate))
                $retValue = new DateTime($this->udate);
            else
                $retValue = $this->udate;

            BwtLogger::log("getUDate()", "retValue=", getDateStr($retValue));
        }
        else
            $retValue = new DateTime();
        return $retValue;
    }
    
    public function setUDate($udate) {
         if (isset($udate) && $udate != null) {
            if (is_object($udate))
                $this->udate = $udate;
            else
                $this->udate = new DateTime($udate);
        }
        
    }
    
    // </editor-fold>
    
   /****************************************************************************
   * Parameters Declarations
   * ***************************************************************************
   */
   
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">

    public $id;
    public $title;
    public $subtitle;
    public $coverpageUrl;
    public $userId;
    public $typeId;
    public $description;
    public $adate;
    public $udate;

// </editor-fold>
    
    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME                = "bwt_book";
    const COL_NAME_ID               = "book_id";
    const COL_NAME_TITLE            = "book_title";
    const COL_NAME_SUBTITLE         = "book_subtitle";
    const COL_NAME_COVER_PAGE       = "book_coverpage_url";
    const COL_NAME_USER_ID          = "user_id";
    const COL_NAME_TYPE_ID          = "book_type_id";
    const COL_NAME_DESCRIPTION      = "book_desc";
    const COL_NAME_ADATE            = "adate";
    const COL_NAME_UDATE            = "udate";
    
    const COL_IXD_ID                = 0;
    const COL_IDX_TITLE             = 1;
    const COL_IDX_SUBTITLE          = 2;
    const COL_IDX_COVER_PAGE        = 3;
    const COL_IDX_USER_ID           = 4;
    const COL_IDX_TYPE_ID           = 5;
    const COL_IDX_DESCRIPTION       = 6;
    const COL_IDX_ADATE             = 7;
    const COL_IDX_UDATE             = 8;
    

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . Book::TABLE_NAME . "." . Book::COL_NAME_ID . ", " .
                Book::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " . Book::TABLE_NAME . "." . Book::COL_NAME_TITLE . ", " .
                Book::TABLE_NAME . "." . Book::COL_NAME_SUBTITLE . ", " .
                Book::TABLE_NAME . "." . Book::COL_NAME_COVER_PAGE. ", " .
                Book::TABLE_NAME . "." . Book::COL_NAME_USER_ID . ", " .
                Book::TABLE_NAME . "." . Book::COL_NAME_TYPE_ID . ", " .
                Book::TABLE_NAME . "." . Book::COL_NAME_DESCRIPTION . ", " .
                Book::TABLE_NAME . "." . Book::COL_NAME_ADATE . ", " .
                Book::TABLE_NAME . "." . Book::COL_NAME_UDATE;
    }

    public static function getArrayColumns() {
        return array(Book::COL_NAME_ID,
            Book::COL_NAME_TITLE,
            Book::COL_NAME_SUBTITLE,
            Book::COL_NAME_COVER_PAGE,
            Book::COL_NAME_USER_ID,
            Book::COL_NAME_TYPE_ID,
            Book::COL_NAME_DESCRIPTION,
            Book::COL_NAME_ADATE,
            Book::COL_NAME_UDATE);
    }

    // </editor-fold>
}

