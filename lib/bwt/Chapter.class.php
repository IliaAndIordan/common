<?php 
/******************************** HEAD_BEG ************************************
*
* Project                	: ams
* Module                        : ams
* Responsible for module 	: IordIord
*
* Filename               	: Chapter.class.php
*
* Database System        	: MySQL
* Created from                  : IordIord
* Date Creation			: 21.03.2016
*------------------------------------------------------------------------------
*                        Description
*------------------------------------------------------------------------------
* @TODO Insert some description.
*
*------------------------------------------------------------------------------
*                        History
*------------------------------------------------------------------------------
* HISTORY:
* <br>--- $Log: Chapter.class.php,v $
* <br>---
* <br>---
*
********************************* HEAD_END ************************************
*/
require_once("GVDataTable.class.php");

// <editor-fold defaultstate="collapsed" desc="Chapter Class">

/**
 * Description of Chapter class
 *
 * @author IordIord
 */
class Chapter {
/**
 * ***************************************************************************
 * Methods Declarations
 * ***************************************************************************
 */
    
  // <editor-fold defaultstate="collapsed" desc="Update Data">

    public function save() {
        $mn = "Chapter->save()";
        BwtLogger::logBegin($mn);

        BwtLogger::log($mn, "is_object(this)=" . is_object($this));
        
        if (is_object($this)) {
            try {
                BwtLogger::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {
                    BwtLogger::log($mn,  "Insert ");
                    $strSQL = "INSERT INTO " . Chapter::TABLE_NAME . " (".
                    Chapter::COL_NAME_BCH_TITLE . ", ".
                    Chapter::COL_NAME_BCH_SUBTITLE . ", ".
                    Chapter::COL_NAME_BCH_NOTES . ", ".
                    Chapter::COL_NAME_BOOK_ID . ", ".
                    Chapter::COL_NAME_BCH_CONTENT . ", ".
                    Chapter::COL_NAME_BCH_ORDER . ") ".
                    " VALUES(?, ?, ?, ?, ?, ?)";

                    $bound_params_r = ["sssisi",
                        (($this->bch_title == null) ? null : $this->bch_title),
                        (($this->bch_subtitle == null) ? null : $this->bch_subtitle),
                        (($this->bch_notes == null) ? null : $this->bch_notes),
                        (($this->book_id == null) ? null : $this->book_id),
                        (($this->bch_content == null) ? null : $this->bch_content),
                        (($this->bch_order == null) ? 1 : $this->bch_order),
                    ];
                    $conn = BwtConnection::dbConnect();
                    $logModel = BwtLogger::currLogger()->getModule($mn);
                    $this->bch_id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
                
                    BwtLogger::log("$mn", "bch_id=" . $this->bch_id);
                    $this->LoadById($this->bch_id);
                } else {
                    BwtLogger::log($mn,  "Update ");

                    $strSQL = "UPDATE " . Chapter::TABLE_NAME;
                    $strSQL .=" SET " . Chapter::COL_NAME_BCH_TITLE . "=?, ";
                    $strSQL .=Chapter::COL_NAME_BCH_SUBTITLE . "=?, ";
                    $strSQL .=Chapter::COL_NAME_BCH_NOTES . "=?, ";
                    $strSQL .=Chapter::COL_NAME_BOOK_ID . "=?, ";
                    $strSQL .=Chapter::COL_NAME_BCH_CONTENT . "=?, ";
                    $strSQL .=Chapter::COL_NAME_BCH_ORDER . "=? ";
                    $strSQL .=" WHERE " . Chapter::COL_NAME_ID . " = ? ";
                 
                    $bound_params_r = ["sssisii",
                        (($this->bch_title == null) ? null : $this->bch_title),
                        (($this->bch_subtitle == null) ? null : $this->bch_subtitle),
                        (($this->bch_notes == null) ? null : $this->bch_notes),
                        (($this->book_id == null) ? null : $this->book_id),
                        //echo htmlentities($str, ENT_QUOTES, "UTF-8");
                        (($this->bch_content == null) ? null : $this->bch_content),
                        (($this->bch_order == null) ? 1 : $this->bch_order),
                        $this->getId()
                    ];

                    $conn = BwtConnection::dbConnect();
                    $logModel = BwtLogger::currLogger()->getModule($mn);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    BwtLogger::log($mn, "affectedRows=" . $affectedRows);
                    
                }
            } catch (Exception $ex) {
                BwtLogger::log($mn, "Error id " . $this->getId());
                BwtLogger::logError($mn, $ex);
            }
        }
        BwtLogger::logEnd($mn);
        return $this;
    }
    
    public static function deleteById($id) {
        $mn = "Chapter.deleteById()";
        BwtLogger::logBegin($mn);
        BwtLogger::log($mn, " id = ".$id);
        //$this->setId($id);
        $sql = "DELETE ".
                " FROM ".Chapter::TABLE_NAME." ".
                " WHERE ".Chapter::COL_NAME_ID."=?";
        $bound_params_r = ["i",$id];
         $conn = BwtConnection::dbConnect();
        $logModel = BwtLogger::currLogger()->getModule($mn);
        $deleted_id = $conn->preparedDelete($sql, $bound_params_r, $logModel);
        BwtLogger::logEnd($mn);
    }

// </editor-fold>
    
    
  // <editor-fold defaultstate="collapsed" desc="Load Data">
    
    
    public function loadById($id)
    {
        $mn = "ams:Chapter.loadById()";
        BwtLogger::logBegin($mn);
        BwtLogger::log($mn, "id = ".$id);
        $this->setId($id);
        $sql = "SELECT ".Chapter::getAllColumnsSQL().
                ", ".Chapter::COL_NAME_UDATE." ".
                " FROM ".Chapter::TABLE_NAME." ".
                " WHERE ".Chapter::COL_NAME_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = BwtConnection::dbConnect();
        $logModel = BwtLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        BwtLogger::log($mn, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //BwtLogger::log($mn, "count(data)=".count($data));
        }
        //BwtLogger::log("$mn", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        BwtLogger::log($mn, "Airline is ".$this->toString());

        BwtLogger::logEnd($mn);
    }
    
    public function loadFromArray($result) {
        $mn = "Chapter.loadFromArray()";
         BwtLogger::logBegin($mn);
        try{
            if(!$result==null) {
                BwtLogger::log($mn, "result: ".implode(",", $result));
               
                $this->bch_id = ($result[Chapter::COL_NAME_ID]);
                $this->bch_title = ($result[Chapter::COL_NAME_BCH_TITLE]);
                $this->bch_subtitle = ($result[Chapter::COL_NAME_BCH_SUBTITLE]);
                $this->bch_notes = ($result[Chapter::COL_NAME_BCH_NOTES]);
                $this->book_id = ($result[Chapter::COL_NAME_BOOK_ID]);
                //html_entity_decode($string, ENT_HTML401, 'UTF-8');
                $this->bch_content = array_key_exists(Chapter::COL_NAME_BCH_CONTENT, $result)?($result[Chapter::COL_NAME_BCH_CONTENT]):null;
                $this->bch_order = ($result[Chapter::COL_NAME_BCH_ORDER]);
                $this->setADate($result[Chapter::COL_NAME_ADATE]);
                if(array_key_exists(Chapter::COL_NAME_UDATE,$result)){$this->setUDate($result[Chapter::COL_NAME_UDATE]);}
            }
        }
        catch(Exception $ex){BwtLogger::log($mn, "Error id ".$this->getId());BwtLogger::logError($mn, $ex);}
        BwtLogger::logEnd($mn);
    }

    public function toString()
    {
        $retValue = $this->toJSON();
        return $retValue;
    }
    
    public function toJSON() {
        return json_encode($this);
    }
    
     // </editor-fold>
    
  
   /****************************************************************************
   * Parameters Declarations
   * ***************************************************************************
   */
   
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">
    
    public function getId() {
        return $this->bch_id;
    }
    
    public function setId($id) {
        $this->bch_id = $id;
    }
    
    public function getADate() {
        $retValue = null;
        if (isset($this->adate) && $this->adate != null) {
            if (!is_object($this->adate))
                $retValue = new DateTime($this->adate);
            else
                $retValue = $this->adate;

            BwtLogger::log("getADate()", "retValue=", getDateStr($retValue));
        }
        else
            $retValue = new DateTime();
        return $retValue;
    }
    
    public function setADate($adate) {
         if (isset($adate) && $adate != null) {
            if (is_object($adate))
                $this->adate = $adate;
            else
                $this->adate = new DateTime($adate);
        }
        
    }
    
    public function getUDate() {
        $retValue = null;
        if (isset($this->udate) && $this->udate != null) {
            if (!is_object($this->udate))
                $retValue = new DateTime($this->udate);
            else
                $retValue = $this->udate;

            BwtLogger::log("getUDate()", "retValue=", getDateStr($retValue));
        }
        else
            $retValue = new DateTime();
        return $retValue;
    }
    
    public function setUDate($udate) {
         if (isset($udate) && $udate != null) {
            if (is_object($udate))
                $this->udate = $udate;
            else
                $this->udate = new DateTime($udate);
        }
        
    }

    public $bch_id;
    public $bch_title;
    public $bch_subtitle;
    public $bch_notes;
    public $book_id;
    public $bch_content;
    public $bch_order;
    public $adate;
    public $udate;
   
// </editor-fold>
    
    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME                = "iordanov_bwt.bwt_book_chapter";
    
    const COL_NAME_ID               = "bch_id";
    const COL_NAME_BCH_TITLE        = "bch_title";
    const COL_NAME_BCH_SUBTITLE     = "bch_subtitle";
    const COL_NAME_BCH_NOTES        = "bch_notes";
    const COL_NAME_BOOK_ID          = "book_id";
    const COL_NAME_BCH_CONTENT      = "bch_content";
    const COL_NAME_BCH_ORDER        = "bch_order";
    const COL_NAME_ADATE            = "adate";
    const COL_NAME_UDATE            = "udate";
    
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . Chapter::COL_NAME_ID . ", " .
                Chapter::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " .  Chapter::COL_NAME_BCH_TITLE . ", " .
                Chapter::COL_NAME_BCH_SUBTITLE . ", " .
                Chapter::COL_NAME_BCH_NOTES . ", " .
                Chapter::COL_NAME_BOOK_ID . ", " .
                Chapter::COL_NAME_BCH_CONTENT . ", " .
                Chapter::COL_NAME_BCH_ORDER . ", " .
                Chapter::COL_NAME_ADATE . ", " .
                 Chapter::COL_NAME_ADATE;
    }

    // </editor-fold>
}


// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Chapter GVDT">

// </editor-fold>

