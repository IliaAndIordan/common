<?php 
/******************************** HEAD_BEG ************************************
*
* Project                	: isgt
* Module                        : user
* Responsible for module 	: IordIord
*
* Filename               	: User.class.php
*
* Database System        	: ORCL, MySQL
* Created from                  : IordIord
* Date Creation			: 19.12.2018
*------------------------------------------------------------------------------
*                        Description
*------------------------------------------------------------------------------
* @TODO Insert some description.
*
*------------------------------------------------------------------------------
*                        History
*------------------------------------------------------------------------------
* HISTORY:
* <br>--- $Log: User.class.php,v $
* <br>---
* <br>---
*
********************************* HEAD_END ************************************
*/


/**
 * Description of User class
 *
 * @author IordIord
 */
class User {

/**
 * ***************************************************************************
 * Methods Declarations
 * ***************************************************************************
 */
    /**
     *
     * @param <type> $id 
     */
    public function loadById($id) {
        $mn = "user:User.loadById()";
         BwtLogger::logBegin($mn);
        BwtLogger::log($mn, "id = ".$id);
        $this->setDT_RowId($id);
        $sql = "SELECT ".User::getAllColumnsSQL(). ", " . User::COL_NAME_LAST_LOGED.
                " FROM ".User::TABLE_NAME." ".
                " WHERE ".User::COL_NAME_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = BwtConnection::dbConnect();
        $logModel = BwtLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        BwtLogger::log($mn, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //BwtLogger::log($mn, "count(data)=".count($data));
        }
        //BwtLogger::log("$mn", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        BwtLogger::log($mn, "User is ".$this->toString());

        BwtLogger::logEnd($mn);
    }
    
    public static function login ($user_email, $user_password)
    {
        $mn="user:User.login()";
         BwtLogger::logBegin($mn);
        $out = null;
        $loginName = validate_search_string($user_email);
        $password = validate_search_string($user_password);

        //$user_email = base64_encode($loginName);
        //$user_password = md5($password);

        $sqlStr ="SELECT ".User::COL_NAME_ID." FROM ".User::TABLE_NAME.
                    " WHERE ".User::COL_NAME_EMAIL."=? ".
                    " AND ".User::COL_NAME_PSW."=?";
        $bound_params_r = array('ss', $user_email, $user_password);

        $conn = BwtConnection::dbConnect();
        $logModel = BwtLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        if(count($result_r)>0)
        {
           
            BwtLogger::log($mn, prArr($result_r[0]));
            $id = $result_r[0][User::COL_NAME_ID];
            BwtLogger::log($mn, "Found userId".$id." for E-Mail ".$user_email."!");
            $ipAddress = $_SERVER['REMOTE_ADDR']; 
            
            if ($id>-1)
            {
                $out = new User();
                $out->loadById($id);
                $out->setIpAddress($ipAddress);
                $out->setUpdated(CurrenDateTime());
                $out->save();
            }
        }
        
        BwtLogger::logEnd($mn);
        return $out;
    }
    
    /**
     *
     * @param <type> $email
     * @return <type> 
     */
    public static function isExistEMail ($email) {
        $mn="user.User:isExistEMail()";
         BwtLogger::logBegin($mn);
        $user_email = validate_search_string($email);

        $sqlStr ="SELECT count(*) AS Rows FROM ".
            User::TABLE_NAME." WHERE ".User::COL_NAME_EMAIL."=?";

        $bound_params_r = array('s', $user_email);
        BwtLogger::log($mn,$sqlStr);
        $conn = BwtConnection::dbConnect();
        $logModel = BwtLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        BwtLogger::log($mn, prArr($result_r[0]));
        $nrRows = $result_r[0]["Rows"];
        BwtLogger::log($mn, "Found ".$nrRows." users with E-Mail ".$user_email."!");
        BwtLogger::logEnd($mn);
        return ($nrRows>0?TRUE:FALSE);
    }

    public static function forgotenPassword($email, $conf)
    {
        $mn="user.User:forgotenPassword()";
        $st = BwtLogger::logBegin($mn);
        $id = -1;
        $retValue = false;
        $user_email = validate_search_string($email);
        if(User::isExistEMail($user_email)>0)
        {
            $sqlStr ="SELECT ".User::COL_NAME_ID." FROM ".
                User::TABLE_NAME." WHERE ".User::COL_NAME_EMAIL."=?";

            $bound_params_r = array('s', $user_email);
            BwtLogger::log($mn,$sqlStr);
            $conn = BwtConnection::dbConnect();
            $logModel = BwtLogger::currLogger()->getModule($mn);
            $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
            BwtLogger::log($mn, prArr($result_r[0]));
            $id = $result_r[0][User::COL_NAME_ID];
            BwtLogger::log($mn, "Found ".$id." user ID with E-Mail ".$user_email."!");
            if($id>0)
            {
                $user = new User();
                $user->loadById($id);
                $retValue = sendMailRegistration($user, $conf);
            }
        }
        BwtLogger::logEnd($mn);
        return $retValue;
    }
    /**
     *
     * @param <type> $id
     */
    public static function deleteById($id) {
        $mn = "user:User.deleteById()";
         BwtLogger::logBegin($mn);
        BwtLogger::log($mn, "id = ".$id);
        //$this->setId($id);
        $sql = "DELETE ".
                " FROM ".User::TABLE_NAME." ".
                " WHERE ".User::COL_NAME_ID."=?";
        $bound_params_r = array("i",$id);
        $conn = BwtConnection::dbConnect();
        $logModel = BwtLogger::currLogger()->getModule($mn);
        $id = $conn->preparedDelete($sql, $bound_params_r, $logModel);

        BwtLogger::logEnd($mn);
    }
    
    public static function loadAll()
    {
        $mn = "user:User.loadAll()";
        $st = BwtLogger::logBegin($mn);


        $sql = "SELECT ".User::getAllColumnsSQL() . ", " . User::COL_NAME_LAST_LOGED.
                " FROM ".User::TABLE_NAME." ";

        $conn = BwtConnection::dbConnect();
        $logModel = BwtLogger::currLogger()->getModule($mn);
        $data = $conn->dbExecuteSQL($sql, $logModel);
        BwtLogger::log($mn, "count(data)=".count($data));
        $retArray = array();
        for ($index = 0, $max_count = sizeof( $data ); $index < $max_count; $index++)
        {
           $result = $data[$index];
           $item = new User();
           $item->loadFromPosArray($result);
           $retArray[] = $item;
           BwtLogger::log($mn, "Add Item =".$item->toString());
        }

        BwtLogger::log($mn, "count(retArray)=".count($retArray));
        BwtLogger::logEnd($mn);
        return $retArray;
    }
    
    public static function CreateRowData($eMail, $password) {
        $mn = "user:User.CreateRowData()";
         BwtLogger::logBegin($mn);

        $rowData = new User();
        
        $rowData->setEMail($eMail);
        $rowData->setPassword($password);
        $rowData->setName($rowData->getNameFromEMail());
        $rowData->setRole(0);
        $rowData->setIsReceiveEMails(1);
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $rowData->setIpAddress($ipAddress);
        
        $rowData->save();
        BwtLogger::log($mn, "after save.");
        BwtLogger::logEnd($mn);
        return $rowData;
    }

    public function save() {
        $mn = "user:User.save()";
        $st = BwtLogger::logBegin($mn);

        BwtLogger::log($mn, "is_object(this)=" . is_object($this));
        BwtLogger::log($mn, "ID=" . $this->getId());
        try {
            if (is_object($this)) {
                BwtLogger::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {

                    BwtLogger::log($mn, "Insert ");
                    $strSQL = "INSERT INTO " . User::TABLE_NAME . " (" . User::getAllColumnsNoIdSQL() . ") ";
                    $strSQL .=" VALUES( ?, ?, ?, ?, ?, ?, ?)";

                    $bound_params_r = array("sssiiss",
                        (($this->getEMail() == null) ? null : $this->getEMail()),
                        (($this->getPassword() == null) ? null : $this->getPassword()),
                        (($this->getName() == null) ? null : $this->getName()),
                        ($this->getRole()),
                        ($this->isReceiveEMails()),
                        (($this->getIpAddress() == null) ? null : $this->getIpAddress()),
                        (($this->getLastLogged() == null) ? null : $this->getLastLogged()->format("Y-m-d H:i:s"))
                    );
                    $conn = BwtConnection::dbConnect();
                    $logModel = BwtLogger::currLogger()->getModule($mn);
                    $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);

                    BwtLogger::log("$mn", "id=" . $id);
                    $this->LoadById($id);
                } else {
                    BwtLogger::log($mn, "Update ");
                    //$lngID = $this->getId();
                    $strSQL = "UPDATE " . User::TABLE_NAME;
                    $strSQL .=" SET " . User::COL_NAME_EMAIL . "=?, ";
                    $strSQL .=User::COL_NAME_PSW . "=?, ";
                    $strSQL .=User::COL_NAME_UNNAME . "=?, ";
                    $strSQL .=User::COL_NAME_ROLE . "=?, ";
                    $strSQL .=User::COL_NAME_IS_RECEIVE_EMAILS . "=?, ";
                    $strSQL .=User::COL_NAME_IP_ADDRESS . "=?, ";
                    $strSQL .=User::COL_NAME_UPDATED . "=? ";

                    $strSQL .=" WHERE " . User::COL_NAME_ID . "=? ";

                    $bound_params_r = array("sssiissi",
                        (($this->getEMail() == null) ? null : $this->getEMail()),
                        (($this->getPassword() == null) ? null : $this->getPassword()),
                        (($this->getName() == null) ? null : $this->getName()),
                        ($this->getRole()),
                        ($this->isReceiveEMails()),
                        (($this->getIpAddress() == null) ? null : $this->getIpAddress()),
                        (($this->getUpdated() == null) ? null : $this->getUpdated()->format("Y-m-d H:i:s")),
                        $this->getId()
                    );


                    $conn = BwtConnection::dbConnect();
                    $logModel = BwtLogger::currLogger()->getModule($mn);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    BwtLogger::log($mn, "affectedRows=" . $affectedRows);

                    //$this->LoadById($this->getId());
                }
            }
        } catch (Exception $ex) {
            BwtLogger::log($mn, "Error id " . $this->getId());
            BwtLogger::logError($mn, $ex);
        }
        BwtLogger::logEnd($mn);
        return $this;
    }

    /**
     *
     * @param <type> $result 
     */
    public function loadFromArray($result) {
        $mn = "user:User.loadFromArray()";
         BwtLogger::logBegin($mn);
        try{
            
             if(isset($result) && count($result)>0) {
                 
                $this->setDT_RowId($result[User::COL_NAME_ID]);
                $this->setEMail($result[User::COL_NAME_EMAIL]);
                $this->setPassword($result[User::COL_NAME_PSW]);
                $this->setName($result[User::COL_NAME_UNNAME]);
                $this->setRole($result[User::COL_NAME_ROLE]);
                
                $this->setIsReceiveEMails($result[User::COL_NAME_IS_RECEIVE_EMAILS]);
                $this->setIpAddress($result[User::COL_NAME_IP_ADDRESS]);
                $this->setLastLogged($result[User::COL_NAME_LAST_LOGED]);
                $this->setUpdated($result[User::COL_NAME_UPDATED]);
                
                BwtLogger::log($mn, "id ".$this->getId());
            }
            
        }
        catch(Exception $ex){BwtLogger::log($mn, "Error id ".$this->getId());BwtLogger::logError($mn, $ex);}
        BwtLogger::logEnd($mn);

    }

    public function loadFromPosArray($result) {
        $mn = "user:User.loadFromPosArray()";
         BwtLogger::logBegin($mn);
        if(isset($result) && count($result)>0){
            $this->setDT_RowId($result[User::COL_IXD_ID]);
            $this->setEMail($result[User::COL_IDX_EMAIL]);
            $this->setPassword($result[User::COL_IDX_PSW]);
            $this->setName($result[User::COL_IDX_UNAME]);
            $this->setRole($result[User::COL_IDX_ROLE]);
            
            $this->setIsReceiveEMails($result[User::COL_IDX_IS_RECEIVE_EMAILS]);
            $this->setIpAddress($result[User::COL_IDX_IP_ADDRESS]);
            $this->setUpdated($result[User::COL_IDX_UPDATED]);
            $this->setLastLogged($result[User::COL_IDX_LAST_LOGED]);
        }
        BwtLogger::logEnd($mn);

    }

    public function toString()
    {
         $retValue = $this->toJSON();
         
        return $retValue;
    }
    
    public function toJSON() {
        return json_encode($this);
    }
    /**
     * ***************************************************************************
     * Getters and Setters Declarations
     * ***************************************************************************
     */
    
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters  Declarations">
    

    public function getId() {
        return $this->userId;
    }
    
    public function setDT_RowId($userId) {
        $this->userId = $userId;
    }

    public function getName() {
        $retValue = $this->userName;
        if(!isset($this->userName))
        {
            $retValue  = $this->getNameFromEMail();
        }
        return $retValue;
    }
    
    public function setName($userName) {
        $this->userName = $userName;
        $this->name = $userName;
    }
    
    public function getNameFromEMail() {
        $retValue = $this->eMail;
        if(isset($this->eMail))
        {
           $valueArr = explode( '@', $this->eMail ,2);
           if(sizeof($valueArr)>0)
            $retValue  = $valueArr[0];
        }
        return $retValue;
    }
    
    public function getEMail() {
        return $this->eMail;
    }

    public function setEMail($eMail) {
        $this->eMail = $eMail;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getRole() {
        return $this->role;
    }
    
    public function getRoleName() {
        $retValue = "Airline Manager";
        if ($this->role === 1)
            $retValue = "Administrator";
        
        return $retValue;
    }

    public function setRole($role) {
        $this->role = $role;
    }
    
    public function isAdmin() {
        if ($this->role === 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getIsReceiveEMails() {
        return $this->isReceiveEMails;
    }

    public function setIsReceiveEMails($isReceiveEMails) {
        $this->isReceiveEMails = $isReceiveEMails;
    }

    public function isReceiveEMails() {
        if ($this->isReceiveEMails > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getIpAddress() {
        return $this->ipAddress;
    }

    public function setIpAddress($ipAddress) {
        $this->ipAddress = $ipAddress;
    }

    public function getLastLogged() {
        $retValue = null;
        if (isset($this->lastLogged) && $this->lastLogged != null) {
            if (!is_object($this->lastLogged))
                $retValue = new DateTime($this->lastLogged);
            else
                $retValue = $this->lastLogged;

            BwtLogger::log("getLastLogged()", "retValue=", getDateStr($retValue));
        }
        else
            $retValue = new DateTime();
        return $retValue;
    }
    
     public function getLastLoggedDbStr() {
        $retValue = null;
        if (isset($this->lastLogged) && $this->lastLogged != null) {
            if (!is_object($this->lastLogged)){
                $value = new DateTime($this->lastLogged);
                $retValue = $value->format("Y-m-d H:i:s");
            }
            else
                $retValue = $this->lastLogged->format("Y-m-d H:i:s");

            BwtLogger::log("getLastLogged()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setLastLogged($lastLogged) {
         if (isset($lastLogged) && $lastLogged != null) {
            if (is_object($lastLogged))
                $this->lastLogged = $data;
            else
                $this->lastLogged = new DateTime($lastLogged);
        }
        
    }

    public function getUpdated() {
        $retValue = null;
        if (isset($this->updated) && $this->updated != null) {
            if (!is_object($this->updated))
                $retValue = new DateTime($this->updated);
            else
                $retValue = $this->updated;

            BwtLogger::log("getUpdated()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setUpdated($updated) {
        if (isset($data) && $data != null) {
            if (is_object($data))
                $this->updated = $data;
            else
                $this->updated = new DateTime($data);
        }
    }

    // </editor-fold>
    
   /****************************************************************************
   * Parameters Declarations
   * ***************************************************************************
   */
   
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">

    public $userId;
    public $eMail;
    public $password;
    public $role = 0; //2 predefined roles 0-user, 1 admin, 2 editor
    
    public $isReceiveEMails = null;
    public $ipAddress;
    public $lastLogged = 0;
    public $updated = 0;
    public $userName;
    public $name;

// </editor-fold>
    
    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME                = "bwt_user";
    const COL_NAME_ID               = "user_id";
    const COL_NAME_EMAIL            = "e_mail";
    const COL_NAME_PSW              = "password";
    const COL_NAME_ROLE             = "user_role";
    const COL_NAME_IS_RECEIVE_EMAILS = "is_receive_emails";
    const COL_NAME_IP_ADDRESS       = "ip_address";
    const COL_NAME_LAST_LOGED       = "adate";
    const COL_NAME_UPDATED          = "udate";
    const COL_NAME_UNNAME           = "user_name";
    
    const COL_IXD_ID                = 0;
    const COL_IDX_EMAIL             = 1;
    const COL_IDX_PSW               = 2;
    const COL_IDX_UNAME             = 3;
    const COL_IDX_ROLE              = 4;
    const COL_IDX_IS_RECEIVE_EMAILS = 5;
    const COL_IDX_IP_ADDRESS        = 6;
    const COL_IDX_UPDATED           = 7;
    const COL_IDX_LAST_LOGED        = 8;
    

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . User::TABLE_NAME . "." . User::COL_NAME_ID . ", " .
                User::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " . User::TABLE_NAME . "." . User::COL_NAME_EMAIL . ", " .
                User::TABLE_NAME . "." . User::COL_NAME_PSW . ", " .
                User::TABLE_NAME . "." . User::COL_NAME_UNNAME. ", " .
                User::TABLE_NAME . "." . User::COL_NAME_ROLE . ", " .
                User::TABLE_NAME . "." . User::COL_NAME_IS_RECEIVE_EMAILS . ", " .
                User::TABLE_NAME . "." . User::COL_NAME_IP_ADDRESS . ", " .
                User::TABLE_NAME . "." . User::COL_NAME_UPDATED;
    }

    public static function getArrayColumns() {
        return array(User::COL_NAME_ID,
            User::COL_NAME_EMAIL,
            User::COL_NAME_PSW,
            User::COL_NAME_UNNAME,
            User::COL_NAME_ROLE,
            User::COL_NAME_IS_RECEIVE_EMAILS,
            User::COL_NAME_IP_ADDRESS,
            User::COL_NAME_UPDATED,
            User::COL_NAME_LAST_LOGED);
    }

    // </editor-fold>
}

