<?php

// <editor-fold defaultstate="collapsed" desc="Response Class">

/**
 * Description of Response class
 *
 * @author IordIord
 */
class Response {

//    public function __construct() {
//        $this->status = "success";
//        $this->data = array();
//        $this->message = "";
//    }

    function __construct() {
        $argv = func_get_args();
        switch (func_num_args()) {
            case 0:
                $this->status = "success";
                $this->data = array();
                $this->message = "";
                break;
            case 1:
                self::__construct1($argv[0]);
                break;
            case 2:
                self::__construct2($argv[0], $argv[1]);
                break;
            case 3:
                self::__construct2($argv[0], $argv[1], $argv[2]);
        }
    }

    public function __construct1(Exception $ex) {

        $this->status = "error";
        $this->statusCode=400;
        $this->data = array();
        $this->data["error_obj"] = json_encode($ex);
        $this->message = "Exception: " . (isset($ex) ? $ex->getMessage() . "See data error_obj parameter for details." : "An unknown error occurred.");
    }

    public function __construct3($status_, $data, $msg) {

        $this->status = $status_;
        if (!isset($this->data))
            $this->data = array();
        if (!isset($data))
            $data = array();
        $this->data = $data;
        $this->message = $msg;
    }

    public function __construct2($status_, $msg) {

        $this->status = $status_;
        if (!isset($this->data))
            $this->data = array();
        $this->message = $msg;
    }

    /**
     * ***************************************************************************
     * Methods Declarations
     * ***************************************************************************
     */
    public function addData($parameter_name, $valueObj) {

        if (!isset($this->data))
            $this->data = array();
        $this->data[$parameter_name] = $valueObj;
    }

    public function toJSON() {

        if (!isset($this->status))
            $this->status = "success";
        return json_encode($this);
    }

    /*
     * **************************************************************************
     * Parameters Declarations
     * **************************************************************************
     */

    public $status;
    public $data;
    public $message;
    public $statusCode = 200;

}

// </editor-fold>

