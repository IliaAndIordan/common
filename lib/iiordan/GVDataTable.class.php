<?php

/*
 * Version:     1.00 2013-02-22
 * Author:      Iordan Z Iordanov <iordanov@iordanov.info>
 * Info:        www.iordanov.info
 * Note:        Requires PHP version 5.3.0 or later
 * 
 * Copyright 2013 Iordan Z. Iordanov, Ilia A. Iliev
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once("Functions.php");

// <editor-fold defaultstate="collapsed" desc="GVDataTableBase">

class GVDataTableBase {

    public function __construct() {
        $this->cols = array();
        $this->rows = array();
    }

    public function toJSON() {
        return json_encode($this);
    }

    // <editor-fold defaultstate="collapsed" desc="Property Declarations">

    public function getCols() {
        return $this->cols;
    }

    public function setCols($cols) {
        $this->cols = $cols;
    }

    public function getRows() {
        return $this->rows;
    }

    public function setRows($rows) {
        $this->rows = $rows;
    }

    public function getSql() {
        return $this->sql;
    }

    public function setSql($sql) {
        $this->sql = $sql;
    }

    public function getBoundParamsArr() {
        return $this->boundParamsArr;
    }

    public function setBoundParamsArr($boundParamsArr) {
        $this->boundParamsArr = $boundParamsArr;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Property Declarations">

    public $cols;
    public $rows;
    private $sql;
    private $boundParamsArr;

    // </editor-fold>

    public static function GTDateSelectStr($fieldName) {
        return " concat('new Date(', DATE_FORMAT(" . $fieldName . ", '%Y,%m,%d'),')') as ".$fieldName." ";
    }

    public static function GTDateUxTimestampStr($fieldName, $aliasName) {
        return " concat('\/Date(', unix_timestamp(" . $fieldName . "),')\/') as ".$aliasName." ";
    }
    
    public static function GTDateAsStr($fieldName, $aliasName) {
        return " DATE_FORMAT(" . $fieldName . ", '%d.%m.%Y') as ".$aliasName." ";
    }
    
    public static function GTDateTimeSelectStr($fieldName) {
        return " concat('new Date(', DATE_FORMAT(" . $fieldName . ", '%Y,%m, %d, %H, %i, %s'),')') as ".$fieldName." ";
    }
    public static function GTDateTimeSelectStr2($fieldName, $aliasName) {
        //return " concat('new Date(', DATE_FORMAT(" . $fieldName . ", '%Y,%m, %d, %H, %i, %s'),')') as ".$aliasName." ";
        return " concat('Date(', YEAR(" . $fieldName . "),', ', MONTH(" . $fieldName . ")-1,', ', DATE_FORMAT(" . $fieldName . ",'%d, %H, %i, %s'),')') as ".$aliasName." "; 
    }
     public static function GTDateStr($fieldName) {
        return " DATE_FORMAT(" . $fieldName . ", '%d.%m.%Y') as ".$fieldName." ";
    }
    
    public function GetData($conn, $logModel) {
        $MN = "GVDataTableBase.GetData()";
        $logModel->logBegin($MN);
        $logModel->logDebug($MN, "SQL=" . $this->sql);
        try {
            $result_r = $conn->preparedSelect($this->sql, $this->boundParamsArr, $logModel);
        } 
        catch (Exception $ex) {
            $logModel->logError($MN, $ex);
        } 
        $logModel->logDebug($MN, "count(result_r)=" . count($result_r));

        $this->rows = array();

        for ($index = 0, $max_count = sizeof($result_r); $index < $max_count; $index++) {
            $rowData = $result_r[$index];
            $row = new GVRow();
            
            for ($colIdx = 0, $max_colls = sizeof($this->cols); $colIdx < $max_colls; $colIdx++) {
                $gvColumn = $this->cols[$colIdx];
                //logDebug($MN, "id =" . $gvColumn->toJSON());
                if (array_key_exists($gvColumn->id, $rowData))
                {
                    try {
                        if($gvColumn->type==GVColumnType::BOOL)
                            $gvCell = new GVCell($rowData[$gvColumn->id]==1?true:false);
                        if($gvColumn->type==GVColumnType::TIMEOFDAY){
                            $arrTmp = explode(",", $rowData[$gvColumn->id]);
                            $gvCell = new GVCell($arrTmp);
                        }
                        else
                            $gvCell = new GVCell($rowData[$gvColumn->id]);
                        
                        $row->addCell($gvCell);
                    } catch (Exception $ex) {
                        $logModel->logError($MN, $ex);
                    }   
                }
                else {
                     $logModel->logDebug($MN, "Missing key =" . $gvColumn->id);
                      $gvCell = new GVCell(null);
                      $row->addCell($gvCell);
                }
                //logDebug($MN, "Add cell =" . $gvCell->toJSON());
            }
            $this->rows[] = $row;
            //logDebug($MN, "Add row =" . $row->toJSON());
        }
        $logModel->logEnd($MN);
        return $this;
    }
    
    public function GetDataMaster($conn, $logModel) {
        $MN = "GVDataTableBase.GetDataMaster()";
        $logModel->logBegin($MN);

        $logModel->logDebug($MN, "SQL=" . $this->sql);
       
        try {
            $result_r = $conn->preparedSelect($this->sql, $this->boundParamsArr);
        } 
        catch (Exception $ex) {
            $logModel->logError($MN, $ex);
        } 
        $logModel->logDebug($MN, "count(result_r)=" . count($result_r));

        $this->rows = array();

        for ($index = 0, $max_count = sizeof($result_r); $index < $max_count; $index++) {
            $rowData = $result_r[$index];
            $row = new GVRow();
            
            for ($colIdx = 0, $max_colls = sizeof($this->cols); $colIdx < $max_colls; $colIdx++) {
                $gvColumn = $this->cols[$colIdx];
                //logDebug($MN, "id =" . $gvColumn->toJSON());
                if (array_key_exists($gvColumn->id, $rowData))
                {
                    try {
                        if($gvColumn->type==GVColumnType::BOOL)
                            $gvCell = new GVCell($rowData[$gvColumn->id]==1?true:false);
                        if($gvColumn->type==GVColumnType::TIMEOFDAY){
                            $arrTmp = explode(",", $rowData[$gvColumn->id]);
                            $gvCell = new GVCell($arrTmp);
                        }
                        else
                            $gvCell = new GVCell($rowData[$gvColumn->id]);
                        
                        $row->addCell($gvCell);
                    } catch (Exception $ex) {
                        $logModel->logError($MN, $ex);
                    }   
                }
                else {
                     $logModel->logDebug($MN, "Missing key =" . $gvColumn->id);
                      $gvCell = new GVCell(null);
                      $row->addCell($gvCell);
                }
                //$logModel->logDebug($MN, "Add cell =" . $gvCell->toJSON());
            }
            $this->rows[] = $row;
            //$logModel->logDebug($MN, "Add row =" . $row->toJSON());
        }
        $logModel->logEnd($MN);
        return $this;
    }

    public function getValue($rowIdx, $colName, $logModel)
    {
        $MN = "GVDataTableBase.getValue(".$rowIdx.",".$colName.")";
        $logModel->logBegin($MN);
        //$logModel->logDebug($MN, "start");
        
        $retValue = "";
        $colIdx = -1;
        //$logModel->logDebug($MN, "Cols=".sizeof($this->getCols()));
        
        for ($idx = 0; $idx < sizeof($this->getCols()); $idx++) {
            
            $value= $this->getCols()[$idx]->id;
            //$logModel->logDebug($MN, "value=".$value);
            if($value==$colName)
            {
                $colIdx = $idx;
                break;
            }
        }
        //$logModel->logDebug($MN, "colIdx=".$colIdx);
        if($colIdx>-1 && sizeof($this->getRows())>=$rowIdx)
        {
            $retValue = $this->getRows()[$rowIdx]->getCells()[$colIdx]->Value();
        }
        //$logModel->logDebug($MN, "retValue=".$retValue);
        $logModel->logEnd($MN);
        return $retValue;

    }
}

class GVRow {

    public function __construct() {
        $this->c = array();
    }

    public function addCell($gvCell) {
        $this->c[] = $gvCell;
    }
    
     public function getCells() {
        return $this->c;
    }

    public function toJSON() {
        return json_encode($this);
    }

    // <editor-fold defaultstate="collapsed" desc="Property Declarations">

    public $c;

    // </editor-fold>
}

class GVCell {

    public function __construct($op_v) {
        if (isset($op_v))
            $this->v = $op_v;
    }

    public function toJSON() {
        return json_encode($this);
    }

    // <editor-fold defaultstate="collapsed" desc="Property Declarations">

    public $v;
    public $f;
    public $p;

    public function Value() {
        return $this->v;
    }
    // </editor-fold>
}

class GVColumn {

    public function __construct($op_id, $op_label, $gvColumnType) {
        if (isset($op_id))
            $this->id = $op_id;
        if (isset($op_label))
            $this->label = $op_label;

        $this->type = $gvColumnType;
    }

    public function toJSON() {
        return json_encode($this);
    }

    // <editor-fold defaultstate="collapsed" desc="Property Declarations">

    public $id;
    public $label;
    public $type;

    // </editor-fold>
}

final class GVColumnType {

    private function __construct() {
        
    }

    const STRING = "string";
    const NUMBER = "number";
    const BOOL = "boolean";
    const DATE = "date";
    const DATETIME = "datetime";
    const TIMEOFDAY = "timeofday";

}

// </editor-fold>

