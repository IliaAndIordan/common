<?php

/* 
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Apr 13, 2018 
 *  Filename            : rest_reader.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */


// Allow from any origin
if (isset($_SERVER["HTTP_ORIGIN"])) {
    // You can decide if the origin in $_SERVER['HTTP_ORIGIN'] is something you want to allow, or as we do here, just allow all
    //header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']} ");
    header("Access-Control-Allow-Origin: *");
} else {
    //No HTTP_ORIGIN set, so we allow any. You can disallow if needed here
    header("Access-Control-Allow-Origin: *");
}


header("Access-Control-Max-Age: ".(2*60*60)." ");    // number of seconds the results can be cached.

if ($_SERVER["REQUEST_METHOD"] == "OPTIONS") {
    if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_METHOD"]))
        header("Access-Control-Allow-Methods: POST, OPTIONS, DELETE, PUT"); //Make sure you remove those you do not want to support

    if (isset($_SERVER["HTTP_ACCESS_CONTROL_REQUEST_HEADERS"]))
        header("Access-Control-Allow-Headers: Authorization, X-Authorization, Origin, Content-Type, Accept, Accept-Encoding, Accept-Language");

    header("Access-Control-Allow-Credentials: true");
    header('Content-Length: 0');
    //header('Content-Type: text/plain');
    header("Content-type: application/json; charset=utf-8");
    //Just exit with 200 OK with the above headers for OPTIONS method
    exit(0);
}
else {
    header("Access-Control-Expose-Headers: Authorization, X-Authorization, Origin, Content-Type, Accept");
}

header('Cache-control: private');
header("Content-type: application/json; charset=utf-8");


