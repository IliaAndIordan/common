<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: SimpleRest.class.php
 *
 * Database System        	: MySQL
 * Created from                  : IordIord
 * Date Creation			: 21.03.2016
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * A simple RESTful webservices base class
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: SimpleRest.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */

/**
 * Description of SimpleRest
 *
 * @author IIordan
 */
class SimpleRest {

    private $httpVersion = "HTTP/1.1";

    public function setHttpHeaders($contentType, $statusCode) {

        $statusMessage = $this->getHttpStatusMessage($statusCode);

        header($this->httpVersion . " " . $statusCode . " " . $statusMessage);
        header("Content-Type:" . $contentType);
    }

    public function getHttpStatusMessage($statusCode) {
        $httpStatus = array(
            100 => 'Continue',
            101 => 'Switching Protocols',
            200 => 'OK',
            201 => 'Created',
            202 => 'Accepted',
            203 => 'Non-Authoritative Information',
            204 => 'No Content',
            205 => 'Reset Content',
            206 => 'Partial Content',
            300 => 'Multiple Choices',
            301 => 'Moved Permanently',
            302 => 'Found',
            303 => 'See Other',
            304 => 'Not Modified',
            305 => 'Use Proxy',
            306 => '(Unused)',
            307 => 'Temporary Redirect',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            406 => 'Not Acceptable',
            407 => 'Proxy Authentication Required',
            408 => 'Request Timeout',
            409 => 'Conflict',
            410 => 'Gone',
            411 => 'Length Required',
            412 => 'Precondition Failed',
            413 => 'Request Entity Too Large',
            414 => 'Request-URI Too Long',
            415 => 'Unsupported Media Type',
            416 => 'Requested Range Not Satisfiable',
            417 => 'Expectation Failed',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Bad Gateway',
            503 => 'Service Unavailable',
            504 => 'Gateway Timeout',
            505 => 'HTTP Version Not Supported');
        return ($httpStatus[$statusCode]) ? $httpStatus[$statusCode] : $status[500];
    }

    /**
     * ***************************************************************************
     * REST WEB SERVICE FUNCTIONS Declarations
     * ***************************************************************************
     */
// <editor-fold defaultstate="collapsed" desc="Encode Array Functions">

    public function EncodeResponce($data) {
        $responseObj = null;

        if (!isset($data) || $data == null) {
            $statusCode = 404;
            $responseObj = new Response("error", "No Data Found", "Unknown error");
        } else {
            if(isset($data->statusCode))
                $statusCode = $data->statusCode;
            else
                $statusCode = 200;
        }

        if (is_object($data)) {
            $responseObj = $data;
        } 
        else if (is_array($data)) {
            $responseObj = new Response();
            $responseObj->status = "success";
            $responseObj->data = $data;
        }
        else {
            $responseObj = new Response("success", $data, "Array of data.");
        }
        
        $response = '';
        $requestContentType = isset($_SERVER['HTTP_ACCEPT']) && strlen($_SERVER['HTTP_ACCEPT']) > 3 ? $_SERVER['HTTP_ACCEPT'] : "application/json";
        
        $this->setHttpHeaders($requestContentType, $statusCode);

        if (strpos($requestContentType, 'application/json') !== false) {
            $response = $this->encodeJson($responseObj);
        } else if (strpos($requestContentType, 'text/html') !== false) {
            $response = $this->encodeJson($responseObj);
            //$response = $this->encodeHtml($responseObj);
        } else if (strpos($requestContentType, 'application/xml') !== false) {
            $response = $this->encodeXml($responseObj);
        } else
            $response = $this->encodeJson($responseObj);
        
        echo $response;
    }

    public function EncodeResponceArray($dataArr) {
        
        if (empty($dataArr)) {
            $statusCode = 404;
            $dataArr = array("status" => "error", "data" => "No Data Found", "message" => "Unknown error");
        } else {
            $statusCode = 200;
        }
        $response = '';
        $requestContentType = isset($_SERVER['HTTP_ACCEPT']) && strlen($_SERVER['HTTP_ACCEPT']) > 3 ? $_SERVER['HTTP_ACCEPT'] : "application/json";
        
        $this->setHttpHeaders($requestContentType, $statusCode);

        if (strpos($requestContentType, 'application/json') !== false) {
            $response = $this->encodeJson($dataArr);
        } else if (strpos($requestContentType, 'text/html') !== false) {
            $response = $this->encodeHtml($dataArr);
        } else if (strpos($requestContentType, 'application/xml') !== false) {
            $response = $this->encodeXml($dataArr);
        } else
            $response = $this->encodeJson($dataArr);
        
        echo $response;
    }

    public function encodeHtml($responseData) {

        $htmlResponse = "<table border='1'>";

        foreach ($responseData as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $key1 => $value1) {
                    if (is_array($value1)) {
                        $htmlResponse .= "<tr><td>" . $key1 . "</td><td>" . $this->encodeHtml($value1) . "</td></tr>";
                    }
                    else if (is_object($value1)) {
                        if (empty($value1)) {
                            $htmlResponse .= "<tr><td>" . $key1 . "</td><td>Empty Array</td></tr>";
                        } else {
                            $htmlResponse .= "<tr><td>" . $key1 . "</td><td><ul>";
                            foreach ($value1 as $prop => $pv) {

                                $htmlResponse .= "<li>" . $prop . " -> " . (is_object($pv) ? json_encode($pv) : $pv) . "</li>";
                            }
                            $htmlResponse .= "</ul></td></tr>";
                        }
                        //$htmlResponse .= "<tr><td>" . $key . "</td><td>" . json_encode($value) . "</td></tr>";
                    } else
                        $htmlResponse .= "<tr><td>" . $key1 . "</td><td>" . (is_object($value1) ? json_encode($value1) : $value1) . "</td></tr>";
                }
            }
            else if (is_object($value)) {
                if (empty($value)) {
                    $htmlResponse .= "<tr><td>" . $key . "</td><td>Empty Array</td></tr>";
                } else {
                    $htmlResponse .= "<tr><td>" . $key . "</td><td><ul>";
                    foreach ($value as $prop => $pv) {

                        $htmlResponse .= "<li>" . $prop . " -> " . (is_object($pv) ? json_encode($pv) : $pv) . "</li>";
                    }
                    $htmlResponse .= "</ul></td></tr>";
                }
                //$htmlResponse .= "<tr><td>" . $key . "</td><td>" . json_encode($value) . "</td></tr>";
            } else
                $htmlResponse .= "<tr><td>" . $key . "</td><td>" . (is_object($value) ? json_encode($value) : $value) . "</td></tr>";
        }

        $htmlResponse .= "</table>";
        return $htmlResponse;
    }

    public function encodeJson($responseData) {
        $jsonResponse = json_encode($responseData);
        return $jsonResponse;
    }

    function encodeXml($responseData) {
        // creating object of SimpleXMLElement
        $xml = new SimpleXMLElement('<?xml version="1.0"?><mobile></mobile>');
        foreach ($responseData as $key => $value) {
            $xml->addChild($key, $value);
        }
        return $xml->asXML();
    }

// </editor-fold>
}
