<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once("Logger.php");

/**
 * Description of LoggerBase
 *
 * @author izior
 */
class LoggerBase {

    public $logger = null;
    public $MN = "LoggerBase";
    public $logModules = [];

    // <editor-fold defaultstate="collapsed" desc="Constructors">
    public function __construct() {
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public function getModule($mn) {

        $retValue = null;
        for ($index = 0; $index < sizeof($this->logModules); $index++) {
            $mod = $this->logModules[$index];
            if ($mod->moduleName == $mn) {
                $retValue = $mod;
                break;
            }
        }
        if (!isset($retValue)) {
            $retValue = new LogModule($mn);
            $retValue->logger = $this->logger;
        }
        return $retValue;
    }

    public function begin($mn) {
        $mod = $this->getModule($mn);
        $this->logger->debug($mod->name() . "->");
    }

    public function end($mn) {
        $now = microtime(true);
        $mod = $this->getModule($mn);
        $ms = (float) (($now - $mod->beginTime) * 1000);
        $time_str = LogModule::duartionFormat($ms) . " !";
        
        $msg = $mod->name() . "<- proc time: ".$time_str; 
        if ($ms > 500) {
            $this->logger->warn(msg);
        } else{
            $this->logger->debug($msg);
        }
        
        //$this->logger->debug($mod->name() . "<- ".$time_str );
    }

    public function debug($mn, $msg) {
        $now = microtime(true);
        $mod = $this->getModule($mn);
        $ms = (float) (($now - $mod->beginTime) * 1000);

        if(strlen($msg)>2000){
            $msg = substr($msg,0,2000);
        }
        $this->logger->debug(
                $mod->name() . " " . $msg . " proc time: " .
                LogModule::duartionFormat($ms) . " !");
        
    }

    function error($mn, $ex) {
        $mod = $this->getModule($mn);
        $target = $mod->name();
        $this->logger->error($target . "ERRORR Appear see Error or Debug log for details");
        $this->logger->error($target . "------------------------------------------------");
        if (isset($ex) && $ex != null) {
            $this->logger->error($target . "Exception");
            $this->logger->error($target . "------------------------------------------------");
            $this->logger->error($target . (is_object($ex) ? $ex->getMessage() : "No Message:" . $ex));
            $this->logger->error($target . "------------------------------------------------");
        }
    }

    // </editor-fold>
}

class LogModule {

    public $moduleName = null;
    public $childrens = [];
    //"msec sec"
    public $beginTime = null;
    public $logger = null;
    public $iOffset = 0;

    public function __construct($mn) {
        $this->moduleName = $mn;
        $this->beginTime = microtime(true);
    }

    public function ofset() {
        $strOffset = "";
        for ($index = 0; $index < $this->iOffset; $index++) {
            $strOffset .= "-";
        }
        return $strOffset;
    }

    public function name() {
        return $this->ofset() . $this->moduleName . ": ";
    }

    public function getChaild($mn) {
        // childrens
        $retValue = null;
        if(sizeof($this->childrens)>0){
            $index = 0;
            foreach ($this->childrens as $value) {
                if ($value->moduleName == $mn) {
                    $retValue = $value;
                    break;
                }
                $index++;
            }
        }

        return $retValue;
    }

    public function getChaildIdx($mn) {
        // childrens
        $retValue = -1;
        if(sizeof($this->childrens)>0){
            $index = 0;
            foreach ($this->childrens as $value) {
                if ($value->moduleName == $mn) {
                    $retValue = $index;
                    break;
                }
                $index++;
            }
        }

        return $retValue;
    }

    public function removeChaildIdx($mn) {
        // childrens
        $mnIdx = $this->getChaildIdx($mn);
        if ($mnIdx > -1 && $mnIdx < sizeof($this->childrens)) {
            unset($this->childrens[$mnIdx]);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Query Json Methods">
    public function getModule($mn) {

        $retValue = $this->getChaild($mn);
        if (!isset($retValue)) {
            $retValue = new LogModule($mn);
            $retValue->logger = $this->logger;
            $this->childrens[] = $retValue;
            $retValue->iOffset = $this->iOffset + 1;
        }
        return $retValue;
    }

    public function logBegin($mn) {
        $mod = $this->getModule($mn);
        $this->logger->debug($mod->name() . "->");
    }

    public function logEnd($mn) {
        $now = microtime(true);
        $mod = $this->getModule($mn);

        $ms = (float) (($now - $mod->beginTime) * 1000);
        $time_str = LogModule::duartionFormat($ms) . " !";
        
        if ($ms > ((float)1000)) {
            $this->logger->trace(
                    $mod->name() . " procesing time " .
                    LogModule::duartionFormat($ms) . " !");
        }

        $this->logger->debug($mod->name() . "<- " .$time_str);
        $this->removeChaildIdx($mn);
    }

    public function logDebug($mn, $msg) {
        $now = microtime(true);
        $mod = $this->getModule($mn);
        $ms = (float) (($now - $mod->beginTime) * 1000);
        $time_str = LogModule::duartionFormat($ms) . " !";
        
        if(strlen($msg)>2000)
            $msg = substr($msg,0,2000);
        
        $this->logger->debug(
                $mod->name() . " " . $msg . " proc time: " .$time_str. " !");
        
    }

    function logError($mn, $ex) {
        $mod = $this->getModule($mn);
        $target = $mod->name();
        //$this->logger->error($target . " ERRORR Appear see Error or Debug log for details");
        $this->logger->error($target . "------------------------------------------------");
        if (isset($ex) && $ex != null) {
            //$this->logger->error($target . " Exception");
            //$this->logger->error($target . "------------------------------------------------");
            $this->logger->error($target . (is_object($ex) ? $ex->getMessage() : "No Message:" . $ex));
            $this->logger->error($target . "------------------------------------------------");
        }
    }

    public static function duartionFormat($ms) {
        $uSec = $ms % 1000;
        $input = floor($ms / 1000);

        $seconds = $input % 60;
        $input = floor($input / 60);

        $minutes = $input % 60;
        $input = floor($input / 60);

        $retValue = ($minutes > 0 ? (number_format($minutes, 0, '.', ' ') . ':') : "") .
                str_pad($seconds, 2, '0', '0') . '.' .
                str_pad($uSec, 3, '0', '0');
        return $retValue;
    }

}
