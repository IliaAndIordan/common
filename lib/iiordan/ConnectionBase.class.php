<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: mlws
 * Module 				 	: package_name
 * Responsible for module 	: IordIord
 *
 * Filename               	: Connect.php
 *
 * Database System        	: ORCL, MySQL
 * Created from			: IordIord
 * Date Creation		: 14.09.2009
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 * 	 
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: Connect.php,v $
 * <br>---
 * <br>--- 
 *
 * ******************************** HEAD_END ************************************
 */


require_once("Functions.php");
require_once("Response.class.php");
//require_once("LoggerBase.php");



class ConnectionBase {

    public $connection = null;

    // <editor-fold defaultstate="collapsed" desc="Construct">
    
    public function __construct($db_host, $db_user, $db_pass, $db_name, $db_port) {
        $MN = "common:ConnectionBase::__construct()";
        //$ST = logBegin($MN);
        
        $this->connection = new mysqli($db_host, $db_user, $db_pass, $db_name, $db_port);
        
        //echo 'include_path... '.ini_get('include_path').' <br/>';    
        $this->connection->query("SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ");
        //$this->connection->query("SET lower_case_table_names 1");
        //$this->connection->set_charset("usf8"); mysql:host=localhost;port=3307;dbname=testdb
        try {

            $this->connection->query("SET NAMES 'utf8' COLLATE 'utf8_unicode_ci'");

            
            // Will not affect $mysqli->real_escape_string();
            $this->connection->query("SET CHARACTER SET utf8");

            // But, this will affect $mysqli->real_escape_string();
            $this->connection->set_charset('utf8');

            $charset = $this->connection->character_set_name();
            //logDebug($MN, "db charset=".$charset);
            if (mysqli_connect_errno()) {
                $contentPage = "pp_maintenance";
                logDebug("$MN", "Database connect Error : "
                        . mysqli_connect_error($this->connection));

                header('Location: /dberror.html');
                //die();
                //header("Location: ".$url);
                ob_flush();
            }
        } catch (Exception $ex) {
            echo $ex;
            $contentPage = "pp_maintenance";
        }
        
        //logEndST($MN, $ST);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Json Query">
    
    public function dbExecuteSQLJson($strSQL, $logModel) {

        $MN = "ConnectionBase->dbExecuteSQLJson()";
        $logModel->logBegin($MN);

        $retArray = array();
        $idx = 0;
        
        $logModel->logDebug($MN, "SQL=" . $strSQL);

        try {
            $result = $this->connection->query($strSQL);
            //$logModel->logDebug($MN, "result =" . var_dump($result));
            if ($result) {
                while ($row = $result->fetch_assoc()) {
                    $retArray[$idx] = $row;
                    $idx++;
                    //$logModel->logDebug($MN, "row[" . $idx . "] =" . var_dump($row));
                }
                
                $result->close();
            }
        } catch (Exception $ex) {
            $logModel->logError($MN, $ex);
        }
        $logModel->logEnd($MN);
        return $retArray;
    }
    
    public function SelectJson($query, $bind_params_r, $logModel) {
        $MN = "common:Connect.SelectJson()";
        //$logModel->logBegin($MN);
        $result_r = array();
        try{
            //$logModel->logDebug($MN, "query=".$query);
            //$logModel->logDebug($MN, "bind_params_r=".json_encode($bind_params_r));
            $statement = $this->runPreparedQuery($query, $bind_params_r, $logModel);
            $statement->execute();
            //$logModel->logDebug($MN, "after execute()");
            $statement->store_result();
            //$logModel->logDebug($MN, "after store_result()");
            //$logModel->logDebug($MN, "Number of rows: ". $statement->num_rows);
            $result_r = $this->fetch($statement, $logModel);
            //$logModel->logDebug($MN, "after fetch()");
            $statement->close();
            //$logModel->logDebug($MN, "result_r=".json_encode($result_r));
        } catch (Exception $ex) {
            $logModel->logDebug($MN, "query=".$query);
            $logModel->logDebug($MN, "bind_params_r=".json_encode($bind_params_r));
            $logModel->logError($MN, $ex);
        }
         //$logModel->logEnd($MN);
        return $result_r;
    }

    public function fetch($result, $logModel) {
        $MN = "common:Connect.fetch()";
        $array = array();

        if ($result instanceof mysqli_stmt) {
            //$result->store_result();
            //$logModel->logDebug($MN, "mysqli_stmt number of rows: ". $result->num_rows);
            $variables = array();
            $data = array();
            $meta = $result->result_metadata();

            while ($field = $meta->fetch_field())
                $variables[] = &$data[$field->name]; // pass by reference

            call_user_func_array(array($result, 'bind_result'), $variables);

            $i = 0;
            while ($result->fetch()) {
                $array[$i] = array();
                foreach ($data as $k => $v)
                    $array[$i][$k] = $v;
                $i++;

                // don't know why, but when I tried $array[] = $data, I got the same one result in all rows
            }
        } elseif ($result instanceof mysqli_result) {
            //$logModel->logDebug($MN, "mysqli_result");
            while ($row = $result->fetch_assoc())
                $array[] = $row;
        }

        return $array;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Standart Query">
    
    public function dbExecuteSQL($strSQL, $logModel) {

        $MN = "ConnectionBase->dbExecuteSQL()";
        $logModel->logBegin($MN);

        $retArray = array();
        $idx = 0;
        
        //$strSQL = strtolower($strSQL);
        $logModel->logDebug("$MN", "SQL=" . $strSQL);

        $result = $this->connection->query($strSQL);

        if ($result) {
            while ($row = $result->fetch_row()) {
                $retArray[$idx] = $row;
                $idx++;
                //$logModel->logDebug($MN, "row[" . $idx . "] =" . prArr($row));
            }
            $result->close();
        }

        $logModel->logEnd($MN);
        return $retArray;
    }
    
    // </editor-fold>
    
     // <editor-fold defaultstate="collapsed" desc="Prepared Query">
    public function preparedSelectJson($query, $bind_params_r, $logModel) {
        $MN = "common:Connect.preparedSelectJson()";
        $logModel->logDebug($MN, " query: " . $query);
        
        $sth = $this->runPreparedQuery($query, $bind_params_r, $logModel);
        //$fields_r = $this->fetchFields($select);
        //Return next row as an anonymous object with column names as properties
        $results_r = $sth->get_result();
        $result = $results_r->fetch_all(MYSQLI_ASSOC);
        
       $logModel->logDebug($MN, " result=" . json_encode($result));
        return $result;
    }
    public function preparedSelect($query, $bind_params_r, $logModel) {
        $MN = "common:Connect.preparedSelect()";
        //$logModel->logBegin($MN);
        
        $select = $this->runPreparedQuery($query, $bind_params_r, $logModel);
        $fields_r = $this->fetchFields($select);

        foreach ($fields_r as $field) {
            $bind_result_r[] = &${$field};
        }

        $this->bindResult($select, $bind_result_r);

        $result_r = array();
        $i = 0;
        while ($select->fetch()) {

            foreach ($fields_r as $field) {

                $result_r[$i][$field] = $$field;
                /*
                  if(mb_detect_encoding($$field) === "UTF-8") {
                  $result_r[$i][$field] = $$field;
                  }
                  else{
                  $result_r[$i][$field] =  utf8_encode( $$field);
                  } */
            }
            $i++;
        }
        $select->close();
        //logDebug($MN, "result_r=".prArr($result_r));
        //logEndST($MN, $ST);
        return $result_r;
    }
    
    public function runPreparedQuery($query, $bind_params_r, $logModel) {
        $MN = "ConnectionBase->runPreparedQuery()";
        //$logModel->logBegin($MN);

        //$logModel->logDebug($MN, "SQL=" . $query);
        //$logModel->logDebug($MN, "get_server_info=".$this->connection->get_server_info());
        if (!$stmt = $this->connection->prepare($query)) {
            $logModel->logDebug($MN, "Prepare failed: (" . $this->connection->errno . ") " . $this->connection->error);
            return 0;
        }
        
        $this->bindParameters($stmt, $bind_params_r, $logModel);
        //$logModel->logDebug($MN, "after bindParameters");
        $res = $stmt->execute();
        
        if ($res) {
            //$logModel->logDebug($MN, "after execute ");
            //$logModel->logEnd($MN);
            return $stmt;
        } else {
            $logModel->logDebug($MN, mysqli_error($this->connection));
            $logModel->logDebug("$MN", "Error in query: " . $query);
            $logModel->logDebug("$MN", "Error in bind_params_r: " . json_encode($bind_params_r));
            $logModel->logDebug("$MN", "Error: " . mysqli_error($this->connection));
            echo "Error: " . mysqli_error($this->connection);
            $logModel->logEnd($MN);
            return 0;
        }
    }
    
     public function preparedInsert($query, $bind_params_r, $logModel) {
        $MN = "common:Connect.preparedInsert()";
        $logModel->logBegin($MN);

        $select = $this->runPreparedQuery($query, $bind_params_r, $logModel);

        $id = $this->connection->insert_id;

        if(isset($select))$select->close();
        $logModel->logDebug($MN, "Insert id=".$id);
        $logModel->logEnd($MN);
        return $id;
    }

    public function preparedDelete($query, $bind_params_r, $logModel) {
        $MN = "common:Connect.preparedDelete()";
        $logModel->logBegin($MN);
        $rowsAffected = -1;
        $select = $this->runPreparedQuery($query, $bind_params_r, $logModel);
        $rowsAffected = $this->connection->affected_rows;
        $select->close();
        $logModel->logDebug($MN, "rowsAffected=".$rowsAffected);
        $logModel->logEnd($MN);
        return $rowsAffected;
    }

    public function preparedUpdate($query, $bind_params_r, $logModel) {
        $MN = "common:Connect.preparedUpdate()";
        //$logModel->logBegin($MN);
        $rowsAffected = -1;
        $stmt = $this->runPreparedQuery($query, $bind_params_r, $logModel);
        $rowsAffected = $this->connection->affected_rows;
        $stmt->close();
        //$logModel->logDebug($MN, "rowsAffected=".$rowsAffected);
        //$logModel->logEnd($MN);
        return $rowsAffected;
    }
    
    private function bindParameters(&$stmt, &$bind_params_r, $logModel) {
        $MN = "ConnectionBase->bindParameters()";
        //$logModel->logBegin($MN);
        $tmp = array();
        foreach ($bind_params_r as $key => $value) {
            $tmp[$key] = &$bind_params_r[$key];
            //$logModel->logDebug($MN, "key=" . $key . " value=" . $value);
        }

        if (!call_user_func_array([$stmt, "bind_param"], $tmp)) {
            $msgStr = "Param Bind failed, [" . implode(",", $bind_params_r) . "]:" . $bind_params_r[0] . " (" . $stmt->errno . ") " . $stmt->error;
            //$logModel->logDebug($MN, $msgStr);
            echo $msgStr;
        }

        //$logModel->logEnd($MN);
    }
    
     private function bindResult(&$obj, &$bind_result_r) {
        
        call_user_func_array(array($obj, "bind_result"), $bind_result_r);
        
    }
    
    private function fetchFields($selectStmt) {
        $MN = "common:Connect.fetchFields()";
        //$ST = logBegin($MN);
        $metadata = $selectStmt->result_metadata();
        $fields_r = array();
        while ($field = $metadata->fetch_field()) {
            $fields_r[] = $field->name;
        }
        //logEndST($MN, $ST);
        return $fields_r;
    }
    
    // </editor-fold>
}







/**
 * ******************************************************************************
 *                        Iordan Z Iordanov 2018
 * ******************************************************************************
 * */
