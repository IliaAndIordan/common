<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: isgt
 * Module                        : user
 * Responsible for module 	: IordIord
 *
 * Filename               	: VlbUser.class.php
 *
 * Database System        	: ORCL, MySQL
 * Created from                  : IordIord
 * Date Creation			: 14.10.2016
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: VlbUser.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */

/**
 * Description of VlbUser class
 *
 * @author IordIord
 */
class User {

    public $id;
    public $eMail;
    public $name;
    public $role = 0; //2 predefined roles 0-user, 1 admin, 2 editor
    public $roleName;
    public $isReceiveEMails = 1;
    public $ipAddress;
    public $lastLogged = 0;
    public $updated = 0;

    function __construct($vlbUser) {
        $this->id = $vlbUser->getId();
        $this->eMail = $vlbUser->getEMail();
        $this->name = $vlbUser->getName();
        $this->role = $vlbUser->getRole();
        $this->roleName = $vlbUser->getRoleName();
        $this->isReceiveEMails = $vlbUser->getIsReceiveEMails();
        $this->ipAddress = $vlbUser->getIpAddress();
        ;
        $this->lastLogged = $vlbUser->getLastLogged();
        $this->updated = $vlbUser->getUpdated();
    }

}

class VlbUser {
    /**
     * ***************************************************************************
     * Methods Declarations
     * ***************************************************************************
     */

    /**
     *
     * @param <type> $id 
     */
    public function loadById($id) {
        $MN = "user:VlbUser.loadById()";
        LoggerVlb::logBegin($MN);
        LoggerVlb::log($MN, "id = " . $id);
        $this->setId($id);
        $sql = "SELECT " . VlbUser::getAllColumnsSQL() . ", " . VlbUser::COL_NAME_UPDATED .
                " FROM " . VlbUser::TABLE_NAME . " " .
                " WHERE " . VlbUser::COL_NAME_ID . "=?";
        $bound_params_r = array('i', $id);
        $conn = ConnectionVlb::dbConnect();
        $logModel = LoggerVlb::currLogger()->getModule($MN);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        LoggerVlb::log($MN, "count(result_r)=" . count($result_r));
        $data = null;
        if (count($result_r) > 0) {
            $data = $result_r[0];
            //LoggerVlb::log($MN, "count(data)=".count($data));
        }
        //LoggerVlb::log("$MN", "ret Value=".prArr($data) );
        if (isset($data) && count($data) > 0) {
            $this->loadFromArray($data);
        }
        LoggerVlb::log($MN, "VlbUser is " . $this->toString());

        LoggerVlb::logEnd($MN);
    }

    public static function login($user_email, $user_password) {
        $MN = "user:VlbUser.login()";
        LoggerVlb::logBegin($MN);
        $out = null;
        $loginName = validate_search_string($user_email);
        $password = validate_search_string($user_password);

        //$user_email = base64_encode($loginName);
        //$user_password = md5($password);

        $sqlStr = "SELECT " . VlbUser::COL_NAME_ID . " FROM " . VlbUser::TABLE_NAME .
                " WHERE " . VlbUser::COL_NAME_EMAIL . "=? " .
                " AND " . VlbUser::COL_NAME_PSW . "=?";
        $bound_params_r = array('ss', $user_email, $user_password);

        $conn = ConnectionVlb::dbConnect();
        $logModel = LoggerVlb::currLogger()->getModule($MN);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        if (count($result_r) > 0) {

            LoggerVlb::log($MN, prArr($result_r[0]));
            $id = $result_r[0][VlbUser::COL_NAME_ID];
            LoggerVlb::log($MN, "Found userId" . $id . " for E-Mail " . $user_email . "!");
            $ipAddress = $_SERVER['REMOTE_ADDR'];

            if ($id > -1) {
                $out = new VlbUser();
                $out->loadById($id);
                $out->setIpAddress($ipAddress);
                $out->setLastLogged(CurrenDateTime());
                $out->save();
            }
        }

        LoggerVlb::logEnd($MN);
        return $out;
    }

    /**
     *
     * @param <type> $email
     * @return <type> 
     */
    public static function isExistEMail($email) {
        $MN = "user.VlbUser:isExistEMail()";
        LoggerVlb::logBegin($MN);
        $user_email = validate_search_string($email);

        $sqlStr = "SELECT count(*) AS Rows FROM " .
                VlbUser::TABLE_NAME . " WHERE " . VlbUser::COL_NAME_EMAIL . "=?";

        $bound_params_r = array('s', $user_email);
        LoggerVlb::log($MN, $sqlStr);
        $conn = ConnectionVlb::dbConnect();
        $logModel = LoggerVlb::currLogger()->getModule($MN);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        LoggerVlb::log($MN, prArr($result_r[0]));
        $nrRows = $result_r[0]["Rows"];
        LoggerVlb::log($MN, "Found " . $nrRows . " users with E-Mail " . $user_email . "!");
        LoggerVlb::logEnd($MN);
        return ($nrRows > 0 ? TRUE : FALSE);
    }

    public static function forgotenPassword($email, $conf) {
        $MN = "user.VlbUser:forgotenPassword()";
        $st = logBegin($MN);
        $id = -1;
        $retValue = false;
        $user_email = validate_search_string($email);
        if (VlbUser::isExistEMail($user_email) > 0) {
            $sqlStr = "SELECT " . VlbUser::COL_NAME_ID . " FROM " .
                    VlbUser::TABLE_NAME . " WHERE " . VlbUser::COL_NAME_EMAIL . "=?";

            $bound_params_r = array('s', $user_email);
            LoggerVlb::log($MN, $sqlStr);
            $conn = ConnectionVlb::dbConnect();
            $logModel = LoggerVlb::currLogger()->getModule($MN);
            $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
            LoggerVlb::log($MN, prArr($result_r[0]));
            $id = $result_r[0][VlbUser::COL_NAME_ID];
            LoggerVlb::log($MN, "Found " . $id . " user ID with E-Mail " . $user_email . "!");
            if ($id > 0) {
                $user = new VlbUser();
                $user->loadById($id);
                $retValue = sendMailRegistration($user, $conf);
            }
        }
        logEnd($MN, $st);
        return $retValue;
    }

    /**
     *
     * @param <type> $id
     */
    public static function deleteById($id) {
        $MN = "user:VlbUser.deleteById()";
        LoggerVlb::logBegin($MN);
        LoggerVlb::log($MN, "id = " . $id);
        //$this->setId($id);
        $sql = "DELETE " .
                " FROM " . VlbUser::TABLE_NAME . " " .
                " WHERE " . VlbUser::COL_NAME_ID . "=?";
        $bound_params_r = array("i", $id);
        $conn = ConnectionVlb::dbConnect();
        $logModel = LoggerVlb::currLogger()->getModule($MN);
        $id = $conn->preparedDelete($sql, $bound_params_r, $logModel);

        LoggerVlb::logEnd($MN);
    }

    public static function loadAll() {
        $mn = "user:VlbUser.loadAll()";
        $st = logBegin($mn);


        $sql = "SELECT " . VlbUser::getAllColumnsSQL() . ", " . VlbUser::COL_NAME_UPDATED .
                " FROM " . VlbUser::TABLE_NAME . " ";

        $conn = ConnectionVlb::dbConnect();
        $logModel = LoggerVlb::currLogger()->getModule($MN);
        $data = $conn->dbExecuteSQL($sql, $logModel);
        LoggerVlb::log($mn, "count(data)=" . count($data));
        $retArray = array();
        for ($index = 0, $max_count = sizeof($data); $index < $max_count; $index++) {
            $result = $data[$index];
            $item = new VlbUser();
            $item->loadFromPosArray($result);
            $retArray[] = $item;
            LoggerVlb::log($mn, "Add Item =" . $item->toString());
        }

        LoggerVlb::log($mn, "count(retArray)=" . count($retArray));
        LoggerVlb::logEnd($mn, $st);
        return $retArray;
    }

    public static function CreateRowData($eMail, $password) {
        $MN = "user:VlbUser.CreateRowData()";
        LoggerVlb::logBegin($MN);

        $rowData = new VlbUser();

        $rowData->setEMail($eMail);
        $rowData->setPassword($password);
        $rowData->setRole(0);
        $rowData->setIsReceiveEMails(1);
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $rowData->setIpAddress($ipAddress);

        $rowData->save();
        LoggerVlb::log($MN, "after save.");
        logEnd($MN);
        return $rowData;
    }

    public function save() {
        $mn = "user:VlbUser.save()";
        LoggerVlb::logBegin($mn);

        LoggerVlb::log($mn, "is_object(this)=" . is_object($this));
        LoggerVlb::log($mn, "ID=" . $this->getId());
        try {
            if (is_object($this)) {
                LoggerVlb::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {

                    LoggerVlb::log($mn, "Insert ");
                    $strSQL = "INSERT INTO " . VlbUser::TABLE_NAME . " (" . VlbUser::getAllColumnsNoIdSQL() . ") ";
                    $strSQL .= " VALUES( ?, ?, ?, ?, ?, ?, ?)";

                    $bound_params_r = array("sssiiss",
                        (($this->getEMail() == null) ? null : $this->getEMail()),
                        (($this->getPassword() == null) ? null : $this->getPassword()),
                        (($this->getName() == null) ? null : $this->getName()),
                        ($this->getRole()),
                        ($this->isReceiveEMails()),
                        (($this->getIpAddress() == null) ? null : $this->getIpAddress()),
                        (($this->getLastLogged() == null) ? null : $this->getLastLogged()->format("Y-m-d H:i:s"))
                    );
                    $conn = ConnectionVlb::dbConnect();
                    $logModel = LoggerVlb::currLogger()->getModule($MN);
                    $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);

                    LoggerVlb::log("$mn", "id=" . $id);
                    $this->LoadById($id);
                } else {
                    LoggerVlb::log($mn, "Update ");
                    //$lngID = $this->getId();
                    $strSQL = "UPDATE " . VlbUser::TABLE_NAME;
                    $strSQL .= " SET " . VlbUser::COL_NAME_EMAIL . "=?, ";
                    $strSQL .= VlbUser::COL_NAME_PSW . "=?, ";
                    $strSQL .= VlbUser::COL_NAME_NAME . "=?, ";
                    $strSQL .= VlbUser::COL_NAME_ROLE . "=?, ";
                    $strSQL .= VlbUser::COL_NAME_IS_RECEIVE_EMAILS . "=?, ";
                    $strSQL .= VlbUser::COL_NAME_IP_ADDRESS . "=?, ";
                    $strSQL .= VlbUser::COL_NAME_LAST_LOGED . "=? ";

                    $strSQL .= " WHERE " . VlbUser::COL_NAME_ID . "=? ";

                    $bound_params_r = array("sssiissi",
                        (($this->getEMail() == null) ? null : $this->getEMail()),
                        (($this->getPassword() == null) ? null : $this->getPassword()),
                        (($this->getName() == null) ? null : $this->getName()),
                        ($this->getRole()),
                        ($this->isReceiveEMails()),
                        (($this->getIpAddress() == null) ? null : $this->getIpAddress()),
                        (($this->getLastLogged() == null) ? null : $this->getLastLogged()->format("Y-m-d H:i:s")),
                        $this->getId()
                    );


                    $conn = ConnectionVlb::dbConnect();
                    $logModel = LoggerVlb::currLogger()->getModule($MN);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    LoggerVlb::log($mn, "affectedRows=" . $affectedRows);

                    //$this->LoadById($this->getId());
                }
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn, $ex);
        }
        LoggerVlb::logEnd($mn);
        return $this;
    }

    /**
     *
     * @param <type> $result 
     */
    public function loadFromArray($result) {
        $MN = "user:VlbUser.loadFromArray()";
        LoggerVlb::logBegin($MN);
        try {

            if (isset($result) && count($result) > 0) {

                $this->setId($result[VlbUser::COL_NAME_ID]);

                $this->setEMail($result[VlbUser::COL_NAME_EMAIL]);
                $this->setName($result[VlbUser::COL_NAME_NAME]);
                $this->setPassword($result[VlbUser::COL_NAME_PSW]);
                $this->setRole($result[VlbUser::COL_NAME_ROLE]);

                $this->setIsReceiveEMails($result[VlbUser::COL_NAME_IS_RECEIVE_EMAILS]);

                $this->setIpAddress($result[VlbUser::COL_NAME_IP_ADDRESS]);

                $this->setLastLogged($result[VlbUser::COL_NAME_LAST_LOGED]);
                $this->setUpdated($result[VlbUser::COL_NAME_UPDATED]);

                LoggerVlb::log($MN, "id " . $this->getId());
            }
        } catch (Exception $ex) {
            LoggerVlb::log($MN, "Error id " . $this->getId());
            logError($MN, $ex);
        }
        LoggerVlb::logEnd($MN);
    }

    public function loadFromPosArray($result) {
        $MN = "user:VlbUser.loadFromPosArray()";
        LoggerVlb::logBegin($MN);
        if (isset($result) && count($result) > 0) {
            $this->setId($result[VlbUser::COL_IXD_ID]);
            $this->setEMail($result[VlbUser::COL_IDX_EMAIL]);
            $this->setName($result[VlbUser::COL_IDX_NAME]);
            $this->setPassword($result[VlbUser::COL_IDX_PSW]);
            $this->setRole($result[VlbUser::COL_IDX_ROLE]);

            $this->setIsReceiveEMails($result[VlbUser::COL_IDX_IS_RECEIVE_EMAILS]);
            $this->setIpAddress($result[VlbUser::COL_IDX_IP_ADDRESS]);
            $this->setLastLogged($result[VlbUser::COL_IDX_LAST_LOGED]);
            $this->setUpdated($result[VlbUser::COL_IDX_UPDATED]);
        }
        LoggerVlb::logEnd($MN);
    }

    public function toString() {
        $retValue = $this->toJson();

        return $retValue;
    }

    public function toJson() {
        return json_encode($this);
    }

    /**
     * ***************************************************************************
     * Getters and Setters Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters  Declarations">

    public function getId() {
        return $this->DT_RowId;
    }

    public function setId($DT_RowId) {
        $this->DT_RowId = $DT_RowId;
    }

    public function getName() {
        $retValue = $this->name;
//        if(isset($this->eMail))
//        {
//           $valueArr = explode( '@', $this->eMail ,2);
//           if(sizeof($valueArr)>0)
//            $retValue  = $valueArr[0];
//        }
        return $retValue;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getEMail() {
        return $this->eMail;
    }

    public function setEMail($eMail) {
        $this->eMail = $eMail;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getRole() {
        return $this->role;
    }

    public function getRoleName() {
        $retValue = "User";
        if ($this->role === 1)
            $retValue = "Administrator";

        return $retValue;
    }

    public function setRole($role) {
        $this->role = $role;
    }

    public function isAdmin() {
        if ($this->role === 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getIsReceiveEMails() {
        return $this->isReceiveEMails;
    }

    public function setIsReceiveEMails($isReceiveEMails) {
        $this->isReceiveEMails = $isReceiveEMails;
    }

    public function isReceiveEMails() {
        if ($this->isReceiveEMails > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getIpAddress() {
        return $this->ipAddress;
    }

    public function setIpAddress($ipAddress) {
        $this->ipAddress = $ipAddress;
    }

    public function getLastLogged() {
        $retValue = null;
        if (isset($this->lastLogged) && $this->lastLogged != null) {
            if (!is_object($this->lastLogged))
                $retValue = new DateTime($this->lastLogged);
            else
                $retValue = $this->lastLogged;

            LoggerVlb::log("getLastLogged()", "retValue=", getDateStr($retValue));
        } else
            $retValue = new DateTime();
        return $retValue;
    }

    public function getLastLoggedDbStr() {
        $retValue = null;
        if (isset($this->lastLogged) && $this->lastLogged != null) {
            if (!is_object($this->lastLogged)) {
                $value = new DateTime($this->lastLogged);
                $retValue = $value->format("Y-m-d H:i:s");
            } else
                $retValue = $this->lastLogged->format("Y-m-d H:i:s");

            LoggerVlb::log("getLastLogged()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setLastLogged($lastLogged) {
        if (isset($lastLogged) && $lastLogged != null) {
            if (is_object($lastLogged))
                $this->lastLogged = $data;
            else
                $this->lastLogged = new DateTime($lastLogged);
        }
    }

    public function getUpdated() {
        $retValue = null;
        if (isset($this->updated) && $this->updated != null) {
            if (!is_object($this->updated))
                $retValue = new DateTime($this->updated);
            else
                $retValue = $this->updated;

            LoggerVlb::log("getUpdated()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setUpdated($updated) {
        if (isset($updated) && $updated != null) {
            if (is_object($updated))
                $this->updated = $updated;
            else
                $this->updated = new DateTime($updated);
        }
    }

    // </editor-fold>

    /*     * **************************************************************************
     * Parameters Declarations
     * ***************************************************************************
     */

// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">

    public $DT_RowId;
    public $eMail;
    public $password;
    public $name;
    public $role = 0; //2 predefined roles 0-user, 1 admin, 2 editor
    public $isReceiveEMails = null;
    public $ipAddress;
    public $lastLogged0;
    public $updated;

// </editor-fold>

    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME = "vlb_user";
    const COL_NAME_ID = "user_id";
    const COL_NAME_EMAIL = "e_mail";
    const COL_NAME_PSW = "password";
    const COL_NAME_NAME = "user_name";
    const COL_NAME_ROLE = "user_role";
    const COL_NAME_IS_RECEIVE_EMAILS = "is_receive_emails";
    const COL_NAME_IP_ADDRESS = "ip_address";
    const COL_NAME_LAST_LOGED = "adate";
    const COL_NAME_UPDATED = "udate";
    const COL_IXD_ID = 0;
    const COL_IDX_EMAIL = 1;
    const COL_IDX_PSW = 2;
    const COL_IDX_NAME = 3;
    const COL_IDX_ROLE = 4;
    const COL_IDX_IS_RECEIVE_EMAILS = 5;
    const COL_IDX_IP_ADDRESS = 6;
    const COL_IDX_LAST_LOGED = 7;
    const COL_IDX_UPDATED = 8;

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . VlbUser::TABLE_NAME . "." . VlbUser::COL_NAME_ID . ", " .
                VlbUser::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " . VlbUser::TABLE_NAME . "." . VlbUser::COL_NAME_EMAIL . ", " .
                VlbUser::TABLE_NAME . "." . VlbUser::COL_NAME_PSW . ", " .
                VlbUser::TABLE_NAME . "." . VlbUser::COL_NAME_NAME . ", " .
                VlbUser::TABLE_NAME . "." . VlbUser::COL_NAME_ROLE . ", " .
                VlbUser::TABLE_NAME . "." . VlbUser::COL_NAME_IS_RECEIVE_EMAILS . ", " .
                VlbUser::TABLE_NAME . "." . VlbUser::COL_NAME_IP_ADDRESS . ", " .
                VlbUser::TABLE_NAME . "." . VlbUser::COL_NAME_LAST_LOGED . ", " .
                VlbUser::TABLE_NAME . "." . VlbUser::COL_NAME_UPDATED;
    }

    public static function getArrayColumns() {
        return array(VlbUser::COL_NAME_ID,
            VlbUser::COL_NAME_EMAIL,
            VlbUser::COL_NAME_PSW,
            VlbUser::COL_NAME_NAME,
            VlbUser::COL_NAME_ROLE,
            VlbUser::COL_NAME_IS_RECEIVE_EMAILS,
            VlbUser::COL_NAME_IP_ADDRESS,
            VlbUser::COL_NAME_LAST_LOGED,
            VlbUser::COL_NAME_UPDATED);
    }

    // </editor-fold>
}
