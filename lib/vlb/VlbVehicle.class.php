<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Apr 12, 2018 
 *  Filename            : VlbVehicle.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of VlbVehicle
 *
 * @author IZIordanov
 */
class VlbVehicle {
    
    // <editor-fold defaultstate="collapsed" desc="Load Data Methods">
    
    public static function LoadByUserId($userId) {
        $mn = "VlbVehicle.Create(" . $userId . ")";
        LoggerVlb::logBegin($mn);
        if (isset($userId)) {
            $sql = "SELECT " . VlbVehicle::getAllColumnsSQL() .
                    " FROM " . VlbVehicle::TABLE_NAME . " " .
                    " WHERE " . VlbVehicle::COL_NAME_USER_ID . " = ? ".
                    " ORDER BY " . VlbVehicle::COL_NAME_PURCHASE_DATE . " DESC";

            $bound_params_r = array('i', $userId);
            $conn = ConnectionVlb::dbConnect();
            $logModel = LoggerVlb::currLogger()->getModule($mn);
            $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            LoggerVlb::log($mn, "count(result_r)=" . count($result_r));
            $retArray = array();
            for ($index = 0, $max_count = sizeof($result_r); $index < $max_count; $index++) {
                $result = $result_r[$index];
                $item = new VlbVehicle();
                $item->loadFromArray($result);
                $retArray[] = $item;
            }
        }

        LoggerVlb::log($mn, "count(retArray)=" . count($retArray));
        LoggerVlb::logEnd($mn);
        return $retArray;
    }

    //</editor-fold>
     
    // <editor-fold defaultstate="collapsed" desc="Create, Update Methods">
    
    public static function Create($userId, $model, $millageKmStart) {
        $mn = "VlbVehicle.Create(" . $userId . ", " . $model . ", " . $millageKmStart . ")";
        LoggerVlb::logBegin($mn);

        $rowData = new VlbVehicle();
        $today = date("m.d.y");
        $currencyLbl = 'bg-BG';
        $rowData->setUserId($userId);
        $rowData->setName($model);
        $rowData->setModel($model);
        $rowData->setMilageKmStart($millageKmStart);
        $rowData->setMilageKmCurrent($millageKmStart);
        $rowData->setPurchaseDate($today);
        $rowData->setCurrencyLbl($currencyLbl);
        $rowData->save();

        LoggerVlb::logEnd($mn);
        return $rowData;
    }

    public static function DeleteById($id) {
        $mn = "VlbVehicle.DeleteById(" . $id . ")";
        LoggerVlb::logBegin($mn);
        LoggerVlb::log($mn, "id = " . $id);

        $sql = "DELETE " .
                " FROM " . VlbVehicle::TABLE_NAME . " " .
                " WHERE " . VlbVehicle::COL_NAME_ID . "=?";
        $bound_params_r = array("i", $id);
        $conn = ConnectionVlb::dbConnect();
        $logModel = LoggerVlb::currLogger()->getModule($mn);
        $id = $conn->preparedDelete($sql, $bound_params_r, $logModel);

        LoggerVlb::logEnd($MN);
    }

    public function loadById($id) {
        $mn = "VlbVehicle.loadById(" . $id . ")";
        LoggerVlb::logBegin($mn);
        LoggerVlb::log($mn, "id = " . $id);

        $this->setId($id);
        $sql = "SELECT " . VlbVehicle::getAllColumnsSQL() .
                " FROM " . VlbVehicle::TABLE_NAME . " " .
                " WHERE " . VlbVehicle::COL_NAME_ID . "=?";
        $bound_params_r = array('i', $id);
        $conn = ConnectionVlb::dbConnect();
        $logModel = LoggerVlb::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        LoggerVlb::log($mn, "count(result_r)=" . count($result_r));
        $data = null;
        if (count($result_r) > 0) {
            $data = $result_r[0];
            //LoggerVlb::log($mn, "count(data)=".count($data));
        }
        //LoggerVlb::log("$MN", "ret Value=".prArr($data) );
        if (isset($data) && count($data) > 0) {
            $this->loadFromArray($data);
        }
        LoggerVlb::log($mn, "viehicle: " . $this->toJSON());

        LoggerVlb::logEnd($mn);
    }

    public function save() {
        $mn = "VlbVehicle.save()";
        LoggerVlb::logBegin($mn);
        LoggerVlb::log($mn, "is_object(this)=" . is_object($this));
        LoggerVlb::log($mn, "id=" . $this->getId());

        if (is_object($this)) {

            if ($this->getId() === null || $this->getId() === "") {
                LoggerVlb::log($mn, "Insert=");

                $strSQL = "INSERT INTO " . VlbVehicle::TABLE_NAME . " (" . VlbVehicle::getAllColumnsNoIdSQL() . ") ";
                $strSQL .= " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                $bound_params_r = ["isssiississ",
                    (($this->getUserId() == null) ? null : $this->getUserId()),
                    (($this->getName() == null) ? null : $this->getName()),
                    (($this->getFirstRegistration() == null) ? null : $this->getFirstRegistration()->format("Y-m-d")),
                    (($this->getModel() == null) ? null : $this->getModel()),
                    (($this->getMilageKmStart() == null) ? null : $this->getMilageKmStart()),
                    (($this->getMilageKmCurrent() == null) ? null : $this->getMilageKmCurrent()),
                    (($this->getImage() == null) ? null : $this->getImage()),
                    (($this->getPurchaseDate() == null) ? null : $this->getPurchaseDate()->format("Y-m-d H:i:s")),
                    (($this->getTrackInMi() == null) ? 0 : $this->getTrackInMi()),
                    (($this->getCurrencyLbl() == null) ? 0 : $this->getCurrencyLbl()),
                    (($this->getSaleDate() == null) ? null : $this->getSaleDate()->format("Y-m-d H:i:s"))
                ];
                $conn = ConnectionVlb::dbConnect();
                $logModel = LoggerVlb::currLogger()->getModule($MN);
                $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
                LoggerVlb::log($mn, "id=" . $id);

                $this->LoadById($id);
            } else {
                LoggerVlb::log($mn, "Update");

                $strSQL = "UPDATE " . VlbVehicle::TABLE_NAME;
                $strSQL .= " SET " . VlbVehicle::COL_NAME_USER_ID . "=?, ";
                $strSQL .= VlbVehicle::COL_NAME_NAME . "=?, ";
                $strSQL .= VlbVehicle::COL_NAME_FIRST_REG . "=?, ";
                $strSQL .= VlbVehicle::COL_NAME_MODEL . "=?, ";
                $strSQL .= VlbVehicle::COL_NAME_MILAGE_KM_START . "=?, ";
                $strSQL .= VlbVehicle::COL_NAME_MILAGE_KM_CURRENT . "=?, ";
                $strSQL .= VlbVehicle::COL_NAME_IMAGE . "=?, ";
                $strSQL .= VlbVehicle::COL_NAME_PURCHASE_DATE . "=?, ";
                $strSQL .= VlbVehicle::COL_NAME_TRACK_IN_MI . "=?, ";
                $strSQL .= VlbVehicle::COL_NAME_CURRENCY_LBL . "=?, ";
                $strSQL .= VlbVehicle::COL_NAME_SALE_DATE . "=? ";
                $strSQL .= " WHERE " . VlbVehicle::COL_NAME_ID . " = ? ";

                $bound_params_r = ["isssiississi",
                    (($this->getUserId() == null) ? null : $this->getUserId()),
                    (($this->getName() == null) ? null : $this->getName()),
                    (($this->getFirstRegistration() == null) ? null : $this->getFirstRegistration()->format("Y-m-d")),
                    (($this->getModel() == null) ? null : $this->getModel()),
                    (($this->getMilageKmStart() == null) ? null : $this->getMilageKmStart()),
                    (($this->getMilageKmCurrent() == null) ? null : $this->getMilageKmCurrent()),
                    (($this->getImage() == null) ? null : $this->getImage()),
                    (($this->getPurchaseDate() == null) ? null : $this->getPurchaseDate()->format("Y-m-d H:i:s")),
                    (($this->getTrackInMi() == null) ? 0 : $this->getTrackInMi()),
                    (($this->getCurrencyLbl() == null) ? 0 : $this->getCurrencyLbl()),
                    (($this->getSaleDate() == null) ? null : $this->getSaleDate()->format("Y-m-d H:i:s")),
                    $this->getId()
                ];


                $conn = ConnectionVlb::dbConnect();
                $logModel = LoggerVlb::currLogger()->getModule($MN);
                $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                LoggerVlb::log($mn, "affectedRows=" . $affectedRows);

                $this->LoadById($this->getId());
            }
        }

        LoggerVlb::logEnd($mn);
        return $this;
    }

    //</editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Load Arrays">

    public function loadFromArray($result) {
        $mn = "VlbVehicle.loadFromArray(" . sizeof($result) . ")";
        LoggerVlb::logBegin($mn);
        try {
            if (!$result == null) {
                $this->setId($result[VlbVehicle::COL_NAME_ID]);

                $this->setUserId($result[VlbVehicle::COL_NAME_USER_ID]);
                $this->setName($result[VlbVehicle::COL_NAME_NAME]);
                $this->setFirstRegistration($result[VlbVehicle::COL_NAME_FIRST_REG]);
                $this->setModel($result[VlbVehicle::COL_NAME_MODEL]);
                $this->setMilageKmStart($result[VlbVehicle::COL_NAME_MILAGE_KM_START]);
                $this->setMilageKmCurrent($result[VlbVehicle::COL_NAME_MILAGE_KM_CURRENT]);
                $this->setImage($result[VlbVehicle::COL_NAME_IMAGE]);
                $this->setPurchaseDate($result[VlbVehicle::COL_NAME_PURCHASE_DATE]);
                $this->setSaleDate($result[VlbVehicle::COL_NAME_SALE_DATE]);
                $this->setTrackInMi($result[VlbVehicle::COL_NAME_TRACK_IN_MI]);
                $this->setCurrencyLbl($result[VlbVehicle::COL_NAME_CURRENCY_LBL]);
                LoggerVlb::log($mn, "Vehicle=" . $this->toJSON());
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn, $ex);
        }
        LoggerVlb::logEnd($mn);
    }

    public function loadFromPosArray($result) {
        $mn = "VlbVehicle.loadFromPosArray(" . sizeof($result) . ")";
        LoggerVlb::logBegin($mn);
        try {
            if (!$result == null) {

                $this->setId($result[VlbVehicle::COL_IDX_ID]);
                $this->setUserId($result[VlbVehicle::COL_IDX_USER_ID]);
                $this->setName($result[VlbVehicle::COL_IDX_NAME]);
                $this->setFirstRegistration($result[VlbVehicle::COL_IDX_FIRST_REG]);
                $this->setModel($result[VlbVehicle::COL_IDX_MODEL]);
                $this->setMilageKmStart($result[VlbVehicle::COL_IDX_MILAGE_KM_START]);
                $this->setMilageKmCurrent($result[VlbVehicle::COL_IDX_MILAGE_KM_CURRENT]);
                $this->setImage($result[VlbVehicle::COL_IDX_IMAGE]);
                $this->setPurchaseDate($result[VlbVehicle::COL_IDX_PURCHASE_DATE]);
                $this->setSaleDate($result[VlbVehicle::COL_IDX_SALE_DATE]);
                $this->setTrackInMi($result[VlbVehicle::COL_IDX_TRACK_IN_MI]);
                $this->setCurrencyLbl($result[VlbVehicle::COL_IDX_CURRENCY_LBL]);
                LoggerVlb::log($mn, "Vehicle=" . $this->toJSON());
            }
        } catch (Exception $ex) {
            LoggerVlb::logError($mn, $ex);
        }
        LoggerVlb::logEnd($mn);
    }

    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Additional Parameters">
    
    /**
     * toJSON
     * @return type
     */
    public function toJSON() {
        return json_encode($this);
    }
    
    /**
     * Used to store value in DB
     * @param type $millage
     * @return type
     */
    public function getMillageMeasure() {
        if($this->getTrackInMi()==1){
            return "mi";
        }
        else{
            return "km";
        }
    }
   
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters  Declarations">
    
    public function getId() {
        return $this->vehicleId;
    }

    public function setId($Id) {
        $this->vehicleId = $Id;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function setUserId($Id) {
        $this->user_id = $Id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($data) {
        $this->name = $data;
    }
    
    public function getFirstRegistration() {
        $retValue = null;
        if (isset($this->first_reg) && $this->first_reg != null) {
            if (!is_object($this->first_reg)) {
                $retValue = new DateTime($this->first_reg);
            } else {
                $retValue = $this->first_reg;
            }
        }
        return $retValue;
    }

    public function setFirstRegistration($data) {
         if (isset($data) && $data != null) {
            if (is_object($data))
                $this->first_reg = $data;
            else
                $this->first_reg = new DateTime($data);
        }
    }

    
    public function getModel() {
        return $this->model;
    }

    public function setModel($model) {
        $this->model = $model;
    }

    public function getMilageKmStart() {
        return $this->milage_km_start;
    }

    public function setMilageKmStart($milage_km_start) {
        $this->milage_km_start = $milage_km_start;
    }
    
    public function getMilageKmCurrent() {
        return $this->milage_km_current;
    }

    public function setMilageKmCurrent($milage_km_current) {
        $this->milage_km_current = $milage_km_current;
    }
    
    public function getImage() {
        return $this->image;
    }

    public function setImage($image) {
        $this->image = $image;
    }
    
    public function getPurchaseDate() {
        $retValue = null;
        if (isset($this->purchase_date) && $this->purchase_date != null) {
            if (!is_object($this->purchase_date)) {
                $retValue = new DateTime($this->purchase_date);
            } else {
                $retValue = $this->purchase_date;
            }
        }
        return $retValue;
    }

    public function setPurchaseDate($purchase_date) {
        if (isset($purchase_date) && $purchase_date != null) {
            if (is_object($purchase_date))
                $this->purchase_date = $purchase_date;
            else
                $this->purchase_date = new DateTime($purchase_date);
        }
    }
    
    public function getSaleDate() {
        $retValue = null;
        if (isset($this->sale_date) && $this->sale_date != null) {
            if (!is_object($this->sale_date)) {
                $retValue = new DateTime($this->sale_date);
            } else {
                $retValue = $this->sale_date;
            }

            //logDebug("getSaleDate()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setSaleDate($sale_date) {
        if (isset($sale_date) && $sale_date != null) {
            if (is_object($sale_date))
                $this->sale_date = $sale_date;
            else
                $this->sale_date = new DateTime($sale_date);
        }
    }

    public function getTrackInMi() {
       
        return $this->track_in_mi;
    }

    public function setTrackInMi($track_in_mi) {
        $this->track_in_mi = $track_in_mi;
    }
    
      public function getCurrencyLbl() {
        return $this->currency_lbl;
    }

    public function setCurrencyLbl($currency_lbl) {
        $this->currency_lbl = $currency_lbl;
    }
    
    // </editor-fold>
    
   
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">

    public $vehicleId;
    public $user_id;
    public $name;
    public $first_reg;
    public $model;
    public $milage_km_start;
    public $milage_km_current;
    public $image;
    public $purchase_date;
    public $track_in_mi=0;
    public $currency_lbl="bg_BG";
    public $sale_date;

// </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="Constants">

    const TABLE_NAME                 = "iordanov_vlb.vlb_vehicle";
    const COL_NAME_ID                = "vehicle_id";
    const COL_NAME_USER_ID           = "user_id";
    const COL_NAME_NAME              = "vehicle_name";
    const COL_NAME_FIRST_REG         = "vehicle_first_reg";
    const COL_NAME_MODEL             = "vehicle_model";
    const COL_NAME_MILAGE_KM_START   = "milage_km_start";
    const COL_NAME_MILAGE_KM_CURRENT = "milage_km_curr";
    const COL_NAME_IMAGE             = "vehicle_image";
    const COL_NAME_PURCHASE_DATE     = "purchase_date";
    const COL_NAME_TRACK_IN_MI       = "track_in_mi";
    const COL_NAME_CURRENCY_LBL      = "currency_lbl";
    const COL_NAME_SALE_DATE         = "sale_date";
    
    const COL_IDX_ID                = 0;
    const COL_IDX_USER_ID           = 1;
    const COL_IDX_NAME              = 2;
    const COL_IDX_FIRST_REG         = 3;
    const COL_IDX_MODEL             = 4;
    const COL_IDX_MILAGE_KM_START   = 5;
    const COL_IDX_MILAGE_KM_CURRENT = 6;
    const COL_IDX_IMAGE             = 7;
    const COL_IDX_PURCHASE_DATE     = 8;
    const COL_IDX_TRACK_IN_MI       = 9;
    const COL_IDX_CURRENCY_LBL      = 10;
    const COL_IDX_SALE_DATE         = 11;
    
    
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Columns List">

    public static function getAllColumnsSQL() {
        return " " . VlbVehicle::COL_NAME_ID . ", " .
                VlbVehicle::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " .  VlbVehicle::COL_NAME_USER_ID . ", " .
                VlbVehicle::COL_NAME_NAME . ", " .
                VlbVehicle::COL_NAME_FIRST_REG . ", " .
                VlbVehicle::COL_NAME_MODEL . ", " .
                VlbVehicle::COL_NAME_MILAGE_KM_START . ", " .
                VlbVehicle::COL_NAME_MILAGE_KM_CURRENT . ", " .
                VlbVehicle::COL_NAME_IMAGE . ", " .
                VlbVehicle::COL_NAME_PURCHASE_DATE . ", " .
                VlbVehicle::COL_NAME_TRACK_IN_MI . ", " .
                VlbVehicle::COL_NAME_CURRENCY_LBL . ", " .
                VlbVehicle::COL_NAME_SALE_DATE;

    }

    public static function getArrayColumns() {
        return array(VlbVehicle::COL_NAME_ID,
            VlbVehicle::COL_NAME_USER_ID,
            VlbVehicle::COL_NAME_NAME,
            VlbVehicle::COL_NAME_FIRST_REG,
            VlbVehicle::COL_NAME_MODEL,
            VlbVehicle::COL_NAME_MILAGE_KM_START,
            VlbVehicle::COL_NAME_MILAGE_KM_CURRENT,
            VlbVehicle::COL_NAME_IMAGE,
            VlbVehicle::COL_NAME_PURCHASE_DATE,
            VlbVehicle::COL_NAME_TRACK_IN_MI,
            VlbVehicle::COL_NAME_CURRENCY_LBL,
             VlbVehicle::COL_NAME_SALE_DATE);
    }

    // </editor-fold>
}
