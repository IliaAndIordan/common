<?php 
/******************************** HEAD_BEG ************************************
*
* Project                	: isgt
* Module                        : user
* Responsible for module 	: IordIord
*
* Filename               	: AmsUser.class.php
*
* Database System        	: ORCL, MySQL
* Created from                  : IordIord
* Date Creation			: 19.12.2018
*------------------------------------------------------------------------------
*                        Description
*------------------------------------------------------------------------------
* @TODO Insert some description.
*
*------------------------------------------------------------------------------
*                        History
*------------------------------------------------------------------------------
* HISTORY:
* <br>--- $Log: AmsUser.class.php,v $
* <br>---
* <br>---
*
********************************* HEAD_END ************************************
*/


/**
 * Description of AmsUser class
 *
 * @author IordIord
 */
class AmsUser {

/**
 * ***************************************************************************
 * Methods Declarations
 * ***************************************************************************
 */
    /**
     *
     * @param <type> $id 
     */
    public static function loadById($id) {
        $mn = "user:AmsUser.loadById()";
         AmsLogger::logBegin($mn);
        AmsLogger::log($mn, "id = ".$id);
        $this->setDT_RowId($id);
        $sql = "SELECT ".AmsUser::getAllColumnsSQL(). ", " . AmsUser::COL_NAME_UPDATED.
                " FROM ".AmsUser::TABLE_NAME." ".
                " WHERE ".AmsUser::COL_NAME_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        AmsLogger::log($mn, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //AmsLogger::log($mn, "count(data)=".count($data));
        }
        //AmsLogger::log("$mn", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        AmsLogger::log($mn, "AmsUser is ".$this->toString());

        AmsLogger::logEnd($mn);
    }
    
    public static function login($user_email, $user_password)
    {
        $mn="user:AmsUser.login()";
         AmsLogger::logBegin($mn);
        $out = null;
        $loginName = validate_search_string($user_email);
        $password = validate_search_string($user_password);

        //$user_email = base64_encode($loginName);
        //$user_password = md5($password);

        $sqlStr ="SELECT ".AmsUser::COL_NAME_ID." FROM ".AmsUser::TABLE_NAME.
                    " WHERE ".AmsUser::COL_NAME_EMAIL."=? ".
                    " AND ".AmsUser::COL_NAME_PSW."=?";
        $bound_params_r = array('ss', $user_email, $user_password);

        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        if(count($result_r)>0)
        {
           
            AmsLogger::log($mn, prArr($result_r[0]));
            $id = $result_r[0][AmsUser::COL_NAME_ID];
            AmsLogger::log($mn, "Found userId".$id." for E-Mail ".$user_email."!");
            $ipAddress = $_SERVER['REMOTE_ADDR']; 
            
            if ($id>-1)
            {
                $out = new AmsUser();
                $out->loadById($id);
                $out->setIpAddress($ipAddress);
                $out->setLastLogged(CurrenDateTime());
                $out->save();
            }
        }
        
        AmsLogger::logEnd($mn);
        return $out;
    }
    
    /**
     *
     * @param <type> $email
     * @return <type> 
     */
    public static function isExistEMail ($email) {
        $mn="user.AmsUser:isExistEMail()";
         AmsLogger::logBegin($mn);
        $user_email = validate_search_string($email);

        $sqlStr ="SELECT count(*) AS Rows FROM ".
            AmsUser::TABLE_NAME." WHERE ".AmsUser::COL_NAME_EMAIL."=?";

        $bound_params_r = array('s', $user_email);
        AmsLogger::log($mn,$sqlStr);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        AmsLogger::log($mn, prArr($result_r[0]));
        $nrRows = $result_r[0]["Rows"];
        AmsLogger::log($mn, "Found ".$nrRows." users with E-Mail ".$user_email."!");
        AmsLogger::logEnd($mn);
        return ($nrRows>0?TRUE:FALSE);
    }

    public static function forgotenPassword($email, $conf)
    {
        $mn="user.AmsUser:forgotenPassword()";
        $st = AmsLogger::logBegin($mn);
        $id = -1;
        $retValue = false;
        $user_email = validate_search_string($email);
        if(AmsUser::isExistEMail($user_email)>0)
        {
            $sqlStr ="SELECT ".AmsUser::COL_NAME_ID." FROM ".
                AmsUser::TABLE_NAME." WHERE ".AmsUser::COL_NAME_EMAIL."=?";

            $bound_params_r = array('s', $user_email);
            AmsLogger::log($mn,$sqlStr);
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
            AmsLogger::log($mn, prArr($result_r[0]));
            $id = $result_r[0][AmsUser::COL_NAME_ID];
            AmsLogger::log($mn, "Found ".$id." user ID with E-Mail ".$user_email."!");
            if($id>0)
            {
                $user = new AmsUser();
                $user->loadById($id);
                $retValue = sendMailRegistration($user, $conf);
            }
        }
        AmsLogger::logEnd($mn);
        return $retValue;
    }
    /**
     *
     * @param <type> $id
     */
    public static function deleteById($id) {
        $mn = "user:AmsUser.deleteById()";
         AmsLogger::logBegin($mn);
        AmsLogger::log($mn, "id = ".$id);
        //$this->setId($id);
        $sql = "DELETE ".
                " FROM ".AmsUser::TABLE_NAME." ".
                " WHERE ".AmsUser::COL_NAME_ID."=?";
        $bound_params_r = array("i",$id);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $id = $conn->preparedDelete($sql, $bound_params_r, $logModel);

        AmsLogger::logEnd($mn);
    }
    
    public static function loadAll()
    {
        $mn = "user:AmsUser.loadAll()";
        $st = AmsLogger::logBegin($mn);


        $sql = "SELECT ".AmsUser::getAllColumnsSQL() . ", " . AmsUser::COL_NAME_UPDATED.
                " FROM ".AmsUser::TABLE_NAME." ";

        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $data = $conn->dbExecuteSQL($sql, $logModel);
        AmsLogger::log($mn, "count(data)=".count($data));
        $retArray = array();
        for ($index = 0, $max_count = sizeof( $data ); $index < $max_count; $index++)
        {
           $result = $data[$index];
           $item = new AmsUser();
           $item->loadFromPosArray($result);
           $retArray[] = $item;
           AmsLogger::log($mn, "Add Item =".$item->toString());
        }

        AmsLogger::log($mn, "count(retArray)=".count($retArray));
        AmsLogger::logEnd($mn);
        return $retArray;
    }
    
    public static function CreateRowData($eMail, $password) {
        $mn = "user:AmsUser.CreateRowData()";
         AmsLogger::logBegin($mn);

        $rowData = new AmsUser();
        
        $rowData->setEMail($eMail);
        $rowData->setPassword($password);
        $rowData->setName($rowData->getNameFromEMail());
        $rowData->setRole(0);
        $rowData->setIsReceiveEMails(1);
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $rowData->setIpAddress($ipAddress);
        
        $rowData->save();
        AmsLogger::log($mn, "after save.");
        AmsLogger::logEnd($mn);
        return $rowData;
    }

    public function save() {
        $mn = "user:AmsUser.save()";
        $st = AmsLogger::logBegin($mn);

        AmsLogger::log($mn, "is_object(this)=" . is_object($this));
        AmsLogger::log($mn, "ID=" . $this->getId());
        try {
            if (is_object($this)) {
                AmsLogger::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {

                    AmsLogger::log($mn, "Insert ");
                    $strSQL = "INSERT INTO " . AmsUser::TABLE_NAME . " (" . AmsUser::getAllColumnsNoIdSQL() . ") ";
                    $strSQL .=" VALUES( ?, ?, ?, ?, ?, ?, ?)";

                    $bound_params_r = array("sssiiss",
                        (($this->getEMail() == null) ? null : $this->getEMail()),
                        (($this->getPassword() == null) ? null : $this->getPassword()),
                        (($this->getName() == null) ? null : $this->getName()),
                        ($this->getRole()),
                        ($this->isReceiveEMails()),
                        (($this->getIpAddress() == null) ? null : $this->getIpAddress()),
                        (($this->getLastLogged() == null) ? null : $this->getLastLogged()->format("Y-m-d H:i:s"))
                    );
                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($mn);
                    $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);

                    AmsLogger::log("$mn", "id=" . $id);
                    $this->LoadById($id);
                } else {
                    AmsLogger::log($mn, "Update ");
                    //$lngID = $this->getId();
                    $strSQL = "UPDATE " . AmsUser::TABLE_NAME;
                    $strSQL .=" SET " . AmsUser::COL_NAME_EMAIL . "=?, ";
                    $strSQL .=AmsUser::COL_NAME_PSW . "=?, ";
                    $strSQL .=AmsUser::COL_NAME_UNNAME . "=?, ";
                    $strSQL .=AmsUser::COL_NAME_ROLE . "=?, ";
                    $strSQL .=AmsUser::COL_NAME_IS_RECEIVE_EMAILS . "=?, ";
                    $strSQL .=AmsUser::COL_NAME_IP_ADDRESS . "=?, ";
                    $strSQL .=AmsUser::COL_NAME_LAST_LOGED . "=? ";

                    $strSQL .=" WHERE " . AmsUser::COL_NAME_ID . "=? ";

                    $bound_params_r = array("sssiissi",
                        (($this->getEMail() == null) ? null : $this->getEMail()),
                        (($this->getPassword() == null) ? null : $this->getPassword()),
                        (($this->getName() == null) ? null : $this->getName()),
                        ($this->getRole()),
                        ($this->isReceiveEMails()),
                        (($this->getIpAddress() == null) ? null : $this->getIpAddress()),
                        (($this->getLastLogged() == null) ? null : $this->getLastLogged()->format("Y-m-d H:i:s")),
                        $this->getId()
                    );


                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($mn);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    AmsLogger::log($mn, "affectedRows=" . $affectedRows);

                    //$this->LoadById($this->getId());
                }
            }
        } catch (Exception $ex) {
            AmsLogger::log($mn, "Error id " . $this->getId());
            AmsLogger::logError($mn, $ex);
        }
        AmsLogger::logEnd($mn);
        return $this;
    }

    /**
     *
     * @param <type> $result 
     */
    public function loadFromArray($result) {
        $mn = "user:AmsUser.loadFromArray()";
         AmsLogger::logBegin($mn);
        try{
            
             if(isset($result) && count($result)>0) {
                 
                $this->setDT_RowId($result[AmsUser::COL_NAME_ID]);
                $this->setEMail($result[AmsUser::COL_NAME_EMAIL]);
                $this->setPassword($result[AmsUser::COL_NAME_PSW]);
                $this->setName($result[AmsUser::COL_NAME_UNNAME]);
                $this->setRole($result[AmsUser::COL_NAME_ROLE]);
                
                $this->setIsReceiveEMails($result[AmsUser::COL_NAME_IS_RECEIVE_EMAILS]);
                $this->setIpAddress($result[AmsUser::COL_NAME_IP_ADDRESS]);
                $this->setLastLogged($result[AmsUser::COL_NAME_LAST_LOGED]);
                $this->setUpdated($result[AmsUser::COL_NAME_UPDATED]);
                
                AmsLogger::log($mn, "id ".$this->getId());
            }
            
        }
        catch(Exception $ex){AmsLogger::log($mn, "Error id ".$this->getId());AmsLogger::logError($mn, $ex);}
        AmsLogger::logEnd($mn);

    }

    public function loadFromPosArray($result) {
        $mn = "user:AmsUser.loadFromPosArray()";
         AmsLogger::logBegin($mn);
        if(isset($result) && count($result)>0){
            $this->setDT_RowId($result[AmsUser::COL_IXD_ID]);
            $this->setEMail($result[AmsUser::COL_IDX_EMAIL]);
            $this->setPassword($result[AmsUser::COL_IDX_PSW]);
            $this->setName($result[AmsUser::COL_IDX_UNAME]);
            $this->setRole($result[AmsUser::COL_IDX_ROLE]);
            
            $this->setIsReceiveEMails($result[AmsUser::COL_IDX_IS_RECEIVE_EMAILS]);
            $this->setIpAddress($result[AmsUser::COL_IDX_IP_ADDRESS]);
            $this->setLastLogged($result[AmsUser::COL_IDX_LAST_LOGED]);
            $this->setUpdated($result[AmsUser::COL_IDX_UPDATED]);
        }
        AmsLogger::logEnd($mn);

    }

    public function toString()
    {
         $retValue = $this->toJSON();
         
        return $retValue;
    }
    
    public function toJSON() {
        return json_encode($this);
    }
    /**
     * ***************************************************************************
     * Getters and Setters Declarations
     * ***************************************************************************
     */
    
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters  Declarations">
    

    public function getId() {
        return $this->userId;
    }
    
    public function setDT_RowId($userId) {
        $this->userId = $userId;
    }

    public function getName() {
        $retValue = $this->userName;
        if(!isset($this->userName))
        {
            $retValue  = $this->getNameFromEMail();
        }
        return $retValue;
    }
    
    public function setName($userName) {
        $this->userName = $userName;
    }
    
    public function getNameFromEMail() {
        $retValue = $this->eMail;
        if(isset($this->eMail))
        {
           $valueArr = explode( '@', $this->eMail ,2);
           if(sizeof($valueArr)>0)
            $retValue  = $valueArr[0];
        }
        return $retValue;
    }
    
    public function getEMail() {
        return $this->eMail;
    }

    public function setEMail($eMail) {
        $this->eMail = $eMail;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getRole() {
        return $this->role;
    }
    
    public function getRoleName() {
        $retValue = "Airline Manager";
        if ($this->role === 1)
            $retValue = "Administrator";
        
        return $retValue;
    }

    public function setRole($role) {
        $this->role = $role;
    }
    
    public function isAdmin() {
        if ($this->role === 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getIsReceiveEMails() {
        return $this->isReceiveEMails;
    }

    public function setIsReceiveEMails($isReceiveEMails) {
        $this->isReceiveEMails = $isReceiveEMails;
    }

    public function isReceiveEMails() {
        if ($this->isReceiveEMails > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getIpAddress() {
        return $this->ipAddress;
    }

    public function setIpAddress($ipAddress) {
        $this->ipAddress = $ipAddress;
    }

    public function getLastLogged() {
        $retValue = null;
        if (isset($this->lastLogged) && $this->lastLogged != null) {
            if (!is_object($this->lastLogged))
                $retValue = new DateTime($this->lastLogged);
            else
                $retValue = $this->lastLogged;

            AmsLogger::log("getLastLogged()", "retValue=", getDateStr($retValue));
        }
        else
            $retValue = new DateTime();
        return $retValue;
    }
    
     public function getLastLoggedDbStr() {
        $retValue = null;
        if (isset($this->lastLogged) && $this->lastLogged != null) {
            if (!is_object($this->lastLogged)){
                $value = new DateTime($this->lastLogged);
                $retValue = $value->format("Y-m-d H:i:s");
            }
            else
                $retValue = $this->lastLogged->format("Y-m-d H:i:s");

            AmsLogger::log("getLastLogged()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setLastLogged($lastLogged) {
         if (isset($lastLogged) && $lastLogged != null) {
            if (is_object($lastLogged))
                $this->lastLogged = $data;
            else
                $this->lastLogged = new DateTime($lastLogged);
        }
        
    }

    public function getUpdated() {
        $retValue = null;
        if (isset($this->updated) && $this->updated != null) {
            if (!is_object($this->updated))
                $retValue = new DateTime($this->updated);
            else
                $retValue = $this->updated;

            AmsLogger::log("getUpdated()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setUpdated($updated) {
        if (isset($data) && $data != null) {
            if (is_object($data))
                $this->updated = $data;
            else
                $this->updated = new DateTime($data);
        }
    }

    // </editor-fold>
    
   /****************************************************************************
   * Parameters Declarations
   * ***************************************************************************
   */
   
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">

    public $userId;
    public $eMail;
    public $password;
    public $role = 0; //2 predefined roles 0-user, 1 admin, 2 editor
    
    public $isReceiveEMails = null;
    public $ipAddress;
    public $lastLogged = 0;
    public $updated = 0;
    public $userName;

// </editor-fold>
    
    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME                = "iordanov_ams_al.ams_user";
    const COL_NAME_ID               = "user_id";
    const COL_NAME_EMAIL            = "e_mail";
    const COL_NAME_PSW              = "password";
    const COL_NAME_ROLE             = "user_role";
    const COL_NAME_IS_RECEIVE_EMAILS = "is_receive_emails";
    const COL_NAME_IP_ADDRESS       = "ip_address";
    const COL_NAME_LAST_LOGED       = "adate";
    const COL_NAME_UPDATED          = "udate";
    const COL_NAME_UNNAME           = "user_name";
    
    const COL_IXD_ID                = 0;
    const COL_IDX_EMAIL             = 1;
    const COL_IDX_PSW               = 2;
    const COL_IDX_UNAME             = 3;
    const COL_IDX_ROLE              = 4;
    const COL_IDX_IS_RECEIVE_EMAILS = 5;
    const COL_IDX_IP_ADDRESS        = 6;
    const COL_IDX_LAST_LOGED        = 7;
    const COL_IDX_UPDATED           = 8;
    

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . AmsUser::TABLE_NAME . "." . AmsUser::COL_NAME_ID . ", " .
                AmsUser::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " . AmsUser::TABLE_NAME . "." . AmsUser::COL_NAME_EMAIL . ", " .
                AmsUser::TABLE_NAME . "." . AmsUser::COL_NAME_PSW . ", " .
                AmsUser::TABLE_NAME . "." . AmsUser::COL_NAME_UNNAME. ", " .
                AmsUser::TABLE_NAME . "." . AmsUser::COL_NAME_ROLE . ", " .
                AmsUser::TABLE_NAME . "." . AmsUser::COL_NAME_IS_RECEIVE_EMAILS . ", " .
                AmsUser::TABLE_NAME . "." . AmsUser::COL_NAME_IP_ADDRESS . ", " .
                AmsUser::TABLE_NAME . "." . AmsUser::COL_NAME_LAST_LOGED;
    }

    public static function getArrayColumns() {
        return array(AmsUser::COL_NAME_ID,
            AmsUser::COL_NAME_EMAIL,
            AmsUser::COL_NAME_PSW,
            AmsUser::COL_NAME_UNNAME,
            AmsUser::COL_NAME_ROLE,
            AmsUser::COL_NAME_IS_RECEIVE_EMAILS,
            AmsUser::COL_NAME_IP_ADDRESS,
            AmsUser::COL_NAME_LAST_LOGED,
            AmsUser::COL_NAME_UPDATED);
    }

    // </editor-fold>
}

