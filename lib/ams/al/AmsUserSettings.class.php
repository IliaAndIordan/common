<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Apr 15, 2019 
 *  Filename          : AmsUserSettings.php
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2019 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of AmsUserSettings
 *
 * @author IZIordanov
 */
class AmsUserSettings {
    
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public function loadById($id) {
        $MN = "user:AmsUserSettings.loadById()";
         AmsLogger::logBegin($MN);
        AmsLogger::log($MN, "id = ".$id);
        $this->setId($id);
        $sql = "SELECT ".AmsUserSettings::getAllColumnsSQL().
                " FROM ".AmsUserSettings::TABLE_NAME." ".
                " WHERE ".AmsUserSettings::COL_NAME_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($MN);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        AmsLogger::log($MN, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //AmsLogger::log($MN, "count(data)=".count($data));
        }
        //AmsLogger::log("$MN", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        AmsLogger::log($MN, "UserSettings is ".$this->toString());

        AmsLogger::logEnd($MN);
    }
    
    public static function CreateRowData($id) {
        $MN = "user:AmsUserSettings.CreateRowData()";
        AmsLogger::logBegin($MN);

        $rowData = new AmsUserSettings();

        $rowData->setId($id);
        $rowData->setDisplayDistanceMl(0);
        $rowData->setDisplayWeightLb(0);

        $rowData->save();
        AmsLogger::log($MN, "after save.");
        AmsLogger::logEnd($MN);
        return $rowData;
    }

    public function save() {
        $mn = "user:AmsUserSettings.save()";
        $st = AmsLogger::logBegin($mn);

        AmsLogger::log($mn, "is_object(this)=" . is_object($this));
        AmsLogger::log($mn, "ID=" . $this->getId());
        try {
            if (is_object($this)) {
                AmsLogger::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {

                    AmsLogger::log($mn, "Insert ");
                    $strSQL = "INSERT INTO " . AmsUserSettings::TABLE_NAME . " (" . AmsUserSettings::getAllColumnsSQL() . ") ";
                    $strSQL .= " VALUES( ?, ?, ?)";

                    $bound_params_r = array("iii",
                        $this->getId(),
                        ( ($this->getDisplayDistanceMl() == null) ? 0 : ($this->getDisplayDistanceMl()?1:0)),
                         ( ($this->getDisplayWeightLb() == null) ? 0 : ($this->getDisplayWeightLb()?1:0))
                    );
                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($MN);
                    $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);

                    AmsLogger::log("$mn", "id=" . $id);
                    $this->LoadById($id);
                } else {
                    AmsLogger::log($mn, "Update ");
                    //$lngID = $this->getDT_RowId();
                    $strSQL = "UPDATE " . AmsUserSettings::TABLE_NAME;
                    $strSQL .= " SET " . AmsUserSettings::COL_NAME_DISTANCE_ML . "=?, ";
                    $strSQL .= AmsUserSettings::COL_NAME_WEIGHT_LB . "=? ";

                    $strSQL .= " WHERE " . AmsUserSettings::COL_NAME_ID . "=? ";

                    $bound_params_r = array("iii",
                         ( ($this->getDisplayDistanceMl() == null) ? 0 : ($this->getDisplayDistanceMl()?1:0)),
                         ( ($this->getDisplayWeightLb() == null) ? 0 : ($this->getDisplayWeightLb()?1:0)),
                        $this->getId()
                    );


                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($MN);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    AmsLogger::log($mn, "affectedRows=" . $affectedRows);

                    //$this->LoadById($this->getDT_RowId());
                }
            }
        } catch (Exception $ex) {
            AmsLogger::log($MN, "Error id " . $this->getId());
            AmsLogger::logError($MN, $ex);
        }
        AmsLogger::logEnd($mn);
        return $this;
    }

    public function loadFromArray($result) {
        $MN = "user:AmsUserSettings.loadFromArray()";
        AmsLogger::logBegin($MN);
        try {

            if (isset($result) && count($result) > 0) {

                $this->setId($result[AmsUserSettings::COL_NAME_ID]);

                $this->setDisplayDistanceMl($result[AmsUserSettings::COL_NAME_DISTANCE_ML]);
                $this->setDisplayWeightLb($result[AmsUserSettings::COL_NAME_WEIGHT_LB]);

                AmsLogger::log($MN, "id " . $this->getId());
            }
        } catch (Exception $ex) {
            AmsLogger::log($MN, "Error id " . $this->getId());
            AmsLogger::logError($MN, $ex);
        }
        AmsLogger::logEnd($MN);
    }

    public function loadFromPosArray($result) {
        $MN = "user:AmsUserSettings.loadFromPosArray()";
        AmsLogger::logBegin($MN);
        if (isset($result) && count($result) > 0) {
            $this->setId($result[AmsUserSettings::COL_IXD_ID]);
            $this->setDisplayDistanceMl($result[AmsUserSettings::COL_IDX_DISTANCE_ML]);
            $this->setDisplayWeightLb($result[AmsUserSettings::COL_IDX_WEIGHT_LB]);
        }
        AmsLogger::logEnd($MN);
    }

    public function toString() {
        $retValue = $this->toJSON();
        return $retValue;
    }

    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    /**
     * ***************************************************************************
     * Getters and Setters Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters  Declarations">


    public function getId() {
        return $this->userId;
    }

    public function setId($userId) {
        $this->userId = $userId;
    }

    public function getDisplayDistanceMl() {
        if ($this->displayDistanceMl == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function setDisplayDistanceMl($displayDistanceMl) {
        $this->displayDistanceMl = $displayDistanceMl;
    }

    public function getDisplayWeightLb() {
        if ($this->displayWeightLb == 0) {
            return false;
        } else {
            return true;
        }
    }

    public function setDisplayWeightLb($displayWeightLb) {
        $this->displayWeightLb = $displayWeightLb;
    }

    // </editor-fold>

    /*     * **************************************************************************
     * Parameters Declarations
     * ***************************************************************************
     */

// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">

    public $userId;
    public $displayDistanceMl = 0;
    public $displayWeightLb = 0;

// </editor-fold>

    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME = "iordanov_ams_al.ams_user_settings";
    const COL_NAME_ID = "user_id";
    const COL_NAME_DISTANCE_ML = "display_distance_ml";
    const COL_NAME_WEIGHT_LB = "display_weight_lb";
    const COL_IXD_ID = 0;
    const COL_IDX_DISTANCE_ML = 1;
    const COL_IDX_WEIGHT_LB = 2;

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . AmsUserSettings::TABLE_NAME . "." . AmsUserSettings::COL_NAME_ID . ", " .
                AmsUserSettings::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " . AmsUserSettings::TABLE_NAME . "." . AmsUserSettings::COL_NAME_DISTANCE_ML . ", " .
                AmsUserSettings::TABLE_NAME . "." . AmsUserSettings::COL_NAME_WEIGHT_LB;
    }

    public static function getArrayColumns() {
        return array(AmsUserSettings::COL_NAME_ID,
            AmsUserSettings::COL_NAME_DISTANCE_ML,
            AmsUserSettings::COL_NAME_WEIGHT_LB);
    }

    // </editor-fold>
}
