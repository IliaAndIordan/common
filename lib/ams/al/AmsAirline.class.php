<?php 
/******************************** HEAD_BEG ************************************
*
* Project                	: ams
* Module                        : ams
* Responsible for module 	: IordIord
*
* Filename               	: AmsAirline.class.php
*
* Database System        	: MySQL
* Created from                  : IordIord
* Date Creation			: 21.03.2016
*------------------------------------------------------------------------------
*                        Description
*------------------------------------------------------------------------------
* @TODO Insert some description.
*
*------------------------------------------------------------------------------
*                        History
*------------------------------------------------------------------------------
* HISTORY:
* <br>--- $Log: AmsAirline.class.php,v $
* <br>---
* <br>---
*
********************************* HEAD_END ************************************
*/
require_once("GVDataTable.class.php");
//require_once("Airport.class.php");
global $km_to_ml;
global $m_to_food;

// <editor-fold defaultstate="collapsed" desc="AmsAirline Class">

/**
 * Description of AmsAirline class
 *
 * @author IordIord
 */
class AmsAirline {
/**
 * ***************************************************************************
 * Methods Declarations
 * ***************************************************************************
 */
    
  // <editor-fold defaultstate="collapsed" desc="Update Data">

    public function save() {
        $mn = "AmsAirline->save()";
        AmsLogger::logBegin($mn);

        AmsLogger::log($mn, "is_object(this)=" . is_object($this));
        
        if (is_object($this)) {
            try {
                AmsLogger::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {
                    AmsLogger::log($mn,  "Insert ");
                    $strSQL = "INSERT INTO " . AmsAirline::TABLE_NAME . " (".
                    AmsAirline::COL_NAME_UAER_ID . ", ".
                    AmsAirline::COL_NAME_NAME . ", ".
                    AmsAirline::COL_NAME_IATA_CODE . ", ".
                    AmsAirline::COL_NAME_HQ_AIRPORT_ID . ", ".
                    AmsAirline::COL_NAME_CREATED . ") ".
                    " VALUES(?, ?, ?, ?, ?)";

                    $bound_params_r = ["issis",
                        (($this->getUserID() == null) ? null : $this->getUserID()),
                        (($this->getName() == null) ? null : $this->getName()),
                        (($this->getCode() == null) ? null : $this->getCode()),
                        (($this->getHqAirportId() == null) ? null : $this->getHqAirportId()),
                        (($this->getCreated() == null) ? null : $this->getCreated()->format("Y-m-d H:i:s")),
                    ];
                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($mn);
                    $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);

                    AmsLogger::log("$mn", "id=" . $id);
                    $this->LoadById($id);
                } else {
                    AmsLogger::log($mn,  "Update ");

                    $strSQL = "UPDATE " . AmsAirline::TABLE_NAME;
                    $strSQL .=" SET " . AmsAirline::COL_NAME_UAER_ID . "=?, ";
                    $strSQL .=AmsAirline::COL_NAME_NAME . "=?, ";
                    $strSQL .=AmsAirline::COL_NAME_IATA_CODE . "=?, ";
                    $strSQL .=AmsAirline::COL_NAME_LOGO . "=?, ";
                    $strSQL .=AmsAirline::COL_NAME_SLOGAN . "=?, ";
                    $strSQL .=AmsAirline::COL_NAME_DESCRIPTION . "=?, ";
                     $strSQL .=AmsAirline::COL_NAME_HQ_AIRPORT_ID . "=? ";
                    // $strSQL .=AmsAirline::COL_NAME_CREATED . "=?, ";
                    $strSQL .=" WHERE " . AmsAirline::COL_NAME_ID . " = ? ";

                    $bound_params_r = ["isssssii",
                        (($this->getUserID() == null) ? null : $this->getUserID()),
                        (($this->getName() == null) ? null : $this->getName()),
                        (($this->getCode() == null) ? null : $this->getCode()),
                        (($this->getLogo() == null) ? null : $this->getLogo()),
                        (($this->getSlogan() == null) ? null : $this->getSlogan()),
                        (($this->getDescription() == null) ? null : $this->getDescription()),
                        (($this->getHqAirportId() == null) ? null : $this->getHqAirportId()),
                        //(($this->getCreated() == null) ? null : $this->getCreated()->format("Y-m-d H:i:s")),
                        $this->getId()
                    ];

                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($mn);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    AmsLogger::log($mn, "affectedRows=" . $affectedRows);
                    
                }
            } catch (Exception $ex) {
                AmsLogger::log($mn, "Error id " . $this->getId());
                AmsLogger::logError($mn, $ex);
            }
        }
        AmsLogger::logEnd($mn);
        return $this;
    }

// </editor-fold>
    
    
  // <editor-fold defaultstate="collapsed" desc="Load Data">
    
    public static function CheckAmsAirlineName($name)
    {
        $mn = "ams:AmsAirline::CheckAmsAirlineName(".$name.")";
        AmsLogger::logBegin($mn);
        
         $sqlStr ="SELECT count(*) AS Rows FROM ".
            AmsAirline::TABLE_NAME." WHERE ".AmsAirline::COL_NAME_NAME." like ?";

        $bound_params_r = array('s', $name."%");
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        AmsLogger::log($mn, "count(result_r)=".count($result_r));
        $nrRows = $result_r[0]["Rows"];
        AmsLogger::log($mn, "Found ".$nrRows." airlines with name ".$name."!");
        AmsLogger::logEnd($mn);
        return $nrRows;
    }
    
    public function IsoCountry()
    {
        $mn = "ams:AmsAirline::IsoCountry(".$this->hq_airport_id.")";
        AmsLogger::logBegin($mn);
        $country_iso2="BG";
        
        $sqlStr ="SELECT country_iso2
                    FROM iordanov_ams_al.cfg_country c
                    join iordanov_ams_al.air_airport a on a.country_id = c.country_id
                    where a.airport_id=?";
        try{
            $bound_params_r = array('i', $this->hq_airport_id."%");
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($mn);
            $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
            AmsLogger::log($mn, "count(result_r)=".count($result_r));
            $country_iso2 = $result_r[0]["country_iso2"];
        }
        catch(Exception $ex){AmsLogger::logError($mn, $ex);$country_iso2="BG";}
        AmsLogger::log($mn,"Found country iso ".$country_iso2."!");
        AmsLogger::logEnd($mn);
        return $country_iso2;
    }
    
    
    public static function CheckAmsAirlineCode($name)
    {
        $mn = "ams:AmsAirline::CheckAmsAirlineCode(".$name.")";
        AmsLogger::logBegin($mn);
       
         $sqlStr ="SELECT count(*) AS Rows FROM ".
            AmsAirline::TABLE_NAME." WHERE ".AmsAirline::COL_NAME_IATA_CODE." = ?";

        $bound_params_r = array('s', $name);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        AmsLogger::log($mn, "count(result_r)=".count($result_r));
        $nrRows = $result_r[0]["Rows"];
        AmsLogger::log($mn,  "Found ".$nrRows." airlines with iata ".$name."!");
       
        AmsLogger::logEnd($mn);
        return $nrRows;
    }
    
    public function loadById($id)
    {
        $mn = "ams:AmsAirline.loadById()";
        AmsLogger::logBegin($mn);
        AmsLogger::log($mn, "id = ".$id);
        $this->setId($id);
        $sql = "SELECT ".AmsAirline::getAllColumnsSQL(). ", " . AmsAirline::COL_NAME_UPDATED.
                " FROM ".AmsAirline::TABLE_NAME." ".
                " WHERE ".AmsAirline::COL_NAME_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        AmsLogger::log($mn, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //AmsLogger::log($mn, "count(data)=".count($data));
        }
        //AmsLogger::log("$mn", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        AmsLogger::log($mn, "Airline is ".$this->toString());

        AmsLogger::logEnd($mn);
    }

    public static function loadByUserId($id)
    {
        $mn = "AmsAirline.loadByUserId()";
        AmsLogger::logBegin($mn);
        AmsLogger::log($mn, "User id = ".$id);
        $airline=null;
        $sql = "SELECT ".AmsAirline::getAllColumnsSQL(). ", " . AmsAirline::COL_NAME_UPDATED.
                " FROM ".AmsAirline::TABLE_NAME." ".
                " WHERE ".AmsAirline::COL_NAME_UAER_ID." = ? ";

        $bound_params_r = array('i', $id);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        AmsLogger::log($mn, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //AmsLogger::log($mn, "count(data)=".count($data));
        }
        //AmsLogger::log("$mn", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
            $airline = new AmsAirline();
            $airline->loadFromArray($data);
        }

        AmsLogger::logEnd($mn);
        return $airline;
    }

    public static function deleteById($id) {
        $mn = "AmsAirline.deleteById()";
        AmsLogger::logBegin($mn);
        AmsLogger::log($mn, " id = ".$id);
        //$this->setId($id);
        $sql = "DELETE ".
                " FROM ".AmsAirline::TABLE_NAME." ".
                " WHERE ".AmsAirline::COL_NAME_ID."=?";
        $bound_params_r = ["i",$id];
         $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $deleted_id = $conn->preparedDelete($sql, $bound_params_r, $logModel);
        AmsLogger::logEnd($mn);
    }
    
    public static function loadAll()
    {
        $mn = "AmsAirline.loadAll()";
        AmsLogger::logBegin($mn);

        $sql = "SELECT ".AmsAirline::getAllColumnsSQL() . ", " . AmsAirline::COL_NAME_UPDATED.
                " FROM ".AmsAirline::TABLE_NAME." ";

        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $data = $conn->dbExecuteSQL($sql, $logModel);
        AmsLogger::log($mn, "count(data)=".count($data));
        $retArray = array();
        for ($index = 0, $max_count = sizeof( $data ); $index < $max_count; $index++)
        {
           $result = $data[$index];
           $item = new User();
           $item->loadFromPosArray($result);
           $retArray[] = $item;
           //AmsLogger::log($mn, "Add Item =".$item->toString());
        }
        AmsLogger::log($mn, "count(retArray)=".count($retArray));
        AmsLogger::logEnd($mn);
        return $retArray;
    }
    
    public function loadFromArray($result) {
        $mn = "AmsAirline.loadFromArray()";
         AmsLogger::logBegin($mn);
        try{
            if(!$result==null) {
                AmsLogger::log($mn, "result: ".implode(",", $result));
               
                $this->setId($result[AmsAirline::COL_NAME_ID]);
                $this->setUserID($result[AmsAirline::COL_NAME_UAER_ID]);
                $this->setName($result[AmsAirline::COL_NAME_NAME]);
                $this->setCode($result[AmsAirline::COL_NAME_IATA_CODE]);
                $this->setLogo($result[AmsAirline::COL_NAME_LOGO]);
                $this->setSlogan($result[AmsAirline::COL_NAME_SLOGAN]);
                $this->setDescription($result[AmsAirline::COL_NAME_DESCRIPTION]);
                $this->setHqAirportId($result[AmsAirline::COL_NAME_HQ_AIRPORT_ID]);
                $this->setCreated($result[AmsAirline::COL_NAME_CREATED]);
                $this->setUpdated($result[AmsAirline::COL_NAME_UPDATED]);
                
                if(isset($this->airline_id)){
                    $this->logoUrl = $this->getLogoUrl();
                     $this->liveryUrl = $this->getLiveryUrl();
                }
            }
        }
        catch(Exception $ex){AmsLogger::log($mn, "Error id ".$this->getId());AmsLogger::logError($mn, $ex);}
        AmsLogger::logEnd($mn);
    }

    public function loadFromPosArray($result) {
        $mn = "AmsAirline.loadFromPosArray()";
        AmsLogger::logBegin($mn);
        if(!$result==null) {
            try{
                AmsLogger::log($mn, "result: ".implode(",", $result));
                $this->setDT_RowId($result[AmsAirline::COL_IDX_ID]);
                $this->setUserID($result[AmsAirline::COL_IDX_UAER_ID]);
                $this->setName($result[AmsAirline::COL_IDX_NAME]);
                $this->setCode($result[AmsAirline::COL_IDX_IATA_CODE]);
                $this->setLogo($result[AmsAirline::COL_IDX_LOGO]);
                $this->setSlogan($result[AmsAirline::COL_IDX_SLOGAN]);
                $this->setDescription($result[AmsAirline::COL_IDX_DESCRIPTION]);
                 $this->setHqAirportId($result[AmsAirline::COL_IDX_HQ_AIRPORT_ID]);
                $this->setCreated($result[AmsAirline::COL_IDX_CREATED]);
                $this->setUpdated($result[AmsAirline::COL_IDX_UPDATED]);
                
                if(isset($this->airline_id)){
                    $this->logoUrl = $this->getLogoUrl();
                     $this->liveryUrl = $this->getLiveryUrl();
                }
            }
            catch(Exception $ex){AmsLogger::log($mn, "Error id ".$this->getId());AmsLogger::logError($mn, $ex);}
        }
        AmsLogger::logEnd($mn);

    }

     public function toString()
    {
        $retValue = $this->toJSON();
        return $retValue;
    }
    
    public function toJSON() {
        return json_encode($this);
    }
    
     // </editor-fold>
    
    /**
     * ***************************************************************************
     * Getters and Setters Declarations
     * ***************************************************************************
     */
    
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters  Declarations">
    public function getId() {
        return $this->airline_id;
    }
    
    public function setId($Id) {
        $this->airline_id = $Id;
    }

    public function getCode() {
        return $this->code;
    }
    
    public function getIata() {
        return $this->code;
    }

    public function setCode($iata) {
        $this->code = strtoupper($iata);
    }
    
    public function getUserID() {
        return $this->user_id;
    }

    public function setUserID($user_id) {
        $this->user_id = $user_id;
    }
    
     public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }
    public static $image_base_url="https://common.iordanov.info/images";
    public function getLogoUrl() {
        $retValue = AmsAirline::$image_base_url."/".$this->getLogoPath();
        
        return $retValue;
    }
    
     public function getLogoPath() {
        $mn = "AmsAirline->getLogoPath()";
        AmsLogger::logBegin($mn);

       
        $retValue = "airline/".$this->getLogo();
        $docRoot = $_SERVER['DOCUMENT_ROOT']."/images/";
         AmsLogger::log($mn, " docRoot=" . $docRoot);
        if (!file_exists($docRoot.$retValue)) 
        {
            AmsLogger::log($mn, "Create logo=".$this->getId());  
            $this->createLogo("/images/airline/logo_0.png");
            $this->save();
        }
        AmsLogger::logEnd($mn);
        return $retValue;
    }
    
    public function getLogo() {
        $retValue = "logo_".$this->getId().".png";
        $this->logo = $retValue;
        return $retValue;
    }
    
    public function createLogo($src) {
        $mn = "ams:AmsAirline::createLogo()";
        AmsLogger::logBegin($mn);
        AmsLogger::log($mn, "User src = ".$src);
        $height = 300;
        $width = 300;
        $fontSize = 126;
        $ctFontSize= 16;
        $iata = "AA";
        $docRoot = $_SERVER['DOCUMENT_ROOT']."/images/";
        //logDebug("createLogo", "docRoot=".$docRoot);  
        $src = $docRoot.$src;
        if (!file_exists($src)) 
        {
            AmsLogger::log($mn,"Can not find file: ".$src.": exist=".file_exists($src)?"true":"false"); 
            return;
        }
        
        $country = strtolower($this->IsoCountry());
        $aname = $this->getName();
        //if(strlen($aname)>10)
        //    $aname = substr($aname, 0, 10);
        $slogan = $this->getSlogan();
        if(!isset($slogan) || strlen($slogan)===0)
                $slogan=$aname;
        if(strlen($slogan)>36){
            $slogan = substr($slogan, 0, 36);
        }

        if(strlen($slogan)<36){
            $maxIdx = (36-strlen($slogan))/2;
            for($idx=0; $idx<$maxIdx ; $idx++)
            {
                $slogan = ($idx===0?"":" ") .$slogan . " ";
            }
        }
        $iata = strtoupper($this->getIata());
        if (strlen($iata) > 2)
            $iata = substr($iata, 0, 2);
        
        $flagSrc = $docRoot."/images/flags/" . $country . ".png";
        
        $imageinfoB = getimagesize($src);
        $imageinfoF = getimagesize($flagSrc);
        
        $flagw = $imageinfoF[0];
        $flagh = $imageinfoF[1];
        $logow = $imageinfoB[0];
        $logoh = $imageinfoB[1];
        
        AmsLogger::log($mn, "flagw=".$flagw);  
        AmsLogger::log($mn, "logow=".$logow);  
        
        $hK = $height / $logoh;
        $fontSize = $fontSize * $hK;
        
        //define iata text size
        $font = $docRoot."/fonts/Champagne & Limousines Bold.ttf"; // "../fonts/Quincho.ttf";:
        $iataSize = imagettfbbox($fontSize, 0, $font, $iata);
        
        $fhK = $fontSize / $flagh;
        
        $imgh = $height-($ctFontSize*2);
        $imgw = $width-($ctFontSize*2);
        
        $logoK = $imgh/$logoh;
        $lh = $imgh*$logoK;
        $lw = $imgw*$logoK;
        $lx = round($ctFontSize);
        $ly = round($ctFontSize);
        
        //$width = $logow * $hK;
        $fw = ($imgw/2)-$fontSize;
        $fh = $fontSize/1.4;
        $fx = ($width/2) -($fw/2);//$width / 3.5;
        $fy = ($imgh-(1.6*$fh));//$height-($height / 4.5);
        $iatax =  $width / 2 - (($iataSize[4] - $iataSize[0] )/2);
        $iatay = ($height / 4)+($ctFontSize*2);
        //--- Create Image
        $rImg = imagecreatetruecolor($width, $height);

        //--- Load Images Logo and Country Flag 
        $srcimage = imagecreatefrompng($src);
        $srcimageFlag = imagecreatefrompng($flagSrc);
        $corIata = imagecolorallocate($rImg, 217, 19, 59);
        //--- paste logo full size and transparent + flag
        imagealphablending($rImg, false);
        imagesavealpha($rImg, true);
        $transparent = imagecolorallocatealpha($rImg, 255, 255, 255, 127);
        imagefilledrectangle($rImg, 0, 0, $width, $height, $transparent);
        imagecopyresampled($rImg, $srcimage, $lx, $ly, 0, 0, $imgw, $imgh, $logow, $logoh);
        
        imagecopyresampled($rImg,$srcimageFlag,$fx, $fy,0,0, $fw,$fh,$flagw,$flagh);
        //--- IATA CODE
        imagettftext($rImg, $fontSize, 0,$iatax, $iatay, $corIata, $font, $iata);
        
         //---- Drow text in cercle
        $degrees = (360/strlen($slogan));
        
        $colorSlogan = imagecolorallocate($rImg, 148, 148, 184);
        $fontSlogan = $docRoot."/fonts/RobotoCondensed-Bold.ttf";
        //--- Center
        $cx = $width/2;
        $cy = ($height/2);
        //--- radius
        $r = (($width/2)-($ctFontSize/2));// + $logoh/4);//-($ctFontSize/4);
        for($idx=0;$idx<strlen($slogan);$idx++) 
        {
            $leterIdx = strlen($slogan)-1-$idx;
            $angle = ($degrees*$idx)-90;
            $cos = cos(deg2rad($angle));
            $sin = sin(deg2rad($angle));
            
            //---
            $px = round($cx + ($r*$cos)); //round($cos*($x) - $sin*($y));
            $py = round($cy + ($r*$sin));//round($sin*($x) + $cos*($y));

          imagettftext($rImg,$ctFontSize,90-$angle,$px,$py,$colorSlogan,$fontSlogan,$slogan[$leterIdx]);
        }
        //--- Save the image to file
        imagepng($rImg, $docRoot."/img/airline/logo_".$this->getId().".png");
        // Destroy image 
        imagedestroy($rImg); 
    }
    
    public function getLiveryUrl() {
        $retValue = AmsAirline::$image_base_url."/".$this->LiveryPath();
        
        return $retValue;
    }
    
    public function LiveryPath() {
        $retValue = "airline/".$this->getLivery();
        $docRoot = $_SERVER['DOCUMENT_ROOT']."/images/";
        if (!file_exists($docRoot.$retValue)) 
        { 
            $this->createLivery();
        }
        return $retValue;
    }
    
    public function getLivery() {
        $retValue = "livery_".$this->getId().".png";
        return $retValue;
    }
    
    public function createLivery() {
        $mn = "ams:AmsAirline::createLivery()";
        AmsLogger::logBegin($mn);
        
        $height = 230;
        $width=1000;
        $fontSize=82;
        $iata = "AA";
        $docRoot = $_SERVER['DOCUMENT_ROOT']."/images/";
        
        //logDebug("createLivery", "docRoot=".$docRoot);  
        $src = $docRoot.$this->getLogoPath();
        AmsLogger::log($mn, " src = ".$src);
        if (!file_exists($src)) 
        {
            AmsLogger::log($mn, "Not found file".$src.": exist=".file_exists($src)?"true":"false"); 
            return;
        }
        
        $aname = $this->getName();
        if(strlen($aname)>10)
            $aname = substr($aname, 0, 10);
        $slogan = $this->getSlogan();
        if(strlen($slogan)>36){
            $slogan = substr($slogan, 0, 36);
        }

        if(strlen($slogan)<36){
            $maxIdx = (36-strlen($slogan))/2;
            for($idx=0; $idx<$maxIdx ; $idx++)
            {
                $slogan = " " .$slogan . " ";
            }
        }
        $country = strtolower($this->IsoCountry());
        AmsLogger::log($mn,  " country=".$country);  
        $iata = strtoupper($this->getIata());
        if (strlen($iata) > 2)
            $iata = substr($iata, 0, 2);
        
        $flagSrc = $docRoot."images/flags/" . $country . ".png";
        
        $imageinfoB = getimagesize($src);
        $imageinfoF = getimagesize($flagSrc);
        
        $flagw = $imageinfoF[0];
        $flagh = $imageinfoF[1];
        $logow = $imageinfoB[0];
        $logoh = $imageinfoB[1];
        
        //logDebug("createLivery", "logow=".$logow);  
        //logo resize facktor
        $logoRes = $height / $logoh;
        //$fontSize = $fontSize * $hK;
        //define iata text size
        $font = $docRoot."/fonts/Champagne & Limousines Bold.ttf"; // "../fonts/Quincho.ttf";:
        $iataSize = imagettfbbox($fontSize, 0, $font, $iata);
        //$fhK = $fontSize / $flagh;
        $lw = $logow * $logoRes;
        $lh = $logoh * $logoRes;
        //--- Flag bottom horizontal
        $fw = $width/2.3;
        $fh = $fontSize/2;
        $fx = $logow / 3.5;
        $fy = $height- $fh-2;//($height / 4.5);
        $iatax =  $width / 2 - (($iataSize[4] - $iataSize[0] )/2);
        $iatay = ($height / 4);
        $flW=$width-($logow/2)-1;//67*$hK;
        $flH=$fh;
        
       
        //---- define text size
        $font = $docRoot."/fonts/Champagne & Limousines Bold.ttf"; 
        $fontSlogan = $docRoot."/fonts/RobotoCondensed-Bold.ttf";
        
        //--- Create Image
        $rImg = imagecreatetruecolor($width, $height);

        //--- Load Images Logo and Country Flag 
        $srcimage = imagecreatefrompng($src);
        $srcimageFlag = imagecreatefrompng($flagSrc);
        $corIata = imagecolorallocate($rImg, 217, 19, 59);
        $corName =imagecolorallocate($rImg, 0, 77, 0);
        $corSlogan = imagecolorallocate($rImg, 148, 148, 184);
        //--- paste logo full size and Transparent + flag
        //imagealphablending($rImg, false);
        //imagesavealpha($rImg, true);
        //$transparent = imagecolorallocatealpha($rImg, 255, 255, 255, 127);
        //imagefilledrectangle($rImg, 0, 0, $width, $height, $transparent);
        
        //-- Image full size and background color
        $white = imagecolorallocate($rImg, 255, 255, 255);
        $lightyelow = imagecolorallocate($rImg, 255, 255, 204);
        imagefilledrectangle($rImg, 0, 0, $width, $height, $lightyelow);
        
        //-- Insert Logo left full
        imagecopyresampled($rImg, $srcimage, 0, 0, 0, 0, $lw, $lh, $logow, $logoh);
        
        //---- Insert Country Flag right vertical
        //--- Flag coordinate bottom horizontal
        $fw = ($lw / 2.5);
        $fh = $height-2;
        $fx = $width-($fw+2);
        $fy=1;
        imagecopyresampled($rImg,$srcimageFlag,$fx, $fy,0,0, $fw,$fh,$flagw,$flagh);
        
        //--- Slogan and Name
        imagettftext($rImg, $fontSize, 0, 336,(14+$fontSize), $corName, $font, $aname);
        imagettftext($rImg, $fontSize/2, 0, 366,((20+$fontSize)+($fontSize)), $corSlogan, $fontSlogan, $slogan);
       
        //--- Save the image to file
        imagepng($rImg, $docRoot."/images/airline/livery_".$this->getId().".png");
        // Destroy image 
        imagedestroy($rImg); 
    }

    public function setLogo($logo) {
        $this->logo = $logo;
    }
    //RegionByIso
    public function getSlogan() {
        return $this->slogan;
    }

    public function setSlogan($slogan) {
        $this->slogan = $slogan;
    }
    
    public function getDescription() {
        return $this->description;
    }
    
    public function setDescription($description) {
        $this->description = $description;
    }
    
    public function getHqAirportId() {
        return $this->hq_airport_id;
    }
    
    public function setHqAirportId($hq_airport_id) {
        $this->hq_airport_id = $hq_airport_id;
    }
    
    public function getCreated() {
        $retValue = null;
        if (isset($this->created) && $this->created != null) {
            if (!is_object($this->created))
                $retValue = new DateTime($this->created);
            else
                $retValue = $this->created;

            //logDebug("getLastLogged()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setCreated($created) {
         if (isset($created) && $created != null) {
            if (is_object($created))
                $this->created = $created;
            else
                $this->created = new DateTime($created);
        }
        
    }
    
    public function getUpdated() {
        $retValue = null;
        if (isset($this->updated) && $this->updated != null) {
            if (!is_object($this->updated))
                $retValue = new DateTime($this->updated);
            else
                $retValue = $this->updated;

            //logDebug("getLastLogged()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setUpdated($created) {
         if (isset($created) && $created != null) {
            if (is_object($created))
                $this->updated = $created;
            else
                $this->updated = new DateTime($created);
        }
        
    }
    
    // </editor-fold>
    
   /****************************************************************************
   * Parameters Declarations
   * ***************************************************************************
   */
   
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">

    public $airline_id;
    public $user_id;
    public $name;
    public $code;
    public $logo;
    public $slogan;
    public $description;
    public $hq_airport_id;
    public $created;
    public $updated = 0;
    
    public $logoUrl;
    public $liveryUrl;
   
// </editor-fold>
    
    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME            = "iordanov_ams_al.ams_airline";
    
    const COL_NAME_ID           = "airline_id";
    const COL_NAME_UAER_ID      = "user_id";
    const COL_NAME_NAME         = "al_name";
    const COL_NAME_IATA_CODE    = "al_code";
    const COL_NAME_LOGO         = "al_logo";
    const COL_NAME_SLOGAN       = "al_slogan";
    const COL_NAME_DESCRIPTION  = "al_description";
    const COL_NAME_HQ_AIRPORT_ID   = "hq_airport_id";
    const COL_NAME_CREATED      = "adate";
    const COL_NAME_UPDATED      = "udate";
   
    const COL_IDX_ID            = 0;
    const COL_IDX_UAER_ID       = 1;
    const COL_IDX_NAME          = 2;
    const COL_IDX_IATA_CODE     = 3;
    const COL_IDX_LOGO          = 4;
    const COL_IDX_SLOGAN        = 5;
    const COL_IDX_DESCRIPTION   = 6;
    const COL_IDX_HQ_AIRPORT_ID    = 7;
    const COL_IDX_CREATED       = 8;
    const COL_IDX_UPDATED       = 9;
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . AmsAirline::COL_NAME_ID . ", " .
                AmsAirline::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " .  AmsAirline::COL_NAME_UAER_ID . ", " .
                AmsAirline::COL_NAME_NAME . ", " .
                AmsAirline::COL_NAME_IATA_CODE . ", " .
                AmsAirline::COL_NAME_LOGO . ", " .
                AmsAirline::COL_NAME_SLOGAN . ", " .
                AmsAirline::COL_NAME_DESCRIPTION . ", " .
                AmsAirline::COL_NAME_HQ_AIRPORT_ID . ", " .
                 AmsAirline::COL_NAME_CREATED;
    }

    public static function getArrayColumns() {
        return array(AmsAirline::COL_NAME_ID,
            AmsAirline::COL_NAME_UAER_ID,
            AmsAirline::COL_NAME_NAME,
            AmsAirline::COL_NAME_IATA_CODE,
            AmsAirline::COL_NAME_LOGO,
            AmsAirline::COL_NAME_SLOGAN,
            AmsAirline::COL_NAME_DESCRIPTION,
            AmsAirline::COL_NAME_HQ_AIRPORT_ID,
            AmsAirline::COL_NAME_CREATED,
            AmsAirline::COL_NAME_UPDATED);
    }

    // </editor-fold>
}


// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="AmsAirline GVDT">

// </editor-fold>

