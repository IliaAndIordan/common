<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Sep 13, 2018 
 *  Filename          : WadAirportEdit.php
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

require_once("AmsWadConnection.php");
require_once("AmsWadLogger.php");
require_once 'GVDataTable.class.php';

/**
 * Description of WadAirportEdit
 *
 * @author IZIordanov
 */
class WadAirportEdit {
   
// <editor-fold defaultstate="collapsed" desc="Country StateStatus Airport">

    public static function CountryStateStatusAirportCount() {
        $mn = "WadAirportEdit.CountryStateStatusAirportCount()";
        AmsWadLogger::logBegin($mn);
        $response = array();
        

        $sql = " SELECT c.country_iso2 as countryIso2, c.country_name as countryName, 
            s.state_name as stateName,  
            count(airport_id) as airportsCount,
            sum(if(a.ams_status=1,1,0)) as airportsCountAmsStatus0,
            if(sum(if(a.ams_status=1,1,0))=count(airport_id),1,0) as checked,
             c.country_id as countryId, s.state_id as stateId
        FROM iordanov_ams_wad.air_airport a
        left join iordanov_ams_wad.cfg_country c on c.country_id = a.country_id
        left join iordanov_ams_wad.cfg_country_state s on s.state_id = a.state_id
        left join iordanov_ams_wad.cfg_city y on y.city_id = a.city_id
        group by c.country_iso2, c.country_name, s.state_name
        order by checked, airportsCountAmsStatus0, airportsCount, countryName";

        try {
            $conn = AmsWadConnection::dbConnect();
            $logModel = AmsWadLogger::loggerWad()->getModule($mn);
            $response = $conn->dbExecuteSQLJson($sql, $logModel);
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        //AmsWadLogger::log($mn, "count($response)=" . count($response));
        AmsWadLogger::logEnd($mn);
        return $response;
    }
    
    public static function AirportListByCountryState($country_id, $state_id) {
        $mn = "WadAirportEdit.AirportListByCountryState(".$country_id.", ". $state_id.")";
        AmsWadLogger::logBegin($mn);
        $response = array();
        

        $sql = " SELECT a.airport_id, concat(ifnull(a.iata, a.icao) , ' ', a.aname) as ap_name, 
                concat( y.city_name, ', ', s.state_name, ', ', c.country_iso2) as city_name,
                st.ams_status_name, t.ap_type_name,
                a.ams_status, a.ap_type_id
                FROM iordanov_ams_wad.air_airport a
                left join iordanov_ams_wad.cfg_country c on c.country_id = a.country_id
                left join iordanov_ams_wad.cfg_country_state s on s.state_id = a.state_id
                left join iordanov_ams_wad.cfg_city y on y.city_id = a.city_id
                left join iordanov_ams_wad.cfg_airport_ams_status st on st.ams_status_id = a.ams_status
                left join iordanov_ams_wad.cfg_airport_type t on t.ap_type_id = a.ap_type_id";
        $bound_params_r = ["ii", $country_id, $state_id];
        if(isset($state_id)){
             $sql =  $sql." where a.country_id=?  and a.state_id=?";
        }
        else{
            $sql =  $sql." where a.country_id=? ";
            $bound_params_r = ["i", $country_id];
        }
        
        $sql =  $sql." order by a.ams_status, a.ap_type_id desc, ap_name";

        try {
            $conn = AmsWadConnection::dbConnect();
            $logModel = AmsWadLogger::loggerWad()->getModule($mn);
           
            //$bound_params_r = ["ii", $country_id, $state_id];
            $response = $conn->SelectJson($sql, $bound_params_r, $logModel);
            //$response->addData("airports",$data);
        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        AmsWadLogger::log($mn, "count(response)=" . count($response));
        AmsWadLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc=" G V D T ">

    
    
    
    
    // </editor-fold>
    
}



