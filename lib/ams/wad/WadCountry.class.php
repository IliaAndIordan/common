<?php

/*
 * ******************************* HEAD_BEG ************************************
 * 
 *  Project                        : ws.ams
 *  Module                         : ws.ams 
 *  Responsible for module         : IZIordanov 
 * 
 *  Filename                       : WadCountry.class.php 
 *  Date Creation                  : Mar 23, 2018 
 *  -----------------------------------------------------------------------------
 *                         Description
 *  -----------------------------------------------------------------------------
 *  Country class is a part of ws.ams project.
 *  @TODO Insert some description of newPHPClass class.
 *  -----------------------------------------------------------------------------
 *                         License
 *  -----------------------------------------------------------------------------
 * Copyright (C) 2018 IZIordanov
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * ******************************* HEAD_END ************************************
 */

require_once("AmsWadConnection.php");
require_once("AmsWadLogger.php");

/**
 * Description of Country
 *
 * @author izior
 */
class WadCountry {

// <editor-fold defaultstate="collapsed" desc="Load Data">

    public static function loadByIso2($iso2) {
        $mn = "WadCountry.loadByIso2(" . $iso2 . ")";
        AmsWadLogger::logBegin($mn);
        if (isset($iso2)) {
            $sql = "SELECT " . WadCountry::getAllColumnsSQL() .
                    " FROM " . WadCountry::SCHEMA_NAME . "." . WadCountry::TABLE_NAME . " " .
                    " WHERE " . WadCountry::COL_NAME_ISO2 . " = ?";

            $conn = AmsWadConnection::dbConnect();
            $logModel = AmsWadLogger::loggerWad()->getModule($mn);
            $bound_params_r = ["s", $iso2];

            $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            //AmsWadLogger::log($mn, "count(result_r)=" . sizeof($result_r));
            $retArray = array();
            for ($index = 0, $max_count = sizeof($result_r); $index < $max_count; $index++) {
                $result = $result_r[$index];
                $item = new WadCountry();
                $item->loadFromArray($result);
                $retArray[] = $item;
            }
        }

        //AmsWadLogger::log($mn, "count(retArray)=" . count($retArray));
        AmsWadLogger::logEnd($mn);
        return $retArray;
    }
    
    public static function loadByRegion($region) {
        $mn = "WadCountry.loadByRegion(" . $region . ")";
        AmsWadLogger::logBegin($mn);
        if (isset($region)) {
            $sql = "SELECT " . WadCountry::getAllColumnsSQL() .
                    " FROM " . WadCountry::SCHEMA_NAME . "." . WadCountry::TABLE_NAME . " " .
                    " WHERE " . WadCountry::COL_NAME_REGION . " = ?";

            $conn = AmsWadConnection::dbConnect();
            $logModel = AmsWadLogger::loggerWad()->getModule($mn);
            $bound_params_r = ["s", $region];

            $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            //AmsWadLogger::log($mn, "count(result_r)=" . sizeof($result_r));
            $retArray = array();
            for ($index = 0, $max_count = sizeof($result_r); $index < $max_count; $index++) {
                $result = $result_r[$index];
                $item = new WadCountry();
                $item->loadFromArray($result);
                $retArray[] = $item;
            }
        }

        //AmsWadLogger::log($mn, "count(retArray)=" . count($retArray));
        AmsWadLogger::logEnd($mn);
        return $retArray;
    }
    
    public static function loadBySubRegion($region) {
        $mn = "WadCountry.loadBySubRegion(" . $region . ")";
        AmsWadLogger::logBegin($mn);
        if (isset($region)) {
            $sql = "SELECT " . WadCountry::getAllColumnsSQL() .
                    " FROM " . WadCountry::SCHEMA_NAME . "." . WadCountry::TABLE_NAME . " " .
                    " WHERE " . WadCountry::COL_NAME_SUBREGION . " = ?";

            $conn = AmsWadConnection::dbConnect();
            $logModel = AmsWadLogger::loggerWad()->getModule($mn);
            $bound_params_r = ["s", $region];

            $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            //AmsWadLogger::log($mn, "count(result_r)=" . sizeof($result_r));
            $retArray = array();
            for ($index = 0, $max_count = sizeof($result_r); $index < $max_count; $index++) {
                $result = $result_r[$index];
                $item = new WadCountry();
                $item->loadFromArray($result);
                $retArray[] = $item;
            }
        }

        //AmsWadLogger::log($mn, "count(retArray)=" . count($retArray));
        AmsWadLogger::logEnd($mn);
        return $retArray;
    }

    /**
     *  Load WadCountry by country id
     * @param type $id
     * @return $this
     */
    public function loadById($id) {
        $mn = "WadCountry.loadById(" . $id . ")";
        AmsWadLogger::logBegin($mn);
        AmsWadLogger::log($mn, "ID = " . $id);

        if (isset($id)) {
            $sql = "SELECT " . WadCountry::getAllColumnsSQL() .
                    " FROM " . WadCountry::SCHEMA_NAME . "." . WadCountry::TABLE_NAME . " " .
                    " WHERE " . WadCountry::COL_NAME_ID . " = " . $id . " ";

            $conn = AmsWadConnection::dbConnect();
            $logModel = AmsWadLogger::loggerWad()->getModule($mn);
            $data = $conn->dbExecuteSQL($sql, $logModel);
            AmsWadLogger::log($mn, "count(data)=" . sizeof($data));

            if (sizeof($data) > 0) {
                $result = $data[0];
                $this->loadFromPosArray($result);
            }
        }
        AmsWadLogger::log($mn, "Item =" . ($this == null ? "NULL" : $this->toString()));
        AmsWadLogger::logEnd($mn);
        return $this;
    }

    /**
     * Delete a row in DB by country id
     * @param <int> $id
     */
    public static function deleteById($id) {
        $MN = "WadCountry.deleteById()";
        AmsWadLogger::logBegin($MN);
        logDebug($MN, "id = " . $id);
        //$this->setId($id);
        $sql = "DELETE " .
                " FROM " . WadCountry::SCHEMA_NAME . "." . WadCountry::TABLE_NAME . " " .
                " WHERE " . WadCountry::COL_NAME_ID . "=?";
        $bound_params_r = ["i", $id];
        $conn = AmsWadConnection::dbConnect();
        $deleted_id = $conn->preparedDelete($sql, $bound_params_r);
        AmsWadLogger::log($MN, "deleted_id = " . $deleted_id);
        AmsWadLogger::logEnd($MN);
    }

    public static function loadAll() {
        $mn = "WadCountry.loadAll()";
        AmsWadLogger::logBegin($mn);
        $sql = "SELECT " . WadCountry::getAllColumnsSQL() .
                " FROM " . WadCountry::SCHEMA_NAME . "." . WadCountry::TABLE_NAME . " ";

        $conn = AmsWadConnection::dbConnect();
        $logModel = AmsWadLogger::loggerWad()->getModule($mn);
        $data = $conn->dbExecuteSQL($sql, $logModel);
        //AmsWadLogger::log($mn, "count(data)=" . count($data));
        $retArray = array();
        for ($index = 0, $max_count = sizeof($data); $index < $max_count; $index++) {
            $result = $data[$index];
            $item = new WadCountry();
            $item->loadFromPosArray($result);
            $retArray[] = $item;
            //AmsWadLogger::log($mn, "Add Item =" . $item->toString());
        }

        AmsWadLogger::log($mn, "count(retArray)=" . count($retArray));
        AmsWadLogger::logEnd($mn);
        return $retArray;
    }

    public function loadFromArray($result) {
        $MN = "WadCountry.loadFromArray()";
        //AmsWadLogger::logBegin($MN);
        try {
            if (!$result == null) {
                //AmsWadLogger::log($MN, "result=" . implode(",", $result));
                $this->setId($result[WadCountry::COL_NAME_ID]);
                $this->setCode($result[WadCountry::COL_NAME_COUNTRY_CODE]);
                $this->setName($result[WadCountry::COL_NAME_COUNTRY_NAME]);
                $this->setIso2($result[WadCountry::COL_NAME_ISO2]);
                $this->setIso3($result[WadCountry::COL_NAME_ISO3]);
                $this->setRegionCode($result[WadCountry::COL_NAME_REGION_CODE]);
                $this->setRegion($result[WadCountry::COL_NAME_REGION]);
                $this->setSubRegionCode($result[WadCountry::COL_NAME_SUBREGION_CODE]);
                $this->setSubRegion($result[WadCountry::COL_NAME_SUBREGION]);

                //AmsWadLogger::log($MN, "id " . $this->getId());
            }
        } catch (Exception $ex) {
            AmsWadLogger::log($MN, "Error id " . $this->getId());
            AmsWadLogger::logError($MN, $ex);
        }
        //AmsWadLogger::logEnd($MN);
    }

    public function loadFromPosArray($result) {
        $MN = "WadCountry.loadFromPosArray()";
        //AmsWadLogger::logBegin($MN);
        if (!$result == null) {
            //AmsWadLogger::log($MN, "result=" . implode(",", $result));
            $this->setId($result[WadCountry::COL_IDX_ID]);
            $this->setCode($result[WadCountry::COL_IDX_COUNTRY_CODE]);
            $this->setName($result[WadCountry::COL_IDX_COUNTRY_NAME]);
            $this->setIso2($result[WadCountry::COL_IDX_ISO2]);
            $this->setIso3($result[WadCountry::COL_IDX_ISO3]);
            $this->setRegionCode($result[WadCountry::COL_IDX_REGION_CODE]);
            $this->setRegion($result[WadCountry::COL_IDX_REGION]);
            $this->setSubRegionCode($result[WadCountry::COL_IDX_SUBREGION_CODE]);
            $this->setSubRegion($result[WadCountry::COL_IDX_SUBREGION]);
        }
        //AmsWadLogger::logEnd($MN);
    }

    public function toString() {
        $retValue = $this->toJSON();
        return $retValue;
    }

    public function toJSON() {
        return json_encode($this);
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Getters and Setters Declarations">

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getCode() {
        return $this->code;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getIso2() {
        return $this->iso2;
    }

    public function setIso2($iso2) {
        $this->iso2 = $iso2;
    }

    public function getIso3() {
        return $this->iso3;
    }

    public function setIso3($iso3) {
        $this->iso3 = $iso3;
    }

    public function getRegionCode() {
        return $this->regionCode;
    }

    public function setRegionCode($data) {
        $this->regionCode = $data;
    }

    public function getRegion() {
        return $this->region;
    }

    public function setRegion($data) {
        $this->region = $data;
    }

    public function getSubRegionCode() {
        return $this->subRegionCode;
    }

    public function setSubRegionCode($data) {
        $this->subRegionCode = $data;
    }

    public function getSubRegion() {
        return $this->subRegion;
    }

    public function setSubRegion($data) {
        $this->subRegion = $data;
    }

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">

    public $id;
    public $code;
    public $name;
    public $iso2;
    public $iso3;
    public $regionCode;
    public $region;
    public $subRegionCode;
    public $subRegion;

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Constants Declarations">
    const SCHEMA_NAME = "iordanov_ams_wad";
    const TABLE_NAME = "cfg_country";
    const COL_NAME_ID = "country_id";
    const COL_NAME_COUNTRY_CODE = "country_code";
    const COL_NAME_COUNTRY_NAME = "country_name";
    const COL_NAME_ISO2 = "country_iso2";
    const COL_NAME_ISO3 = "country_iso3";
    const COL_NAME_REGION_CODE = "region_code";
    const COL_NAME_REGION = "region";
    const COL_NAME_SUBREGION_CODE = "sub_region_code";
    const COL_NAME_SUBREGION = "sub_region";
    const COL_IDX_ID = 0;
    const COL_IDX_COUNTRY_CODE = 1;
    const COL_IDX_COUNTRY_NAME = 2;
    const COL_IDX_ISO2 = 3;
    const COL_IDX_ISO3 = 4;
    const COL_IDX_REGION_CODE = 5;
    const COL_IDX_REGION = 6;
    const COL_IDX_SUBREGION_CODE = 7;
    const COL_IDX_SUBREGION = 8;

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . WadCountry::COL_NAME_ID . ", " .
                WadCountry::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " . WadCountry::COL_NAME_COUNTRY_CODE . ", " .
                WadCountry::COL_NAME_COUNTRY_NAME . ", " .
                WadCountry::COL_NAME_ISO2 . ", " .
                WadCountry::COL_NAME_ISO3 . ", " .
                WadCountry::COL_NAME_REGION_CODE . ", " .
                WadCountry::COL_NAME_REGION . ", " .
                WadCountry::COL_NAME_SUBREGION_CODE . ", " .
                WadCountry::COL_NAME_SUBREGION;
    }

    public static function getArrayColumns() {
        return array(WadCountry::COL_NAME_ID,
            WadCountry::COL_NAME_COUNTRY_CODE,
            WadCountry::COL_NAME_COUNTRY_NAME,
            WadCountry::COL_NAME_ISO2,
            WadCountry::COL_NAME_ISO3,
            WadCountry::COL_NAME_REGION_CODE,
            WadCountry::COL_NAME_REGION,
            WadCountry::COL_NAME_SUBREGION_CODE,
            WadCountry::COL_NAME_SUBREGION);
    }

// </editor-fold>
}
