<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Sep 13, 2018 
 *  Filename          : WadAirportEdit.php
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

require_once("AmsWadConnection.php");
require_once("AmsWadLogger.php");
/**
 * Description of WadAirportEdit
 *
 * @author IZIordanov
 */
class WadAirportEdit {
   
// <editor-fold defaultstate="collapsed" desc="Load Data">

    public static function CountryStateStatusAirportCount() {
        $mn = "WadAirportEdit.CountryStateStatusAirportCount()";
        AmsWadLogger::logBegin($mn);
        $response = array();
        

        $sql = "  SELECT c.country_iso2, c.country_name, s.state_name as country_state,  
            count(airport_id) as airports_count,
            sum(if(a.ams_status=0,1,0)) as airports_count_ams_status_0,
            if(sum(if(a.ams_status=0,1,0))=0,1,0) as checked,
             c.country_id, s.state_id
        FROM iordanov_ams_wad.air_airport a
        left join iordanov_ams_wad.cfg_country c on c.country_id = a.country_id
        left join iordanov_ams_wad.cfg_country_state s on s.state_id = a.state_id
        left join iordanov_ams_wad.cfg_city y on y.city_id = a.city_id
        group by c.country_iso2, c.country_name, s.state_name
        order by checked, airports_count_ams_status_0, airports_count, country_name";

        try {
            $conn = AmsWadConnection::dbConnect();
            $logModel = AmsWadLogger::loggerWad()->getModule($mn);
            $ret_regions = $conn->dbExecuteSQLJson($sql, $logModel);

            if (isset($ret_regions) && count($ret_regions) > 0) {
                $response->data = $ret_regions;
            }
            else
            {
                $response = array("status" => "success", "data" => array(), "message" => "No data found.");
            }

        } catch (Exception $ex) {
            logDebug($mn, " Exception = " . $ex);
            $response = new Response($ex);
        }
        
        //AmsWadLogger::log($mn, "count(retArray)=" . count($retArray));
        AmsWadLogger::logEnd($mn);
        return $retArray;
    }
    
    // </editor-fold>
}
