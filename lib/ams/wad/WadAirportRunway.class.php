<?php 
/******************************** HEAD_BEG ************************************
*
* Project                	: ams
* Module                        : ams
* Responsible for module 	: IordIord
*
* Filename               	: WadAirportRunway.class.php
*
* Database System        	: MySQL
* Created from                  : IordIord
* Date Creation			: 21.03.2016
*------------------------------------------------------------------------------
*                        Description
*------------------------------------------------------------------------------
* @TODO Insert some description.
*
*------------------------------------------------------------------------------
*                        History
*------------------------------------------------------------------------------
* HISTORY:
* <br>--- $Log: WadAirportRunway.class.php,v $
* <br>---
* <br>---
*
********************************* HEAD_END ************************************
*/
require_once("GVDataTable.class.php");

global $km_to_ml;
global $m_to_food;

// <editor-fold defaultstate="collapsed" desc="WadAirportRunway Class">

/**
 * Description of WadAirportRunway class
 *
 * @author IordIord
 */
class WadAirportRunway {
/**
 * ***************************************************************************
 * Methods Declarations
 * ***************************************************************************
 */
    
  // <editor-fold defaultstate="collapsed" desc="Update Data">

    public static function deleteById($id) {
        $mn = "WadAirportRunway.deleteById()";
        AmsLogger::logBegin($mn);
        AmsLogger::log($mn, " id = ".$id);
        //$this->setId($id);
        $sql = "DELETE ".
                " FROM ".WadAirportRunway::TABLE_NAME." ".
                " WHERE ".WadAirportRunway::COL_NAME_ID."=?";
        $bound_params_r = ["i",$id];
         $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $deleted_id = $conn->preparedDelete($sql, $bound_params_r, $logModel);
        AmsLogger::logEnd($mn);
    }
    
    public function save() {
        $mn = "user:WadAirportRunway.save()";
        $st = AmsLogger::logBegin($mn);

        AmsLogger::log($mn, "is_object(this)=" . is_object($this));
        AmsLogger::log($mn, "ID=" . $this->getId());
        try {
            if (is_object($this)) {
                AmsLogger::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {

                    AmsLogger::log($mn, "Insert ");
                    
                    $strSQL = "INSERT INTO " . WadAirportRunway::TABLE_NAME . " (" . 
                            WadAirportRunway::COL_NAME_AIRPORT_ID . ", ".
                            WadAirportRunway::COL_NAME_RTYPE . ", ".
                            WadAirportRunway::COL_NAME_RDIRECTION . ", ".
                            WadAirportRunway::COL_NAME_RSURFACE . ", ".
                            WadAirportRunway::COL_NAME_ELEVATION_M . ", ".
                            WadAirportRunway::COL_NAME_RWIDTH_M . ", ".
                            WadAirportRunway::COL_NAME_RLENGHT_M. ") ";
                    $strSQL .= " VALUES( ?, ?, ?, ?, ?, ?, ?)";

                     $bound_params_r = array("isssiii",
                        ( ($this->airport_id == null) ? null : ($this->airport_id)),
                        ( ($this->rtype == null) ? null : ($this->rtype)),
                        ( ($this->rdirection == null) ? null : ($this->rdirection)),
                        ( ($this->rsurface == null) ? 0 : ($this->rsurface)),
                        ( ($this->elevation_m == null) ? 0 : ($this->elevation_m)),
                        ( ($this->rwidth_m == null) ? 0 : ($this->rwidth_m)),
                        ( ($this->rlenght_m == null) ? 0 : ($this->rlenght_m))
                    );
                   
                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($MN);
                    $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);

                    AmsLogger::log("$mn", "id=" . $id);
                    $this->LoadById($id);
                } else {
                    AmsLogger::log($mn, "Update ");
                    //$lngID = $this->getDT_RowId();
                    $strSQL = "UPDATE " . WadAirportRunway::TABLE_NAME;
                    $strSQL .= " SET " . WadAirportRunway::COL_NAME_AIRPORT_ID . "=?, ";
                    $strSQL .= WadAirportRunway::COL_NAME_RTYPE . "=?, ";
                    $strSQL .= WadAirportRunway::COL_NAME_RDIRECTION . "=?, ";
                    $strSQL .= WadAirportRunway::COL_NAME_RSURFACE . "=?, ";
                    $strSQL .= WadAirportRunway::COL_NAME_ELEVATION_M . "=?, ";
                    $strSQL .= WadAirportRunway::COL_NAME_RWIDTH_M . "=?, ";
                    $strSQL .= WadAirportRunway::COL_NAME_RLENGHT_M . "=?, ";
                    $strSQL .= WadAirportRunway::COL_NAME_FIXED . "=?, ";
                    $strSQL .= WadAirportRunway::COL_NAME_TIMEBAN . "=?, ";
                    $strSQL .= WadAirportRunway::COL_NAME_TIMEBAN_START_H . "=?, ";
                    $strSQL .= WadAirportRunway::COL_NAME_TIMEBAN_DURACTIONT_H . "=?, ";
                    $strSQL .= WadAirportRunway::COL_NAME_NOISE_RESTRICTION_LEVEL . "=?, ";
                   
                    
                    $strSQL .= " WHERE " . WadAirportRunway::COL_NAME_ID . "=? ";

                    $bound_params_r = array("isssiiiiiiiii",
                        ( ($this->airport_id == null) ? null : ($this->airport_id)),
                        ( ($this->rtype == null) ? null : ($this->rtype)),
                        ( ($this->rdirection == null) ? null : ($this->rdirection)),
                        ( ($this->rsurface == null) ? 0 : ($this->rsurface)),
                        ( ($this->elevation_m == null) ? 0 : ($this->elevation_m)),
                        ( ($this->rwidth_m == null) ? 0 : ($this->rwidth_m)),
                        ( ($this->rlenght_m == null) ? 0 : ($this->rlenght_m)),
                        ( ($this->fixed == null) ? 0 : ($this->fixed)),
                        ( ($this->timeban == null) ? 0 : ($this->timeban)),
                        ( ($this->timeban_start_h == null) ? 0 : ($this->timeban_start_h)),
                        ( ($this->timeban_duractiont_h == null) ? 0 : ($this->timeban_duractiont_h)),
                        ( ($this->noise_restriction_level == null) ? null : ($this->noise_restriction_level)),
                        $this->getId()
                    );
                    
                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($MN);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    AmsLogger::log($mn, "affectedRows=" . $affectedRows);
                    
                    $this->LoadById($id);
                }
            }
        } catch (Exception $ex) {
            AmsLogger::log($MN, "Error id " . $this->getId());
            AmsLogger::logError($MN, $ex);
        }
        AmsLogger::logEnd($mn);
        return $this;
    }
    
// </editor-fold>
    
    
  // <editor-fold defaultstate="collapsed" desc="Load Data">
    
    public function loadById($id)
    {
        $mn = "ams:WadAirportRunway.loadById()";
        AmsLogger::logBegin($mn);
        AmsLogger::log($mn, "id = ".$id);
        $this->setId($id);
        $sql = "SELECT ".WadAirportRunway::getAllColumnsSQL(). 
                " FROM ".WadAirportRunway::TABLE_NAME." ".
                " WHERE ".WadAirportRunway::COL_NAME_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        AmsLogger::log($mn, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //AmsLogger::log($mn, "count(data)=".count($data));
        }
        //AmsLogger::log("$mn", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        AmsLogger::log($mn, "Rw is ".$this->toString());

        AmsLogger::logEnd($mn);
    }

    public static function loadByAirportId($id)
    {
        $mn = "WadAirportRunway.loadByAirportId()";
        AmsLogger::logBegin($mn);
        AmsLogger::log($mn, "Airport id = ".$id);
        
        $sql = "SELECT ".WadAirportRunway::getAllColumnsSQL(). 
                " FROM ".WadAirportRunway::TABLE_NAME." ".
                " WHERE ".WadAirportRunway::COL_NAME_AIRPORT_ID." = ? ";

        $bound_params_r = array('i', $id);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        AmsLogger::log($mn, "count(result_r)=".count($result_r));
        
        $retArray = array();
        for ($index = 0, $max_count = sizeof( $result_r ); $index < $max_count; $index++)
        {
           $result = $result_r[$index];
           $item = new WadAirportRunway();
           $item->loadFromPosArray($result);
           $retArray[] = $item;
           //AmsLogger::log($mn, "Add Item =".$item->toString());
        }
        AmsLogger::log($mn, "count(retArray)=".count($retArray));
        AmsLogger::logEnd($mn);
        return $retArray;
        
    }

    
    
    public function loadFromArray($result) {
        $mn = "WadAirportRunway.loadFromArray()";
         AmsLogger::logBegin($mn);
        try{
            if(!$result==null) {
                AmsLogger::log($mn, "result: ".implode(",", $result));
               
                $this->setId($result[WadAirportRunway::COL_NAME_ID]);
                
                $this->airport_id = $result[WadAirportRunway::COL_NAME_AIRPORT_ID];
                $this->rtype = $result[WadAirportRunway::COL_NAME_RTYPE];
                
                $this->rdirection = $result[WadAirportRunway::COL_NAME_RDIRECTION];
                $this->rsurface = $result[WadAirportRunway::COL_NAME_RSURFACE];
                
                $this->elevation_m = $result[WadAirportRunway::COL_NAME_ELEVATION_M];
                $this->rwidth_m = $result[WadAirportRunway::COL_NAME_RWIDTH_M];
                $this->rlenght_m = $result[WadAirportRunway::COL_NAME_RLENGHT_M];
                $this->mtow_kg = $result[WadAirportRunway::COL_NAME_MTOW_KG];
                $this->fixed = $result[WadAirportRunway::COL_NAME_FIXED];
                $this->timeban = $result[WadAirportRunway::COL_NAME_TIMEBAN];
                $this->timeban_start_h = $result[WadAirportRunway::COL_NAME_TIMEBAN_START_H];
                $this->timeban_duractiont_h = $result[WadAirportRunway::COL_NAME_TIMEBAN_DURACTIONT_H];
                $this->noise_restriction_level = $result[WadAirportRunway::COL_NAME_NOISE_RESTRICTION_LEVEL];
                $this->usage_pct = $result[WadAirportRunway::COL_NAME_USAGE_PCT];
                
                
                
            }
        }
        catch(Exception $ex){AmsLogger::log($mn, "Error id ".$this->getId());AmsLogger::logError($mn, $ex);}
        AmsLogger::logEnd($mn);
    }

     public function toString()
    {
        $retValue = $this->toJSON();
        return $retValue;
    }
    
    public function toJSON() {
        return json_encode($this);
    }
    
     // </editor-fold>
    
    /**
     * ***************************************************************************
     * Getters and Setters Declarations
     * ***************************************************************************
     */
    
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters  Declarations">
    public function getId() {
        return $this->runway_id;
    }
    
    public function setId($Id) {
        $this->runway_id = $Id;
    }
    // </editor-fold>
    
   /****************************************************************************
   * Parameters Declarations
   * ***************************************************************************
   */
   
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">

    public $runway_id;
    public $airport_id;
    public $rtype;
    public $rdirection;
    public $rsurface;
    public $elevation_m;
    public $rwidth_m;
    public $rlenght_m;
    public $mtow_kg;
    public $fixed;
    
    public $timeban;
    public $timeban_start_h;
    public $timeban_duractiont_h;
    public $noise_restriction_level;
    public $usage_pct;
    
   
// </editor-fold>
    
    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME            = "iordanov_ams_wad.air_airport_runway";
    
    const COL_NAME_ID                       = "runway_id";
    const COL_NAME_AIRPORT_ID               = "airport_id";
    const COL_NAME_RTYPE                    = "rtype";
    const COL_NAME_RDIRECTION               = "rdirection";
    const COL_NAME_RSURFACE                 = "rsurface";
    const COL_NAME_ELEVATION_M              = "elevation_m";
    const COL_NAME_RWIDTH_M                 = "rwidth_m";
    const COL_NAME_RLENGHT_M                = "rlenght_m";
    const COL_NAME_MTOW_KG                  = "mtow_kg";
    const COL_NAME_FIXED                    = "fixed";
    const COL_NAME_TIMEBAN                  = "timeban";
    const COL_NAME_TIMEBAN_START_H          = "timeban_start_h";
    const COL_NAME_TIMEBAN_DURACTIONT_H     = "timeban_duractiont_h";
    const COL_NAME_NOISE_RESTRICTION_LEVEL  = "noise_restriction_level";
    const COL_NAME_USAGE_PCT                = "usage_pct";
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . WadAirportRunway::COL_NAME_ID . ", " .
                WadAirportRunway::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " .  WadAirportRunway::COL_NAME_AIRPORT_ID . ", " .
                WadAirportRunway::COL_NAME_RTYPE . ", " .
                WadAirportRunway::COL_NAME_RDIRECTION . ", " .
                WadAirportRunway::COL_NAME_RSURFACE . ", " .
                WadAirportRunway::COL_NAME_ELEVATION_M . ", " .
                WadAirportRunway::COL_NAME_RWIDTH_M . ", " .
                WadAirportRunway::COL_NAME_RLENGHT_M . ", " .
                WadAirportRunway::COL_NAME_MTOW_KG . ", " .
                WadAirportRunway::COL_NAME_FIXED . ", " .
                WadAirportRunway::COL_NAME_TIMEBAN . ", " .
                WadAirportRunway::COL_NAME_TIMEBAN_START_H . ", " .
                WadAirportRunway::COL_NAME_TIMEBAN_DURACTIONT_H . ", " .
                WadAirportRunway::COL_NAME_NOISE_RESTRICTION_LEVEL . ", " .
                WadAirportRunway::COL_NAME_USAGE_PCT;
    }


    // </editor-fold>
}


// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="WadAirportRunway GVDT">

// </editor-fold>

