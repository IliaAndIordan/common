<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Apr 15, 2019 
 *  Filename          : WadAirport.php
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2019 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of WadAirport
 *
 * @author IZIordanov
 */
class WadAirport {
    
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public function loadById($id) {
        $MN = "user:WadAirport.loadById(".$id.")";
         AmsLogger::logBegin($MN);
        AmsLogger::log($MN, "id = ".$id);
        $this->setId($id);
        $sql = "SELECT ".WadAirport::getAllColumnsSQL().
                " FROM ".WadAirport::TABLE_NAME." ".
                " WHERE ".WadAirport::COL_NAME_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($MN);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        AmsLogger::log($MN, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //AmsLogger::log($MN, "count(data)=".count($data));
        }
        //AmsLogger::log("$MN", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        AmsLogger::log($MN, "UserSettings is ".$this->toString());

        AmsLogger::logEnd($MN);
    }
    
    public function save() {
        $mn = "user:WadAirport.save()";
        $st = AmsLogger::logBegin($mn);

        AmsLogger::log($mn, "is_object(this)=" . is_object($this));
        AmsLogger::log($mn, "ID=" . $this->getId());
        try {
            if (is_object($this)) {
                AmsLogger::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {

                    AmsLogger::log($mn, "Insert ");
                    /*
                    $strSQL = "INSERT INTO " . WadAirport::TABLE_NAME . " (" . WadAirport::getAllColumnsSQL() . ") ";
                    $strSQL .= " VALUES( ?, ?, ?)";

                    $bound_params_r = array("iii",
                        $this->getId(),
                        ( ($this->getDisplayDistanceMl() == null) ? 0 : ($this->getDisplayDistanceMl()?1:0)),
                         ( ($this->getDisplayWeightLb() == null) ? 0 : ($this->getDisplayWeightLb()?1:0))
                    );
                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($MN);
                    $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);

                    AmsLogger::log("$mn", "id=" . $id);
                     
                     */
                    $this->LoadById($id);
                } else {
                    AmsLogger::log($mn, "Update ");
                    //$lngID = $this->getDT_RowId();
                    $strSQL = "UPDATE " . WadAirport::TABLE_NAME;
                    $strSQL .= " SET " . WadAirport::COL_NAME_IATA . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_ICAO . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_ANAME . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_LAT . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_LON . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_AP_TYPE_ID . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_COUNTRY_ID . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_STATE_ID . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_CITY_ID . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_ACTIVE . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_AMS_STATUS . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_HOME_LINK . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_WIKIPEDIA_LINK . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_NOTES . "=?, ";
                    $strSQL .= WadAirport::COL_NAME_UTC . "=? ";
                    
                    $strSQL .= " WHERE " . WadAirport::COL_NAME_ID . "=? ";

                    $bound_params_r = array("sssddiiiiiisssii",
                        ( ($this->iata == null) ? null : ($this->iata)),
                        ( ($this->icao == null) ? null : ($this->icao)),
                        ( ($this->apName == null) ? null : ($this->apName)),
                        ( ($this->lat == null) ? 0 : ($this->lat)),
                        ( ($this->lon == null) ? 0 : ($this->lon)),
                        ( ($this->apTypeId == null) ? 0 : ($this->apTypeId)),
                        ( ($this->countryId == null) ? 0 : ($this->countryId)),
                        ( ($this->stateId == null) ? 0 : ($this->stateId)),
                        ( ($this->cityId == null) ? 0 : ($this->cityId)),
                        ( ($this->active == null) ? 0 : ($this->active)),
                        ( ($this->amsStatusId == null) ? 0 : ($this->amsStatusId)),
                        ( ($this->homeUrl == null) ? null : ($this->homeUrl)),
                        ( ($this->wikiUrl == null) ? null : ($this->wikiUrl)),
                        ( ($this->notes == null) ? null : ($this->notes)),
                        ( ($this->utc == null) ? 0 : ($this->utc)),
                        $this->getId()
                    );


                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($MN);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    AmsLogger::log($mn, "affectedRows=" . $affectedRows);

                    //$this->LoadById($this->getId());
                }
            }
        } catch (Exception $ex) {
            AmsLogger::log($MN, "Error id " . $this->getId());
            AmsLogger::logError($MN, $ex);
        }
        AmsLogger::logEnd($mn);
        return $this;
    }

    public function loadFromArray($result) {
        $MN = "user:WadAirport.loadFromArray()";
        AmsLogger::logBegin($MN);
        try {

            if (isset($result) && count($result) > 0) {

                $this->setId($result[WadAirport::COL_NAME_ID]);

                $this->iata = $result[WadAirport::COL_NAME_IATA];
                $this->icao = $result[WadAirport::COL_NAME_ICAO];
                $this->apName = $result[WadAirport::COL_NAME_ANAME];
                $this->lat = $result[WadAirport::COL_NAME_LAT];
                $this->lon = $result[WadAirport::COL_NAME_LON];
                $this->apTypeId = $result[WadAirport::COL_NAME_AP_TYPE_ID];
                $this->countryId = $result[WadAirport::COL_NAME_COUNTRY_ID];
                $this->stateId = $result[WadAirport::COL_NAME_STATE_ID];
                $this->cityId = $result[WadAirport::COL_NAME_CITY_ID];
                $this->active = $result[WadAirport::COL_NAME_ACTIVE];
                
                $this->amsStatusId = $result[WadAirport::COL_NAME_AMS_STATUS];
                $this->homeUrl = $result[WadAirport::COL_NAME_HOME_LINK];
                $this->wikiUrl = $result[WadAirport::COL_NAME_WIKIPEDIA_LINK];
                $this->notes = $result[WadAirport::COL_NAME_NOTES];
                $this->utc = $result[WadAirport::COL_NAME_UTC];
                $this->apTypeBase = $result[WadAirport::COL_NAME_ATYPE_BASE];
                $this->active = $result[WadAirport::COL_NAME_ACTIVE];
                $this->active = $result[WadAirport::COL_NAME_ACTIVE];
                $this->setUpdated($result[WadAirport::COL_NAME_LAST_UPDATED]);

                AmsLogger::log($MN, "id " . $this->getId());
      
            }
        } catch (Exception $ex) {
            AmsLogger::log($MN, "Error id " . $this->getId());
            AmsLogger::logError($MN, $ex);
        }
        AmsLogger::logEnd($MN);
    }

    public function toString() {
        $retValue = $this->toJSON();
        return $retValue;
    }

    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    /**
     * ***************************************************************************
     * Getters and Setters Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters  Declarations">


    public function getId() {
        return $this->apId;
    }

    public function setId($userId) {
        $this->apId = $userId;
    }

    public function getUpdated() {
        $retValue = null;
        if (isset($this->lastUpdated) && $this->lastUpdated != null) {
            if (!is_object($this->lastUpdated))
                $retValue = new DateTime($this->lastUpdated);
            else
                $retValue = $this->lastUpdated;

            AmsLogger::log("getUpdated()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setUpdated($lastUpdated) {
        if (isset($lastUpdated) && $lastUpdated != null) {
            if (is_object($lastUpdated))
                $this->lastUpdated = $lastUpdated;
            else
                $this->lastUpdated = new DateTime($lastUpdated);
        }
    }
    
    // </editor-fold>

    /*     * **************************************************************************
     * Parameters Declarations
     * ***************************************************************************
     */

// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">

    public $apId;
    public $iata;
    public $icao;
    public $apName;
    public $lat;
    public $lon;
    public $apTypeId;
    public $countryId;
    public $stateId;
    public $cityId;
    public $active;
    public $amsStatusId;
    public $homeUrl;
    public $wikiUrl;
    public $notes;
    public $utc;
    public $apTypeBase;
    public $lastUpdated;
    

// </editor-fold>

    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME = "iordanov_ams_wad.air_airport";
    const COL_NAME_ID = "airport_id";
    const COL_NAME_IATA = "iata";
    const COL_NAME_ICAO = "icao";
    const COL_NAME_ANAME = "aname";
    const COL_NAME_LAT = "lat";
    const COL_NAME_LON = "lon";
    const COL_NAME_AP_TYPE_ID = "ap_type_id";
    const COL_NAME_COUNTRY_ID = "country_id";
    const COL_NAME_STATE_ID = "state_id";
    const COL_NAME_CITY_ID = "city_id";
    const COL_NAME_ACTIVE = "active";
    const COL_NAME_AMS_STATUS = "ams_status";
    const COL_NAME_HOME_LINK = "home_link";
    const COL_NAME_WIKIPEDIA_LINK = "wikipedia_link";
    const COL_NAME_NOTES = "notes";
    const COL_NAME_UTC = "utc";
    const COL_NAME_ATYPE_BASE = "atype_base";
    const COL_NAME_LAST_UPDATED = "last_updated";
    
    const SQL_SELECT_IAirportModel = "SELECT a.airport_id as apId, a.icao, a.iata, a.aname as apName, a.lat, a.lon, t.ap_type_name as apType, st.ams_status_name as apStatus,
            a.home_link as homeUrl, a.wikipedia_link wikiUrl, a.notes, a.utc, a.atype_base apTypeBase,
            a.active, a.ap_type_id as apTypeId, st.ams_status_id as apStatusId,
            c.region, c.sub_region as sunRegion, c.country_icao as countryIcao, c.wikilink as countryWikiLink,
            c.country_name as countryName, c.country_iso2 as countryIso, cst.state_name as stateName, 
            ci.city_name as cityName, ci.population cityPopulation, ci.wiki_url as cityWikiUrl,
            a.country_id as countryId, a.state_id as stateId, a.city_id as cityId,
            r.svg_path_circle as apSvgCircle, r.rw_lines as apSvgRwLines,
            ad.max_rlenght_m as rwLenghtMaxM, ad.max_mtow_kg as rwMtowMaxKg
            FROM iordanov_ams_wad.air_airport a 
            left join iordanov_ams_wad.cfg_airport_type t on t.ap_type_id = a.ap_type_id
            left join iordanov_ams_wad.cfg_airport_ams_status st on st.ams_status_id = a.ams_status
            left join iordanov_ams_wad.cfg_country c on c.country_id = a.country_id
            left join iordanov_ams_wad.cfg_country_state cst on cst.state_id = a.state_id
            left join iordanov_ams_wad.cfg_city ci on ci.city_id = a.city_id
            left join iordanov_ams_wad.v_air_airport_runway_svg_path r on r.airport_id = a.airport_id
            left join iordanov_ams_wad.air_airport_details ad on ad.airport_id = a.airport_id
            where a.airport_id = ?
            order by apTypeId desc, icao, iata, apName";
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_ID . ", " .
                WadAirport::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " . WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_IATA . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_ICAO . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_ANAME . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_LAT . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_LON . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_AP_TYPE_ID . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_COUNTRY_ID . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_STATE_ID . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_CITY_ID . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_ACTIVE . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_AMS_STATUS . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_HOME_LINK . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_WIKIPEDIA_LINK . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_NOTES . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_UTC . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_ATYPE_BASE . ", " .
                WadAirport::TABLE_NAME . "." . WadAirport::COL_NAME_LAST_UPDATED;
    }

    // </editor-fold>
}
