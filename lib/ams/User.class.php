<?php 
/******************************** HEAD_BEG ************************************
*
* Project                	: isgt
* Module                        : user
* Responsible for module 	: IordIord
*
* Filename               	: User.class.php
*
* Database System        	: ORCL, MySQL
* Created from                  : IordIord
* Date Creation			: 14.10.2016
*------------------------------------------------------------------------------
*                        Description
*------------------------------------------------------------------------------
* @TODO Insert some description.
*
*------------------------------------------------------------------------------
*                        History
*------------------------------------------------------------------------------
* HISTORY:
* <br>--- $Log: User.class.php,v $
* <br>---
* <br>---
*
********************************* HEAD_END ************************************
*/

require_once 'UserSettings.class.php';
/**
 * Description of User class
 *
 * @author IordIord
 */
class User {

/**
 * ***************************************************************************
 * Methods Declarations
 * ***************************************************************************
 */
    /**
     *
     * @param <type> $id 
     */
    public function loadById($id) {
        $MN = "user:User.loadById()";
         AmsLogger::logBegin($MN);
        AmsLogger::log($MN, "id = ".$id);
        $this->setDT_RowId($id);
        $sql = "SELECT ".User::getAllColumnsSQL(). ", " . User::COL_NAME_UPDATED.
                " FROM ".User::TABLE_NAME." ".
                " WHERE ".User::COL_NAME_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($MN);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        AmsLogger::log($MN, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //AmsLogger::log($MN, "count(data)=".count($data));
        }
        //AmsLogger::log("$MN", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        AmsLogger::log($MN, "User is ".$this->toString());

        AmsLogger::logEnd($MN);
    }
    
    public static function login ($user_email, $user_password)
    {
        $MN="user:User.login()";
         AmsLogger::logBegin($MN);
        $out = null;
        $loginName = validate_search_string($user_email);
        $password = validate_search_string($user_password);

        //$user_email = base64_encode($loginName);
        //$user_password = md5($password);

        $sqlStr ="SELECT ".User::COL_NAME_ID." FROM ".User::TABLE_NAME.
                    " WHERE ".User::COL_NAME_EMAIL."=? ".
                    " AND ".User::COL_NAME_PSW."=?";
        $bound_params_r = array('ss', $user_email, $user_password);

        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($MN);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        if(count($result_r)>0)
        {
           
            AmsLogger::log($MN, prArr($result_r[0]));
            $id = $result_r[0][User::COL_NAME_ID];
            AmsLogger::log($MN, "Found userId".$id." for E-Mail ".$user_email."!");
            $ipAddress = $_SERVER['REMOTE_ADDR']; 
            
            if ($id>-1)
            {
                $out = new User();
                $out->loadById($id);
                $out->setIpAddress($ipAddress);
                $out->setLastLogged(CurrenDateTime());
                $out->save();
            }
        }
        
        AmsLogger::logEnd($MN);
        return $out;
    }
    
    /**
     *
     * @param <type> $email
     * @return <type> 
     */
    public static function isExistEMail ($email) {
        $MN="user.User:isExistEMail()";
         AmsLogger::logBegin($MN);
        $user_email = validate_search_string($email);

        $sqlStr ="SELECT count(*) AS Rows FROM ".
            User::TABLE_NAME." WHERE ".User::COL_NAME_EMAIL."=?";

        $bound_params_r = array('s', $user_email);
        AmsLogger::log($MN,$sqlStr);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($MN);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        AmsLogger::log($MN, prArr($result_r[0]));
        $nrRows = $result_r[0]["Rows"];
        AmsLogger::log($MN, "Found ".$nrRows." users with E-Mail ".$user_email."!");
        AmsLogger::logEnd($MN);
        return ($nrRows>0?TRUE:FALSE);
    }

    public static function forgotenPassword($email, $conf)
    {
        $MN="user.User:forgotenPassword()";
        $st = AmsLogger::logBegin($MN);
        $id = -1;
        $retValue = false;
        $user_email = validate_search_string($email);
        if(User::isExistEMail($user_email)>0)
        {
            $sqlStr ="SELECT ".User::COL_NAME_ID." FROM ".
                User::TABLE_NAME." WHERE ".User::COL_NAME_EMAIL."=?";

            $bound_params_r = array('s', $user_email);
            AmsLogger::log($MN,$sqlStr);
            $conn = AmsConnection::dbConnect();
            $logModel = AmsLogger::currLogger()->getModule($MN);
            $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
            AmsLogger::log($MN, prArr($result_r[0]));
            $id = $result_r[0][User::COL_NAME_ID];
            AmsLogger::log($MN, "Found ".$id." user ID with E-Mail ".$user_email."!");
            if($id>0)
            {
                $user = new User();
                $user->loadById($id);
                $retValue = sendMailRegistration($user, $conf);
            }
        }
        AmsLogger::logEnd($MN);
        return $retValue;
    }
    /**
     *
     * @param <type> $id
     */
    public static function deleteById($id) {
        $MN = "user:User.deleteById()";
         AmsLogger::logBegin($MN);
        AmsLogger::log($MN, "id = ".$id);
        //$this->setId($id);
        $sql = "DELETE ".
                " FROM ".User::TABLE_NAME." ".
                " WHERE ".User::COL_NAME_ID."=?";
        $bound_params_r = array("i",$id);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($MN);
        $id = $conn->preparedDelete($sql, $bound_params_r, $logModel);

        AmsLogger::logEnd($MN);
    }
    
    public static function loadAll()
    {
        $mn = "user:User.loadAll()";
        $st = AmsLogger::logBegin($mn);


        $sql = "SELECT ".User::getAllColumnsSQL() . ", " . User::COL_NAME_UPDATED.
                " FROM ".User::TABLE_NAME." ";

        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($MN);
        $data = $conn->dbExecuteSQL($sql, $logModel);
        AmsLogger::log($mn, "count(data)=".count($data));
        $retArray = array();
        for ($index = 0, $max_count = sizeof( $data ); $index < $max_count; $index++)
        {
           $result = $data[$index];
           $item = new User();
           $item->loadFromPosArray($result);
           $retArray[] = $item;
           AmsLogger::log($mn, "Add Item =".$item->toString());
        }

        AmsLogger::log($mn, "count(retArray)=".count($retArray));
        AmsLogger::logEnd($mn);
        return $retArray;
    }
    
    public static function CreateRowData($eMail, $password) {
        $MN = "user:User.CreateRowData()";
         AmsLogger::logBegin($MN);

        $rowData = new User();
        
        $rowData->setEMail($eMail);
        $rowData->setPassword($password);
        $rowData->setRole(0);
        $rowData->setIsReceiveEMails(1);
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $rowData->setIpAddress($ipAddress);
        
        $rowData->save();
        AmsLogger::log($MN, "after save.");
        AmsLogger::logEnd($MN);
        return $rowData;
    }

    public function save() {
        $mn = "user:User.save()";
        $st = AmsLogger::logBegin($mn);

        AmsLogger::log($mn, "is_object(this)=" . is_object($this));
        AmsLogger::log($mn, "ID=" . $this->getDT_RowId());
        try {
            if (is_object($this)) {
                AmsLogger::log($mn, "ID=" . $this->getDT_RowId());
                if ($this->getDT_RowId() === null || $this->getDT_RowId() === "") {

                    AmsLogger::log($mn, "Insert ");
                    $strSQL = "INSERT INTO " . User::TABLE_NAME . " (" . User::getAllColumnsNoIdSQL() . ") ";
                    $strSQL .=" VALUES( ?, ?, ?, ?, ?, ?)";

                    $bound_params_r = array("ssiiss",
                        (($this->getEMail() == null) ? null : $this->getEMail()),
                        (($this->getPassword() == null) ? null : $this->getPassword()),
                        ($this->getRole()),
                        ($this->isReceiveEMails()),
                        (($this->getIpAddress() == null) ? null : $this->getIpAddress()),
                        (($this->getLastLogged() == null) ? null : $this->getLastLogged()->format("Y-m-d H:i:s"))
                    );
                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($MN);
                    $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);

                    AmsLogger::log("$mn", "id=" . $id);
                    $this->LoadById($id);
                } else {
                    AmsLogger::log($mn, "Update ");
                    //$lngID = $this->getDT_RowId();
                    $strSQL = "UPDATE " . User::TABLE_NAME;
                    $strSQL .=" SET " . User::COL_NAME_EMAIL . "=?, ";
                    $strSQL .=User::COL_NAME_PSW . "=?, ";
                    $strSQL .=User::COL_NAME_ROLE . "=?, ";
                    $strSQL .=User::COL_NAME_IS_RECEIVE_EMAILS . "=?, ";
                    $strSQL .=User::COL_NAME_IP_ADDRESS . "=?, ";
                    $strSQL .=User::COL_NAME_LAST_LOGED . "=? ";

                    $strSQL .=" WHERE " . User::COL_NAME_ID . "=? ";

                    $bound_params_r = array("ssiissi",
                        (($this->getEMail() == null) ? null : $this->getEMail()),
                        (($this->getPassword() == null) ? null : $this->getPassword()),
                        ($this->getRole()),
                        ($this->isReceiveEMails()),
                        (($this->getIpAddress() == null) ? null : $this->getIpAddress()),
                        (($this->getLastLogged() == null) ? null : $this->getLastLogged()->format("Y-m-d H:i:s")),
                        $this->getDT_RowId()
                    );


                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($MN);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    AmsLogger::log($mn, "affectedRows=" . $affectedRows);

                    //$this->LoadById($this->getDT_RowId());
                }
            }
        } catch (Exception $ex) {
            AmsLogger::log($MN, "Error id " . $this->getDT_RowId());
            AmsLogger::logError($MN, $ex);
        }
        AmsLogger::logEnd($mn);
        return $this;
    }

    /**
     *
     * @param <type> $result 
     */
    public function loadFromArray($result) {
        $MN = "user:User.loadFromArray()";
         AmsLogger::logBegin($MN);
        try{
            
             if(isset($result) && count($result)>0) {
                 
                $this->setDT_RowId($result[User::COL_NAME_ID]);
                
                $this->setEMail($result[User::COL_NAME_EMAIL]);
                
                $this->setPassword($result[User::COL_NAME_PSW]);
                $this->setRole($result[User::COL_NAME_ROLE]);
                
                $this->setIsReceiveEMails($result[User::COL_NAME_IS_RECEIVE_EMAILS]);
                
                $this->setIpAddress($result[User::COL_NAME_IP_ADDRESS]);
                
                $this->setLastLogged($result[User::COL_NAME_LAST_LOGED]);
                $this->setUpdated($result[User::COL_NAME_UPDATED]);
                
                AmsLogger::log($MN, "id ".$this->getDT_RowId());
            }
            
        }
        catch(Exception $ex){AmsLogger::log($MN, "Error id ".$this->getDT_RowId());AmsLogger::logError($MN, $ex);}
        AmsLogger::logEnd($MN);

    }

    public function loadFromPosArray($result) {
        $MN = "user:User.loadFromPosArray()";
         AmsLogger::logBegin($MN);
        if(isset($result) && count($result)>0){
            $this->setDT_RowId($result[User::COL_IXD_ID]);
            $this->setEMail($result[User::COL_IDX_EMAIL]);
            $this->setPassword($result[User::COL_IDX_PSW]);
            $this->setRole($result[User::COL_IDX_ROLE]);
            
            $this->setIsReceiveEMails($result[User::COL_IDX_IS_RECEIVE_EMAILS]);
            $this->setIpAddress($result[User::COL_IDX_IP_ADDRESS]);
            $this->setLastLogged($result[User::COL_IDX_LAST_LOGED]);
            $this->setUpdated($result[User::COL_IDX_UPDATED]);
        }
        AmsLogger::logEnd($MN);

    }

    public function toString()
    {
         $retValue = $this->toJSON();
         /*
        $retValue = "User Instance with:";
        $retValue .="[id=".$this->getDT_RowId()."], ";
        
        $retValue .="[EMail=".$this->getEMail()."], ";
        $retValue .="[Password=".$this->getPassword()."], ";
        $retValue .="[SecondName=".$this->getRole()."], ";
        
        $retValue .="[IsReceiveEMails=".$this->getIsReceiveEMails()."], ";
        $retValue .="[getIpAddress=".$this->getIpAddress()."], ";
        
        if (isset($this->lastLogged) && $this->lastLogged != null)
            $retValue .="[lastLogged=".$this->getLastLogged()->format("Y-m-d H:i:s")."], ";
        if (isset($this->updated) && $this->updated != null)
            $retValue .="[updated=".$this->getUpdated()->format("Y-m-d H:i:s")."], ";
        */
        
        return $retValue;
    }
    
    public function toJSON() {
        return json_encode($this);
    }
    /**
     * ***************************************************************************
     * Getters and Setters Declarations
     * ***************************************************************************
     */
    
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters  Declarations">
    
    public function getDT_RowId() {
        return $this->DT_RowId;
    }

    public function getId() {
        return $this->DT_RowId;
    }
    
    public function setDT_RowId($DT_RowId) {
        $this->DT_RowId = $DT_RowId;
    }

    public function getName() {
        $retValue = $this->eMail;
        if(isset($this->eMail))
        {
           $valueArr = explode( '@', $this->eMail ,2);
           if(sizeof($valueArr)>0)
            $retValue  = $valueArr[0];
        }
        return $retValue;
    }
    
    public function getEMail() {
        return $this->eMail;
    }

    public function setEMail($eMail) {
        $this->eMail = $eMail;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }

    public function getRole() {
        return $this->role;
    }
    
    public function getRoleName() {
        $retValue = "Airline Manager";
        if ($this->role === 1)
            $retValue = "Administrator";
        
        return $retValue;
    }

    public function setRole($role) {
        $this->role = $role;
    }
    
    public function isAdmin() {
        if ($this->role === 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getIsReceiveEMails() {
        return $this->isReceiveEMails;
    }

    public function setIsReceiveEMails($isReceiveEMails) {
        $this->isReceiveEMails = $isReceiveEMails;
    }

    public function isReceiveEMails() {
        if ($this->isReceiveEMails > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getIpAddress() {
        return $this->ipAddress;
    }

    public function setIpAddress($ipAddress) {
        $this->ipAddress = $ipAddress;
    }

    public function getLastLogged() {
        $retValue = null;
        if (isset($this->lastLogged) && $this->lastLogged != null) {
            if (!is_object($this->lastLogged))
                $retValue = new DateTime($this->lastLogged);
            else
                $retValue = $this->lastLogged;

            AmsLogger::log("getLastLogged()", "retValue=", getDateStr($retValue));
        }
        else
            $retValue = new DateTime();
        return $retValue;
    }
    
     public function getLastLoggedDbStr() {
        $retValue = null;
        if (isset($this->lastLogged) && $this->lastLogged != null) {
            if (!is_object($this->lastLogged)){
                $value = new DateTime($this->lastLogged);
                $retValue = $value->format("Y-m-d H:i:s");
            }
            else
                $retValue = $this->lastLogged->format("Y-m-d H:i:s");

            AmsLogger::log("getLastLogged()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setLastLogged($lastLogged) {
         if (isset($lastLogged) && $lastLogged != null) {
            if (is_object($lastLogged))
                $this->lastLogged = $data;
            else
                $this->lastLogged = new DateTime($lastLogged);
        }
        
    }

    public function getUpdated() {
        $retValue = null;
        if (isset($this->updated) && $this->updated != null) {
            if (!is_object($this->updated))
                $retValue = new DateTime($this->updated);
            else
                $retValue = $this->updated;

            AmsLogger::log("getUpdated()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setUpdated($updated) {
        if (isset($data) && $data != null) {
            if (is_object($data))
                $this->updated = $data;
            else
                $this->updated = new DateTime($data);
        }
    }

    // </editor-fold>
    
   /****************************************************************************
   * Parameters Declarations
   * ***************************************************************************
   */
   
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">

    public $DT_RowId;
    public $eMail;
    public $password;
    public $role = 0; //2 predefined roles 0-user, 1 admin, 2 editor
    
    public $isReceiveEMails = null;
    public $ipAddress;
    public $lastLogged = 0;
    public $updated = 0;

// </editor-fold>
    
    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME                = "a_user";
    const COL_NAME_ID               = "id";
    const COL_NAME_EMAIL            = "e_mail";
    const COL_NAME_PSW              = "password";
    const COL_NAME_ROLE             = "role";
    const COL_NAME_IS_RECEIVE_EMAILS = "is_receive_emails";
    const COL_NAME_IP_ADDRESS       = "ip_address";
    const COL_NAME_LAST_LOGED       = "last_loged";
    const COL_NAME_UPDATED          = "updated";
    
    const COL_IXD_ID                = 0;
    const COL_IDX_EMAIL             = 1;
    const COL_IDX_PSW               = 2;
    const COL_IDX_ROLE              = 3;
    const COL_IDX_IS_RECEIVE_EMAILS = 4;
    const COL_IDX_IP_ADDRESS        = 5;
    const COL_IDX_LAST_LOGED        = 6;
    const COL_IDX_UPDATED           = 7;

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . User::TABLE_NAME . "." . User::COL_NAME_ID . ", " .
                User::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " . User::TABLE_NAME . "." . User::COL_NAME_EMAIL . ", " .
                User::TABLE_NAME . "." . User::COL_NAME_PSW . ", " .
                User::TABLE_NAME . "." . User::COL_NAME_ROLE . ", " .
                User::TABLE_NAME . "." . User::COL_NAME_IS_RECEIVE_EMAILS . ", " .
                User::TABLE_NAME . "." . User::COL_NAME_IP_ADDRESS . ", " .
                User::TABLE_NAME . "." . User::COL_NAME_LAST_LOGED;
    }

    public static function getArrayColumns() {
        return array(User::COL_NAME_ID,
            User::COL_NAME_EMAIL,
            User::COL_NAME_PSW,
            User::COL_NAME_ROLE,
            User::COL_NAME_IS_RECEIVE_EMAILS,
            User::COL_NAME_IP_ADDRESS,
            User::COL_NAME_LAST_LOGED,
            User::COL_NAME_UPDATED);
    }

    // </editor-fold>
}

