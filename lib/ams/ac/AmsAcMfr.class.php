<?php 
/******************************** HEAD_BEG ************************************
*
* Project                	: ams
* Module                        : ams
* Responsible for module 	: IordIord
*
* Filename               	: AmsAcMfr.class.php
*
* Database System        	: MySQL
* Created from                  : IordIord
* Date Creation			: 21.03.2016
*------------------------------------------------------------------------------
*                        Description
*------------------------------------------------------------------------------
* @TODO Insert some description.
*
*------------------------------------------------------------------------------
*                        History
*------------------------------------------------------------------------------
* HISTORY:
* <br>--- $Log: AmsAcMfr.class.php,v $
* <br>---
* <br>---
*
********************************* HEAD_END ************************************
*/
require_once("GVDataTable.class.php");
//require_once("Airport.class.php");
global $km_to_ml;
global $m_to_food;

// <editor-fold defaultstate="collapsed" desc="AmsAcMfr Class">

/**
 * Description of AmsAcMfr class
 *
 * @author IordIord
 */
class AmsAcMfr {
/**
 * ***************************************************************************
 * Methods Declarations
 * ***************************************************************************
 */
    
  // <editor-fold defaultstate="collapsed" desc="Update Data">

    public function save() {
        $mn = "AmsAcMfr->save()";
        AmsLogger::logBegin($mn);

        AmsLogger::log($mn, "is_object(this)=" . is_object($this));
        
        if (is_object($this)) {
            try {
                AmsLogger::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {
                    AmsLogger::log($mn,  "Insert ");
                    $strSQL = "INSERT INTO " . AmsAcMfr::TABLE_NAME . " (".
                    AmsAcMfr::COL_NAME_MFR_NAME . ", ".
                    AmsAcMfr::COL_NAME_HEADQUARTIER . ", ".
                    AmsAcMfr::COL_NAME_NOTES . ", ".
                    AmsAcMfr::COL_NAME_WIKIURL . ", ".
                    AmsAcMfr::COL_NAME_WEBURL . ", ".
                    AmsAcMfr::COL_NAME_LOGOIMG . ", ".
                    AmsAcMfr::COL_NAME_HQ_AIRPORT_ID . ", ".
                    AmsAcMfr::COL_NAME_IATA . ") ".
                    " VALUES(?, ?, ?, ?, ?, ?, ?, ?)";

                    $bound_params_r = ["ssssssis",
                        (($this->mfr_name == null) ? null : $this->mfr_name),
                        (($this->mfr_headquartier == null) ? null : $this->mfr_headquartier),
                        (($this->mfr_notes == null) ? null : $this->mfr_notes),
                        (($this->mfr_wikilink == null) ? null : $this->mfr_wikilink),
                        (($this->mfr_web == null) ? null : $this->mfr_web),
                        (($this->mfr_logo == null) ? null : $this->mfr_logo),
                        (($this->airport_id == null) ? null : $this->airport_id),
                        (($this->mfr_iata == null) ? null : $this->mfr_iata),
                    ];
                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($mn);
                    $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
                
                    AmsLogger::log("$mn", "id=" . $id);
                    $this->LoadById($id);
                } else {
                    AmsLogger::log($mn,  "Update ");

                    $strSQL = "UPDATE " . AmsAcMfr::TABLE_NAME;
                    $strSQL .=" SET " . AmsAcMfr::COL_NAME_MFR_NAME . "=?, ";
                    $strSQL .=AmsAcMfr::COL_NAME_HEADQUARTIER . "=?, ";
                    $strSQL .=AmsAcMfr::COL_NAME_NOTES . "=?, ";
                    $strSQL .=AmsAcMfr::COL_NAME_WIKIURL . "=?, ";
                    $strSQL .=AmsAcMfr::COL_NAME_WEBURL . "=?, ";
                    $strSQL .=AmsAcMfr::COL_NAME_LOGOIMG . "=?, ";
                    $strSQL .=AmsAcMfr::COL_NAME_HQ_AIRPORT_ID . "=? ";
                    $strSQL .=AmsAcMfr::COL_NAME_IATA . "=? ";
                    
                    $strSQL .=" WHERE " . AmsAcMfr::COL_NAME_ID . " = ? ";
                    
                    $bound_params_r = ["ssssssisi",
                        (($this->mfr_name == null) ? null : $this->mfr_name),
                        (($this->mfr_headquartier == null) ? null : $this->mfr_headquartier),
                        (($this->mfr_notes == null) ? null : $this->mfr_notes),
                        (($this->mfr_wikilink == null) ? null : $this->mfr_wikilink),
                        (($this->mfr_web == null) ? null : $this->mfr_web),
                        (($this->mfr_logo == null) ? null : $this->mfr_logo),
                        (($this->airport_id == null) ? null : $this->airport_id),
                        (($this->mfr_iata == null) ? null : $this->mfr_iata),
                        $this->getId()
                    ];

                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($mn);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    AmsLogger::log($mn, "affectedRows=" . $affectedRows);
                    
                }
            } catch (Exception $ex) {
                AmsLogger::log($mn, "Error id " . $this->getId());
                AmsLogger::logError($mn, $ex);
            }
        }
        AmsLogger::logEnd($mn);
        return $this;
    }
    
    public static function deleteById($id) {
        $mn = "AmsAcMfr.deleteById()";
        AmsLogger::logBegin($mn);
        AmsLogger::log($mn, " id = ".$id);
        //$this->setId($id);
        $sql = "DELETE ".
                " FROM ".AmsAcMfr::TABLE_NAME." ".
                " WHERE ".AmsAcMfr::COL_NAME_ID."=?";
        $bound_params_r = ["i",$id];
         $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $deleted_id = $conn->preparedDelete($sql, $bound_params_r, $logModel);
        AmsLogger::logEnd($mn);
    }

// </editor-fold>
    
    
  // <editor-fold defaultstate="collapsed" desc="Load Data">
    
    
    public function loadById($id)
    {
        $mn = "ams:AmsAcMfr.loadById()";
        AmsLogger::logBegin($mn);
        AmsLogger::log($mn, "id = ".$id);
        $this->setId($id);
        $sql = "SELECT ".AmsAcMfr::getAllColumnsSQL().
                " FROM ".AmsAcMfr::TABLE_NAME." ".
                " WHERE ".AmsAcMfr::COL_NAME_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        AmsLogger::log($mn, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //AmsLogger::log($mn, "count(data)=".count($data));
        }
        //AmsLogger::log("$mn", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        AmsLogger::log($mn, "Airline is ".$this->toString());

        AmsLogger::logEnd($mn);
    }
    
    public function loadFromArray($result) {
        $mn = "AmsAcMfr.loadFromArray()";
         AmsLogger::logBegin($mn);
        try{
            if(!$result==null) {
                AmsLogger::log($mn, "result: ".implode(",", $result));
               
                $this->mfr_id = ($result[AmsAcMfr::COL_NAME_ID]);
                $this->mfr_name = ($result[AmsAcMfr::COL_NAME_MFR_NAME]);
                $this->mfr_headquartier = ($result[AmsAcMfr::COL_NAME_HEADQUARTIER]);
                $this->mfr_notes = ($result[AmsAcMfr::COL_NAME_NOTES]);
                $this->mfr_wikilink = ($result[AmsAcMfr::COL_NAME_WIKIURL]);
                $this->mfr_web = ($result[AmsAcMfr::COL_NAME_WEBURL]);
                $this->mfr_logo = ($result[AmsAcMfr::COL_NAME_LOGOIMG]);
                $this->airport_id = ($result[AmsAcMfr::COL_NAME_HQ_AIRPORT_ID]);
                $this->mfr_iata = ($result[AmsAcMfr::COL_NAME_IATA]);
            }
        }
        catch(Exception $ex){AmsLogger::log($mn, "Error id ".$this->getId());AmsLogger::logError($mn, $ex);}
        AmsLogger::logEnd($mn);
    }

    public function toString()
    {
        $retValue = $this->toJSON();
        return $retValue;
    }
    
    public function toJSON() {
        return json_encode($this);
    }
    
     // </editor-fold>
    
  
   /****************************************************************************
   * Parameters Declarations
   * ***************************************************************************
   */
   
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">
    
    public function getId() {
        return $this->mfr_id;
    }
    
    public function setId($id) {
        $this->mfr_id = $id;
    }

    public $mfr_id;
    public $mfr_name;
    public $mfr_headquartier;
    public $mfr_notes;
    public $mfr_wikilink;
    public $mfr_web;
    public $mfr_logo;
    public $airport_id;
    public $mfr_iata;
   
// </editor-fold>
    
    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME            = "iordanov_ams_ac.cfg_manufacturer";
    
    const COL_NAME_ID           = "mfr_id";
    const COL_NAME_MFR_NAME     = "mfr_name";
    const COL_NAME_HEADQUARTIER = "mfr_headquartier";
    const COL_NAME_NOTES        = "mfr_notes";
    const COL_NAME_WIKIURL      = "mfr_wikilink";
    const COL_NAME_WEBURL       = "mfr_web";
    const COL_NAME_LOGOIMG      = "mfr_logo";
    const COL_NAME_HQ_AIRPORT_ID= "airport_id";
    const COL_NAME_IATA         = "mfr_iata";
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . AmsAcMfr::COL_NAME_ID . ", " .
                AmsAcMfr::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " .  AmsAcMfr::COL_NAME_MFR_NAME . ", " .
                AmsAcMfr::COL_NAME_HEADQUARTIER . ", " .
                AmsAcMfr::COL_NAME_NOTES . ", " .
                AmsAcMfr::COL_NAME_WIKIURL . ", " .
                AmsAcMfr::COL_NAME_WEBURL . ", " .
                AmsAcMfr::COL_NAME_LOGOIMG . ", " .
                AmsAcMfr::COL_NAME_HQ_AIRPORT_ID . ", " .
                 AmsAcMfr::COL_NAME_IATA;
    }

    // </editor-fold>
}


// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="AmsAcMfr GVDT">

// </editor-fold>

