<?php 
/******************************** HEAD_BEG ************************************
*
* Project                	: ams
* Module                        : ams
* Responsible for module 	: IordIord
*
* Filename               	: AmsMac.class
*
* Database System        	: MySQL
* Created from                  : IordIord
* Date Creation			: 21.03.2019
*------------------------------------------------------------------------------
*                        Description
*------------------------------------------------------------------------------
* @TODO Insert some description.
*
*------------------------------------------------------------------------------
*                        History
*------------------------------------------------------------------------------
* HISTORY:
* <br>--- $Log: AmsMac.class.php,v $
* <br>---
* <br>---
*
********************************* HEAD_END ************************************
*/
require_once("GVDataTable.class.php");
//require_once("Airport.class.php");
global $km_to_ml;
global $m_to_food;

// <editor-fold defaultstate="collapsed" desc="AmsMac Class">

/**
 * Description of AmsMac class
 *
 * @author IordIord
 */
class AmsMac {
/**
 * ***************************************************************************
 * Methods Declarations
 * ***************************************************************************
 */
    
  // <editor-fold defaultstate="collapsed" desc="Update Data">

    public function save() {
        $mn = "AmsMac->save()";
        AmsLogger::logBegin($mn);

        AmsLogger::log($mn, "is_object(this)=" . is_object($this));
        
        if (is_object($this)) {
            try {
                AmsLogger::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {
                    AmsLogger::log($mn,  "Insert ");
                    $strSQL = "INSERT INTO " . AmsMac::TABLE_NAME . " (".
                             AmsMac::COL_NAME_MFR_ID . ", " .
                            AmsMac::COL_NAME_MODEL . ", " .
                            AmsMac::COL_NAME_AC_TYPE_ID . ", " .
                            AmsMac::COL_NAME_MAX_RANGE_KM . ", " .
                            AmsMac::COL_NAME_FUEL_TANK_L . ", " .
                            
                            AmsMac::COL_NAME_PILOTS . ", " .
                            AmsMac::COL_NAME_CREW_SEATS . ", " .
                            AmsMac::COL_NAME_MAX_SEATING . ", " .
                            AmsMac::COL_NAME_USEFUL_LOAD_KG . ", " .
                            AmsMac::COL_NAME_PRICE . ", " .
                            
                            AmsMac::COL_NAME_AVG_FUEL_CONSUMPTION_LP100KM . ", " .
                            AmsMac::COL_NAME_MAX_CRUISE_ALTITUDE_M . ", " .
                            AmsMac::COL_NAME_CRUISE_SPEED_KMPH . ", " .
                            AmsMac::COL_NAME_TAKE_OFF_M . ", " .
                            AmsMac::COL_NAME_LANDING_M . ", " .
                            
                            AmsMac::COL_NAME_OPERATION_TIME . ", " .
                            AmsMac::COL_NAME_COST_PER_FH . ", " .
                            AmsMac::COL_NAME_ASMI_RATE . ", " .
                            AmsMac::COL_NAME_WIKI_LINK . ", " .
                            AmsMac::COL_NAME_POWERPLANT . ", " .
                            
                            AmsMac::COL_NAME_PRODUCTION_START . ", " .
                            AmsMac::COL_NAME_PRODUCTION_RATE_H . ", " .
                            AmsMac::COL_NAME_LAST_PRODUCED_ON . ", " .
                            AmsMac::COL_NAME_NOTES . ", " .
                            AmsMac::COL_NAME_POPULARITY . ", " .
                            
                            AmsMac::COL_NAME_NUMBER_BUILD . ", " .
                            AmsMac::COL_NAME_MTWO_KG . ", " .
                            AmsMac::COL_NAME_MTOW_EMPTY . ", " .
                            AmsMac::COL_NAME_SEATS_PER_ROW . ", " .
                            AmsMac::COL_NAME_CABIN_CONFIG_BASE_PRICE.
                             ") ".
                    " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, "
                            . "?, ?, ?, ?, ?, ?, ?, ?, ?, ?,"
                            . " ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                    $bound_params_r = ["isiiiiiiiddiiiiiiisssissiiddid",
                        (($this->mfr_id == null) ? null : $this->mfr_id),
                        (($this->model == null) ? null : $this->model),
                        (($this->ac_type_id == null) ? null : $this->ac_type_id),
                        (($this->max_range_km == null) ? null : $this->max_range_km),
                        (($this->fuel_tank_l == null) ? null : $this->fuel_tank_l),
                        
                        (($this->pilots == null) ? null : $this->pilots),
                        (($this->crew_seats == null) ? null : $this->crew_seats),
                        (($this->max_seating == null) ? null : $this->max_seating),
                        (($this->useful_load_kg == null) ? null : $this->useful_load_kg),
                        (($this->price == null) ? null : $this->price),
                        
                        (($this->avg_fuel_consumption_lp100km == null) ? null : $this->avg_fuel_consumption_lp100km),
                        (($this->max_cruise_altitude_m == null) ? null : $this->max_cruise_altitude_m),
                        (($this->cruise_speed_kmph == null) ? null : $this->cruise_speed_kmph),
                        (($this->take_off_m == null) ? null : $this->take_off_m),
                        (($this->landing_m == null) ? null : $this->landing_m),
                        
                        (($this->operation_time == null) ? null : $this->operation_time),
                        (($this->cost_per_fh == null) ? null : $this->cost_per_fh),
                        (($this->asmi_rate == null) ? null : $this->asmi_rate),
                        (($this->wiki_link == null) ? null : $this->wiki_link),
                        (($this->powerplant == null) ? null : $this->powerplant),
                        
                        (($this->production_start == null) ? null : DateTimeDbStr(StrToDateObj($this->production_start))),
                        (($this->production_rate_h == null) ? null : $this->production_rate_h),
                        (($this->last_produced_on == null) ? null : DateTimeDbStr(StrToDateObj($this->last_produced_on))),
                        (($this->notes == null) ? null : $this->notes),
                        (($this->popularity == null) ? null : $this->popularity),
                        
                        (($this->number_build == null) ? null : $this->number_build),
                        (($this->mtwo_kg == null) ? null : $this->mtwo_kg),
                        (($this->mtow_empty == null) ? null : $this->mtow_empty),
                        (($this->seats_per_row == null) ? null : $this->seats_per_row),
                        (($this->cabin_config_base_price == null) ? null : $this->cabin_config_base_price),
                    ];
                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($mn);
                    $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
                
                    AmsLogger::log("$mn", "id=" . $id);
                    $this->LoadById($id);
                } else {
                    AmsLogger::log($mn,  "Update ");

                    $strSQL = "UPDATE " . AmsMac::TABLE_NAME;
                    $strSQL .=" SET " . AmsMac::COL_NAME_MFR_ID . "=?, ";
                    $strSQL .=AmsMac::COL_NAME_MODEL . "=?, ";
                    $strSQL .=AmsMac::COL_NAME_AC_TYPE_ID . "=?, ";
                    $strSQL .=AmsMac::COL_NAME_MAX_RANGE_KM . "=?, ";
                    $strSQL .=AmsMac::COL_NAME_FUEL_TANK_L . "=?, ";
                    
                    $strSQL .=AmsMac::COL_NAME_PILOTS . "=?, ";
                    $strSQL .=AmsMac::COL_NAME_CREW_SEATS . "=? ";
                    $strSQL .=AmsMac::COL_NAME_MAX_SEATING . "=? ";
                    $strSQL .=AmsMac::COL_NAME_USEFUL_LOAD_KG . "=? ";
                    $strSQL .=AmsMac::COL_NAME_PRICE . "=? ";
                    
                    $strSQL .=AmsMac::COL_NAME_AVG_FUEL_CONSUMPTION_LP100KM . "=? ";
                    $strSQL .=AmsMac::COL_NAME_MAX_CRUISE_ALTITUDE_M . "=? ";
                    $strSQL .=AmsMac::COL_NAME_CRUISE_SPEED_KMPH . "=? ";
                    $strSQL .=AmsMac::COL_NAME_TAKE_OFF_M . "=? ";
                    $strSQL .=AmsMac::COL_NAME_LANDING_M . "=? ";
                    
                    $strSQL .=AmsMac::COL_NAME_OPERATION_TIME . "=? ";
                    $strSQL .=AmsMac::COL_NAME_COST_PER_FH . "=? ";
                    $strSQL .=AmsMac::COL_NAME_ASMI_RATE . "=? ";
                    $strSQL .=AmsMac::COL_NAME_WIKI_LINK . "=? ";
                    $strSQL .=AmsMac::COL_NAME_POWERPLANT . "=? ";
                    
                    $strSQL .=AmsMac::COL_NAME_PRODUCTION_START . "=? ";
                    $strSQL .=AmsMac::COL_NAME_PRODUCTION_RATE_H . "=? ";
                    $strSQL .=AmsMac::COL_NAME_LAST_PRODUCED_ON . "=? ";
                    $strSQL .=AmsMac::COL_NAME_NOTES . "=? ";
                    $strSQL .=AmsMac::COL_NAME_POPULARITY . "=? ";
                    
                    $strSQL .=AmsMac::COL_NAME_NUMBER_BUILD . "=? ";
                    $strSQL .=AmsMac::COL_NAME_MTWO_KG . "=? ";
                    $strSQL .=AmsMac::COL_NAME_MTOW_EMPTY . "=? ";
                    $strSQL .=AmsMac::COL_NAME_SEATS_PER_ROW . "=? ";
                    $strSQL .=AmsMac::COL_NAME_CABIN_CONFIG_BASE_PRICE . "=? ";
                    
                    $strSQL .=" WHERE " . AmsMac::COL_NAME_ID . " = ? ";
                    
                    $bound_params_r = ["isiiiiiiiddiiiiiiisssissiiddidi",
                       (($this->mfr_id == null) ? null : $this->mfr_id),
                        (($this->model == null) ? null : $this->model),
                        (($this->ac_type_id == null) ? null : $this->ac_type_id),
                        (($this->max_range_km == null) ? null : $this->max_range_km),
                        (($this->fuel_tank_l == null) ? null : $this->fuel_tank_l),
                        
                        (($this->pilots == null) ? null : $this->pilots),
                        (($this->crew_seats == null) ? null : $this->crew_seats),
                        (($this->max_seating == null) ? null : $this->max_seating),
                        (($this->useful_load_kg == null) ? null : $this->useful_load_kg),
                        (($this->price == null) ? null : $this->price),
                        
                        (($this->avg_fuel_consumption_lp100km == null) ? null : $this->avg_fuel_consumption_lp100km),
                        (($this->max_cruise_altitude_m == null) ? null : $this->max_cruise_altitude_m),
                        (($this->cruise_speed_kmph == null) ? null : $this->cruise_speed_kmph),
                        (($this->take_off_m == null) ? null : $this->take_off_m),
                        (($this->landing_m == null) ? null : $this->landing_m),
                        
                        (($this->operation_time == null) ? null : $this->operation_time),
                        (($this->cost_per_fh == null) ? null : $this->cost_per_fh),
                        (($this->asmi_rate == null) ? null : $this->asmi_rate),
                        (($this->wiki_link == null) ? null : $this->wiki_link),
                        (($this->powerplant == null) ? null : $this->powerplant),
                        
                        (($this->production_start == null) ? null : DateTimeDbStr(StrToDateObj($this->production_start))),
                        (($this->production_rate_h == null) ? null : $this->production_rate_h),
                        (($this->last_produced_on == null) ? null : DateTimeDbStr(StrToDateObj($this->last_produced_on))),
                        (($this->notes == null) ? null : $this->notes),
                        (($this->popularity == null) ? null : $this->popularity),
                        
                        (($this->number_build == null) ? null : $this->number_build),
                        (($this->mtwo_kg == null) ? null : $this->mtwo_kg),
                        (($this->mtow_empty == null) ? null : $this->mtow_empty),
                        (($this->seats_per_row == null) ? null : $this->seats_per_row),
                        (($this->cabin_config_base_price == null) ? null : $this->cabin_config_base_price),
                        $this->getId()
                    ];

                    $conn = AmsConnection::dbConnect();
                    $logModel = AmsLogger::currLogger()->getModule($mn);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    AmsLogger::log($mn, "affectedRows=" . $affectedRows);
                    
                }
            } catch (Exception $ex) {
                AmsLogger::log($mn, "Error id " . $this->getId());
                AmsLogger::logError($mn, $ex);
            }
        }
        AmsLogger::logEnd($mn);
        return $this;
    }
    
    public static function deleteById($id) {
        $mn = "AmsMac.deleteById()";
        AmsLogger::logBegin($mn);
        AmsLogger::log($mn, " id = ".$id);
        //$this->setId($id);
        $sql = "DELETE ".
                " FROM ".AmsMac::TABLE_NAME." ".
                " WHERE ".AmsMac::COL_NAME_ID."=?";
        $bound_params_r = ["i",$id];
         $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $deleted_id = $conn->preparedDelete($sql, $bound_params_r, $logModel);
        AmsLogger::logEnd($mn);
    }

// </editor-fold>
    
    
  // <editor-fold defaultstate="collapsed" desc="Load Data">
    
    
    public function loadById($id)
    {
        $mn = "ams:AmsMac.loadById()";
        AmsLogger::logBegin($mn);
        AmsLogger::log($mn, "id = ".$id);
        $this->setId($id);
        $sql = "SELECT ".AmsMac::getAllColumnsSQL().
                " FROM ".AmsMac::TABLE_NAME." ".
                " WHERE ".AmsMac::COL_NAME_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = AmsConnection::dbConnect();
        $logModel = AmsLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        AmsLogger::log($mn, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //AmsLogger::log($mn, "count(data)=".count($data));
        }
        //AmsLogger::log("$mn", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        AmsLogger::log($mn, "Aircraft is ".$this->toString());

        AmsLogger::logEnd($mn);
    }
    
    public function loadFromArray($result) {
        $mn = "AmsMac.loadFromArray()";
         AmsLogger::logBegin($mn);
        try{
            if(!$result==null) {
                AmsLogger::log($mn, "result: ".implode(",", $result));
               
                $this->mac_id = ($result[AmsMac::COL_NAME_ID]);
                $this->mfr_id = ($result[AmsMac::COL_NAME_MFR_ID]);
                $this->model = ($result[AmsMac::COL_NAME_MODEL]);
                $this->ac_type_id = ($result[AmsMac::COL_NAME_AC_TYPE_ID]);
                $this->max_range_km = ($result[AmsMac::COL_NAME_MAX_RANGE_KM]);
                $this->fuel_tank_l = ($result[AmsMac::COL_NAME_FUEL_TANK_L]);
                $this->pilots = ($result[AmsMac::COL_NAME_PILOTS]);
                $this->crew_seats = ($result[AmsMac::COL_NAME_CREW_SEATS]);
                $this->max_seating = ($result[AmsMac::COL_NAME_MAX_SEATINGAX]);
                
                $this->useful_load_kg = ($result[AmsMac::COL_NAME_USEFUL_LOAD_KG]);
                $this->price = ($result[AmsMac::COL_NAME_PRICE]);
                $this->avg_fuel_consumption_lp100km = ($result[AmsMac::COL_NAME_AVG_FUEL_CONSUMPTION_LP100KM]);
                $this->max_cruise_altitude_m = ($result[AmsMac::COL_NAME_MAX_CRUISE_ALTITUDE_M]);
                $this->cruise_speed_kmph = ($result[AmsMac::COL_NAME_CRUISE_SPEED_KMPH]);
                $this->take_off_m = ($result[AmsMac::COL_NAME_TAKE_OFF_M]);
                $this->landing_m = ($result[AmsMac::COL_NAME_LANDING_M]);
                $this->operation_time = ($result[AmsMac::COL_NAME_OPERATION_TIME]);
                $this->cost_per_fh = ($result[AmsMac::COL_NAME_COST_PER_FH]);
                $this->asmi_rate = ($result[AmsMac::COL_NAME_ASMI_RATE]);
                $this->wiki_link = ($result[AmsMac::COL_NAME_WIKI_LINK]);
                $this->powerplant = ($result[AmsMac::COL_NAME_POWERPLANT]);
                $this->production_start = ($result[AmsMac::COL_NAME_PRODUCTION_START]);
                $this->production_rate_h = ($result[AmsMac::COL_NAME_PRODUCTION_RATE_H]);
                $this->last_produced_on = ($result[AmsMac::COL_NAME_LAST_PRODUCED_ON]);
                
                $this->notes = ($result[AmsMac::COL_NAME_NOTES]);
                $this->popularity = ($result[AmsMac::COL_NAME_POPULARITY]);
                $this->number_build = ($result[AmsMac::COL_NAME_NUMBER_BUILD]);
                $this->mtwo_kg = ($result[AmsMac::COL_NAME_MTWO_KG]);
                $this->mtow_empty = ($result[AmsMac::COL_NAME_MTOW_EMPTY]);
                $this->seats_per_row = ($result[AmsMac::COL_NAME_SEATS_PER_ROW]);
                $this->cabin_config_base_price = ($result[AmsMac::COL_NAME_CABIN_CONFIG_BASE_PRICE]);
            }
        }
        catch(Exception $ex){AmsLogger::log($mn, "Error id ".$this->getId());AmsLogger::logError($mn, $ex);}
        AmsLogger::logEnd($mn);
    }

    public function toString()
    {
        $retValue = $this->toJSON();
        return $retValue;
    }
    
    public function toJSON() {
        return json_encode($this);
    }
    
    
    public function getDateTime($dtStr) {
        $retValue = null;
        if (isset($dtStr) && $dtStr != null) {
            if (!is_object($dtStr))
                $retValue = new DateTime($dtStr);
            else
                $retValue = $dtStr;

            AmsLogger::log("getDateTime()", "retValue=", getDateStr($retValue));
        }
        else
            $retValue = new DateTime();
        return $retValue;
    }
     // </editor-fold>
    
  
   /****************************************************************************
   * Parameters Declarations
   * ***************************************************************************
   */
   
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">
    
    public function getId() {
        return $this->mac_id;
    }
    
    public function setId($id) {
        $this->mac_id = $id;
    }

    public $mac_id;
    public $mfr_id;
    public $model;
    public $ac_type_id;
    public $max_range_km;
    public $fuel_tank_l;
    public $pilots;
    public $crew_seats;
    public $max_seating;
    public $useful_load_kg;
    public $price;
    public $avg_fuel_consumption_lp100km; 
    public $max_cruise_altitude_m;
    public $cruise_speed_kmph;
    public $take_off_m ; 
    public $landing_m ; 
    public $operation_time; 
    public $cost_per_fh ; 
    public $asmi_rate ;
    public $wiki_link ; 
    public $powerplant;
    public $production_start;
    public $production_rate_h;
    public $last_produced_on;
    public $notes;
    public $popularity;
    public $number_build; 
    public $mtwo_kg;
    public $mtow_empty; 
    public $seats_per_row; 
    public $cabin_config_base_price;
   
// </editor-fold>
    
    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME                    = "iordanov_ams_ac.cfg_mac";
    
    const COL_NAME_ID                   = "mac_id";
    const COL_NAME_MFR_ID               = "mfr_id";
    const COL_NAME_MODEL                = "model";
    const COL_NAME_AC_TYPE_ID           = "ac_type_id";
    const COL_NAME_MAX_RANGE_KM         = "max_range_km";
    const COL_NAME_FUEL_TANK_L          = "fuel_tank_l";
    const COL_NAME_PILOTS               = "pilots";
    const COL_NAME_CREW_SEATS           = "crew_seats";
    const COL_NAME_MAX_SEATING          = "max_seating";
    
    const COL_NAME_USEFUL_LOAD_KG       = "useful_load_kg";
    const COL_NAME_PRICE                = "price";
    const COL_NAME_AVG_FUEL_CONSUMPTION_LP100KM = "avg_fuel_consumption_lp100km";
    const COL_NAME_MAX_CRUISE_ALTITUDE_M = "max_cruise_altitude_m";
    const COL_NAME_CRUISE_SPEED_KMPH    = "cruise_speed_kmph";
    const COL_NAME_TAKE_OFF_M           = "take_off_m";
    const COL_NAME_LANDING_M            = "landing_m";
    const COL_NAME_OPERATION_TIME       = "operation_time";
    const COL_NAME_COST_PER_FH          = "cost_per_fh";
    const COL_NAME_ASMI_RATE            = "asmi_rate";
    const COL_NAME_WIKI_LINK            = "wiki_link";
    const COL_NAME_POWERPLANT           = "powerplant";
    const COL_NAME_PRODUCTION_START     = "production_start";
    const COL_NAME_PRODUCTION_RATE_H    = "production_rate_h";
    const COL_NAME_LAST_PRODUCED_ON     = "last_produced_on";
    const COL_NAME_NOTES                = "notes";
    const COL_NAME_POPULARITY           = "popularity";
    const COL_NAME_NUMBER_BUILD         = "number_build";
    const COL_NAME_MTWO_KG              = "mtwo_kg";
    const COL_NAME_MTOW_EMPTY           = "mtow_empty";
    
    const COL_NAME_SEATS_PER_ROW        = "seats_per_row";
    const COL_NAME_CABIN_CONFIG_BASE_PRICE = "cabin_config_base_price";
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . AmsMac::COL_NAME_ID . ", " .
                AmsMac::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " .  AmsMac::COL_NAME_MFR_ID . ", " .
                AmsMac::COL_NAME_MODEL . ", " .
                AmsMac::COL_NAME_AC_TYPE_ID . ", " .
                AmsMac::COL_NAME_MAX_RANGE_KM . ", " .
                AmsMac::COL_NAME_FUEL_TANK_L . ", " .
                AmsMac::COL_NAME_PILOTS . ", " .
                AmsMac::COL_NAME_CREW_SEATS . ", " .
                
                AmsMac::COL_NAME_MAX_SEATING . ", " .
                AmsMac::COL_NAME_USEFUL_LOAD_KG . ", " .
                AmsMac::COL_NAME_PRICE . ", " .
                AmsMac::COL_NAME_AVG_FUEL_CONSUMPTION_LP100KM . ", " .
                AmsMac::COL_NAME_MAX_CRUISE_ALTITUDE_M . ", " .
                AmsMac::COL_NAME_CRUISE_SPEED_KMPH . ", " .
                AmsMac::COL_NAME_TAKE_OFF_M . ", " .
                AmsMac::COL_NAME_LANDING_M . ", " .
                AmsMac::COL_NAME_OPERATION_TIME . ", " .
                AmsMac::COL_NAME_COST_PER_FH . ", " .
                AmsMac::COL_NAME_ASMI_RATE . ", " .
                AmsMac::COL_NAME_WIKI_LINK . ", " .
                AmsMac::COL_NAME_POWERPLANT . ", " .
                AmsMac::COL_NAME_PRODUCTION_START . ", " .
                AmsMac::COL_NAME_PRODUCTION_RATE_H . ", " .
                
                AmsMac::COL_NAME_LAST_PRODUCED_ON . ", " .
                AmsMac::COL_NAME_NOTES . ", " .
                AmsMac::COL_NAME_POPULARITY . ", " .
                AmsMac::COL_NAME_NUMBER_BUILD . ", " .
                AmsMac::COL_NAME_MTWO_KG . ", " .
                AmsMac::COL_NAME_MTOW_EMPTY . ", " .
                AmsMac::COL_NAME_SEATS_PER_ROW . ", " .
                
                 AmsMac::COL_NAME_CABIN_CONFIG_BASE_PRICE;
    }

    // </editor-fold>
}


// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="AmsMac GVDT">

// </editor-fold>

