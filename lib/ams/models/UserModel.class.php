<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Sep 27, 2018 
 *  Filename          : UserModel.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of UserModel
 *
 * @author IZIordanov
 */
class UserModel {
    //put your code here
    
    function __construct($amsUser) {
        $this->id = $amsUser->getId();
        $this->eMail = $amsUser->getEMail();
        $this->name = $amsUser->getName();
        $this->role = $amsUser->getRole();
        $this->roleName = $amsUser->getRoleName();
        $this->isReceiveEMails = $amsUser->getIsReceiveEMails();
        $this->ipAddress = $amsUser->getIpAddress();
        ;
        $this->lastLogged = $amsUser->getLastLogged();
        $this->updated = $amsUser->getUpdated();
    }
}
