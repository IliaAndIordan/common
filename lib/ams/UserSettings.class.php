<?php 
/******************************** HEAD_BEG ************************************
*
* Project                	: isgt
* Module                        : user
* Responsible for module 	: IordIord
*
* Filename               	: UserSettings.class.php
*
* Database System        	: ORCL, MySQL
* Created from                  : IordIord
* Date Creation			: 14.10.2016
*------------------------------------------------------------------------------
*                        Description
*------------------------------------------------------------------------------
* @TODO Insert some description.
*
*------------------------------------------------------------------------------
*                        History
*------------------------------------------------------------------------------
* HISTORY:
* <br>--- $Log: UserSettings.class.php,v $
* <br>---
* <br>---
*
********************************* HEAD_END ************************************
*/
global $km_to_ml;
global $label_km;
global $m_to_food;
global $label_m;
global $kg_to_lbs;
global $label_kg;
global $usg_to_l;
global $label_usg;
global $label_lp100km;
/**
 * Description of UserSettings class
 *
 * @author IordIord
 */
class UserSettings {
/**
 * ***************************************************************************
 * Methods Declarations
 * ***************************************************************************
 */
    /**
     *
     * @param <type> $id 
     */
    public function loadById($id) {
        $MN = "user:UserSettings.loadById()";
        $ST = logBegin($MN);
        logDebug($MN, "id = ".$id);
        $this->setDT_RowId($id);
        $sql = "SELECT ".UserSettings::getAllColumnsSQL().
                " FROM ".UserSettings::TABLE_NAME." ".
                " WHERE ".UserSettings::COL_NAME_USER_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = new Connect();
        $result_r = $conn->preparedSelect($sql, $bound_params_r);
        logDebug($MN, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //logDebug($MN, "count(data)=".count($data));
        }
        //logDebug("$MN", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        logDebug($MN, "UserSettings is ".$this->toString());

        logEndST($MN, $ST);
    }
    
    
    /**
     *
     * @param <type> $id
     */
    public static function deleteById($id) {
        $MN = "user:UserSettings.deleteById()";
        $ST = logBegin($MN);
        logDebug($MN, "id = ".$id);
        //$this->setId($id);
        $sql = "DELETE ".
                " FROM ".UserSettings::TABLE_NAME." ".
                " WHERE ".UserSettings::COL_NAME_USER_ID."=?";
        $bound_params_r = array("i",$id);
        $conn = new Connect();
        $id = $conn->preparedDelete($sql, $bound_params_r);

        logEndST($MN, $ST);
    }
    
    public static function loadAll()
    {
        $mn = "user:UserSettings.loadAll()";
        $st = logBegin($mn);


        $sql = "SELECT ".UserSettings::getAllColumnsSQL() .
                " FROM ".UserSettings::TABLE_NAME." ";

        $conn = new Connect();
        $data = $conn->dbExecuteSQL($sql);
        logDebug($mn, "count(data)=".count($data));
        $retArray = array();
        for ($index = 0, $max_count = sizeof( $data ); $index < $max_count; $index++)
        {
           $result = $data[$index];
           $item = new UserSettings();
           $item->loadFromPosArray($result);
           $retArray[] = $item;
           logDebug($mn, "Add Item =".$item->toString());
        }

        logDebug($mn, "count(retArray)=".count($retArray));
        logEndST($mn, $st);
        return $retArray;
    }
    
    public static function CreateRowData($user_id, $displayDistanceInMiles) {
        $MN = "user:UserSettings.CreateRowData()";
        $ST = logBegin($MN);

        $rowData = new UserSettings();
        
        $rowData->setDT_RowId($user_id);
        $rowData->setDisplayDistanceInMiles($displayDistanceInMiles);
        
        $rowData->save();
        logDebug($MN, "after save.");
        logEnd($MN, $ST);
        return $rowData;
    }

    public function save() {
        $mn = "user:UserSettings.save()";
        $st = logBegin($mn);

        logDebug($mn, "is_object(this)=" . is_object($this));
        logDebug($mn, "ID=" . $this->getDT_RowId());
        try {
            if (is_object($this)) {
                logDebug($mn, "ID=" . $this->getDT_RowId());
                if ($this->getDT_RowId() === null || $this->getDT_RowId() === "") {

                    logDebug($mn, "Insert ");
                    $strSQL = "INSERT INTO " . UserSettings::TABLE_NAME . " (" . UserSettings::getAllColumnsSQL() . ") ";
                    $strSQL .=" VALUES( ?, ?)";

                    $bound_params_r = array("ii",
                        (($this->getDisplayDistanceInMiles() == null) ? null : $this->getDisplayDistanceInMiles()),
                        (($this->getDT_RowId() == null) ? null : $this->getDT_RowId())
                    );
                    $conn = new Connect();
                    $id = $conn->preparedInsert($strSQL, $bound_params_r);

                    logDebug("$mn", "id=" . $id);
                    $this->LoadById($id);
                } else {
                    logDebug($mn, "Update ");
                    //$lngID = $this->getDT_RowId();
                    $strSQL = "UPDATE " . UserSettings::TABLE_NAME;
                    $strSQL .=" SET " . UserSettings::COL_NAME_DISPLAY_DISTANCE_ML . "=?, ";
                   

                    $strSQL .=" WHERE " . UserSettings::COL_NAME_USER_ID . "=? ";

                    $bound_params_r = array("ii",
                        (($this->getDisplayDistanceInMiles() == null) ? null : $this->getDisplayDistanceInMiles()),
                        $this->getDT_RowId()
                    );


                    $conn = new Connect();
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r);
                    logDebug($mn, "affectedRows=" . $affectedRows);

                    //$this->LoadById($this->getDT_RowId());
                }
            }
        } catch (Exception $ex) {
            logDebug($MN, "Error id " . $this->getDT_RowId());
            logError($MN, $ex);
        }
        logEndST($mn, $st);
        return $this;
    }

    public static function GetKmToMile($user_id) {
        $MN = "user:UserSettings.GetKmToMile()";
        $retValue = 1;
        $ST = logBegin($MN);

        $rowData = new UserSettings();
        
        $rowData->loadById($user_id);
        if($rowData!=null && $rowData->displayDistanceInMiles==1){
            $retValue = $rowData->km_to_ml;
        }
        
        logDebug($MN, "KmToMile = ".$retValue);
        logEnd($MN, $ST);
        return $retValue;
    }
    
    public static function GetMeterToFood($user_id) {
        $MN = "user:UserSettings.GetKmToMile()";
        $retValue = 1;
        $ST = logBegin($MN);

        $rowData = new UserSettings();
        
        $rowData->loadById($user_id);
        if($rowData!=null && $rowData->displayDistanceInMiles==1){
            $retValue = $rowData->m_to_food;
        }
        
        logDebug($MN, "KmToMile = ".$retValue);
        logEnd($MN, $ST);
        return $retValue;
    }
    /**
     *
     * @param <type> $result 
     */
    public function loadFromArray($result) {
        $MN = "user:UserSettings.loadFromArray()";
        $ST = logBegin($MN);
        try{
            
             if(isset($result) && count($result)>0) {
                 
                $this->setDT_RowId($result[UserSettings::COL_NAME_USER_ID]);
                
                $this->setDisplayDistanceInMiles($result[UserSettings::COL_NAME_DISPLAY_DISTANCE_ML]);
                
                logDebug($MN, "id ".$this->getDT_RowId());
            }
            
        }
        catch(Exception $ex){logDebug($MN, "Error id ".$this->getDT_RowId());logError($MN, $ex);}
        logEndST($MN, $ST);

    }

    public function loadFromPosArray($result) {
        $MN = "user:UserSettings.loadFromPosArray()";
        $ST = logBegin($MN);
        if(isset($result) && count($result)>0){
            $this->setDT_RowId($result[UserSettings::COL_IXD_USER_ID]);
            $this->setDisplayDistanceInMiles($result[UserSettings::COL_IDX_DISPLAY_DISTANCE_ML]);
        }
        logEndST($MN, $ST);

    }

    public function toString()
    {
         $retValue = $this->toJSON();
        
        return $retValue;
    }
    
    public function toJSON() {
        return json_encode($this);
    }
    /**
     * ***************************************************************************
     * Getters and Setters Declarations
     * ***************************************************************************
     */
    
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters  Declarations">
    
    
    
    public function getDT_RowId() {
        return $this->DT_RowId;
    }
    public function setDT_RowId($id) {
         $this->DT_RowId = $id;
    }

    public function setDisplayDistanceInMiles($displayDistanceInMiles) {
        $this->displayDistanceInMiles = $displayDistanceInMiles;
    }
    
    public function isDisplayDistanceInMiles() {
        if ($this->displayDistanceInMiles === 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getDisplayDistanceInMiles() {
        return $this->displayDistanceInMiles;
    }

    
    // </editor-fold>
    
   /****************************************************************************
   * Parameters Declarations
   * ***************************************************************************
   */
   
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">
    public $km_to_ml = 0.621371;
    public $m_to_food = 3.28084;
    public $DT_RowId;
    public $displayDistanceInMiles;
   

// </editor-fold>
    
    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME                    = "a_user_settings";
    const COL_NAME_USER_ID              = "user_id";
    const COL_NAME_DISPLAY_DISTANCE_ML  = "display_distance_ml";
   
    
    const COL_IXD_USER_ID               = 0;
    const COL_IDX_DISPLAY_DISTANCE_ML   = 1;

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . UserSettings::TABLE_NAME . "." . UserSettings::COL_NAME_USER_ID . ", " .
                UserSettings::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " . UserSettings::TABLE_NAME . "." . UserSettings::COL_NAME_DISPLAY_DISTANCE_ML . " ";
    }

    public static function getArrayColumns() {
        return array(UserSettings::COL_NAME_USER_ID,
            UserSettings::COL_NAME_DISPLAY_DISTANCE_ML);
    }

    // </editor-fold>
}

