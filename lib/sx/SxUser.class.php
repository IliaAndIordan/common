<?php 
/******************************** HEAD_BEG ************************************
*
* Project                	: isgt
* Module                        : user
* Responsible for module 	: IordIord
*
* Filename               	: SxUser.class.php
*
* Database System        	: ORCL, MySQL
* Created from                  : IordIord
* Date Creation			: 19.12.2018
*------------------------------------------------------------------------------
*                        Description
*------------------------------------------------------------------------------
* @TODO Insert some description.
*
*------------------------------------------------------------------------------
*                        History
*------------------------------------------------------------------------------
* HISTORY:
* <br>--- $Log: SxUser.class.php,v $
* <br>---
* <br>---
*
********************************* HEAD_END ************************************
*/


/**
 * Description of SxUser class
 *
 * @author IordIord
 */
class SxUser {

/**
 * ***************************************************************************
 * Methods Declarations
 * ***************************************************************************
 */
    /**
     *
     * @param <type> $id 
     */
    public function loadById($id) {
        $mn = "user:SxUser.loadById()";
         SxLogger::logBegin($mn);
        SxLogger::log($mn, "id = ".$id);
        $this->setId($id);
        $sql = "SELECT ".SxUser::getAllColumnsSQL(). ", " . SxUser::COL_NAME_UPDATED.
                " FROM ".SxUser::TABLE_NAME." ".
                " WHERE ".SxUser::COL_NAME_ID."=?";
        $bound_params_r = array('i', $id);
        $conn = SxConnection::dbConnect();
        $logModel = SxLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        SxLogger::log($mn, "count(result_r)=".count($result_r));
        $data = null;
        if(count($result_r)>0)
        {
            $data = $result_r[0];
            //SxLogger::log($mn, "count(data)=".count($data));
        }
        //SxLogger::log("$mn", "ret Value=".prArr($data) );
        if(isset($data) && count($data)>0)
        {
          $this->loadFromArray($data);
        }
        SxLogger::log($mn, "SxUser is ".$this->toString());

        SxLogger::logEnd($mn);
    }
    
    public static function login ($user_email, $user_password)
    {
        $mn="user:SxUser.login()";
         SxLogger::logBegin($mn);
        $out = null;
        $loginName = validate_search_string($user_email);
        $password = validate_search_string($user_password);

        //$user_email = base64_encode($loginName);
        //$user_password = md5($password);

        $sqlStr ="SELECT ".SxUser::COL_NAME_ID." FROM ".SxUser::TABLE_NAME.
                    " WHERE ".SxUser::COL_NAME_EMAIL."=? ".
                    " AND ".SxUser::COL_NAME_PSW."=?";
        $bound_params_r = array('ss', $user_email, $user_password);

        $conn = SxConnection::dbConnect();
        $logModel = SxLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        if(count($result_r)>0)
        {
           
            SxLogger::log($mn, prArr($result_r[0]));
            $id = $result_r[0][SxUser::COL_NAME_ID];
            SxLogger::log($mn, "Found userId".$id." for E-Mail ".$user_email."!");
            $ipAddress = $_SERVER['REMOTE_ADDR']; 
            
            if ($id>-1)
            {
                $out = new SxUser();
                $out->loadById($id);
                $out->setIpAddress($ipAddress);
                $out->setLastLogged(CurrenDateTime());
                $out->save();
            }
        }
        
        SxLogger::logEnd($mn);
        return $out;
    }
    
    /**
     *
     * @param <type> $email
     * @return <type> 
     */
    public static function isExistEMail ($email) {
        $mn="user.SxUser:isExistEMail()";
         SxLogger::logBegin($mn);
        $user_email = validate_search_string($email);

        $sqlStr ="SELECT count(*) AS Rows FROM ".
            SxUser::TABLE_NAME." WHERE ".SxUser::COL_NAME_EMAIL."=?";

        $bound_params_r = array('s', $user_email);
        SxLogger::log($mn,$sqlStr);
        $conn = SxConnection::dbConnect();
        $logModel = SxLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        SxLogger::log($mn, prArr($result_r[0]));
        $nrRows = $result_r[0]["Rows"];
        SxLogger::log($mn, "Found ".$nrRows." users with E-Mail ".$user_email."!");
        SxLogger::logEnd($mn);
        return ($nrRows>0?TRUE:FALSE);
    }

    public static function forgotenPassword($email, $conf)
    {
        $mn="user.SxUser:forgotenPassword()";
        $st = SxLogger::logBegin($mn);
        $id = -1;
        $retValue = false;
        $user_email = validate_search_string($email);
        if(SxUser::isExistEMail($user_email)>0)
        {
            $sqlStr ="SELECT ".SxUser::COL_NAME_ID." FROM ".
                SxUser::TABLE_NAME." WHERE ".SxUser::COL_NAME_EMAIL."=?";

            $bound_params_r = array('s', $user_email);
            SxLogger::log($mn,$sqlStr);
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
            SxLogger::log($mn, prArr($result_r[0]));
            $id = $result_r[0][SxUser::COL_NAME_ID];
            SxLogger::log($mn, "Found ".$id." user ID with E-Mail ".$user_email."!");
            if($id>0)
            {
                $user = new SxUser();
                $user->loadById($id);
                $retValue = sendMailRegistration($user, $conf);
            }
        }
        SxLogger::logEnd($mn);
        return $retValue;
    }
    /**
     *
     * @param <type> $id
     */
    public static function deleteById($id) {
        $mn = "user:SxUser.deleteById()";
         SxLogger::logBegin($mn);
        SxLogger::log($mn, "id = ".$id);
        //$this->setId($id);
        $sql = "DELETE ".
                " FROM ".SxUser::TABLE_NAME." ".
                " WHERE ".SxUser::COL_NAME_ID."=?";
        $bound_params_r = array("i",$id);
        $conn = SxConnection::dbConnect();
        $logModel = SxLogger::currLogger()->getModule($mn);
        $id = $conn->preparedDelete($sql, $bound_params_r, $logModel);

        SxLogger::logEnd($mn);
    }
    
    public static function loadAll()
    {
        $mn = "user:SxUser.loadAll()";
        $st = SxLogger::logBegin($mn);


        $sql = "SELECT ".SxUser::getAllColumnsSQL() . ", " . SxUser::COL_NAME_UPDATED.
                " FROM ".SxUser::TABLE_NAME." ";

        $conn = SxConnection::dbConnect();
        $logModel = SxLogger::currLogger()->getModule($mn);
        $data = $conn->dbExecuteSQL($sql, $logModel);
        SxLogger::log($mn, "count(data)=".count($data));
        $retArray = array();
        for ($index = 0, $max_count = sizeof( $data ); $index < $max_count; $index++)
        {
           $result = $data[$index];
           $item = new SxUser();
           $item->loadFromPosArray($result);
           $retArray[] = $item;
           SxLogger::log($mn, "Add Item =".$item->toString());
        }

        SxLogger::log($mn, "count(retArray)=".count($retArray));
        SxLogger::logEnd($mn);
        return $retArray;
    }
    
    public static function CreateRowData($eMail, $password) {
        $mn = "user:SxUser.CreateRowData()";
         SxLogger::logBegin($mn);

        $rowData = new SxUser();
        
        $rowData->setEMail($eMail);
        $rowData->setPassword($password);
        $rowData->setName($rowData->getNameFromEMail());
        $rowData->setRole(0);
        $rowData->setIsReceiveEMails(1);
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        $rowData->setIpAddress($ipAddress);
        
        $rowData->save();
        SxLogger::log($mn, "after save.");
        SxLogger::logEnd($mn);
        return $rowData;
    }

    public function save() {
        $mn = "user:SxUser.save()";
        $st = SxLogger::logBegin($mn);

        SxLogger::log($mn, "is_object(this)=" . is_object($this));
        SxLogger::log($mn, "ID=" . $this->getId());
        try {
            if (is_object($this)) {
                SxLogger::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {

                    SxLogger::log($mn, "Insert ");
                    $strSQL = "INSERT INTO " . SxUser::TABLE_NAME . " (" . SxUser::getAllColumnsNoIdSQL() . ") ";
                    $strSQL .=" VALUES( ?, ?, ?, ?, ?, ?, ?, ?)";

                    $bound_params_r = array("sssiissi",
                        (($this->getEMail() == null) ? null : $this->getEMail()),
                        (($this->getPassword() == null) ? null : $this->getPassword()),
                        (($this->getName() == null) ? null : $this->getName()),
                        ($this->getRole()),
                        ($this->isReceiveEMails()),
                        (($this->getIpAddress() == null) ? null : $this->getIpAddress()),
                        (($this->getLastLogged() == null) ? null : $this->getLastLogged()->format("Y-m-d H:i:s")),
                        (($this->getCompanyId() == null) ? null : $this->getCompanyId()),
                    );
                    $conn = SxConnection::dbConnect();
                    $logModel = SxLogger::currLogger()->getModule($mn);
                    $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);

                    SxLogger::log("$mn", "id=" . $id);
                    $this->LoadById($id);
                } else {
                    SxLogger::log($mn, "Update ");
                    //$lngID = $this->getId();
                    $strSQL = "UPDATE " . SxUser::TABLE_NAME;
                    $strSQL .=" SET " . SxUser::COL_NAME_EMAIL . "=?, ";
                    $strSQL .=SxUser::COL_NAME_PSW . "=?, ";
                    $strSQL .=SxUser::COL_NAME_UNNAME . "=?, ";
                    $strSQL .=SxUser::COL_NAME_ROLE . "=?, ";
                    $strSQL .=SxUser::COL_NAME_IS_RECEIVE_EMAILS . "=?, ";
                    $strSQL .=SxUser::COL_NAME_IP_ADDRESS . "=?, ";
                    $strSQL .=SxUser::COL_NAME_LAST_LOGED . "=?, ";
                    $strSQL .=SxUser::COL_NAME_COMPANY_ID . "=? ";
                    $strSQL .=" WHERE " . SxUser::COL_NAME_ID . "=? ";

                    $bound_params_r = array("sssiissii",
                        (($this->getEMail() == null) ? null : $this->getEMail()),
                        (($this->getPassword() == null) ? null : $this->getPassword()),
                        (($this->getName() == null) ? null : $this->getName()),
                        ($this->getRole()),
                        ($this->isReceiveEMails()),
                        (($this->getIpAddress() == null) ? null : $this->getIpAddress()),
                        (($this->getLastLogged() == null) ? null : $this->getLastLogged()->format("Y-m-d H:i:s")),
                        (($this->getCompanyId() == null) ? null : $this->getCompanyId()),
                        $this->getId()
                    );


                    $conn = SxConnection::dbConnect();
                    $logModel = SxLogger::currLogger()->getModule($mn);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    SxLogger::log($mn, "affectedRows=" . $affectedRows);

                    $this->LoadById($this->getId());
                }
            }
        } catch (Exception $ex) {
            SxLogger::log($mn, "Error id " . $this->getId());
            SxLogger::logError($mn, $ex);
        }
        SxLogger::logEnd($mn);
        return $this;
    }

    /**
     *
     * @param <type> $result 
     */
    public function loadFromArray($result) {
        $mn = "user:SxUser.loadFromArray()";
         SxLogger::logBegin($mn);
        try{
            
             if(isset($result) && count($result)>0) {
                 
                $this->setId($result[SxUser::COL_NAME_ID]);
                $this->setEMail($result[SxUser::COL_NAME_EMAIL]);
                $this->setPassword($result[SxUser::COL_NAME_PSW]);
                $this->setName($result[SxUser::COL_NAME_UNNAME]);
                $this->setRole($result[SxUser::COL_NAME_ROLE]);
                
                $this->setIsReceiveEMails($result[SxUser::COL_NAME_IS_RECEIVE_EMAILS]);
                $this->setIpAddress($result[SxUser::COL_NAME_IP_ADDRESS]);
                $this->setLastLogged($result[SxUser::COL_NAME_LAST_LOGED]);
                $this->setUpdated($result[SxUser::COL_NAME_UPDATED]);
                $this->setCompanyId($result[SxUser::COL_NAME_COMPANY_ID]);
                
                SxLogger::log($mn, "id ".$this->getId());
            }
            
        }
        catch(Exception $ex){SxLogger::log($mn, "Error id ".$this->getId());SxLogger::logError($mn, $ex);}
        SxLogger::logEnd($mn);

    }

    public function loadFromPosArray($result) {
        $mn = "user:SxUser.loadFromPosArray()";
         SxLogger::logBegin($mn);
        if(isset($result) && count($result)>0){
            $this->setId($result[SxUser::COL_IXD_ID]);
            $this->setEMail($result[SxUser::COL_IDX_EMAIL]);
            $this->setPassword($result[SxUser::COL_IDX_PSW]);
            $this->setName($result[SxUser::COL_IDX_UNAME]);
            $this->setRole($result[SxUser::COL_IDX_ROLE]);
            
            $this->setIsReceiveEMails($result[SxUser::COL_IDX_IS_RECEIVE_EMAILS]);
            $this->setIpAddress($result[SxUser::COL_IDX_IP_ADDRESS]);
            $this->setLastLogged($result[SxUser::COL_IDX_LAST_LOGED]);
            $this->setUpdated($result[SxUser::COL_IDX_UPDATED]);
            $this->setCompanyId($result[SxUser::COL_IDX_COMPANY_ID]);
        }
        SxLogger::logEnd($mn);

    }

    public function toString()
    {
         $retValue = $this->toJSON();
         
        return $retValue;
    }
    
    public function toJSON() {
        return json_encode($this);
    }
    /**
     * ***************************************************************************
     * Getters and Setters Declarations
     * ***************************************************************************
     */
    
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters  Declarations">
    

    public function getId() {
        return $this->userId;
    }
    
    public function setId($userId) {
        $this->userId = $userId;
    }

    public function getName() {
        $retValue = $this->userName;
        if(!isset($this->userName))
        {
            $retValue  = $this->getNameFromEMail();
        }
        return $retValue;
    }
    
    public function setName($userName) {
        $this->userName = $userName;
    }
    
     public function getCompanyId() {
        $retValue = $this->company_id;
        return $retValue;
    }
    
    public function setCompanyId($value) {
        $this->company_id = $value;
    }
    
    public function getNameFromEMail() {
        $retValue = $this->eMail;
        if(isset($this->eMail))
        {
           $valueArr = explode( '@', $this->eMail ,2);
           if(sizeof($valueArr)>0)
            $retValue  = $valueArr[0];
        }
        return $retValue;
    }
    
    
    public function getEMail() {
        return $this->eMail;
    }
    
    

    public function setEMail($eMail) {
        $this->eMail = $eMail;
        $this->e_mail = $eMail;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
       if(isset($password) && strlen($password)>2)
        {
           $this->password = $password;
        }
        
    }

    public function getRole() {
        return $this->role;
    }

    public function setRole($role) {
        $this->role = $role;
    }
    
    public function isAdmin() {
        if ($this->role === 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getIsReceiveEMails() {
        return $this->isReceiveEMails;
    }

    public function setIsReceiveEMails($isReceiveEMails) {
        $this->isReceiveEMails = $isReceiveEMails;
    }

    public function isReceiveEMails() {
        if ($this->isReceiveEMails > 0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function getIpAddress() {
        return $this->ipAddress;
    }

    public function setIpAddress($ipAddress) {
        $this->ipAddress = $ipAddress;
    }

    public function getLastLogged() {
        $retValue = null;
        if (isset($this->lastLogged) && $this->lastLogged != null) {
            if (!is_object($this->lastLogged))
                $retValue = new DateTime($this->lastLogged);
            else
                $retValue = $this->lastLogged;

            SxLogger::log("getLastLogged()", "retValue=", getDateStr($retValue));
        }
        else
            $retValue = new DateTime();
        return $retValue;
    }
    
     public function getLastLoggedDbStr() {
        $retValue = null;
        if (isset($this->lastLogged) && $this->lastLogged != null) {
            if (!is_object($this->lastLogged)){
                $value = new DateTime($this->lastLogged);
                $retValue = $value->format("Y-m-d H:i:s");
            }
            else
                $retValue = $this->lastLogged->format("Y-m-d H:i:s");

            SxLogger::log("getLastLogged()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setLastLogged($lastLogged) {
         if (isset($lastLogged) && $lastLogged != null) {
            if (is_object($lastLogged))
                $this->lastLogged = $data;
            else
                $this->lastLogged = new DateTime($lastLogged);
        }
        
    }

    public function getUpdated() {
        $retValue = null;
        if (isset($this->updated) && $this->updated != null) {
            if (!is_object($this->updated))
                $retValue = new DateTime($this->updated);
            else
                $retValue = $this->updated;

            SxLogger::log("getUpdated()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setUpdated($updated) {
        if (isset($data) && $data != null) {
            if (is_object($data))
                $this->updated = $data;
            else
                $this->updated = new DateTime($data);
        }
    }

    // </editor-fold>
    
   /****************************************************************************
   * Parameters Declarations
   * ***************************************************************************
   */
   
// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">

    public $userId;
    public $eMail;
    public $e_mail;
    public $password;
    public $role = 0; //2 predefined roles 0-user, 1 admin, 2 editor
    
    public $isReceiveEMails = null;
    public $ipAddress;
    public $lastLogged = 0;
    public $updated = 0;
    public $userName;
    public $company_id;

// </editor-fold>
    
    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME                = "iordanov_sx.sx_user";
    const COL_NAME_ID               = "user_id";
    const COL_NAME_EMAIL            = "e_mail";
    const COL_NAME_PSW              = "password";
    const COL_NAME_ROLE             = "user_role";
    const COL_NAME_IS_RECEIVE_EMAILS = "is_receive_emails";
    const COL_NAME_IP_ADDRESS       = "ip_address";
    const COL_NAME_LAST_LOGED       = "udate";
    const COL_NAME_UPDATED          = "adate";
    const COL_NAME_UNNAME           = "user_name";
    const COL_NAME_COMPANY_ID       = "company_id";
    
    const COL_IXD_ID                = 0;
    const COL_IDX_EMAIL             = 1;
    const COL_IDX_PSW               = 2;
    const COL_IDX_UNAME             = 3;
    const COL_IDX_ROLE              = 4;
    const COL_IDX_IS_RECEIVE_EMAILS = 5;
    const COL_IDX_IP_ADDRESS        = 6;
    const COL_IDX_LAST_LOGED        = 7;
    const COL_IDX_UPDATED           = 8;
    const COL_IDX_COMPANY_ID        = 9;
    

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . SxUser::TABLE_NAME . "." . SxUser::COL_NAME_ID . ", " .
                SxUser::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " . SxUser::TABLE_NAME . "." . SxUser::COL_NAME_EMAIL . ", " .
                SxUser::TABLE_NAME . "." . SxUser::COL_NAME_PSW . ", " .
                SxUser::TABLE_NAME . "." . SxUser::COL_NAME_UNNAME. ", " .
                SxUser::TABLE_NAME . "." . SxUser::COL_NAME_ROLE . ", " .
                SxUser::TABLE_NAME . "." . SxUser::COL_NAME_IS_RECEIVE_EMAILS . ", " .
                SxUser::TABLE_NAME . "." . SxUser::COL_NAME_IP_ADDRESS . ", " .
                SxUser::TABLE_NAME . "." . SxUser::COL_NAME_LAST_LOGED . ", " .
                SxUser::TABLE_NAME . "." . SxUser::COL_NAME_COMPANY_ID;
    }

    public static function getArrayColumns() {
        return array(SxUser::COL_NAME_ID,
            SxUser::COL_NAME_EMAIL,
            SxUser::COL_NAME_PSW,
            SxUser::COL_NAME_UNNAME,
            SxUser::COL_NAME_ROLE,
            SxUser::COL_NAME_IS_RECEIVE_EMAILS,
            SxUser::COL_NAME_IP_ADDRESS,
            SxUser::COL_NAME_LAST_LOGED,
            SxUser::COL_NAME_UPDATED,
            SxUser::COL_NAME_COMPANY_ID);
    }

    // </editor-fold>
}

