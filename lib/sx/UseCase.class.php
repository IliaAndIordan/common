<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Oct 2, 2020 
 *  Filename          : UseCase.class.php
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2020 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of UseCase
 *
 * @author IZIordanov
 */
class UseCase {
    // <editor-fold defaultstate="collapsed" desc="UseCase Methods">
    
    public static function UseCaseCreate($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_psm.psm_usecase
            (project_id, usecase_name,
            usecase_order, pstatus_id, usecase_notes,
            usecase_img_url, user_id)
            VALUES(?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["isiisii",
            ((!isset($dataJson->project_id)) ? null : $dataJson->project_id),
            ((!isset($dataJson->usecase_name)) ? null : $dataJson->usecase_name),
            ((!isset($dataJson->usecase_order)) ? 1 : $dataJson->usecase_order),
            ((!isset($dataJson->pstatus_id)) ? 1 : $dataJson->pstatus_id),
            ((!isset($dataJson->usecase_notes)) ? null : $dataJson->usecase_notes),
            ((!isset($dataJson->usecase_img_url)) ? null : $dataJson->usecase_img_url),
            ((!isset($dataJson->user_id)) ? null : $dataJson->user_id)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    public static function UseCaseUpdate($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_psm.psm_usecase
            SET
            project_id=?, usecase_name=?, usecase_order=?,
            pstatus_id=?, usecase_notes=?, usecase_img_url=?, user_id=?
            WHERE usecase_id = ? " ;

        $bound_params_r = ["isiissii",
            ((!isset($dataJson->project_id)) ? null : $dataJson->project_id),
            ((!isset($dataJson->usecase_name)) ? null : $dataJson->usecase_name),
            ((!isset($dataJson->usecase_order)) ? 1 : $dataJson->usecase_order),
            ((!isset($dataJson->pstatus_id)) ? 1 : $dataJson->pstatus_id),
            ((!isset($dataJson->usecase_notes)) ? null : $dataJson->usecase_notes),
            ((!isset($dataJson->usecase_img_url)) ? null : $dataJson->usecase_img_url),
            ((!isset($dataJson->user_id)) ? null : $dataJson->user_id),
            ($dataJson->usecase_id),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->usecase_id;
    }
    
    function UseCaseJson($usecase_id, $conn, $mn, $logModel){
        
        $sql = "SELECT uc.usecase_id, uc.project_id, p.project_name, p.project_account_id,
                uc.usecase_name, uc.usecase_order, uc.pstatus_id, uc.usecase_notes,
                uc.usecase_img_url, uc.adate, uc.user_id, u.user_name, u.e_mail, uc.udate
                FROM iordanov_psm.psm_usecase uc
                join iordanov_psm.psm_project p on p.project_id = uc.project_id
                left join iordanov_sx.sx_user u on u.user_id = uc.user_id 
                where uc.usecase_id=?" ;

        $bound_params_r = ["i",$usecase_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    // <editor-fold defaultstate="collapsed" desc="Properties">
    
    public $usecase_id;
    public $project_id;
    public $usecase_name;
    public $usecase_order;
    public $pstatus_id;
    public $usecase_notes;
    public $usecase_img_url;
    public $adate;
    public $udate;
    public $user_id;
    
    // </editor-fold>
}
