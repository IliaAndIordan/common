<?php

/* * ****************************** HEAD_BEG ************************************
 *
 * Project                	: ams
 * Module                        : ams
 * Responsible for module 	: IordIord
 *
 * Filename               	: Company.class.php
 *
 * Database System        	: MySQL
 * Created from                  : IordIord
 * Date Creation			: 21.03.2016
 * ------------------------------------------------------------------------------
 *                        Description
 * ------------------------------------------------------------------------------
 * @TODO Insert some description.
 *
 * ------------------------------------------------------------------------------
 *                        History
 * ------------------------------------------------------------------------------
 * HISTORY:
 * <br>--- $Log: Company.class.php,v $
 * <br>---
 * <br>---
 *
 * ******************************** HEAD_END ************************************
 */
require_once("GVDataTable.class.php");

// <editor-fold defaultstate="collapsed" desc="Company Class">

/**
 * Description of Company class
 *
 * @author IordIord
 */
class Company {

    /**
     * ***************************************************************************
     * Methods Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Update Data">

    public function save() {
        $mn = "Company->save()";
        SxLogger::logBegin($mn);

        SxLogger::log($mn, "is_object(this)=" . is_object($this));

        if (is_object($this)) {
            try {
                SxLogger::log($mn, "ID=" . $this->getId());
                if ($this->getId() === null || $this->getId() === "") {
                    $this->setStatusId(1);
                    SxLogger::log($mn, "Insert ");
                    $strSQL = "INSERT INTO " . Company::TABLE_NAME . " (" .
                            Company::COL_NAME_NAME . ", " .
                            Company::COL_NAME_TYPE_ID . ", " .
                            Company::COL_NAME_PARENT_ID . ", " .
                            Company::COL_NAME_BRANCH . ", " .
                            Company::COL_NAME_LOGO_URL . ", " .
                            Company::COL_NAME_WEB_URL . ", " .
                            Company::COL_NAME_NOTES . ", " .
                            Company::COL_NAME_EAN_MFR_CODE . ", " .
                            Company::COL_NAME_COUNTRY_ID . ", " .
                            Company::COL_NAME_ACTOR_ID . ", " .
                            Company::COL_NAME_STATUS_ID . ") " .
                            " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                    $bound_params_r = ["siissssssiii",
                        (($this->getName() == null) ? null : $this->getName()),
                        (($this->getTypeID() == null) ? null : $this->getTypeID()),
                        (($this->getParentId() == null) ? null : $this->getParentId()),
                        (($this->getBranch() == null) ? null : $this->getBranch()),
                        (($this->getLogoUrl() == null) ? null : $this->getLogoUrl()),
                        (($this->getWebUrl() == null) ? null : $this->getWebUrl()),
                        (($this->getNodes() == null) ? null : $this->getNodes()),
                        //(($this->getCreated() == null) ? null : $this->getCreated()->format("Y-m-d H:i:s")),
                        (($this->getEanMfrCode() == null) ? null : $this->getEanMfrCode()),
                        (($this->getCountryId() == null) ? null : $this->getCountryId()),
                        (($this->getActorId() == null) ? null : $this->getActorId()),
                        (($this->getStatusId() == null) ? null : $this->getStatusId()),
                    ];
                    $conn = SxConnection::dbConnect();
                    $logModel = SxLogger::currLogger()->getModule($mn);
                    $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
                    $this->setId($id);
                    SxLogger::log("$mn", "id=" . $id);
                    $this->LoadById($id);
                } else {
                    SxLogger::log($mn, "Update ");
                     $this->setStatusId(2);
                    $strSQL = "UPDATE " . Company::TABLE_NAME;
                    $strSQL .= " SET " . Company::COL_NAME_NAME . "=?, ";
                    $strSQL .= Company::COL_NAME_TYPE_ID . "=?, ";
                    $strSQL .= Company::COL_NAME_PARENT_ID . "=?, ";
                    $strSQL .= Company::COL_NAME_BRANCH . "=?, ";
                    $strSQL .= Company::COL_NAME_LOGO_URL . "=?, ";
                    $strSQL .= Company::COL_NAME_WEB_URL . "=?, ";
                    $strSQL .= Company::COL_NAME_NOTES . "=?, ";
                    $strSQL .= Company::COL_NAME_EAN_MFR_CODE . "=?, ";
                    $strSQL .= Company::COL_NAME_COUNTRY_ID . "=?, ";
                    $strSQL .= Company::COL_NAME_ACTOR_ID . "=?, ";
                    $strSQL .= Company::COL_NAME_STATUS_ID . "=? ";
                    $strSQL .= " WHERE " . Company::COL_NAME_ID . "=? ";

                    $bound_params_r = ["siisssssiiii",
                        (($this->getName() == null) ? null : $this->getName()),
                        (($this->getTypeID() == null) ? null : $this->getTypeID()),
                        (($this->getParentId() == null) ? null : $this->getParentId()),
                        (($this->getBranch() == null) ? null : $this->getBranch()),
                        (($this->getLogoUrl() == null) ? null : $this->getLogoUrl()),
                        (($this->getWebUrl() == null) ? null : $this->getWebUrl()),
                        (($this->getNodes() == null) ? null : $this->getNodes()),
                        (($this->getEanMfrCode() == null) ? null : $this->getEanMfrCode()),
                        (($this->getCountryId() == null) ? null : $this->getCountryId()),
                        (($this->getActorId() == null) ? null : $this->getActorId()),
                        (($this->getStatusId() == null) ? null : $this->getStatusId()),
                        $this->getId()
                    ];

                    $conn = SxConnection::dbConnect();
                    $logModel = SxLogger::currLogger()->getModule($mn);
                    $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                    SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    $this->deleteSxLiveryAndLogo();
                    $this->LoadById($this->getId());
                }
            } catch (Exception $ex) {
                SxLogger::log($mn, "Error id " . $this->getId());
                SxLogger::logError($mn, $ex);
            }
        }
        SxLogger::logEnd($mn);
        return $this;
    }

// </editor-fold>
 
    // <editor-fold defaultstate="collapsed" desc="Load Data">


    public function IsoCountry() {
        $mn = "Company::IsoCountry(" . $this->company_id . ")";
        SxLogger::logBegin($mn);
        $country_iso2 = "BG";

        $sqlStr = "SELECT country_iso2
                    FROM iordanov_ams_wad.cfg_rs_country
                    where country_id=?";
        try {
            $bound_params_r = array('i', $this->country_id . "%");
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
            SxLogger::log($mn, "count(result_r)=" . count($result_r));
            $country_iso2 = $result_r[0]["country_iso2"];
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $country_iso2 = "BG";
        }
        SxLogger::log($mn, "Found country iso " . $country_iso2 . "!");
        SxLogger::logEnd($mn);
        return $country_iso2;
    }

    public static function CheckCompanyName($name, $branch) {
        $mn = "Company::CheckCompanyName(" . $name . ", " . $branch . ")";
        SxLogger::logBegin($mn);

        $sqlStr = "SELECT count(*) AS Rows FROM " .
                Company::TABLE_NAME . " WHERE " . Company::COL_NAME_NAME . " = ? " .
                " and " . Company::COL_NAME_BRANCH . " = ? ";

        $bound_params_r = array('ss', $name, $branch);
        $conn = SxConnection::dbConnect();
        $logModel = SxLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sqlStr, $bound_params_r, $logModel);
        SxLogger::log($mn, "count(result_r)=" . count($result_r));
        $nrRows = $result_r[0]["Rows"];
        SxLogger::log($mn, "Found " . $nrRows . " airlines with iata " . $name . "!");

        SxLogger::logEnd($mn);
        return $nrRows;
    }

    public function loadById($id) {
        $mn = "Company.loadById()";
        SxLogger::logBegin($mn);
        SxLogger::log($mn, "id = " . $id);
        $this->setId($id);
        $sql = "SELECT " . Company::getAllColumnsSQL() . ", " . Company::COL_NAME_UPDATED .
                " FROM " . Company::TABLE_NAME . " " .
                " WHERE " . Company::COL_NAME_ID . "=?";
        $bound_params_r = array('i', $id);
        $conn = SxConnection::dbConnect();
        $logModel = SxLogger::currLogger()->getModule($mn);
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        SxLogger::log($mn, "count(result_r)=" . count($result_r));
        $data = null;
        if (count($result_r) > 0) {
            $data = $result_r[0];
            //SxLogger::log($mn, "count(data)=".count($data));
        }
        //SxLogger::log("$mn", "ret Value=".prArr($data) );
        if (isset($data) && count($data) > 0) {
            $this->loadFromArray($data);
        }
        SxLogger::log($mn, "Company is " . $this->toString());

        SxLogger::logEnd($mn);
    }

    public static function deleteById($id) {
        $mn = "Company.deleteById()";
        SxLogger::logBegin($mn);
        SxLogger::log($mn, " id = " . $id);
        //$this->setId($id);
        $sql = "DELETE " .
                " FROM " . Company::TABLE_NAME . " " .
                " WHERE " . Company::COL_NAME_ID . "=?";
        $bound_params_r = ["i", $id];
        $conn = SxConnection::dbConnect();
        $logModel = SxLogger::currLogger()->getModule($mn);
        $deleted_id = $conn->preparedDelete($sql, $bound_params_r, $logModel);
        SxLogger::logEnd($mn);
    }

    public function loadFromArray($result) {
        $mn = "Company.loadFromArray()";
        SxLogger::logBegin($mn);
        try {
            if (!$result == null) {
                SxLogger::log($mn, "result: " . implode(",", $result));

                $this->setId($result[Company::COL_NAME_ID]);
                $this->setName($result[Company::COL_NAME_NAME]);
                $this->setTypeID($result[Company::COL_NAME_TYPE_ID]);
                $this->setParentId($result[Company::COL_NAME_PARENT_ID]);
                $this->setBranch($result[Company::COL_NAME_BRANCH]);
                $this->setLogoUrl($result[Company::COL_NAME_LOGO_URL]);
                $this->setWebUrl($result[Company::COL_NAME_WEB_URL]);
                $this->setNodes($result[Company::COL_NAME_NOTES]);

                $this->setCreated($result[Company::COL_NAME_CREATED]);
                $this->setUpdated($result[Company::COL_NAME_UPDATED]);
                $this->setEanMfrCode($result[Company::COL_NAME_EAN_MFR_CODE]);
                $this->setCountryId($result[Company::COL_NAME_COUNTRY_ID]);
                $this->setActorId($result[Company::COL_NAME_ACTOR_ID]);
                $this->setStatusId($result[Company::COL_NAME_STATUS_ID]);

                if (isset($this->company_id)) {
                    $this->getSxLogoUrl();
                    $this->getSxLiveryUrl();
                }
            }
        } catch (Exception $ex) {
            SxLogger::log($mn, "Error id " . $this->getId());
            SxLogger::logError($mn, $ex);
        }
        SxLogger::logEnd($mn);
    }

    public function toString() {
        $retValue = $this->toJSON();
        return $retValue;
    }

    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>

    /**
     * ***************************************************************************
     * Getters and Setters Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters  Declarations">
    public function getId() {
        return $this->company_id;
    }

    public function setId($Id) {
        $this->company_id = $Id;
    }

    public function getName() {
        return $this->company_name;
    }

    public function setName($value) {
        $this->company_name = $value; //strtoupper($iata);
    }

    public function getTypeID() {
        return $this->company_type_id;
    }

    public function setTypeID($value) {
        $this->company_type_id = isset($value)?$value:null;
    }

    public function getParentId() {
        return $this->parent_company_id;
    }

    public function setParentId($value) {
        $this->parent_company_id = isset($value)?$value:null;
    }

    public static $image_base_url = "https://common.iordanov.info";

    public function getLogoUrl() {
        return $this->logo_url;
    }
    
     public function setLogoUrl($value) {
        $this->logo_url = isset($value) && strlen($value) > 2 ? $value :null;
    }

     public function getSxLogoUrl() {
        $this->sx_logo_url = Company::$image_base_url . "/" . $this->getLogoPath();
        return $this->sx_logo_url;
    }
    
    public function getLogoPath() {
        $mn = "Company->getLogoPath()";
        SxLogger::logBegin($mn);


        $retValue = "/images/company/" . $this->getLogo();
        $docRoot =  "/home/iordanov/common";
        SxLogger::log($mn, " docRoot=" . $docRoot);
        if (!file_exists($docRoot . $retValue)) {
            SxLogger::log($mn, "Create logo=" . $this->getId());
            $this->createLogo("/images/company/logo_0.png");
            //$this->save();
        }
        SxLogger::logEnd($mn);
        return $retValue;
    }

    public function getLogo() {
        $mn = "Company->getLogo()";
        $retValue = "logo_" . $this->getId() . ".png";
        SxLogger::log($mn, "retValue=" . $retValue);
        return $retValue;
    }

    public function createLogo($src) {
        $mn = "Company::createLogo()";
        SxLogger::logBegin($mn);
        SxLogger::log($mn, "User src = " . $src);
        $height = 300;
        $width = 300;
        $fontSize = 126;
        $ctFontSize = 16;
        $iata = "AA";
        $docRoot = "/home/iordanov/common";
        //logDebug("createLogo", "docRoot=".$docRoot);  
        $src = $docRoot . $src;
        if (!file_exists($src)) {
            SxLogger::log($mn, "Can not find file: " . $src . ": exist=" . file_exists($src) ? "true" : "false");
            return;
        }

        $country = strtolower($this->IsoCountry());
        $aname = $this->getName();
        //if(strlen($aname)>10)
        //    $aname = substr($aname, 0, 10);
        $slogan = $this->getName();
        if (!isset($slogan) || strlen($slogan) === 0)
            $slogan = $aname;
        if (strlen($slogan) > 36) {
            $slogan = substr($slogan, 0, 36);
        }

        if (strlen($slogan) < 36) {
            $maxIdx = (36 - strlen($slogan)) / 2;
            for ($idx = 0; $idx < $maxIdx; $idx++) {
                $slogan = ($idx === 0 ? "" : " ") . $slogan . " ";
            }
        }
        $iata = strtoupper($this->getBranch());
        if (strlen($iata) > 2)
            $iata = substr($iata, 0, 2);

        $flagSrc = $docRoot . "/images/flags/" . $country . ".png";

        $imageinfoB = getimagesize($src);
        $imageinfoF = getimagesize($flagSrc);

        $flagw = $imageinfoF[0];
        $flagh = $imageinfoF[1];
        $logow = $imageinfoB[0];
        $logoh = $imageinfoB[1];

        SxLogger::log($mn, "flagw=" . $flagw);
        SxLogger::log($mn, "logow=" . $logow);

        $hK = $height / $logoh;
        $fontSize = $fontSize * $hK;

        //define iata text size
        $font = $docRoot . "/fonts/Champagne & Limousines Bold.ttf"; // "../fonts/Quincho.ttf";:
        $iataSize = imagettfbbox($fontSize, 0, $font, $iata);

        $fhK = $fontSize / $flagh;

        $imgh = $height - ($ctFontSize * 2);
        $imgw = $width - ($ctFontSize * 2);

        $logoK = $imgh / $logoh;
        $lh = $imgh * $logoK;
        $lw = $imgw * $logoK;
        $lx = round($ctFontSize);
        $ly = round($ctFontSize);

        //$width = $logow * $hK;
        $fw = ($imgw / 2) - $fontSize;
        $fh = $fontSize / 1.4;
        $fx = ($width / 2) - ($fw / 2); //$width / 3.5;
        $fy = ($imgh - (1.6 * $fh)); //$height-($height / 4.5);
        $iatax = $width / 2 - (($iataSize[4] - $iataSize[0] ) / 2);
        $iatay = ($height / 4) + ($ctFontSize * 2);
        //--- Create Image
        $rImg = imagecreatetruecolor($width, $height);

        //--- Load Images Logo and Country Flag 
        $srcimage = imagecreatefrompng($src);
        $srcimageFlag = imagecreatefrompng($flagSrc);
        $corIata = imagecolorallocate($rImg, 217, 19, 59);
        //--- paste logo full size and transparent + flag
        imagealphablending($rImg, false);
        imagesavealpha($rImg, true);
        $transparent = imagecolorallocatealpha($rImg, 255, 255, 255, 127);
        imagefilledrectangle($rImg, 0, 0, $width, $height, $transparent);
        imagecopyresampled($rImg, $srcimage, $lx, $ly, 0, 0, $imgw, $imgh, $logow, $logoh);

        imagecopyresampled($rImg, $srcimageFlag, $fx, $fy, 0, 0, $fw, $fh, $flagw, $flagh);
        //--- IATA CODE
        imagettftext($rImg, $fontSize, 0, $iatax, $iatay, $corIata, $font, $iata);

        //---- Drow text in cercle
        $degrees = (360 / strlen($slogan));

        $colorSlogan = imagecolorallocate($rImg, 148, 148, 184);
        $fontSlogan = $docRoot . "/fonts/RobotoCondensed-Bold.ttf";
        //--- Center
        $cx = $width / 2;
        $cy = ($height / 2);
        //--- radius
        $r = (($width / 2) - ($ctFontSize / 2)); // + $logoh/4);//-($ctFontSize/4);
        for ($idx = 0; $idx < strlen($slogan); $idx++) {
            $leterIdx = strlen($slogan) - 1 - $idx;
            $angle = ($degrees * $idx) - 90;
            $cos = cos(deg2rad($angle));
            $sin = sin(deg2rad($angle));

            //---
            $px = round($cx + ($r * $cos)); //round($cos*($x) - $sin*($y));
            $py = round($cy + ($r * $sin)); //round($sin*($x) + $cos*($y));

            imagettftext($rImg, $ctFontSize, 90 - $angle, $px, $py, $colorSlogan, $fontSlogan, $slogan[$leterIdx]);
        }
        //--- Save the image to file
        imagepng($rImg, $docRoot . "/images/company/logo_" . $this->getId() . ".png");
        // Destroy image 
        imagedestroy($rImg);
    }

    public function getSxLiveryUrl() {
        $this->sx_livery_url = Company::$image_base_url . "/" . $this->LiveryPath();

        return $this->sx_livery_url;
    }

    public function LiveryPath() {
        $retValue = "/images/company/" . $this->getLivery();
        $docRoot = "/home/common/";//$_SERVER['DOCUMENT_ROOT'] . "/images/";
        if (!file_exists($docRoot . $retValue)) {
            $this->createLivery();
        }
        return $retValue;
    }
    
    public function deleteSxLiveryAndLogo() {
        $docRoot = "/home/common/";//$_SERVER['DOCUMENT_ROOT'] . "/images/";
        $livery = "images/company/" . $this->getLivery();
        $logo = "images/company/" . $this->getLogo();
        if (file_exists($docRoot . $livery)) {
            !unlink($docRoot . $livery);
        }
        if (file_exists($docRoot . $logo)) {
            !unlink($docRoot . $logo);
        }
    }

    public function getLivery() {
        $retValue = "livery_" . $this->getId() . ".png";
        return $retValue;
    }

    public function createLivery() {
        $mn = "Company::createLivery()";
        SxLogger::logBegin($mn);

        $height = 230;
        $width = 1000;
        $fontSize = 82;
        $iata = "AA";
        $docRoot = "/home/iordanov/common/";//$_SERVER['DOCUMENT_ROOT'] . "/images/";

        //logDebug("createLivery", "docRoot=".$docRoot);  
        $src = $docRoot . $this->getLogoPath();
        SxLogger::log($mn, " src = " . $src);
        if (!file_exists($src)) {
            SxLogger::log($mn, "Not found file" . $src . ": exist=" . file_exists($src) ? "true" : "false");
            return;
        }

        $aname = $this->getName();
        if (strlen($aname) > 10)
            $aname = substr($aname, 0, 10);
        $slogan = $this->getBranch();
        if (strlen($slogan) > 36) {
            $slogan = substr($slogan, 0, 36);
        }

        if (strlen($slogan) < 36) {
            $maxIdx = (36 - strlen($slogan)) / 2;
            for ($idx = 0; $idx < $maxIdx; $idx++) {
                $slogan = " " . $slogan . " ";
            }
        }
        $country = strtolower($this->IsoCountry());
        SxLogger::log($mn, " country=" . $country);
        $iata = strtoupper($country);
        if (strlen($iata) > 2)
            $iata = substr($iata, 0, 2);

        $flagSrc = $docRoot . "images/flags/" . $country . ".png";

        $imageinfoB = getimagesize($src);
        $imageinfoF = getimagesize($flagSrc);

        $flagw = $imageinfoF[0];
        $flagh = $imageinfoF[1];
        $logow = $imageinfoB[0];
        $logoh = $imageinfoB[1];

        //logDebug("createLivery", "logow=".$logow);  
        //logo resize facktor
        $logoRes = $height / $logoh;
        //$fontSize = $fontSize * $hK;
        //define iata text size
        $font = $docRoot . "/fonts/Champagne & Limousines Bold.ttf"; // "../fonts/Quincho.ttf";:
        $iataSize = imagettfbbox($fontSize, 0, $font, $iata);
        //$fhK = $fontSize / $flagh;
        $lw = $logow * $logoRes;
        $lh = $logoh * $logoRes;
        //--- Flag bottom horizontal
        $fw = $width / 2.3;
        $fh = $fontSize / 2;
        $fx = $logow / 3.5;
        $fy = $height - $fh - 2; //($height / 4.5);
        $iatax = $width / 2 - (($iataSize[4] - $iataSize[0] ) / 2);
        $iatay = ($height / 4);
        $flW = $width - ($logow / 2) - 1; //67*$hK;
        $flH = $fh;


        //---- define text size
        $font = $docRoot . "/fonts/Champagne & Limousines Bold.ttf";
        $fontSlogan = $docRoot . "/fonts/RobotoCondensed-Bold.ttf";

        //--- Create Image
        $rImg = imagecreatetruecolor($width, $height);

        //--- Load Images Logo and Country Flag 
        $srcimage = imagecreatefrompng($src);
        $srcimageFlag = imagecreatefrompng($flagSrc);
        $corIata = imagecolorallocate($rImg, 217, 19, 59);
        $corName = imagecolorallocate($rImg, 0, 77, 0);
        $corSlogan = imagecolorallocate($rImg, 148, 148, 184);
        //--- paste logo full size and Transparent + flag
        //imagealphablending($rImg, false);
        //imagesavealpha($rImg, true);
        //$transparent = imagecolorallocatealpha($rImg, 255, 255, 255, 127);
        //imagefilledrectangle($rImg, 0, 0, $width, $height, $transparent);
        //-- Image full size and background color
        $white = imagecolorallocate($rImg, 255, 255, 255);
        $lightyelow = imagecolorallocate($rImg, 255, 255, 204);
        imagefilledrectangle($rImg, 0, 0, $width, $height, $lightyelow);

        //-- Insert Logo left full
        imagecopyresampled($rImg, $srcimage, 0, 0, 0, 0, $lw, $lh, $logow, $logoh);

        //---- Insert Country Flag right vertical
        //--- Flag coordinate bottom horizontal
        $fw = ($lw / 2.5);
        $fh = $height - 2;
        $fx = $width - ($fw + 2);
        $fy = 1;
        imagecopyresampled($rImg, $srcimageFlag, $fx, $fy, 0, 0, $fw, $fh, $flagw, $flagh);

        //--- Slogan and Name
        imagettftext($rImg, $fontSize, 0, 336, (14 + $fontSize), $corName, $font, $aname);
        imagettftext($rImg, $fontSize / 2, 0, 366, ((20 + $fontSize) + ($fontSize)), $corSlogan, $fontSlogan, $slogan);

        //--- Save the image to file
        imagepng($rImg, $docRoot . "/images/company/livery_" . $this->getId() . ".png");
        // Destroy image 
        imagedestroy($rImg);
    }

    //RegionByIso
    public function getBranch() {
        return $this->branch_code;
    }

    public function setBranch($value) {
        $this->branch_code = $value;
    }

    public function getNodes() {
        return $this->notes;
    }

    public function setNodes($value) {
        $this->notes = $value;
    }

    public function getWebUrl() {
        return $this->web_url;
    }

    public function setWebUrl($value) {
        $this->web_url = $value;
    }

    public function getCreated() {
        $retValue = null;
        if (isset($this->created) && $this->created != null) {
            if (!is_object($this->created))
                $retValue = new DateTime($this->created);
            else
                $retValue = $this->created;

            //logDebug("getLastLogged()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setCreated($created) {
        if (isset($created) && $created != null) {
            if (is_object($created))
                $this->created = $created;
            else
                $this->created = new DateTime($created);
        }
    }

    public function getUpdated() {
        $retValue = null;
        if (isset($this->updated) && $this->updated != null) {
            if (!is_object($this->updated))
                $retValue = new DateTime($this->updated);
            else
                $retValue = $this->updated;

            //logDebug("getLastLogged()", "retValue=", getDateStr($retValue));
        }
        return $retValue;
    }

    public function setUpdated($created) {
        if (isset($created) && $created != null) {
            if (is_object($created))
                $this->updated = $created;
            else
                $this->updated = new DateTime($created);
        }
    }

    public function getEanMfrCode() {
        return $this->ean_mfr_code;
    }

    public function setEanMfrCode($value) {
        $this->ean_mfr_code = $value;
    }

    public function getCountryId() {
        return $this->country_id;
    }

    public function setCountryId($value) {
        $this->country_id = $value;
    }
    
     public function getActorId() {
        return $this->actor_id;
    }

    public function setActorId($value) {
        $this->actor_id = $value;
    }
    
     public function getStatusId() {
        return $this->company_status_id;
    }

    public function setStatusId($value) {
        $this->company_status_id = $value;
    }

    // </editor-fold>

    /*     * **************************************************************************
     * Parameters Declarations
     * ***************************************************************************
     */

// <editor-fold defaultstate="collapsed" desc="Parameters Declarations">

    public $company_id;
    public $company_name;
    public $company_type_id;
    public $parent_company_id;
    public $branch_code;
    public $logo_url;
    public $web_url;
    public $notes;
    public $adate;
    public $udate = 0;
    public $ean_mfr_code;
    public $actor_id;
    public $company_status_id;
    public $sx_logo_url;
    public $sx_livery_url;

// </editor-fold>

    /**
     * ***************************************************************************
     * Constants Declarations
     * ***************************************************************************
     */
    // <editor-fold defaultstate="collapsed" desc="Constants Declarations">

    const TABLE_NAME = "iordanov_sm.sm_company";
    const COL_NAME_ID = "company_id";
    const COL_NAME_NAME = "company_name";
    const COL_NAME_TYPE_ID = "company_type_id";
    const COL_NAME_PARENT_ID = "parent_company_id";
    const COL_NAME_BRANCH = "branch_code";
    const COL_NAME_LOGO_URL = "logo_url";
    const COL_NAME_WEB_URL = "web_url";
    const COL_NAME_NOTES = "notes";
    const COL_NAME_CREATED = "adate";
    const COL_NAME_UPDATED = "udate";
    const COL_NAME_EAN_MFR_CODE = "ean_mfr_code";
    const COL_NAME_COUNTRY_ID = "country_id";
    const COL_NAME_ACTOR_ID = "actor_id";
    const COL_NAME_STATUS_ID = "company_status_id";
    
    const COL_IDX_ID = 0;
    const COL_IDX_NAME = 1;
    const COL_IDX_TYPE_ID = 2;
    const COL_IDX_PARENT_ID = 3;
    const COL_IDX_BRANCH = 4;
    const COL_IDX_LOGO_URL = 5;
    const COL_IDX_WEB_URL = 6;
    const COL_IDX_NOTES = 7;
    const COL_IDX_CREATED = 8;
    const COL_IDX_UPDATED = 9;
    const COL_IDX_EAN_MFR_CODE = 10;
    const COL_IDX_COUNTRY_ID = 11;
    const COL_IDX_ACTOR_ID = 12;
    const COL_IDX_STATUS_ID = 13;

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Columns Declarations">

    public static function getAllColumnsSQL() {
        return " " . Company::COL_NAME_ID . ", " .
                Company::getAllColumnsNoIdSQL();
    }

    public static function getAllColumnsNoIdSQL() {
        return " " . Company::COL_NAME_NAME . ", " .
                Company::COL_NAME_TYPE_ID . ", " .
                Company::COL_NAME_PARENT_ID . ", " .
                Company::COL_NAME_BRANCH . ", " .
                Company::COL_NAME_LOGO_URL . ", " .
                Company::COL_NAME_WEB_URL . ", " .
                Company::COL_NAME_NOTES . ", " .
                Company::COL_NAME_CREATED . ", " .
                Company::COL_NAME_EAN_MFR_CODE . ", " .
                Company::COL_NAME_COUNTRY_ID . ", " .
                Company::COL_NAME_ACTOR_ID . ", " .
                Company::COL_NAME_STATUS_ID;
    }

    public static function getArrayColumns() {
        return array(Company::COL_NAME_ID,
            Company::COL_NAME_NAME,
            Company::COL_NAME_TYPE_ID,
            Company::COL_NAME_PARENT_ID,
            Company::COL_NAME_BRANCH,
            Company::COL_NAME_LOGO_URL,
            Company::COL_NAME_WEB_URL,
            Company::COL_NAME_NOTES,
            Company::COL_NAME_CREATED,
            Company::COL_NAME_UPDATED .
            Company::COL_NAME_EAN_MFR_CODE,
            Company::COL_NAME_COUNTRY_ID,
            Company::COL_NAME_ACTOR_ID,
            Company::COL_NAME_STATUS_ID);
    }

    // </editor-fold>
}

// </editor-fold>


