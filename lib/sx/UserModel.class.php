<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Sep 27, 2018 
 *  Filename          : UserModel.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of UserModel
 *
 * @author IZIordanov
 */
class UserModel {
    //put your code here
    
    function __construct($amsUser) {
        $this->user_id = $amsUser->getId();
        $this->e_mail = $amsUser->getEMail();
        $this->user_name = $amsUser->getName();
        $this->user_role = $amsUser->getRole();
        $this->roleName = $amsUser->getRoleName();
        $this->is_receive_emails = $amsUser->getIsReceiveEMails();
        $this->ip_address = $amsUser->getIpAddress();
        $this->company_id = $amsUser->getCompanyId();;
        $this->lastLogged = $amsUser->getLastLogged();
        $this->udate = $amsUser->getUpdated();
    }
}
