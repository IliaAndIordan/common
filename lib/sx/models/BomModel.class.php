<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : models    
 *  Date Creation       : Feb 18, 2021 
 *  Filename            : BomModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of Country
 *
 * @author IZIordanov
 */
class BomModel {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $bomId;
    public $bomName;
    public $description;
    public $companyId;
    public $userId;
    public $adate;
    public $udate;
    
    public $itemsCount;
    public $userName;
    public $userEmail;
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public function LoadById($bom_id) {
        $mn = "BomModel::LoadById(".$bom_id.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
                $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = BomModel::SelectJson($bom_id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = $objArrJ[0];
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public function Save($dataJson) {
        $mn = "BomModel::Save()";
        SxLogger::logBegin($mn);
        $response = null;
        // SxLogger::log($mn, " bomId = " . $dataJson->bomId);
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = null;
            if(isset($dataJson->bomId)){
               SxLogger::log($mn, "Update  bomId =" . $dataJson->bomId);
               $id = $dataJson->bomId;
               $id = BomModel::Update($dataJson, $conn, $mn, $logModel);

            } else{
                SxLogger::log($mn, "Create bom");
                $id = BomModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            SxLogger::log($mn, " bom_id =" . $id);
            if(isset($id)){
                $objArrJ = BomModel::SelectJson($id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = $objArrJ[0];
                }
            }
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
       return $response;
    }
    
    public function GetTable($params) {
        $mn = "BomModel::GetTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT b.bom_id as bomId,
                        b.bom_name as bomName,
                        b.bom_description as description,
                        b.company_id as companyId,
                        b.user_id as userId,
                        b.adate, b.udate, 
                        count(bi.bom_item_id) as itemsCount,
                        u.user_name as userName,
                        u.e_mail as userEmail
                    FROM iordanov_sx.sx_bom b
                    left join iordanov_sx.sx_bom_item bi on bi.bom_id = b.bom_id 
                    left join iordanov_sx.sx_user u on u.user_id = b.user_id ";
            
            
            
            if(isset($params->companyId) && strlen($params->companyId)>0){
                $sqlWhere = " WHERE b.company_id = ".$params->companyId." ";
            }
            else if(isset($params->userId) && strlen($params->userId)>0){
                $sqlWhere = " WHERE b.user_id = ".$params->userId." ";
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (b.bom_name like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE (b.bom_description like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = " group by b.bom_id ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by b.bom_id desc, bomName ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("boms", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_sx.sx_bom b
                    left join iordanov_sx.sx_bom_item bi on bi.bom_id = b.bom_id 
                    left join iordanov_sx.sx_user u on u.user_id = b.user_id ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $sql .= " group by b.bom_id ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    function SelectJson($bom_id, $conn, $mn, $logModel){
        
        $sql = " SELECT b.bom_id as bomId,
                    b.bom_name as bomName,
                    b.bom_description as description,
                    b.company_id as companyId,
                    b.user_id as userId,
                    b.adate, b.udate, 
                    count(bi.bom_item_id) as itemsCount
                FROM iordanov_sx.sx_bom b
                    left join iordanov_sx.sx_bom_item bi on bi.bom_id = b.bom_id
                    where b.bom_id=?
                    group by b.bom_id " ;

        $bound_params_r = ["i",$bom_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_sx.sx_bom
            (bom_name, bom_description, company_id, user_id)
            VALUES(?, ?, ?, ?)" ;

        $bound_params_r = ["ssii",
            ((!isset($dataJson->bomName)) ? null : $dataJson->bomName),
            ((!isset($dataJson->description)) ? null : $dataJson->description),
            ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->userId)) ? null : $dataJson->userId)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_sx.sx_bom
                    SET bom_name=?, 
                    bom_description=?, 
                    company_id=?, 
                    user_id=?
                    WHERE bom_id = ? " ;

        $bound_params_r = ["ssiii",
            ((!isset($dataJson->bomName)) ? null : $dataJson->bomName),
            ((!isset($dataJson->description)) ? null : $dataJson->description),
            ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->userId)) ? null : $dataJson->userId),
            ($dataJson->bomId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->bomId;
    }
    
    function Delete($bom_id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM iordanov_sx.sx_bom
                   WHERE bom_id = ? " ;

        $bound_params_r = ["i", $bom_id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "deleted bom_id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}
