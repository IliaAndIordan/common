<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : models    
 *  Date Creation       : Feb 18, 2021 
 *  Filename            : PsmCompanyModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of Country
 *
 * @author IZIordanov
 */
class PsmCompanyModel {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $companyId;
    public $name;
    public $typeId;
    public $parentCompanyId;
    public $branch;
    public $logoUrl;
    public $webUrl;
    public $notes;
    public $eanMfrCode;
    public $countryId;
    public $actorId;
    public $companyStatusId;
    public $adate;
    public $udate;
    public $iso2;
    public $countryName;
   
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    
    
    public static function LoadById($id) {
        $mn = "PsmCompanyModel::LoadById(".$id.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = PsmCompanyModel::SelectJson($id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($dataJson) {
        $mn = "PsmCompanyModel::Save()";
        SxLogger::logBegin($mn);
        $response = null;
        // SxLogger::log($mn, " userId = " . $dataJson->companyId);
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = null;
            if(isset($dataJson->companyId)){
               SxLogger::log($mn, "Update  companyId =" . $dataJson->companyId);
               $id = $dataJson->companyId;
               $id = PsmCompanyModel::Update($dataJson, $conn, $mn, $logModel);

            } else{
                SxLogger::log($mn, "Create companyId Item");
                $id = PsmCompanyModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            SxLogger::log($mn, " companyId =" . $id);
            if(isset($id)){
                $objArrJ = PsmCompanyModel::SelectJson($id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
       return $response;
    }
    
    
    public static function GetTable($params) {
        $mn = "PsmCompanyModel::GetTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT c.company_id as companyId,
                    c.company_name as name,
                    c.company_type_id as typeId,
                    c.parent_company_id as parentCompanyId,
                    c.branch_code as branch,
                    c.logo_url as logoUrl,
                    c.web_url as webUrl,
                    c.notes as notes,
                    c.ean_mfr_code as eanMfrCode,
                    c.country_id as countryId,
                    c.actor_id as actorId,
                    c.company_status_id as companyStatusId,
                    c.adate, c.udate,
                    a.user_name as actorName
                    FROM iordanov_sm.sm_company c
                    left join iordanov_sx.sx_user a on a.user_id = c.actor_id ";
            
             if(isset($params->company_name) && strlen($params->company_name)>1){
                    $sqlWhere = " WHERE (c.company_name = '".$params->company_name."' ";
                    $sqlWhere .= "  AND c.branch_code = '".$params->branch_code."')  ";
             }
             else if(isset($params->parentCompanyId) && strlen($params->parentCompanyId)>0){
                $sqlWhere = " WHERE c.parent_company_id = ".$params->parentCompanyId." ";
             }
            
            if(isset($params->typeId) && strlen($params->typeId)>0){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND c.company_type_id = ".$params->typeId." ";
                } else{
                   $sqlWhere = " WHERE c.company_type_id = ".$params->typeId." "; 
                }
            }
            
            if(isset($params->companyStatusId) && strlen($params->companyStatusId)>0){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND c.company_status_id = ".$params->companyStatusId." ";
                } else{
                   $sqlWhere = " WHERE c.company_status_id = ".$params->companyStatusId." "; 
                }
            }
            
            if(isset($params->countryId) && strlen($params->countryId)>0){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND c.country_id = ".$params->countryId." ";
                } else{
                   $sqlWhere = " WHERE c.country_id = ".$params->countryId." "; 
                }
                
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (c.company_nam like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR c.branch_code like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR c.ean_mfr_code like '%".$params->qry_filter."%' )";
                }
            else{
                    $sqlWhere .= " WHERE (c.company_nam like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR c.branch_code like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR c.ean_mfr_code like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = " ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by company_nam, branch_code ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("companies", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_sx.sx_user ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            // $sql .= " group by b.bom_id ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function  SelectJson($id, $conn, $mn, $logModel){
        
        $sql = " SELECT 
                    c.company_id as companyId,
                    c.company_name as name,
                    c.company_type_id as typeId,
                    c.parent_company_id as parentCompanyId,
                    c.branch_code as branch,
                    c.logo_url as logoUrl,
                    c.web_url as webUrl,
                    c.notes as notes,
                    c.ean_mfr_code as eanMfrCode,
                    c.country_id as countryId,
                    c.actor_id as actorId,
                    c.company_status_id as companyStatusId,
                    c.adate, c.udate, 
                    count(distinct(u.user_id)) as usersCount,
                    ct.country_iso2 as iso2, ct.country_name as countryName,
                    count(distinct(p.product_id)) as pmsProductCount,
                    count(distinct(j.project_id)) as projectsCount
                    FROM iordanov_sm.sm_company c
                    left join iordanov_sx.sx_user u on u.company_id = c.company_id
                    left join ams_wad.cfg_country ct on ct.country_id = c.country_id
                    left join iordanov_psm.psm_product p on p.company_id = c.company_id
                    left join iordanov_psm.psm_project j on j.company_id = c.company_id
                    where c.company_id=? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_sm.sm_company
                (company_name, company_type_id, parent_company_id,
                branch_code, logo_url, web_url, notes,
                ean_mfr_code, country_id, actor_id, company_status_id)
            VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["siisssssiii",
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->typeId)) ? null : $dataJson->typeId),
            ((!isset($dataJson->parentCompanyId)) ? null : $dataJson->parentCompanyId),
            ((!isset($dataJson->branch)) ? null : $dataJson->branch),
            ((!isset($dataJson->logoUrl)) ? null : $dataJson->logoUrl),
            ((!isset($dataJson->webUrl)) ? null : $dataJson->webUrl),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->eanMfrCode)) ? null : $dataJson->eanMfrCode),
            ((!isset($dataJson->countryId)) ? null : $dataJson->countryId),
            ((!isset($dataJson->actorId)) ? null : $dataJson->actorId),
            ((!isset($dataJson->companyStatusId)) ? null : $dataJson->companyStatusId)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_sm.sm_company
                    SET company_name=?, 
                    company_type_id=?, 
                    parent_company_id=?, 
                    branch_code=?, 
                    logo_url=?,
                    web_url=?, 
                    notes=?, 
                    ean_mfr_code=?, 
                    country_id=?, 
                    actor_id=?, 
                    company_status_id=?
                    WHERE company_id = ? " ;

        $bound_params_r = ["siisssssiiii",
             ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->typeId)) ? null : $dataJson->typeId),
            ((!isset($dataJson->parentCompanyId)) ? null : $dataJson->parentCompanyId),
            ((!isset($dataJson->branch)) ? null : $dataJson->branch),
            ((!isset($dataJson->logoUrl)) ? null : $dataJson->logoUrl),
            ((!isset($dataJson->webUrl)) ? null : $dataJson->webUrl),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->eanMfrCode)) ? null : $dataJson->eanMfrCode),
            ((!isset($dataJson->countryId)) ? null : $dataJson->countryId),
            ((!isset($dataJson->actorId)) ? null : $dataJson->actorId),
            ((!isset($dataJson->companyStatusId)) ? null : $dataJson->companyStatusId),
            ($dataJson->companyId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->companyId;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM iordanov_sm.sm_company
                   WHERE company_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "deleted user_id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}

