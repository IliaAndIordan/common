<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : models    
 *  Date Creation       : Feb 18, 2021 
 *  Filename            : PsmModuleModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */


/**
 * Description of PsmModuleModel
 *
 * @author IZIordanov
 */
class PsmModuleModel {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $moduleId;
    public $name;
    public $moduleTypeId;
    public $psmProductId;
    public $notes;
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadByProductId($id) {
        $mn = "PsmModuleModel::LoadByProductId(".$id.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = PsmModuleModel::SelectJsonProductId($id, $conn, $mn, $logModel);
            $response = json_decode(json_encode($objArrJ));
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function LoadById($id) {
        $mn = "PsmModuleModel::LoadById(".$id.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = PsmModuleModel::SelectJson($id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($dataJson) {
        $mn = "PsmModuleModel::Save()";
        SxLogger::logBegin($mn);
        $response = null;
        // SxLogger::log($mn, " cpId = " . $dataJson->cpId);
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = null;
            if(isset($dataJson->moduleId)){
               SxLogger::log($mn, "Update  moduleId =" . $dataJson->moduleId);
               $id = $dataJson->moduleId;
               $id = PsmModuleModel::Update($dataJson, $conn, $mn, $logModel);

            } else{
                SxLogger::log($mn, "Create moduleId Item");
                $id = PsmModuleModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            //SxLogger::log($mn, " moduleId =" . $id);
            if(isset($id)){
                $objArrJ = PsmModuleModel::SelectJson($id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
       return $response;
    }
    
    public static function GetTable($params) {
        $mn = "PsmModuleModel::GetTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT m.module_id as moduleId,
                        m.module_name as name,
                        m.module_type_id as moduleTypeId,
                        m.product_id as psmProductId,
                        m.notes,
                        p.product_key as psmProductKey,
                        p.product_name as psmProductName,
                        p.company_id as companyId
                   FROM iordanov_psm.psm_module m
                   left join iordanov_psm.psm_product p on p.product_id = m.product_id";
            
            if(isset($params->psmProductId) && strlen($params->psmProductId)>0){
                $sqlWhere = " WHERE m.product_id = ".$params->psmProductId." ";
            }
            
            if(isset($params->companyId) && strlen($params->companyId)>0){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND p.company_id = ".$params->companyId." ";
                } else{
                   $sqlWhere = " WHERE p.company_id = ".$params->companyId." "; 
                }
            }
            
            if(isset($params->moduleTypeId) && strlen($params->moduleTypeId)>0){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND m.module_type_id = ".$params->moduleTypeId." ";
                } else{
                   $sqlWhere = " WHERE m.module_type_id = ".$params->moduleTypeId." "; 
                }
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (m.module_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.product_key like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.product_name like '%".$params->qry_filter."%' )";
                }
            else{
                    $sqlWhere .= " WHERE (m.module_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.product_key like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.product_name like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = " ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by moduleTypeId, name ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("psmModules", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_psm.psm_module m
                   left join iordanov_psm.psm_product p on p.product_id = m.product_id ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            // $sql .= " group by b.bom_id ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function  SelectJsonProductId($id, $conn, $mn, $logModel){
        
        $sql = " SELECT module_id as moduleId,
                    module_name as name,
                    module_type_id as moduleTypeId,
                    product_id as psmProductId,
                    notes
                 FROM iordanov_psm.psm_module
                 where product_id=?" ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function  SelectJson($id, $conn, $mn, $logModel){
        
        $sql = " SELECT module_id as moduleId,
                    module_name as name,
                    module_type_id as moduleTypeId,
                    product_id as psmProductId,
                    notes
                 FROM iordanov_psm.psm_module
                where module_id=?" ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_psm.psm_module
                (module_name, module_type_id, product_id, notes)
                VALUES( ?, ?, ?, ?)" ;

        $bound_params_r = ["siis",
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->moduleTypeId)) ? null : $dataJson->moduleTypeId),
            ((!isset($dataJson->psmProductId)) ? null : $dataJson->psmProductId),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_psm.psm_module
            SET module_name = ?, 
                module_type_id = ?, 
                product_id = ?,
                notes = ?
            WHERE module_id = ? " ;

        $bound_params_r = ["siisi",
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->moduleTypeId)) ? null : $dataJson->moduleTypeId),
            ((!isset($dataJson->psmProductId)) ? null : $dataJson->psmProductId),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ($dataJson->moduleId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->moduleId;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM iordanov_psm.psm_module
                   WHERE module_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "deleted id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}

