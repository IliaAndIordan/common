<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : models    
 *  Date Creation       : Feb 18, 2021 
 *  Filename            : PsmUserModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of Country
 *
 * @author IZIordanov
 */
class PsmUserModel {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $userId;
    public $name;
    public $email;
    public $password;
    public $companyId;
    public $role;
    public $isReceiveEmails;
    public $ipAddress;
    public $adate;
    public $udate;
   
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function loadJSON($Obj, $json)
    {
        $dcod = json_decode($json);
        $prop = get_object_vars ( $dcod );
        foreach($prop as $key => $lock)
        {
            if(property_exists ( $Obj ,  $key ))
            {
                if(is_object($dcod->$key))
                {
                    loadJSON($Obj->$key, json_encode($dcod->$key));
                }
                else
                {
                    $Obj->$key = $dcod->$key;
                }
            }
        }
    }
    
    public static function Login($email, $pwd) {
        $mn = "PsmUserModel::Login()";
        SxLogger::logBegin($mn);
        SxLogger::log($mn, " eMail: " . $email.", password".$pwd);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = PsmUserModel::LoginJson($email, $pwd, $conn, $mn, $logModel);
            SxLogger::log($mn, "Login: Found users count(objArrJ) = ".count($objArrJ)."!");
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
           $logModel->logDebug($mn, "response=".json_encode($response));
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function CountRowsEmail($email) {
        $mn = "PsmUserModel::CountRowsEmail(email)";
        SxLogger::logBegin($mn);
        //SxLogger::log($mn, " eMail: " . $email);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $response = PsmUserModel::SelectRowsEmail($email, $conn, $mn, $logModel);
           //$logModel->logDebug($mn, "response=".json_encode($response));
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function LoadById($userId) {
        $mn = "PsmUserModel::LoadById(".$userId.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = PsmUserModel::SelectJson($userId, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "PsmUserModel::Save()";
        SxLogger::logBegin($mn);
        $response = null;
        SxLogger::log($mn, " user = " . json_encode($data));
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = $data->userId;
            if(isset($id)){
               SxLogger::log($mn, "Update  userId =" . $data->userId);
               $id = $data->userId;
               $id = PsmUserModel::Update($data, $conn, $mn, $logModel);

            } else{
                SxLogger::log($mn, "Create userId Item");
                $id = PsmUserModel::Create($data, $conn, $mn, $logModel);
            }
            
            SxLogger::log($mn, " userId =" . $id);
            if(isset($id)){
                $objArrJ = PsmUserModel::SelectJson($id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
       return $response;
    }
    
    
    public static function GetTable($params) {
        $mn = "PsmUserModel::GetTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT user_id as userId,
                    user_name as name,
                    e_mail as email,
                    password as password,
                    company_id as companyId,
                    user_role as role,
                    is_receive_emails as isReceiveEmails,
                    ip_address as ipAddress,
                    adate, udate
                    FROM iordanov_sx.sx_user ";
            
            
            
            if(isset($params->companyId) && strlen($params->companyId)>0){
                $sqlWhere = " WHERE company_id = ".$params->companyId." ";
            }
            
            
            if(isset($params->filter) && strlen($params->filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (user_name like '%".$params->filter."%' ";
                    $sqlWhere .= "  OR e_mail like '%".$params->filter."%' ";
                    $sqlWhere .= "  OR ip_address like '%".$params->filter."%' )";
                }
            else{
                    $sqlWhere .= " WHERE (user_name like '%".$params->filter."%' ";
                    $sqlWhere .= "  OR e_mail like '%".$params->filter."%' ";
                    $sqlWhere .= "  OR ip_address like '%".$params->filter."%' )";
                }
               
            }
            $sqlOrder = " ";
            if(isset($params->sortCol)){
                $sqlOrder .= " order by ".$params->sortCol." ".($params->sortDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by user_name, e_mail ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->limit, $params->offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("users", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_sx.sx_user ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            // $sql .= " group by b.bom_id ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function  SelectJson($id, $conn, $mn, $logModel){
        
        $sql = " SELECT user_id as userId,
                    user_name as name,
                    e_mail as email,
                    password as password,
                    company_id as companyId,
                    user_role as role,
                    is_receive_emails as isReceiveEmails,
                    ip_address as ipAddress,
                    adate, udate
                    FROM iordanov_sx.sx_user
                    where user_id=? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function LoginJson($email, $pwd, $conn, $mn, $logModel){
        
        $sql = "SELECT user_id as userId,
                    user_name as name,
                    e_mail as email,
                    password as password,
                    company_id as companyId,
                    user_role as role,
                    is_receive_emails as isReceiveEmails,
                    ip_address as ipAddress,
                    adate, udate
                    FROM iordanov_sx.sx_user
                    WHERE e_mail = ? and password =?" ;

        $bound_params_r = ["ss",$email, $pwd];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_sx.sx_user
            (user_name, e_mail, password, company_id, user_role, 
            is_receive_emails, ip_address )
            VALUES(?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["sssiiis",
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->email)) ? null : $dataJson->email),
            ((!isset($dataJson->password)) ? null : $dataJson->password),
            ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->role)) ? null : $dataJson->role),
            ((!isset($dataJson->isReceiveEmails)) ? null : $dataJson->isReceiveEmails),
            ((!isset($dataJson->ipAddress)) ? null : $dataJson->ipAddress)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_sx.sx_user
                    SET user_name=?, 
                    e_mail=?, password=?, company_id=?, user_role=?,
                    is_receive_emails=?, ip_address=?
                    WHERE user_id = ? " ;

        $bound_params_r = ["sssiiisi",
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->email)) ? null : $dataJson->email),
            ((!isset($dataJson->password)) ? null : $dataJson->password),
            ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->role)) ? null : $dataJson->role),
            ((!isset($dataJson->isReceiveEmails)) ? null : $dataJson->isReceiveEmails),
            ((!isset($dataJson->ipAddress)) ? null : $dataJson->ipAddress),
            ($dataJson->userId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->userId;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM iordanov_sx.sx_user
                   WHERE user_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "deleted user_id =" . $id);
                    
        return $id;
    }
    
    static function SelectRowsEmail($email, $conn, $mn, $logModel){
        
        $sql = "SELECT count(*) AS rows, '".$email."' as userEmail
                    FROM iordanov_sx.sx_user u
                    WHERE u.e_mail = ? " ;

        $bound_params_r = ["s",$email];
        $nrRows = -1;
        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
        if(isset($ret_json_data) && count($ret_json_data)>0){
            $response = json_decode(json_encode($ret_json_data[0]));
            $nrRows = $response->rows;
         }
         //SxLogger::log($mn, "Found ".$nrRows." users with E-Mail ".$email."!");
          
         return $nrRows;
    }
    
    // </editor-fold>
}

