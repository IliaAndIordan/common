<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : models    
 *  Date Creation       : Feb 18, 2021 
 *  Filename            : PsmProductModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of PsmCompanyProductModel
 *
 * @author IZIordanov
 */
class PsmCompanyProductModel {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $cpId;
    public $companyId;
    public $productId;
    public $adate;
   
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadByCompanyId($id) {
        $mn = "PsmCompanyProductModel::LoadByCompanyId(".$id.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = PsmCompanyProductModel::SelectJsonCompanyId($id, $conn, $mn, $logModel);
            $response = json_decode(json_encode($objArrJ));
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function LoadById($id) {
        $mn = "PsmCompanyProductModel::LoadById(".$id.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = PsmCompanyProductModel::SelectJson($id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($dataJson) {
        $mn = "PsmCompanyProductModel::Save()";
        SxLogger::logBegin($mn);
        $response = null;
        // SxLogger::log($mn, " cpId = " . $dataJson->cpId);
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = null;
            if(isset($dataJson->cpId)){
               //SxLogger::log($mn, "Update  cpId =" . $dataJson->cpId);
               $id = $dataJson->cpId;
               $id = PsmCompanyProductModel::Update($dataJson, $conn, $mn, $logModel);

            } else{
                //SxLogger::log($mn, "Create cpId Item");
                $id = PsmCompanyProductModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            //SxLogger::log($mn, " cpId =" . $id);
            if(isset($id)){
                $objArrJ = PsmCompanyProductModel::SelectJson($id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
       return $response;
    }
    
    
    public static function GetTable($params) {
        $mn = "PsmCompanyProductModel::GetTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT c.product_id as pmsProductId,
                    c.product_key as pmsProductKey,
                    c.product_name as name,
                    c.product_image_url as logoUrl,
                    c.product_web_url as webUrl,
                    c.product_note as notes,
                    c.company_id as companyId,
                    c.user_id as actorId,
                    c.adate, c.udate,
                    a.user_name as actorName
                    FROM iordanov_psm.psm_product c
                    left join iordanov_sx.sx_user a on a.user_id = c.actor_id  ";
            
             if(isset($params->companyId) && strlen($params->companyId)>0){
                $sqlWhere = " WHERE c.company_id = ".$params->companyId." ";
             }
             else if(isset($params->actorId) && strlen($params->actorId)>0){
                $sqlWhere = " WHERE c.user_id = ".$params->actorId." ";
             }
            
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (c.product_key like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR c.product_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR c.product_note like '%".$params->qry_filter."%' )";
                }
            else{
                    $sqlWhere .= " WHERE (c.product_key like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR c.product_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR c.product_note like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = " ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by product_name, product_key ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            //SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("psmProducts", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_sx.sx_user ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            // $sql .= " group by b.bom_id ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function  SelectJsonCompanyId($id, $conn, $mn, $logModel){
        
        $sql = " SELECT 
            company_product_id as cpId,
            company_id as companyId,
            product_id as productId,
            adate
            FROM iordanov_sm.sm_company_product
            where company_id = ? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function  SelectJson($id, $conn, $mn, $logModel){
        
        $sql = " SELECT 
            company_product_id as cpId,
            company_id as companyId,
            product_id as productId,
            adate
            FROM iordanov_sm.sm_company_product
            where company_product_id = ? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_sm.sm_company_product
                (company_id, product_id)
                VALUES( ?, ?)" ;

        $bound_params_r = ["ii",
            ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->productId)) ? null : $dataJson->productId)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        //SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_sm.sm_company_product
            SET company_id=?, 
            product_id=?
            WHERE company_product_id = ? " ;

        $bound_params_r = ["iii",
            ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->productId)) ? null : $dataJson->productId),
            ($dataJson->cpId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->cpId;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM iordanov_sm.sm_company_product
                   WHERE company_product_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        //SxLogger::log($mn, "deleted id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}
