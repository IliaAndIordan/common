<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : models    
 *  Date Creation       : Feb 18, 2021 
 *  Filename            : PsmProjectModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

require_once("Functions.php");
/**
 * Description of PsmProjectModel
 *
 * @author IZIordanov
 */
class PsmProjectModel {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $projectId;
    public $name;
    public $statusId;
    public $companyId;
    public $productId;
    public $accountKey;
    public $managerId;
    public $estimatorId;
    public $notes;
    public $dayStart;
    public $dayEnd;
    public $adate;
    public $udate;
    
    public $productName;
    public $managerName;
    public $managerEmail;
    public $estimatorName;
    public $estimatorEmail;
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadByCompanyId($id) {
        $mn = "PsmProjectModel::LoadByCompanyId(".$id.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = PsmProjectModel::SelectJsonCompanyId($id, $conn, $mn, $logModel);
            $response = json_decode(json_encode($objArrJ));
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function LoadById($id) {
        $mn = "PsmProjectModel::LoadById(".$id.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = PsmProjectModel::SelectJson($id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($dataJson) {
        $mn = "PsmProjectModel::Save()";
        SxLogger::logBegin($mn);
        $response = null;
        // SxLogger::log($mn, " cpId = " . $dataJson->cpId);
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = null;
            if(isset($dataJson->projectId)){
               SxLogger::log($mn, "Update  projectId =" . $dataJson->projectId);
               $id = $dataJson->projectId;
               $id = PsmProjectModel::Update($dataJson, $conn, $mn, $logModel);

            } else{
                SxLogger::log($mn, "Create projectId Item");
                $id = PsmProjectModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            //SxLogger::log($mn, " psmProductId =" . $id);
            if(isset($id)){
                $objArrJ = PsmProjectModel::SelectJson($id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
       return $response;
    }
    
    public static function GetTable($params) {
        $mn = "PsmProjectModel::GetTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT p.project_id as projectId,
                    p.project_name as name,
                    p.pstatus_id as statusId,
                    p.company_id as companyId,
                    p.product_id as productId,
                    p.project_account_id as accountKey,
                    p.manager_id as managerId,
                    p.estimator_id as estimatorId,
                    p.notes,
                    p.start_date as dayStart,
                    p.end_date as dayEnd,
                    p.adate, p.udate,
                    d.product_name as productName,
                    a.user_name as managerName,
                    a.e_mail as managerEmail,
                    e.user_name as estimatorName,
                    e.e_mail as estimatorEmail
                FROM iordanov_psm.psm_project p
                left join iordanov_psm.psm_product d on d.product_id = p.product_id
                left join iordanov_sx.sx_user a on a.user_id = p.manager_id
                left join iordanov_sx.sx_user e on e.user_id = p.estimator_id ";
            
            if(isset($params->companyId) && strlen($params->companyId)>0){
                $sqlWhere = " WHERE p.company_id = ".$params->companyId." ";
            }
             
            if(isset($params->productId) && strlen($params->productId)>0){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND p.product_id = ".$params->productId." ";
                } else{
                   $sqlWhere = " WHERE p.product_id = ".$params->productId." "; 
                }
            }
            
            if(isset($params->statusId) && strlen($params->statusId)>0){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND p.pstatus_id = ".$params->statusId." ";
                } else{
                   $sqlWhere = " WHERE p.pstatus_id = ".$params->statusId." "; 
                }
            }
            
            if(isset($params->managerId) && strlen($params->managerId)>0){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND p.manager_id = ".$params->managerId." ";
                } else{
                   $sqlWhere = " WHERE p.manager_id = ".$params->managerId." "; 
                }
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (p.project_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR d.product_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.project_account_id like '%".$params->qry_filter."%' )";
                }
            else{
                    $sqlWhere .= " WHERE (p.project_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR d.product_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.project_account_id like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = " ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by udate desc, name, project_id ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("projects", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_psm.psm_project p
                    left join iordanov_psm.psm_product d on d.product_id = p.product_id
                    left join iordanov_sx.sx_user a on a.user_id = p.manager_id
                    left join iordanov_sx.sx_user e on e.user_id = p.estimator_id ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            // $sql .= " group by b.bom_id ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function  SelectJsonCompanyId($id, $conn, $mn, $logModel){
        
        $sql = " SELECT p.project_id as projectId,
                    p.project_name as name,
                    p.pstatus_id as statusId,
                    p.company_id as companyId,
                    p.product_id as productId,
                    p.project_account_id as accountKey,
                    p.manager_id as managerId,
                    p.estimator_id as estimatorId,
                    p.notes,
                    p.start_date as dayStart,
                    p.end_date as dayEnd,
                    p.adate, p.udate,
                    d.product_name as productName,
                    a.user_name as managerName,
                    a.e_mail as managerEmail,
                    e.user_name as estimatorName,
                    e.e_mail as estimatorEmail
                FROM iordanov_psm.psm_project p
                left join iordanov_psm.psm_product d on d.product_id = p.product_id
                left join iordanov_sx.sx_user a on a.user_id = p.manager_id
                left join iordanov_sx.sx_user e on e.user_id = p.estimator_id
                where p.company_id=?" ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function  SelectJson($id, $conn, $mn, $logModel){
        
        $sql = " SELECT p.project_id as projectId,
                    p.project_name as name,
                    p.pstatus_id as statusId,
                    p.company_id as companyId,
                    p.product_id as productId,
                    p.project_account_id as accountKey,
                    p.manager_id as managerId,
                    p.estimator_id as estimatorId,
                    p.notes,
                    p.start_date as dayStart,
                    p.end_date as dayEnd,
                    p.adate, p.udate,
                    d.product_name as productName,
                    a.user_name as managerName,
                    a.e_mail as managerEmail,
                    e.user_name as estimatorName,
                    e.e_mail as estimatorEmail
                FROM iordanov_psm.psm_project p
                left join iordanov_psm.psm_product d on d.product_id = p.product_id
                left join iordanov_sx.sx_user a on a.user_id = p.manager_id
                left join iordanov_sx.sx_user e on e.user_id = p.estimator_id
                where p.project_id=?" ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_psm.psm_project
                (project_name, pstatus_id, company_id,
                product_id, project_account_id, manager_id,
                estimator_id, notes, start_date,
                end_date)
                VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["siiisiisss",
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->statusId)) ? 1 : $dataJson->statusId),
            ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->productId)) ? null : $dataJson->productId),
            ((!isset($dataJson->accountKey)) ? null : $dataJson->accountKey),
            ((!isset($dataJson->managerId)) ? null : $dataJson->managerId),
            ((!isset($dataJson->estimatorId)) ? null : $dataJson->estimatorId),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->dayStart)) ? null : gePhpDate($dataJson->dayStart)->format("Y-m-d H:i:s")),
            ((!isset($dataJson->dayEnd)) ? null : gePhpDate($dataJson->dayEnd)->format("Y-m-d H:i:s")),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_psm.psm_project
            SET project_name = ?, 
                pstatus_id = ?, 
                company_id = ?,
                product_id = ?, 
                project_account_id = ?, 
                manager_id = ?, 
                estimator_id=?, 
                notes = ?, start_date = ?, end_date = ?
            WHERE project_id = ? " ;

        $bound_params_r = ["siiisiisssi",
           ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->statusId)) ? 1 : $dataJson->statusId),
            ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->productId)) ? null : $dataJson->productId),
            ((!isset($dataJson->accountKey)) ? null : $dataJson->accountKey),
            ((!isset($dataJson->managerId)) ? null : $dataJson->managerId),
            ((!isset($dataJson->estimatorId)) ? null : $dataJson->estimatorId),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->dayStart)) ? null : gePhpDate($dataJson->dayStart)->format("Y-m-d H:i:s")),
            ((!isset($dataJson->dayEnd)) ? null : gePhpDate($dataJson->dayEnd)->format("Y-m-d H:i:s")),
            ($dataJson->projectId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->projectId;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM iordanov_psm.psm_project
                   WHERE project_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "deleted id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}

