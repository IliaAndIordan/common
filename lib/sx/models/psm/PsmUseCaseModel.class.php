<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : models    
 *  Date Creation       : April 01, 2022 
 *  Filename            : PsmUseCaseModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2022 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */


/**
 * Description of PsmUseCaseModel
 *
 * @author IZIordanov
 */
class PsmUseCaseModel {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $ucId;
    public $name;
    public $ucOrder;
    public $notes;
    public $imageUrl;
    public $statusId;
    public $psmProductId;
    public $parentUcId;
    public $actorId;
    public $adate;
    public $udate;
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadByProductId($id) {
        $mn = "PsmUseCaseModel::LoadByProductId(".$id.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = PsmUseCaseModel::SelectJsonProductId($id, $conn, $mn, $logModel);
            $response = json_decode(json_encode($objArrJ));
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function LoadById($id) {
        $mn = "PsmUseCaseModel::LoadById(".$id.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = PsmUseCaseModel::SelectJson($id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($dataJson) {
        $mn = "PsmUseCaseModel::Save()";
        SxLogger::logBegin($mn);
        $response = null;
        // SxLogger::log($mn, " ucId = " . $dataJson->cpId);
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = null;
            if(isset($dataJson->ucId)){
               SxLogger::log($mn, "Update  ucId =" . $dataJson->ucId);
               $id = $dataJson->ucId;
               $id = PsmUseCaseModel::Update($dataJson, $conn, $mn, $logModel);

            } else{
                SxLogger::log($mn, "Create ucId Item");
                $id = PsmUseCaseModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            //SxLogger::log($mn, " moduleId =" . $id);
            if(isset($id)){
                $objArrJ = PsmUseCaseModel::SelectJson($id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
       return $response;
    }
    
    public static function GetTable($params) {
        $mn = "PsmUseCaseModel::GetTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            
            $sql = "SELECT uc.uc_id as ucId,
                        uc.uc_name as name,
                        uc.uc_order as ucOrder,
                        uc.notes as notes,
                        uc.image_url as imageUrl,
                        uc.pstatus_id as statusId,
                        uc.product_id as psmProductId,
                        uc.parent_uc_id as parentUcId,
                        uc.user_id as actorId,
                        uc.adate, uc.udate,
                        p.company_id as companyId,
                        p.product_name as productName,
                        p.product_key as productKey,
                        u.user_name as actorName,
                        u.e_mail as actorEmail
                    FROM iordanov_psm.psm_usecase uc
                    left join iordanov_psm.psm_product p on p.product_id = uc.product_id
                    left join iordanov_sx.sx_user u on u.user_id = uc.user_id";
            
            if(isset($params->psmProductId) && strlen($params->psmProductId)>0){
                $sqlWhere = " WHERE uc.product_id = ".$params->psmProductId." ";
            }
            
            if(isset($params->companyId) && strlen($params->companyId)>0){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND p.company_id = ".$params->companyId." ";
                } else{
                   $sqlWhere = " WHERE p.company_id = ".$params->companyId." "; 
                }
            }
            
            if(isset($params->statusId) && strlen($params->statusId)>0){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND uc.pstatus_id = ".$params->statusId." ";
                } else{
                   $sqlWhere = " WHERE uc.pstatus_id = ".$params->statusId." "; 
                }
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (uc.uc_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.product_key like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.product_name like '%".$params->qry_filter."%' )";
                }
            else{
                    $sqlWhere .= " WHERE (m.module_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.product_key like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.product_name like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = " ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by ucOrder, name ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("psmModules", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_psm.psm_usecase uc
                    left join iordanov_psm.psm_product p on p.product_id = uc.product_id
                    left join iordanov_sx.sx_user u on u.user_id = uc.user_id ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            // $sql .= " group by b.bom_id ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function  SelectJsonProductId($id, $conn, $mn, $logModel){
        
        $sql = " SELECT uc.uc_id as ucId,
                    uc.uc_name as name,
                    uc.uc_order as ucOrder,
                    uc.notes as notes,
                    uc.image_url as imageUrl,
                    uc.pstatus_id as statusId,
                    uc.product_id as psmProductId,
                    uc.parent_uc_id as parentUcId,
                    uc.user_id as actorId,
                    uc.adate, uc.udate,
                    p.company_id as companyId,
                    p.product_name as productName,
                    p.product_key as productKey,
                    u.user_name as actorName,
                    u.e_mail as actorEmail
                FROM iordanov_psm.psm_usecase uc
                left join iordanov_psm.psm_product p on p.product_id = uc.product_id
                left join iordanov_sx.sx_user u on u.user_id = uc.user_id
                 where p.product_id=?" ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function  SelectJson($id, $conn, $mn, $logModel){
        
        $sql = " SELECT uc.uc_id as ucId,
                    uc.uc_name as name,
                    uc.uc_order as ucOrder,
                    uc.notes as notes,
                    uc.image_url as imageUrl,
                    uc.pstatus_id as statusId,
                    uc.product_id as psmProductId,
                    uc.parent_uc_id as parentUcId,
                    uc.user_id as actorId,
                    uc.adate, uc.udate,
                    p.company_id as companyId,
                    p.product_name as productName,
                    p.product_key as productKey,
                    u.user_name as actorName,
                    u.e_mail as actorEmail
                FROM iordanov_psm.psm_usecase uc
                left join iordanov_psm.psm_product p on p.product_id = uc.product_id
                left join iordanov_sx.sx_user u on u.user_id = uc.user_id
                 where uc.uc_id=?" ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_psm.psm_usecase
                (uc_name, uc_order,
                notes, image_url, pstatus_id,
                product_id, parent_uc_id, user_id)
                VALUES( ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["sissiiii",
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->ucOrder)) ? null : $dataJson->ucOrder),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->imageUrl)) ? null : $dataJson->imageUrl),
            ((!isset($dataJson->statusId)) ? null : $dataJson->statusId),
            ((!isset($dataJson->psmProductId)) ? null : $dataJson->psmProductId),
            ((!isset($dataJson->parentUcId)) ? null : $dataJson->parentUcId),
            ((!isset($dataJson->actorId)) ? null : $dataJson->actorId),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_psm.psm_usecase
            SET uc_name = ?, 
                uc_order = ?,
                notes = ?,
                image_url = ?,
                pstatus_id = ?,
                product_id = ?,
                parent_uc_id = ?,
                user_id = ?
            WHERE uc_id = ? " ;

        $bound_params_r = ["sissiiii",
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->ucOrder)) ? null : $dataJson->ucOrder),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->imageUrl)) ? null : $dataJson->imageUrl),
            ((!isset($dataJson->statusId)) ? null : $dataJson->statusId),
            ((!isset($dataJson->psmProductId)) ? null : $dataJson->psmProductId),
            ((!isset($dataJson->parentUcId)) ? null : $dataJson->parentUcId),
            ((!isset($dataJson->actorId)) ? null : $dataJson->actorId),
            ($dataJson->ucId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->ucId;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM iordanov_psm.psm_usecase
                   WHERE uc_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "deleted id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}

