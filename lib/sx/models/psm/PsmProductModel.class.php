<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : models    
 *  Date Creation       : Feb 18, 2021 
 *  Filename            : PsmProductModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */


/**
 * Description of PsmProductModel
 *
 * @author IZIordanov
 */
class PsmProductModel {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $psmProductId;
    public $psmProductKey;
    public $name;
    public $logoUrl;
    public $webUrl;
    public $notes;
    
    public $companyId;
    public $actorId;
    public $adate;
    public $udate;
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadByCompanyId($id) {
        $mn = "PsmProductModel::LoadByCompanyId(".$id.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = PsmProductModel::SelectJsonCompanyId($id, $conn, $mn, $logModel);
            $response = json_decode(json_encode($objArrJ));
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function LoadById($id) {
        $mn = "PsmProductModel::LoadById(".$id.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = PsmProductModel::SelectJson($id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($dataJson) {
        $mn = "PsmProductModel::Save()";
        SxLogger::logBegin($mn);
        $response = null;
        // SxLogger::log($mn, " cpId = " . $dataJson->cpId);
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = null;
            if(isset($dataJson->psmProductId)){
               SxLogger::log($mn, "Update  psmProductId =" . $dataJson->psmProductId);
               $id = $dataJson->psmProductId;
               $id = PsmProductModel::Update($dataJson, $conn, $mn, $logModel);

            } else{
                SxLogger::log($mn, "Create psmProductId Item");
                $id = PsmProductModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            //SxLogger::log($mn, " psmProductId =" . $id);
            if(isset($id)){
                $objArrJ = PsmProductModel::SelectJson($id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
       return $response;
    }
    
    public static function GetTable($params) {
        $mn = "PsmProductModel::GetTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT p.product_id as psmProductId,
                    p.product_key as psmProductKey,
                    p.product_name as name,
                    p.product_image_url as logoUrl,
                    p.product_web_url as webUrl,
                    p.product_note as notes,
                    p.company_id as companyId,
                    p.user_id as actorId,
                    u.user_name as actorName,
                    p.adate, p.udate
                    FROM iordanov_psm.psm_product p
                    left join iordanov_sx.sx_user u on u.user_id = p.user_id ";
            
            if(isset($params->companyId) && strlen($params->companyId)>0){
                $sqlWhere = " WHERE p.company_id = ".$params->companyId." ";
             }
            
            if(isset($params->actorId) && strlen($params->actorId)>0){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND p.user_id = ".$params->actorId." ";
                } else{
                   $sqlWhere = " WHERE p.user_id = ".$params->actorId." "; 
                }
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (p.product_key like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.product_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.product_note like '%".$params->qry_filter."%' )";
                }
            else{
                    $sqlWhere .= " WHERE (p.product_key like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.product_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR p.product_note like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = " ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by udate desc, name, psmProductKey ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("psmProducts", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_psm.psm_product p
                    left join iordanov_sx.sx_user u on u.user_id = p.user_id ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            // $sql .= " group by b.bom_id ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function  SelectJsonCompanyId($id, $conn, $mn, $logModel){
        
        $sql = " SELECT product_id as psmProductId,
                product_key as psmProductKey,
                product_name as name,
                product_image_url as logoUrl,
                product_web_url as webUrl,
                product_note as notes,
                company_id as companyId,
                user_id as actorId,
                adate, udate
                FROM iordanov_psm.psm_product
                where company_id=?" ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function  SelectJson($id, $conn, $mn, $logModel){
        
        $sql = " SELECT product_id as psmProductId,
                product_key as psmProductKey,
                product_name as name,
                product_image_url as logoUrl,
                product_web_url as webUrl,
                product_note as notes,
                company_id as companyId,
                user_id as actorId,
                adate, udate
                FROM iordanov_psm.psm_product
                where product_id=?" ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_psm.psm_product
                (product_key, product_name, product_image_url,
                product_web_url, product_note, company_id, user_id)
                VALUES( ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["sssssii",
            ((!isset($dataJson->psmProductKey)) ? null : $dataJson->psmProductKey),
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->logoUrl)) ? null : $dataJson->logoUrl),
            ((!isset($dataJson->webUrl)) ? null : $dataJson->webUrl),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->actorId)) ? null : $dataJson->actorId)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_psm.psm_product
            SET product_key = ?, 
                product_name = ?, product_image_url = ?,
                product_web_url = ?, product_note = ?, company_id = ?, user_id=?
            WHERE product_id = ? " ;

        $bound_params_r = ["sssssiii",
            ((!isset($dataJson->psmProductKey)) ? null : $dataJson->psmProductKey),
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->logoUrl)) ? null : $dataJson->logoUrl),
            ((!isset($dataJson->webUrl)) ? null : $dataJson->webUrl),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->actorId)) ? null : $dataJson->actorId),
            ($dataJson->psmProductId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->psmProductId;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM iordanov_psm.psm_product
                   WHERE product_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "deleted id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}

