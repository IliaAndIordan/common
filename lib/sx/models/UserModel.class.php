<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Dec 22, 2020 
 *  Filename            : UserModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of UserModel
 *
 * @author IZIordanov
 */
class SxUser {
    
   // <editor-fold defaultstate="collapsed" desc="Fields">

    public $userId;
    public $userName;
    public $userEmail;
    public $password;
    public $companyId;
    public $userRole = 1; //2 predefined roles 1-user, 3 admin, 2 editor
    
    public $isReceiveEmails;
    public $ipAddress;
    public $adate;
    public $udate;
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">

    public function Login($email, $pwd) {
        $mn = "SxUser::Login()";
        SxLogger::logBegin($mn);
        //SxLogger::log($mn, " eMail: " . $email.", password".$pwd); 
        $response = new SxUser();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $userJ = SxUser::LoginJson($email, $pwd, $conn, $mn, $logModel);
            $valJson = json_encode($userJ[0]);
            $user = json_decode($valJson);
            $response->userId = $user->userId;
            $response->userName = $user->userName;
            $response->userEmail = $user->userEmail;
            $response->companyId = $user->companyId;
            $response->password = $user->password;
            $response->userRole = $user->userRole;
            $response->ipAddress = $user->ipAddress;
            $response->isReceiveEmails = $user->isReceiveEmails;
            $response->adate = $user->adate;
            $response->udate = $user->udate;
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public function LoadById($user_id) {
        $mn = "SxUser::LoadById(".$user_id.")";
        SxLogger::logBegin($mn);
        $response = new SxUser();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $userJ = SxUser::SelectJson($user_id, $conn, $mn, $logModel);
            $valJson = json_encode($userJ[0]);
            $user = json_decode($valJson);
            $response->userId = $user->userId;
            $response->userName = $user->userName;
            $response->userEmail = $user->userEmail;
            $response->companyId = $user->companyId;
            $response->password = $user->password;
            $response->userRole = $user->userRole;
            $response->ipAddress = $user->ipAddress;
            $response->isReceiveEmails = $user->isReceiveEmails;
            $response->adate = $user->adate;
            $response->udate = $user->udate;
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public function Save($data) {
        $mn = "SxUser::Save()";
        SxLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //SxLogger::log($mn, " userEmail = " . $dataJson->userEmail);
        $response;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $user_id = null;
            if(isset($dataJson->userId)){
               //SxLogger::log($mn, "Update  userId =" . $dataJson->userId);
               $user_id = $dataJson->userId;
               SxUser::Update($dataJson, $conn, $mn, $logModel);

            } else{
                //SxLogger::log($mn, "Create user");
                $user_id = SxUser::Create($dataJson, $conn, $mn, $logModel);
            }
            
            //SxLogger::log($mn, " user_id =" . $user_id);
            $response = SxUser::LoadById($user_id);
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
        }

        //SxLogger::log($mn, " response = " . json_encode($response));
        SxLogger::logEnd($mn);
        return $response;
    }
    
    function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_sx.sx_user
            (user_name, e_mail, password,
            user_role, is_receive_emails, ip_address, company_id)
            VALUES(?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["sssiisi",
             ((!isset($dataJson->userName)) ? null : $dataJson->userName),
            ((!isset($dataJson->userEmail)) ? null : $dataJson->userEmail),
            ((!isset($dataJson->password)) ? null : $dataJson->password),
            ((!isset($dataJson->userRole)) ? 3 : $dataJson->userRole),
            ((!isset($dataJson->isReceiveEmails)) ? 0 : $dataJson->isReceiveEmails),
            ((!isset($dataJson->ipAddress)) ? null : json_encode($dataJson->ipAddress)),
            ((!isset($dataJson->companyId)) ? null : json_encode($dataJson->companyId)),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_sx.sx_user
                    SET user_name=?, e_mail=?, 
                    password=?,
                    user_role=?, 
                    is_receive_emails=?, 
                    ip_address=?,
                    company_id=?
                    WHERE user_id = ? " ;

        $bound_params_r = ["sssiisii",
            ((!isset($dataJson->userName)) ? null : $dataJson->userName),
            ((!isset($dataJson->userEmail)) ? null : $dataJson->userEmail),
            ((!isset($dataJson->password)) ? null : $dataJson->password),
            ((!isset($dataJson->userRole)) ? 3 : $dataJson->userRole),
            ((!isset($dataJson->isReceiveEmails)) ? 0 : $dataJson->isReceiveEmails),
            ((!isset($dataJson->ipAddress)) ? null : json_encode($dataJson->ipAddress)),
            ((!isset($dataJson->companyId)) ? null : json_encode($dataJson->companyId)),
            ($dataJson->userId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->userId;
    }
    
    function SelectJson($uaer_id, $conn, $mn, $logModel){
        
        $sql = "SELECT u.user_id    as userId, 
                    u.user_name     as userName,
                    u.e_mail    as userEmail, 
                    u.password      as password, 
                    u.company_id    as companyId,
                    u.user_role     as userRole, 
                    u.is_receive_emails as isReceiveEmails, 
                    u.ip_address    as ipAddress, 
                    u.adate, u.udate
                    FROM iordanov_sx.sx_user u
                    WHERE u.user_id = ? " ;

        $bound_params_r = ["i",$uaer_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    function LoginJson($email, $pwd, $conn, $mn, $logModel){
        
        $sql = "SELECT u.user_id    as userId, 
                    u.user_name     as userName,
                    u.e_mail    as userEmail, 
                    u.password      as password, 
                    u.user_role     as userRole, 
                    u.company_id    as companyId,
                    u.is_receive_emails as isReceiveEmails, 
                    u.ip_address    as ipAddress, 
                    u.adate, u.udate
                    FROM iordanov_sx.sx_user u
                    WHERE u.e_mail = ? and u.password =?" ;

        $bound_params_r = ["ss",$email, $pwd];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    function CheckEmailJson($email, $conn, $mn, $logModel){
        
        $sql = "SELECT count(*) AS rows, '".$email."' as userEmail
                    FROM iordanov_sx.sx_user u
                    WHERE u.e_mail = ? " ;

        $bound_params_r = ["s",$email];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    public function UsersTable($params) {
        $mn = "SxUser::UsersTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT u.user_id as userId, u.user_name as userName,
                u.e_mail as userEmail, u.password as password, u.user_role as userRole, 
                u.company_id as companyId, c.company_name as companyName, c.branch_code as branchCode,
                u.is_receive_emails as isReceiveEmails, u.ip_address as ipAddress,
                u.adate, u.udate
                FROM iordanov_sx.sx_user u
                left join iordanov_sm.sm_company c on c.company_id = u.company_id  ";
            
            
            
            if(isset($params->userRole) && strlen($params->userRole)>0){
                $sqlWhere = " WHERE u.user_role = ".$params->userRole." ";
            }
            else if(isset($params->companyId) && strlen($params->companyId)>0){
                $sqlWhere = " WHERE u.company_id = ".$params->companyId." ";
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (u.e_mail like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or u.user_name like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE (u.e_mail like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or u.user_name like '%".$params->qry_filter."%' )";
                }
               
            }
            
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by u.".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by u.e_mail, u.user_name, u.user_id ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            //SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("usersList", $ret_json_data);
            
            $sql = "SELECT count(*) as total_rows
                    FROM iordanov_sx.sx_user u ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("totals", $ret_json_data);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
}


class SxUserSettings {
    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $userId;
    public $displayDistanceMl;
    public $displayWeightLb;

    public function toJSON() {
        return json_encode($this);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public function LoadById($user_id) {
        $mn = "SxUserSettings::LoadById()";
        SxLogger::logBegin($mn);
        //SxLogger::log($mn, " userId: " . $user_id); 
        $response = new SxUserSettings();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objJ = SxUserSettings::SelectJson($user_id, $conn, $mn, $logModel);
            SxLogger::log($mn, " count objJ: " . count($objJ)); 
            if(!isset($objJ) || count($objJ) == 0){
                $response->userId = $user_id;
                $response->displayDistanceMl = 0;
                $response->displayWeightLb = 0;
                SxUserSettings::Create($response, $conn, $mn, $logModel);
                $objJ = SxUserSettings::SelectJson($user_id, $conn, $mn, $logModel);
            }
            $valJson = json_encode($objJ[0]);
            $obj = json_decode($valJson);
            $response->displayDistanceMl = $obj->displayDistanceMl;
            $response->displayWeightLb = $obj->displayWeightLb;
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public function Save($dataJson) {
        $mn = "SxUserSettings::Save()";
        SxLogger::logBegin($mn);
        $settings = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $user_id = null;
            if(isset($dataJson->userId)){
               SxLogger::log($mn, "Update  userId =" . $dataJson->userId);
               $user_id = $dataJson->userId;
               $user_id = SxUserSettings::Update($dataJson, $conn, $mn, $logModel);

            } else{
                SxLogger::log($mn, "Create user");
                $user_id = SxUserSettings::Create($dataJson, $conn, $mn, $logModel);
            }
            
            SxLogger::log($mn, " user_id =" . $user_id);
            $objJ = SxUserSettings::SelectJson($user_id, $conn, $mn, $logModel);
            $valJson = json_encode($objJ[0]);
            $obj = json_decode($valJson);
            //$ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("settings", $obj);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        $this->EncodeResponce($response);
    }
    
    function SelectJson($uaer_id, $conn, $mn, $logModel){
        
        $sql = "SELECT u.user_id    as userId, 
                    u.display_distance_ml     as displayDistanceMl,
                    u.display_weight_lb    as displayWeightLb
                    FROM iordanov_sx.sx_user_settings u
                    WHERE u.user_id = ? " ;

        $bound_params_r = ["i",$uaer_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_sx.sx_user_settings
            (user_id, display_distance_ml, display_weight_lb)
            VALUES(?, ?, ?)" ;

        $bound_params_r = ["iii",
            ( $dataJson->userId),
            ((!isset($dataJson->displayDistanceMl)) ? 0 : $dataJson->displayDistanceMl),
            ((!isset($dataJson->displayWeightLb)) ? 0 : $dataJson->displayWeightLb)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_sx.sx_user_settings
                    SET display_distance_ml=?, 
                    display_weight_lb=?, 
                    WHERE user_id = ? " ;

        $bound_params_r = ["iii",
            ((!isset($dataJson->displayDistanceMl)) ? 0 : $dataJson->displayDistanceMl),
            ((!isset($dataJson->display_weight_lb)) ? 0 : $dataJson->display_weight_lb),
            ($dataJson->userId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->userId;
    }
    
    // </editor-fold>
    
}
