<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : models    
 *  Date Creation       : Feb 18, 2021 
 *  Filename            : BomItemModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of Country
 *
 * @author IZIordanov
 */
class BomItemModel {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $bomItemId;
    public $bomId;
    public $itemId;
    public $sequenceNr;
    public $qauntity;
    public $quantityUom;
    public $upc;
    public $gtin;
    public $ean;
    public $mfrName;
    public $mfrCatalogCode;
    public $itemName;
    public $description;
    public $distributorCode;
    public $notes;
    public $adate;
    public $udate;
   
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    
    public function LoadById($bomItemId) {
        $mn = "BomItemModel::LoadById(".$bomItemId.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = BomItemModel::SelectJson($bomItemId, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = $objArrJ[0];
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public function Save($dataJson) {
        $mn = "BomItemModel::Save()";
        SxLogger::logBegin($mn);
        $response = null;
        // SxLogger::log($mn, " bomId = " . $dataJson->bomId);
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = null;
            if(isset($dataJson->bomItemId)){
               SxLogger::log($mn, "Update  bomItemId =" . $dataJson->bomItemId);
               $id = $dataJson->bomItemId;
               $id = BomItemModel::Update($dataJson, $conn, $mn, $logModel);

            } else{
                SxLogger::log($mn, "Create bom Item");
                $id = BomItemModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            SxLogger::log($mn, " bomItemId =" . $id);
            if(isset($id)){
                $objArrJ = BomItemModel::SelectJson($id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = $objArrJ[0];
                }
            }
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
       return $response;
    }
    
    public function Import($dataJson) {
        $mn = "BomItemModel::Import()";
        SxLogger::logBegin($mn);
        $response = null;
        $affected_row_ids = array();
        $rowsIdx = 0;
        $bomId = $dataJson->bomId;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);

            if (isset($dataJson->items)) {
                
                $items = $dataJson->items;
                SxLogger::log($mn, "Import items -> " . sizeof($items));
                if (sizeof($items) > 0) {
                    if (isset($bomId)) {
                        foreach ($items as $itemJson) {
                            $itemJson->bomId = $bomId;
                            $id = BomItemModel::Create($itemJson, $conn, $mn, $logModel);
                            $affected_row_ids[$rowsIdx]=$id;
                            $rowsIdx++;
                        }
                       
                    }
                }
            }
            
         } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        $response = $affected_row_ids;
        SxLogger::logEnd($mn);
       return $response;
    }
    
    public function GetTable($params) {
        $mn = "BomItemModel::GetTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT bom_item_id as bomItemId,
                    bom_id as bomId,
                    item_id as itemId,
                    sequence_nr as sequenceNr,
                    qauntity, quantity_uom as quantityUom,
                    upc, gtin, ean,
                    mfr_name as mfrName,
                    mfr_catalog_code as mfrCatalogCode,
                    item_name as itemName,
                    item_description as description,
                    distributor_code as distributorCode,
                    notes, adate, udate
                    FROM iordanov_sx.sx_bom_item ";
            
            
            
            if(isset($params->bomId) && strlen($params->bomId)>0){
                $sqlWhere = " WHERE bom_id = ".$params->bomId." ";
            }
            
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (upc like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR gtin like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR ean like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR mfr_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR mfr_catalog_code like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR item_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR distributor_code like '%".$params->qry_filter."%' )";
                }
            else{
                    $sqlWhere .= " WHERE (upc like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR gtin like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR ean like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR mfr_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR mfr_catalog_code like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR item_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR distributor_code like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = " ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by sequenceNr, upc ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("items", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_sx.sx_bom_item ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            // $sql .= " group by b.bom_id ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    function SelectJson($id, $conn, $mn, $logModel){
        
        $sql = " SELECT bom_item_id as bomItemId,
                    bom_id as bomId,
                    item_id as itemId,
                    sequence_nr as sequenceNr,
                    qauntity, quantity_uom as quantityUom,
                    upc, gtin, ean,
                    mfr_name as mfrName,
                    mfr_catalog_code as mfrCatalogCode,
                    item_name as itemName,
                    item_description as description,
                    distributor_code as distributorCode,
                    notes, adate, udate
                    FROM iordanov_sx.sx_bom_item
                    where bom_item_id=? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_sx.sx_bom_item
            (bom_id, upc, gtin, ean, mfr_name, 
            mfr_catalog_code, item_name, item_description, distributor_code, notes,
            sequence_nr, qauntity, quantity_uom )
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["isssssssssiis",
            ((!isset($dataJson->bomId)) ? null : $dataJson->bomId),
            ((!isset($dataJson->upc)) ? null : $dataJson->upc),
            ((!isset($dataJson->gtin)) ? null : $dataJson->gtin),
            ((!isset($dataJson->ean)) ? null : $dataJson->ean),
            ((!isset($dataJson->mfrName)) ? null : $dataJson->mfrName),
            ((!isset($dataJson->mfrCatalogCode)) ? null : $dataJson->mfrCatalogCode),
            ((!isset($dataJson->itemName)) ? null : $dataJson->itemName),
            ((!isset($dataJson->description)) ? null : $dataJson->description),
            ((!isset($dataJson->distributorCode)) ? null : $dataJson->distributorCode),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->sequenceNr)) ? 0 : $dataJson->sequenceNr),
            ((!isset($dataJson->qauntity)) ? 1 : $dataJson->qauntity),
            ((!isset($dataJson->quantityUom)) ? null : $dataJson->quantityUom)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_sx.sx_bom_item
                    SET bom_id=?, 
                    upc=?, gtin=?, ean=?, mfr_name=?,
                    mfr_catalog_code=?, item_name=?, item_description=?, 
                    distributor_code=?, notes=?,
                    sequence_nr=?, qauntity=?, quantity_uom=? 
                    WHERE bom_item_id = ? " ;

        $bound_params_r = ["isssssssssiisi",
            ((!isset($dataJson->bomId)) ? null : $dataJson->bomId),
            ((!isset($dataJson->upc)) ? null : $dataJson->upc),
            ((!isset($dataJson->gtin)) ? null : $dataJson->gtin),
            ((!isset($dataJson->ean)) ? null : $dataJson->ean),
            ((!isset($dataJson->mfrName)) ? null : $dataJson->mfrName),
            ((!isset($dataJson->mfrCatalogCode)) ? null : $dataJson->mfrCatalogCode),
            ((!isset($dataJson->itemName)) ? null : $dataJson->itemName),
            ((!isset($dataJson->description)) ? null : $dataJson->description),
            ((!isset($dataJson->distributorCode)) ? null : $dataJson->distributorCode),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->sequenceNr)) ? 0 : $dataJson->sequenceNr),
            ((!isset($dataJson->qauntity)) ? 1 : $dataJson->qauntity),
            ((!isset($dataJson->quantityUom)) ? null : $dataJson->quantityUom),
            ($dataJson->bomItemId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->bomId;
    }
    
    function Delete($bom_id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM iordanov_sx.sx_bom_item
                   WHERE bom_item_id = ? " ;

        $bound_params_r = ["i", $bom_id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "deleted bom_id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}
