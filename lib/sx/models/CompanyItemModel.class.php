<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : models    
 *  Date Creation       : Feb 18, 2021 
 *  Filename            : CompanyItemModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of Country
 *
 * @author IZIordanov
 */
class CompanyItemModel {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $citemId;
    public $companyId;
    public $itemId;
    public $upc;
    public $gtin;
    public $ean;
    public $mfrName;
    public $mfrCatalogCode;
    public $mfrId;
    public $itemName;
    public $description;
    public $vendorCode;
    public $imageUrl;
    public $productName;
    public $productId;
    public $mfrNameMapId;
    public $wipId;
    public $itemStatusId;
    public $itemProductId;
    public $itemStatusUdate;
    public $adate;
    public $udate;
    public $cName;
    public $cBranch;
   
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public function LoadById($citemId) {
        $mn = "CompanyItemModel::LoadById(".$citemId.")";
        SxLogger::logBegin($mn);
        $response = null;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $objArrJ = CompanyItemModel::SelectJson($citemId, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = $objArrJ[0];
            }
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
        return $response;
    }
    
    public function Save($dataJson) {
        $mn = "CompanyItemModel::Save()";
        SxLogger::logBegin($mn);
        $response = null;
        // SxLogger::log($mn, " bomId = " . $dataJson->bomId);
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            $id = null;
            if(isset($dataJson->citemId)){
               SxLogger::log($mn, "Update  citemId =" . $dataJson->citemId);
               $id = $dataJson->citemId;
               $id = CompanyItemModel::Update($dataJson, $conn, $mn, $logModel);

            } else{
                SxLogger::log($mn, "Create Item");
                $id = CompanyItemModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            SxLogger::log($mn, " citemId =" . $id);
            if(isset($id)){
                $objArrJ = CompanyItemModel::SelectJson($id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = $objArrJ[0];
                }
            }
            
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        SxLogger::logEnd($mn);
       return $response;
    }
    
    public function Import($dataJson) {
        $mn = "CompanyItemModel::Import()";
        SxLogger::logBegin($mn);
        $response = null;
        $affected_row_ids = array();
        $rowsIdx = 0;
        $bomId = $dataJson->bomId;
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);

            if (isset($dataJson->items)) {
                
                $items = $dataJson->items;
                SxLogger::log($mn, "Import items -> " . sizeof($items));
                if (sizeof($items) > 0) {
                    if (isset($bomId)) {
                        foreach ($items as $itemJson) {
                            //$itemJson->bomId = $bomId;
                            $id = CompanyItemModel::Create($itemJson, $conn, $mn, $logModel);
                            $affected_row_ids[$rowsIdx]=$id;
                            $rowsIdx++;
                        }
                       
                    }
                }
            }
            
         } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = null;
        }
        $response = $affected_row_ids;
        SxLogger::logEnd($mn);
       return $response;
    }
    
    public static function GetTable($params) {
        $mn = "CompanyItemModel::GetTable()";
        SxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = SxConnection::dbConnect();
            $logModel = SxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT i.citem_id as citemId,
                    i.company_id as companyId,
                    i.item_id as itemId,
                    i.upc, i.ean, i.gtin,
                    i.mfr_name as mfrName, 
                    m.mfr_company_id as mfrId,
                    i.mfr_catalog_code as mfrCatalogCode,
                    i.item_name as itemName,
                    i.item_description as description,
                    i.vendor_code as vendorCode,
                    i.image_url as imageUrl,

                    i.product_name as productName,
                    i.item_product_id as productId,
                    i.mfr_name_map_id as mfrNameMapId,
                    i.wip_id as wipId,
                    i.item_status_id as itemStatusId,
                    i.item_status_udate as itemStatusUdate,
                    i.adate,
                    i.udate,
                    c.company_name as cName,
                    c.branch_code as cBranch
                FROM iordanov_sx.sx_company_item i
                left join iordanov_sm.sm_company c on c.company_id = i.company_id
                LEFT JOIN iordanov_sx.sx_mfr_name_map m on m.mfr_name_map_id = i.mfr_name_map_id ";
            
            
            $sqlWhere="";
            if(isset($params->companyId) && strlen($params->companyId)>0){
                $sqlWhere = " WHERE i.company_id = ".$params->companyId." ";
            }
            
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (i.upc like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR i.gtin like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR i.ean like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR i.mfrName like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR i.mfr_catalog_code like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR i.item_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR i.item_description like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR vendor_code like '%".$params->qry_filter."%' )";
                }
            else{
                    $sqlWhere .= " WHERE (i.upc like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR i.gtin like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR i.ean like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR i.mfrName like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR i.mfr_catalog_code like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR i.item_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR i.item_description like '%".$params->qry_filter."%' ";
                    $sqlWhere .= "  OR vendor_code like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = " ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by i.upc, itemName ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            SxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("items", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_sx.sx_company_item i ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            // $sql .= " group by b.bom_id ";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            SxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        SxLogger::log($mn, " response = " . $response->toJSON());
        SxLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    function SelectJson($id, $conn, $mn, $logModel){
        
        $sql = " SELECT i.citem_id as citemId,
                i.company_id as companyId,
                i.item_id as itemId,
                i.upc, i.ean, i.gtin,
                i.mfr_name as mfrName, 
                m.mfr_company_id as mfrId,
                i.mfr_catalog_code as mfrCatalogCode,
                i.item_name as itemName,
                i.item_description as description,
                i.vendor_code as vendorCode,
                i.image_url as imageUrl,

                i.product_name as productName,
                i.item_product_id as productId,
                i.mfr_name_map_id as mfrNameMapId,
                i.wip_id as wipId,
                i.item_status_id as itemStatusId,
                i.item_status_udate as itemStatusUdate,
                i.adate,
                i.udate,
                c.company_name as cName,
                c.branch_code as cBranch
            FROM iordanov_sx.sx_company_item i
            left join iordanov_sm.sm_company c on c.company_id = i.company_id
            LEFT JOIN iordanov_sx.sx_mfr_name_map m on m.mfr_name_map_id = i.mfr_name_map_id
            where i.citem_id = ? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO iordanov_sx.sx_company_item
        ( company_id, item_id, upc, ean, gtin,
        mfr_name, mfr_catalog_code, item_name, item_description, vendor_code,
        image_url, product_name, item_product_id, wip_id, item_status_id,
        item_status_udate)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["iissssssssssiiis",
            ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->itemId)) ? null : $dataJson->itemId),
            ((!isset($dataJson->upc)) ? null : $dataJson->upc),
            ((!isset($dataJson->ean)) ? null : $dataJson->ean),
            ((!isset($dataJson->gtin)) ? null : $dataJson->gtin),
            ((!isset($dataJson->mfrName)) ? null : $dataJson->mfrName),
            ((!isset($dataJson->mfrCatalogCode)) ? null : $dataJson->mfrCatalogCode),
            ((!isset($dataJson->itemName)) ? null : $dataJson->itemName),
            ((!isset($dataJson->description)) ? null : $dataJson->description),
            ((!isset($dataJson->vendorCode)) ? null : $dataJson->vendorCode),
            
            ((!isset($dataJson->imageUrl)) ? null : $dataJson->imageUrl),
            ((!isset($dataJson->productName)) ? null : $dataJson->productName),
            ((!isset($dataJson->productId)) ? null : $dataJson->productId),
            ((!isset($dataJson->wipId)) ? null : $dataJson->wipId),
            ((!isset($dataJson->itemStatusId)) ? null : $dataJson->itemStatusId),
            ((!isset($dataJson->itemStatusUdate)) ? null : CompanyItemModel::getDate($dataJson->itemStatusUdate)->format("Y-m-d H:i:s"))
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        SxLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
     public static function geDate($adate) {
        $retValue = null;
        if (isset($adate) && $adate != null) {
            if (!is_object($adate))
                $retValue = new DateTime($adate);
            else
                $retValue = $adate;
        }
        else
            $retValue = new DateTime();
        return $retValue;
    }
   
    function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE iordanov_sx.sx_company_item
                    SET  company_id = ?,
                        item_id = ?,
                        upc = ?,
                        ean = ?,
                        gtin = ?,
                        mfr_name = ?,
                        mfr_catalog_code = ?,
                        item_name = ?,
                        item_description = ?,
                        vendor_code = ?,
                        
                        image_url = ?,
                        mfr_name_map_id = ?,
                        product_name = ?,
                        item_product_id = ?,
                        wip_id = ?,
                        item_status_id = ?,
                        item_status_udate = ?,
                    WHERE citem_id = ? " ;

        $bound_params_r = ["iisssssssssisiiisi",
             ((!isset($dataJson->companyId)) ? null : $dataJson->companyId),
            ((!isset($dataJson->itemId)) ? null : $dataJson->itemId),
            ((!isset($dataJson->upc)) ? null : $dataJson->upc),
            ((!isset($dataJson->ean)) ? null : $dataJson->ean),
            ((!isset($dataJson->gtin)) ? null : $dataJson->gtin),
            ((!isset($dataJson->mfrName)) ? null : $dataJson->mfrName),
            ((!isset($dataJson->mfrCatalogCode)) ? null : $dataJson->mfrCatalogCode),
            ((!isset($dataJson->itemName)) ? null : $dataJson->itemName),
            ((!isset($dataJson->description)) ? null : $dataJson->description),
            ((!isset($dataJson->vendorCode)) ? null : $dataJson->vendorCode),
            
            ((!isset($dataJson->imageUrl)) ? null : $dataJson->imageUrl),
            ((!isset($dataJson->mfrNameMapId)) ? null : $dataJson->mfrNameMapId),
            ((!isset($dataJson->productName)) ? null : $dataJson->productName),
            ((!isset($dataJson->productId)) ? null : $dataJson->productId),
            ((!isset($dataJson->wipId)) ? null : $dataJson->wipId),
            ((!isset($dataJson->itemStatusId)) ? null : $dataJson->itemStatusId),
            ((!isset($dataJson->itemStatusUdate)) ? null : CompanyItemModel::getDate($dataJson->itemStatusUdate)->format("Y-m-d H:i:s"))
            ($dataJson->citemId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->bomId;
    }
    
    function Delete($bom_id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM iordanov_sx.sx_company_item
                   WHERE citem_id = ? " ;

        $bound_params_r = ["i", $bom_id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        SxLogger::log($mn, "deleted citem_id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}
