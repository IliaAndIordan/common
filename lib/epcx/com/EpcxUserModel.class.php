<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Mart 19, 2024 
 *  Filename            : EpcxUserModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2024-2030 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of EpcxUser Model
 *
 * @author IZIordanov
 */
class EpcxUser {

    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $userId;
    public $name;
    public $email;
    public $password;
    public $role = 1; //2 predefined roles 1-user, 5 admin
    public $emailValid;
    public $adate;
    public $udate;

    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">

    public static function Login($email, $pwd) {
        $mn = "EpcxUser::Login()";
        EpcxLogger::logBegin($mn);
        //EpcxLogger::log($mn, " eMail: " . $email.", password".$pwd); 
        $response = null;
        try {
            $conn = EpcxConnection::dbConnect();
            $logModel = EpcxLogger::currLogger()->getModule($mn);

            $objArrJ = EpcxUser::LoginJson($email, $pwd, $conn, $mn, $logModel);
            //EpcxLogger::log($mn, " objArrJ = " . json_encode($objArrJ));
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            EpcxLogger::logError($mn, $ex);
            $response = null;
        }
        EpcxLogger::logEnd($mn);
        return $response;
    }

    public static function LoadById($user_id) {
        $mn = "EpcxUser::LoadById(" . $user_id . ")";
        EpcxLogger::logBegin($mn);
        $response = new EpcxUser();
        try {
            $conn = EpcxConnection::dbConnect();
            $logModel = EpcxLogger::currLogger()->getModule($mn);
            $objArrJ = EpcxUser::SelectJson($user_id, $conn, $mn, $logModel);

            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            EpcxLogger::logError($mn, $ex);
            $response = null;
        }
        EpcxLogger::logEnd($mn);
        return $response;
    }

    public static function Save($data) {
        $mn = "EpcxUser::Save()";
        EpcxLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        EpcxLogger::log($mn, " dataJson: " . json_encode($dataJson));
        $response = null;
        try {
            $conn = EpcxConnection::dbConnect();
            $logModel = EpcxLogger::currLogger()->getModule($mn);
            $user_id = null;
            if (isset($dataJson->userId)) {
                EpcxLogger::log($mn, "Update  id =" . $dataJson->userId);
                $user_id = $dataJson->userId;
                EpcxUser::Update($dataJson, $conn, $mn, $logModel);
            } else {
                EpcxLogger::log($mn, "Create user");
                $user_id = EpcxUser::Create($dataJson, $conn, $mn, $logModel);
            }

            EpcxLogger::log($mn, " user_id =" . $user_id);
            $response = EpcxUser::LoadById($user_id);
        } catch (Exception $ex) {
            EpcxLogger::logError($mn, $ex);
        }

        EpcxLogger::log($mn, " response = " . json_encode($response));
        EpcxLogger::logEnd($mn);
        return $response;
    }

    public static function ChangePassword($data) {
        $mn = "EpcxUser::ChangePassword()";
        EpcxLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        EpcxLogger::log($mn, " dataJson: " . json_encode($dataJson));
        $response = null;
        try {
            $conn = EpcxConnection::dbConnect();
            $logModel = EpcxLogger::currLogger()->getModule($mn);
            $user_id = null;
            if (isset($dataJson->userId) && isset($dataJson->password)) {
                EpcxLogger::log($mn, "Update  id =" . $dataJson->userId);
                $user_id = $dataJson->userId;
                EpcxUser::UpdatePassword($dataJson, $conn, $mn, $logModel);
            }
            EpcxLogger::log($mn, " user_id =" . $user_id);
            $response = EpcxUser::LoadById($user_id);
        } catch (Exception $ex) {
            EpcxLogger::logError($mn, $ex);
        }

        EpcxLogger::log($mn, " response = " . json_encode($response));
        EpcxLogger::logEnd($mn);
        return $response;
    }

    static function Create($dataJson, $conn, $mn, $logModel) {

        $strSQL = "INSERT INTO iordanov_epcx_com.epcx_user
            (uname, email, upassword,
            user_role_id, email_valid)
            VALUES(?, ?, ?, ?, ?)";

        $bound_params_r = ["sssii",
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->email)) ? null : $dataJson->email),
            ((!isset($dataJson->password)) ? null : $dataJson->password),
            ((!isset($dataJson->role)) ? 1 : $dataJson->role),
            ((!isset($dataJson->emailValid)) ? 0 : $dataJson->emailValid),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        EpcxLogger::log("$mn", "id=" . $id);

        return $id;
    }

    static function Update($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE iordanov_epcx_com.epcx_user
                    SET uname=?, email=?, 
                    upassword=?, 
                    user_role_id=?, 
                    email_valid=?
                    WHERE user_id = ? ";

        $bound_params_r = ["sssiii",
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->email)) ? null : $dataJson->email),
            ((!isset($dataJson->password)) ? null : $dataJson->password),
            ((!isset($dataJson->role)) ? 1 : $dataJson->role),
            ((!isset($dataJson->emailValid)) ? 0 : $dataJson->emailValid),
            ($dataJson->userId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        EpcxLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->id;
    }

    static function UpdatePassword($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE iordanov_epcx_com.epcx_user
                    SET upassword=?
                    WHERE user_id = ? ";

        $bound_params_r = ["si",
            ((!isset($dataJson->password)) ? null : $dataJson->password),
            ($dataJson->userId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        EpcxLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->id;
    }

    static function SelectJson($userId, $conn, $mn, $logModel) {

        $sql = "SELECT u.user_id    as userId, 
                    u.uname         as name,
                    u.email         as email, 
                    u.user_role_id  as role, 
                    u.email_valid   as emailValid,  
                    u.adate, u.udate
                    FROM iordanov_epcx_com.epcx_user u
                    WHERE u.user_id = ? ";

        $bound_params_r = ["i", $userId];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function LoginJson($email, $pwd, $conn, $mn, $logModel) {

        $sql = "SELECT u.user_id    as userId, 
                    u.uname         as name,
                    u.email         as email, 
                    u.user_role_id  as role, 
                    u.email_valid   as emailValid,  
                    u.adate, u.udate
                    FROM iordanov_epcx_com.epcx_user u
                    WHERE u.email = ? and u.upassword =?";

        $bound_params_r = ["ss", $email, $pwd];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function CheckEmailJson($email, $conn, $mn, $logModel) {

        $sql = "SELECT count(*) as rowCount, '" . $email . "' as email
                    FROM iordanov_epcx_com.epcx_user u
                    WHERE u.email = ? 
                    GROUP By u.email ";

        $bound_params_r = ["s", $email];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    public static function UsersTable($params) {
        $mn = "EpcxUser::UsersTable()";
        EpcxLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = EpcxConnection::dbConnect();
            $logModel = EpcxLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT u.user_id    as userId, 
                    u.uname         as name,
                    u.email         as email, 
                    u.user_role_id  as role, 
                    u.email_valid   as emailValid,  
                    u.adate, u.udate
                    FROM iordanov_epcx_com.epcx_user u
                     ";

            $sqlWhere = "";
            if (isset($params->role) && strlen($params->role) > 0) {
                $sqlWhere = " WHERE u.user_role_id = " . $params->role . " ";
            }

            if (isset($params->qry_filter) && strlen($params->qry_filter) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND (u.email like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or u.uname like '%" . $params->qry_filter . "%' )";
                } else {
                    $sqlWhere = " WHERE (u.email like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or u.uname like '%" . $params->qry_filter . "%' )";
                }
            }
            $sqlOrder = "";
            if (isset($params->qry_orderCol)) {
                $sqlOrder .= " order by u." . $params->qry_orderCol . " " . ($params->qry_isDesc ? "desc" : " asc");
            } else {
                $sqlOrder .= "order by u.email, u.uname, u.user_id ";
            }
            $sql .= $sqlWhere . $sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            EpcxLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("users", $ret_json_data);

            $sql = "SELECT count(*) as total_rows
                    FROM iordanov_epcx_com.epcx_user u " . (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $obj->totalRows);
        } catch (Exception $ex) {
            EpcxLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        EpcxLogger::log($mn, " response = " . $response->toJSON());
        EpcxLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
}
