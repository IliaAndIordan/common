<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : ams.common   
 *  Date Creation       : Sep 14, 2018 
 *  Filename            : JwtAuth.php
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */
require_once("config.inc.php");
require_once("JWT.php");
require_once("UserModel.class.php");

/**
 * Description of JwtAuth
 *
 * @author IZIordanov
 */
class JwtAuth {

    public static function signTocken($user_id, $user) {
        $retValue = false;
        if (!isset($user_id))
            return false;
        $mn = "JwtAuth::signTocken()";
        //AmsAlLogger::logBegin($mn);
        //AmsAlLogger::log($mn, " user_id: " . $user_id); 
        $jsonTokenId = $user_id; //SECRET_SERVER_KEY; //base64_encode(mcrypt_create_iv(32));
        $issuedAt = time();
        $notBefore = $issuedAt + 10; //Adding 10 seconds
        $expire = $notBefore + (1 * 60 * 60);// 3600; // Adding 1 hour
        $issuer = SEVER_NAME;   // Retrieve the server name from config file
        $key = SECRET_SERVER_KEY;
        //$userM = new UserModel($user);
        /*
         * Create the token  payload as an array
         */
        $payload = [
            'iat' => $issuedAt, // Issued at: time when the token was generated
            'jti' => $jsonTokenId, // Json Token Id: an unique identifier for the token
            'iss' => $issuer, // Issuer
            'nbf' => $notBefore, // Not before
            'exp' => $expire, // Expire
            'data' => [// Data related to the signer user
                'user_id' => $user_id, // userid from the users table
                'user' => $user, // AmsUser name
            ]
        ];
        //AmsAlLogger::log($mn, "iat:" . $payload->iat . ", exp:" . $payload->iat . "nbf:" . $payload->nbf);
        $token = JWT::encode($payload, $key);
        //AmsAlLogger::log($mn, "token = " . $token);
        try {
            $payload_dec = Jwt::decode($token, $key);
            if (isset($payload_dec)) {
                //AmsAlLogger::log($mn, "payload_dec.data.user_id = " . $payload_dec->data->user_id);
                header('X-Authorization:' . $token);
                //header('Authorization: ' . $token);
                $retValue = true;
            }
        } catch (BeforeValidException $e) {
            AmsAlLogger::logBegin($mn);
            AmsAlLogger::logError($mn, $ex);
            AmsAlLogger::logEnd($mn);
        }

        //AmsAlLogger::logEnd($mn);
        return $retValue;
    }
    
    public static function geRrefreshTocken($token) {
        
        if (!isset($token) || !isset($token->data->user_id )){
            return 'Incorect payload for refresh tocken';
        }
            
        $mn = "JwtAuth::geRrefreshTocken()";
        AmsAlLogger::logBegin($mn);
        $user = new AmsUser();
        $user = AmsUser::LoadById($token->data->user_id);
        $guid = uniqid('ams', false);
        $adate_ms = time();
        $expire = $adate_ms + (1 * 60 * 60); // Adding 1 hour
        $sql = "INSERT INTO ams_al.ams_user_refreshtoken
            (user_id, adate_ms, expire_date_ms, refresh_token)
            VALUES(?, ?, ?, ?)";
        $bound_params_r = array("iiis",$user->userId,$adate_ms,$expire, $guid);
        $conn = AmsAlConnection::dbConnect();
        $logModel = AmsAlLogger::currLogger()->getModule($mn);
        
        $client_id = $conn->preparedInsert($sql, $bound_params_r, $logModel);
        // Retrieve the server name from config file
        $issuer = SEVER_NAME;   
        $key = SECRET_SERVER_KEY;
        //AmsAlLogger::log($mn, "client_id:" . $client_id . ", exp:" . $expire . "guid:" . $guid);
        /*
         * Create the token  payload as an array
         */
        $payload = [
            'client_id' => $client_id,
            'exp' => $expire, // Expire
            'iss' => $issuer, // Issuer
            'guid' => $guid, // Not before
        ];
        //AmsAlLogger::log($mn, "client_id:" . $payload->client_id . ", exp:" . $payload->exp . "guid:" . $payload->guid);
        $refresh_token = JWT::encode($payload, $key);
        //AmsAlLogger::log($mn, "token = " . $refresh_token);
       
        AmsAlLogger::logEnd($mn);
        return $refresh_token;
    }

    /**
     * Check Bearer Token and if valid return payload
     * 
     * @return payload decoded tocken payload if valud or null if not
     */
    public static function Autenticate($checkExpiration = true) {
        $mn = "JwtAuth::Autenticate()";
        //AmsAlLogger::logBegin($mn);

        $result = new AutenticateResult();
        $result->isValud = false;
        $result->message = "Unexpected error during token validation.";

        $payload = null;

        $token = JwtAuth::getBearerToken();
        $key = SECRET_SERVER_KEY;

        if (isset($token)) {
            //AmsAlLogger::log($mn, "token = " . $token);

            try {
                $payload = Jwt::decode($token, $key);
                if (isset($payload)) {
                    $result->payload = $payload;
                    
                    // jti: Json Token Id: an unique identifier for the token
                    //AmsAlLogger::log($mn, "payload.data.user_id = " . $payload->data->user_id . ", jti: " . $payload->jti);
                    if ($payload->data->user_id != $payload->jti) {
                        $result->isValud = false;
                        $result->message = 'Token Invalid! Error: SxUser does not corespond to token. Please, authenticate again.';
                    } else if ($checkExpiration && JwtAuth::isTokenExpired($payload)) {
                        $result->isValud = false;
                        $result->message = JwtAuth::geRrefreshTocken($payload);
                    } else {
                        $result->isValud = true;
                        $result->message = "Token is valid.";
                    }
                }
            } catch (BeforeValidException $e) {
                AmsAlLogger::logBegin($mn);
                AmsAlLogger::logError($mn, $ex);
                AmsAlLogger::logEnd($mn);
                $result->isValud = true;
                $result->message = $ex;
            }
        }
        //AmsAlLogger::logEnd($mn);
        return $result;
    }

    
    /**
     * Check Refresh Tocken Token and if valid return user
     * 
     * @return user
     */
    public static function RefreshTocken($refresh) {
        $mn = "JwtAuth::RefreshTocken()";
        $user = null;
        AmsAlLogger::logBegin($mn);
        if (isset($refresh)) {
            //AmsAlLogger::log($mn, "refresh = " . $refresh);
            $key = SECRET_SERVER_KEY;
            try {
                $payload = Jwt::decode($refresh, $key);
                if (isset($payload)) {
                    //AmsAlLogger::log($mn, "payload.client_id = " . $payload->client_id . ", guid: " . $payload->guid);
                    if (isset($payload->client_id)) {
                        //
                        $sql = "SELECT user_id, adate_ms  
                            FROM ams_al.ams_user_refreshtoken
                            where id = ? and refresh_token = ? and expire_date_ms = ? ";
                        //where id = ? and refresh_token = ? and expire_date_ms > (UNIX_TIMESTAMP(now())) ";
                        
                        $bound_params_r = array("isi", $payload->client_id, $payload->guid, $payload->exp);
                        //$bound_params_r = array("is", $payload->client_id, $payload->guid);
                        //$bound_params_r = array("i", $payload->client_id);
                        $conn = AmsAlConnection::dbConnect();
                        $logModel = AmsAlLogger::currLogger()->getModule($mn);
                        $data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                        //AmsAlLogger::log($mn, "count = " . count($data));
                        if (isset($data) && count($data)>0) {
                            $user_id = $data[0]["user_id"];
                            //AmsAlLogger::log($mn, "user_id = " . $user_id);
                            $user = AmsUser::LoadById($user_id);
                        }
                    }
                     
                }
            } catch (BeforeValidException $e) {
                AmsAlLogger::logError($mn, $ex);
            }
        
        }
        AmsAlLogger::logEnd($mn);
        return $user;
    }

    /**
     * Get hearder Authorization
     * */
    public static function getAuthorizationHeader() {
        $headers = null;
        $mn = "JwtAuth::getAuthorizationHeader()";
        //AmsAlLogger::logBegin($mn);
        if (isset($_SERVER['authorization'])) {
            $headers = trim($_SERVER["authorization"]);
        } else if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } else if (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        } else if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        

        //AmsAlLogger::log($mn, "Header = " . (!empty($headers) ? "Empty" : " elements: " . count($headers)));
        //AmsAlLogger::log($mn, "Header = " . $headers);
        //AmsAlLogger::logEnd($mn);
        return $headers;
    }

    /**
     * get access token from header
     * */
    public static function getBearerToken() {
        $headers = JwtAuth::getAuthorizationHeader();
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }

    private static function isTokenExpired($payload) {
        /*
          'iat' => $issuedAt, // Issued at: time when the token was generated
          'jti' => $jsonTokenId, // Json Token Id: an unique identifier for the token
          'iss' => $issuer, // Issuer
          'nbf' => $notBefore, // Not before
          'exp' => $expire, // Expire
         */
        $retValue = false;
        $mn = "JwtAuth::isTokenExpired()";
        //AmsAlLogger::logBegin($mn);
        //AmsAlLogger::log($mn, "iat:" . $payload->iat . ", exp:" . $payload->exp . ", nbf:" . $payload->nbf);
        //strtotime unix timestamp
        $now = time();
        $interval = $payload->exp - $now;
        //AmsAlLogger::log($mn, "interval = " . $interval);
        //AmsAlLogger::logEnd($mn);

        if ($interval < 0) {
            $retValue = true;
        }
        return $retValue;
    }

}
