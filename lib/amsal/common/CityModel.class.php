<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Jan 7, 2021 
 *  Filename          : CityModel.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of Country
 *
 * @author IZIordanov
 */
class AmsCity {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $cityId;
    public $ctName;
    public $population;
    public $countryId;
    public $stateId;
    public $wikiUrl;
    public $amsStatus;
    public $wadStatus;
    public $udate;
    public $stateCapital;
    public $countryCapital;
    
    public $airports;
    public $apActive;
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($city_id) {
        $mn = "AmsCity::LoadById(".$city_id.")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsCity::SelectJson($city_id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = $objArrJ[0];
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($dataJson) {
        $mn = "AmsCity::Save()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        AmsAlLogger::log($mn, " dataJson = " . json_encode($dataJson));
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $city_id = null;
            if(isset($dataJson->cityId)){
               AmsAlLogger::log($mn, "Update  cityId =" . $dataJson->cityId);
               $city_id = $dataJson->cityId;
               $city_id = AmsCity::Update($dataJson, $conn, $mn, $logModel);

            } else{
                AmsAlLogger::log($mn, "Create state");
                $city_id = AmsCity::Create($dataJson, $conn, $mn, $logModel);
            }
            
            AmsAlLogger::log($mn, " city_id =" . $city_id);
            if(isset($city_id)){
                $objArrJ = AmsCity::SelectJson($city_id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = $objArrJ[0];
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
       return $response;
    }
    
    public static function CityTable($params) {
        $mn = "AmsCity::CityTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT ct.city_id as cityId,  ct.city_name as ctName,
                    ct.population, 
                    ct.country_id as countryId, ct.state_id as stateId,
                    ct.city_wikiurl as wikiUrl,
                    ct.ams_status_id as amsStatus, ct.wad_status_id as wadStatus,
                    ct.udate, 
                    count(distinct(a.ap_id)) as airports,
                    sum(if(a.ams_status_id=4,1,0)) as apActive,
                    ct.state_capital as stateCapital,
                    ct.country_capital as countryCapital,
                    pld.pax_demand_for_day as paxDemandDay,
                    pld.pax_demand_for_period as paxDemandPeriod,
                    pld.cargo_demand_for_period_kg as cargoDemandPeriodKg,
                    pld.pax_demand_period_days as paxDemandPeriodDays,
                    pld.ap_type_sum as apTypeSum
                    FROM ams_wad.cfg_city ct
                    left join ams_wad.calc_city_payload_demand pld on pld.city_id = ct.city_id
                    left join ams_wad.cfg_airport a on a.city_id = ct.city_id ";
            
            
            
            if(isset($params->stateId) && strlen($params->stateId)>0){
                $sqlWhere = " WHERE ct.state_id = ".$params->stateId." ";
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (ct.city_name like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE (ct.city_name like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = " group by ct.city_id ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by population desc ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("cities", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_wad.cfg_city ct ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function CountCityNoPaxDemend() {
        $mn = "AmsCity::CountCityNoPaxDemend()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "select count(*) as rowsCount from
                (SELECT count(a.ap_id) as apCount, a.city_id as city_id, sum(apd.pax_demand_for_period) as pax_demand_for_period
                FROM ams_wad.cfg_airport a 
                left join ams_wad.calc_airport_payload_demand apd on apd.ap_id = a.ap_id
                group by a.city_id) d
                where d.pax_demand_for_period is null and 1=?";
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->rowsCount);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function CalcCityPayloadDemand() {
        $mn = "FlightModel::CalcCityPayloadDemand()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        $obj = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);

            $sql = "call ams_wad.add_city_payload_demand()";
            //$bound_params_r = ["i", 1];
            //$result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            $result_r = $conn->dbExecuteSQLJson($sql,  $logModel);
            if (isset($result_r) && count($result_r) > 0) {
                $rowJson = json_decode(json_encode($result_r[0]));
                $response->addData("rowsCount", $rowJson->rowsCount);
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function CalcCityPayloadDemandCityId($id) {
        $mn = "FlightModel::CalcCityPayloadDemandCityId(".$id.")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        $obj = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);

            $sql = "call ams_wad.calc_city_payload_demand_insert(?)";
            $bound_params_r = ["i", $id];
            $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            //$result_r = $conn->dbExecuteSQLJson($sql,  $logModel);
            if (isset($result_r) && count($result_r) > 0) {
                $rowJson = json_decode(json_encode($result_r[0]));
                $response->addData("rowsCount", $rowJson->rowsCount);
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
   
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function SelectJson($city_id, $conn, $mn, $logModel){
        
        $sql = "SELECT ct.city_id as cityId,  ct.city_name as ctName,
                    ct.population, 
                    ct.country_id as countryId, ct.state_id as stateId,
                    ct.city_wikiurl as wikiUrl,
                    ct.ams_status_id as amsStatus, ct.wad_status_id as wadStatus,
                    ct.udate, 
                    count(distinct(a.ap_id)) as airports,
                    sum(if(a.ams_status_id=4,1,0)) as apActive,
                    ct.state_capital as stateCapital,
                    ct.country_capital as countryCapital,
                    pld.pax_demand_for_day as paxDemandDay,
                    pld.pax_demand_for_period as paxDemandPeriod,
                    pld.cargo_demand_for_period_kg as cargoDemandPeriodKg,
                    pld.pax_demand_period_days as paxDemandPeriodDays,
                    pld.ap_type_sum as apTypeSum
                    FROM ams_wad.cfg_city ct
                    left join ams_wad.calc_city_payload_demand pld on pld.city_id = ct.city_id
                    left join ams_wad.cfg_airport a on a.city_id = ct.city_id 
                    where ct.city_id=?";

        $bound_params_r = ["i",$city_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_wad.cfg_city
            (city_name, population, country_id, state_id,
            city_wikiurl,  
            ams_status_id, wad_status_id, state_capital, country_capital)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["siiisiiii",
            ((!isset($dataJson->ctName)) ? null : $dataJson->ctName),
            ((!isset($dataJson->population)) ? 0 : $dataJson->population),
            ((!isset($dataJson->countryId)) ? null : $dataJson->countryId),
            ((!isset($dataJson->stateId)) ? null : $dataJson->stateId),
            ((!isset($dataJson->wikiUrl)) ? null : $dataJson->wikiUrl),
            ((!isset($dataJson->amsStatus)) ? 1 : $dataJson->amsStatus),
            ((!isset($dataJson->wadStatus)) ? 1 : $dataJson->wadStatus),
            ((!isset($dataJson->stateCapital)) ? 0 : $dataJson->stateCapital),
            ((!isset($dataJson->countryCapital)) ? 0 : $dataJson->countryCapital)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_wad.cfg_city
                    SET city_name=?, 
                    population=?, 
                    country_id=?, 
                    state_id=?,
                    city_wikiurl=?, 
                    ams_status_id=?, 
                    wad_status_id=?, 
                    state_capital=?, 
                    country_capital=? WHERE city_id = ? " ;

        $bound_params_r = ["siiisiiiii",
            ((!isset($dataJson->ctName)) ? null : $dataJson->ctName),
            ((!isset($dataJson->population)) ? 0 : $dataJson->population),
            ((!isset($dataJson->countryId)) ? null : $dataJson->countryId),
            ((!isset($dataJson->stateId)) ? null : $dataJson->stateId),
            ((!isset($dataJson->wikiUrl)) ? null : $dataJson->wikiUrl),
            ((!isset($dataJson->amsStatus)) ? 1 : $dataJson->amsStatus),
            ((!isset($dataJson->wadStatus)) ? 1 : $dataJson->wadStatus),
            ((!isset($dataJson->stateCapital)) ? 0 : $dataJson->stateCapital),
            ((!isset($dataJson->countryCapital)) ? 0 : $dataJson->countryCapital),
            ($dataJson->cityId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->cityId;
    }
    
    static function Delete($city_id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM ams_wad.cfg_city
                   WHERE city_id = ? " ;

        $bound_params_r = ["i", $city_id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted state_id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}
