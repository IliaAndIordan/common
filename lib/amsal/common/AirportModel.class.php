<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Jan 7, 2021 
 *  Filename            : AirportModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of Country
 *
 * @author IZIordanov
 */
class AmsAirport {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $apId;
    public $iata;
    public $icao;
    public $apName;
    public $lat;
    public $lon;
    public $elevation;
    public $typeId;
    public $regionId;
    public $subregionId;
    public $countryId;
    public $iso2;
    public $cName;
    public $stateId;
    public $cityId;
    public $ctName;
    public $active;
    public $wikiUrl;
    public $amsStatus;
    public $wadStatus;
    public $homeUrl;
    public $notes;
    public $utc;
    public $baseTypeId;
    public $svgline;
    public $svgcircle;
    public $adate;
    public $udate;
    public $oldApId;
    public $runwaysCount;
    public $maxRwLenght;
    public $maxMtow;
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($ap_id) {
        $mn = "AmsAirport::LoadById(".$ap_id.")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsAirport::SelectJson($ap_id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = $objArrJ[0];
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($dataJson) {
        $mn = "AmsAirport::Save()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        AmsAlLogger::log($mn, " dataJson = " . json_encode($dataJson));
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $ap_id = null;
            if(isset($dataJson->apId)){
               AmsAlLogger::log($mn, "Update  apId =" . $dataJson->apId);
               $ap_id = $dataJson->apId;
               $ap_id = AmsAirport::Update($dataJson, $conn, $mn, $logModel);

            } else{
                AmsAlLogger::log($mn, "Create ap ");
                $ap_id = AmsAirport::Create($dataJson, $conn, $mn, $logModel);
            }
            
            AmsAlLogger::log($mn, " ap_id =" . $ap_id);
            if(isset($ap_id)){
                $objArrJ = AmsAirport::SelectJson($ap_id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = $objArrJ[0];
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
       return $response;
    }
    
    public static function AirportTable($params) {
        $mn = "AmsAirport::AirportTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT a.ap_id as apId,
                    a.ap_iata as iata,
                    a.ap_icao as icao,
                    a.ap_name as apName,
                    a.ap_lat as lat, a.ap_lon as lon,
                    a.ap_elevation_m as elevation,
                    a.ap_type_id as typeId,
                    a.region_id as regionId,
                    a.subregion_id as subregionId,
                    a.country_id as countryId,
                    c.country_iso2 as iso2, c.country_name as cName,
                    a.state_id as stateId,
                    st.state_name as stName,
                    a.city_id as cityId,
                    ct.city_name as ctName,
                    a.ap_active as active,
                    a.ap_wikiurl as wikiUrl,
                    a.ams_status_id as amsStatus,
                    a.wad_status_id as wadStatus,
                    a.ap_homeurl as homeUrl,
                    a.ap_notes as notes,
                    a.ap_utc as utc,
                    a.ap_type_base as baseTypeId,
                    a.rw_svg_line as svgline,
                    a.rw_svg_path_circle as svgcircle,
                    a.adate , a.udate,
                    count(rw.rw_id) as runwaysCount,
                    a.air_airport_id as oldApId,
                    max_rw_lenght_m as maxRwLenght,
                    max_mtow_kg as maxMtow,
                    pld.pax_demand_for_day as paxDemandDay,
                    pld.pax_demand_for_period as paxDemandPeriod,
                    pld.cargo_demand_for_period_kg as cargoDemandPeriodKg,
                    pld.period_days as paxDemandPeriodDays,
                    pld.period_nexttime as plddate
                    FROM ams_wad.cfg_airport a
                    left join ams_wad.cfg_city ct on ct.city_id = a.city_id
                    left join ams_wad.cfg_country c on c.country_id = a.country_id 
                    left join ams_wad.cfg_country_state st on st.state_id = a.state_id 
                    left join ams_wad.cfg_airport_runway rw on rw.ap_id = a.ap_id
                    left join ams_wad.calc_airport_payload_demand pld on pld.ap_id = a.ap_id ";
            
            
            
            if(isset($params->regionId) && strlen($params->regionId)>0){
                $sqlWhere = " WHERE a.region_id = ".$params->regionId." ";
            } else if(isset($params->subregionId) && strlen($params->subregionId)>0){
                $sqlWhere = " WHERE a.subregion_id = ".$params->subregionId." ";
            } else if(isset($params->countryId) && strlen($params->countryId)>0){
                $sqlWhere = " WHERE a.country_id = ".$params->countryId." ";
            } else if(isset($params->stateId) && strlen($params->stateId)>0){
                $sqlWhere = " WHERE a.state_id = ".$params->stateId." ";
            }else if(isset($params->cityId) && strlen($params->cityId)>0){
                $sqlWhere = " WHERE a.city_id = ".$params->cityId." ";
            }
           
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND ( a.ap_iata like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or a.ap_icao like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or a.ap_name like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE ( a.ap_iata like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or a.ap_icao like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or a.ap_name like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = "group by a.ap_id ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc").", apName";
            }
            else{
                $sqlOrder .= " order by typeId desc, baseTypeId desc, apName ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("airports", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_wad.cfg_airport a ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function AirportTableSolr($params) {
        $mn = "AmsAirport::AirportTableSolr()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT a.* FROM ams_wad.v_solr_airport a ";
            $sqlWhere=" ";
            
            if(isset($params->regionId) && strlen($params->regionId)>0){
                $sqlWhere = " WHERE a.region_id = ".$params->regionId." ";
            } else if(isset($params->subregionId) && strlen($params->subregionId)>0){
                $sqlWhere = " WHERE a.subregion_id = ".$params->subregionId." ";
            } else if(isset($params->countryId) && strlen($params->countryId)>0){
                $sqlWhere = " WHERE a.country_id = ".$params->countryId." ";
            } else if(isset($params->stateId) && strlen($params->stateId)>0){
                $sqlWhere = " WHERE a.state_id = ".$params->stateId." ";
            }else if(isset($params->cityId) && strlen($params->cityId)>0){
                $sqlWhere = " WHERE a.city_id = ".$params->cityId." ";
            }
            
           if(isset($params->active)){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND a.active = ".$params->active." ";
                }
                else{
                    $sqlWhere .= " WHERE a.active = ".$params->active." ";
                }
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND ( a.ap_iata like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or a.ap_icao like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or a.ap_name like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE ( a.ap_iata like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or a.ap_icao like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or a.ap_name like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = "";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc").", apName";
            }
            else{
                $sqlOrder .= " order by typeId desc, baseTypeId desc, apName ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("airports", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_wad.v_solr_airport a ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function AirportCountByAdateMonth() {
        $mn = "AmsAirport::AirportCountByAdateMonth()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            
            $sql = "SELECT count(*) apCount, year(adate) as ayear, month(adate) as amonth 
                FROM ams_wad.cfg_airport
                where 1=?
                group by  year(adate), month(adate)
                order by ayear, amonth ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("apAdateStat", $ret_json_data);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function SelectJson($ap_id, $conn, $mn, $logModel){
        
        $sql = " SELECT a.ap_id as apId,
                a.ap_iata as iata,
                a.ap_icao as icao,
                a.ap_name as apName,
                a.ap_lat as lat, a.ap_lon as lon,
                a.ap_elevation_m as elevation,
                a.ap_type_id as typeId,
                a.region_id as regionId,
                a.subregion_id as subregionId,
                a.country_id as countryId,
                c.country_iso2 as iso2, c.country_name as cName,
                a.state_id as stateId,
                st.state_name as stName,
                a.city_id as cityId,
                ct.city_name as ctName,
                a.ap_active as active,
                a.ap_wikiurl as wikiUrl,
                a.ams_status_id as amsStatus,
                a.wad_status_id as wadStatus,
                a.ap_homeurl as homeUrl,
                a.ap_notes as notes,
                a.ap_utc as utc,
                a.ap_type_base as baseTypeId,
                a.rw_svg_line as svgline,
                a.rw_svg_path_circle as svgcircle,
                a.adate , a.udate,
                count(rw.rw_id) as runwaysCount,
                a.air_airport_id as oldApId,
                max_rw_lenght_m as maxRwLenght,
                max_mtow_kg as maxMtow
                FROM ams_wad.cfg_airport a
                left join ams_wad.cfg_city ct on ct.city_id = a.city_id
                left join ams_wad.cfg_country c on c.country_id = a.country_id
                left join ams_wad.cfg_country_state st on st.state_id = a.state_id 
                left join ams_wad.cfg_airport_runway rw on rw.ap_id = a.ap_id
                where a.ap_id = ? " ;

        $bound_params_r = ["i",$ap_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_wad.cfg_airport
            (ap_iata, ap_icao, ap_name, ap_lat, ap_lon, ap_elevation_m,
            ap_type_id, region_id, subregion_id, country_id, state_id, city_id,
            ap_active, ap_wikiurl, ams_status_id, wad_status_id, ap_homeurl, 
            ap_notes, ap_utc, ap_type_base)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["sssddiiiiiiiisiissii",
            ((!isset($dataJson->iata) || strlen($dataJson->iata)==0) ? null : $dataJson->iata),
            ((!isset($dataJson->icao)) ? null : $dataJson->icao),
            ((!isset($dataJson->apName)) ? null : $dataJson->apName),
            ((!isset($dataJson->lat)) ? null : $dataJson->lat),
            ((!isset($dataJson->lon)) ? null : $dataJson->lon),
            ((!isset($dataJson->elevation)) ? null : $dataJson->elevation),
            ((!isset($dataJson->typeId)) ? null : $dataJson->typeId),
            ((!isset($dataJson->regionId)) ? null : $dataJson->regionId),
            ((!isset($dataJson->subregionId)) ? null : $dataJson->subregionId),
            ((!isset($dataJson->countryId)) ? null : $dataJson->countryId),
            
            ((!isset($dataJson->stateId)) ? null : $dataJson->stateId),
            ((!isset($dataJson->cityId)) ? null : $dataJson->cityId),
            ((!isset($dataJson->active)) ? 0 : $dataJson->active),
            ((!isset($dataJson->wikiUrl)) ? null : $dataJson->wikiUrl),
            ((!isset($dataJson->amsStatus)) ? 1 : $dataJson->amsStatus),
            ((!isset($dataJson->wadStatus)) ? 1 : $dataJson->wadStatus),
            ((!isset($dataJson->homeUrl)) ? null : $dataJson->homeUrl),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->utc)) ? null : $dataJson->utc),
            ((!isset($dataJson->baseTypeId)) ? null : $dataJson->baseTypeId)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_wad.cfg_airport
                    SET ap_iata=?,  ap_icao=?, ap_name=?, 
                    ap_lat=?, ap_lon=?, ap_elevation_m=?,
                    ap_type_id=?, region_id=?, subregion_id=?, 
                    country_id=?, state_id=?, city_id=?,
                    ap_active=?, ap_wikiurl=?, ams_status_id=?, 
                    wad_status_id=?, ap_homeurl=?, 
                    ap_notes=?, ap_utc=?, ap_type_base=?
                     WHERE ap_id = ? " ;

        $bound_params_r = ["sssddiiiiiiiisiissiii",
            ((!isset($dataJson->iata) || strlen($dataJson->iata)==0) ? null : $dataJson->iata),
            ((!isset($dataJson->icao)) ? null : $dataJson->icao),
            ((!isset($dataJson->apName)) ? null : $dataJson->apName),
            ((!isset($dataJson->lat)) ? null : $dataJson->lat),
            ((!isset($dataJson->lon)) ? null : $dataJson->lon),
            ((!isset($dataJson->elevation)) ? null : $dataJson->elevation),
            ((!isset($dataJson->typeId)) ? null : $dataJson->typeId),
            ((!isset($dataJson->regionId)) ? null : $dataJson->regionId),
            ((!isset($dataJson->subregionId)) ? null : $dataJson->subregionId),
            ((!isset($dataJson->countryId)) ? null : $dataJson->countryId),
            
            ((!isset($dataJson->stateId)) ? null : $dataJson->stateId),
            ((!isset($dataJson->cityId)) ? null : $dataJson->cityId),
            ((!isset($dataJson->active)) ? 0 : $dataJson->active),
            ((!isset($dataJson->wikiUrl)) ? null : $dataJson->wikiUrl),
            ((!isset($dataJson->amsStatus)) ? 1 : $dataJson->amsStatus),
            ((!isset($dataJson->wadStatus)) ? 1 : $dataJson->wadStatus),
            ((!isset($dataJson->homeUrl)) ? null : $dataJson->homeUrl),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->utc)) ? null : $dataJson->utc),
            ((!isset($dataJson->baseTypeId)) ? null : $dataJson->baseTypeId),
            ($dataJson->apId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->apId;
    }
    
    static function Delete($ap_id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM ams_wad.cfg_airport
                   WHERE ap_id = ? " ;

        $bound_params_r = ["i", $ap_id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted state_id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Table PPD CPD Methods">
    
    public static function CpdTable($params) {
        $mn = "AmsAirport::CpdTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT cpd.cpd_id as cpdId,
                cpd.dest_id as destId,
                cpd.dep_ap_id as depApId,
                d.arr_ap_id as arrApId,
                concat(if(dep.ap_iata is null, dep.ap_icao, dep.ap_iata), ' -> ', if(arr.ap_iata is null, arr.ap_icao, arr.ap_iata)) as apCodes,
                cpd.payload_kg as payloadKg,
                cpd.pax_class_id as paxClassId,
                cpd.ppd_type_id as ppdTypeId,
                cpd.max_ticket_price as maxPrice,
                cpd.note, cpd.sequence,  cpd.adate, cpd.udate,
                ifnull(cpd.booking_sequence_arr, 0) as bookingSeqArr, 
                ifnull(cpd.booking_sequence,0) as bookingSeqDep
                FROM ams_wad.ams_airport_cpd cpd
                join ams_wad.cfg_airport_destination d on d.dest_id = cpd.dest_id
                join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
                join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id ";
            
            
            $sqlWhere  = null;
            if(isset($params->depApId) && strlen($params->depApId)>0){
                $sqlWhere = " WHERE cpd.dep_ap_id = ".$params->depApId." ";
            }
            if(isset($params->destId) && strlen($params->destId)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (cpd.dest_id = ".$params->destId." )";
                }
                else{
                    $sqlWhere .= " WHERE (cpd.dest_id = ".$params->destId." )";
                }
            }
            if(isset($params->arrApId) && strlen($params->arrApId)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (d.arr_ap_id = ".$params->arrApId." )";
                }
                else{
                    $sqlWhere .= " WHERE (d.arr_ap_id = ".$params->arrApId." )";
                }
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    
                    $sqlWhere .= " AND (cpd.note like '%".$params->qry_filter."%'  OR ";
                    $sqlWhere .= " if(arr.ap_iata is null, arr.ap_icao, arr.ap_iata) like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE (cpd.note like '%".$params->qry_filter."%'  OR ";
                    $sqlWhere .= " if(arr.ap_iata is null, arr.ap_icao, arr.ap_iata) like '%".$params->qry_filter."%' )";
                }
               
            }
            
            $sqlOrder = " ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by udate desc, adate desc ";
            }
            $sql .= (isset($sqlWhere)?$sqlWhere:"").$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("payloads", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_wad.ams_airport_cpd cpd
                    join ams_wad.cfg_airport_destination d on d.dest_id = cpd.dest_id
                    join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
                    join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id ".
                    (isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function PpdTable($params) {
        $mn = "AmsAirport::PpdTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT ppd.ppd_id as ppdId,
                ppd.dest_id as destId,
                ppd.dep_ap_id as depApId,
                d.arr_ap_id as arrApId,
                concat(if(dep.ap_iata is null, dep.ap_icao, dep.ap_iata), ' -> ', if(arr.ap_iata is null, arr.ap_icao, arr.ap_iata)) as apCodes,
                ppd.pax as pax,
                ppd.pax_class_id as paxClassId,
                ppd.ppd_type_id as ppdTypeId,
                ppd.payload_kg as payloadKg,
                ppd.max_ticket_price as maxPrice,
                ppd.note, ppd.sequence,  ppd.adate, ppd.udate,
                ifnull(ppd.booking_sequence_arr, 0) as bookingSeqArr, 
                ifnull(ppd.booking_sequence,0) as bookingSeqDep
                FROM ams_wad.ams_airport_ppd ppd
                join ams_wad.cfg_airport_destination d on d.dest_id = ppd.dest_id
                join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
                join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id ";
            
            
            $sqlWhere  = null;
            if(isset($params->depApId) && strlen($params->depApId)>0){
                $sqlWhere = " WHERE ppd.dep_ap_id = ".$params->depApId." ";
            }
            if(isset($params->destId) && strlen($params->destId)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (ppd.dest_id = ".$params->destId." )";
                }
                else{
                    $sqlWhere .= " WHERE (ppd.dest_id = ".$params->destId." )";
                }
            }
            if(isset($params->arrApId) && strlen($params->arrApId)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (d.arr_ap_id = ".$params->arrApId." )";
                }
                else{
                    $sqlWhere .= " WHERE (d.arr_ap_id = ".$params->arrApId." )";
                }
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (ppd.note like '%".$params->qry_filter."%'  OR ";
                    $sqlWhere .= " if(arr.ap_iata is null, arr.ap_icao, arr.ap_iata) like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE (ppd.note like '%".$params->qry_filter."%'  OR ";
                    $sqlWhere .= " if(arr.ap_iata is null, arr.ap_icao, arr.ap_iata) like '%".$params->qry_filter."%' )";
                }
               
            }
            
            $sqlOrder = " ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by udate desc, adate desc ";
            }
            $sql .= (isset($sqlWhere)?$sqlWhere:"").$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("payloads", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_wad.ams_airport_ppd ppd
                    join ams_wad.cfg_airport_destination d on d.dest_id = ppd.dest_id
                    join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
                    join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id ".
                    (isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function PayloadDemandSumByDayTable($params) {
        $mn = "AmsAirport::PayloadDemandSumByDayTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT  sum(if(pax>0 or cpd>0, 1, 0)) as destinations,
                    sum(ppd_e) as paxE, sum(ppd_b) as paxB, sum(ppd_f) as paxF,
                    sum(pax) as pax,
                    sum(cpd)as cargoKg, date(adate) as adate
                     FROM ams_wad.st_airport_destination_day";
            
            
            $sqlWhere  = null;
            if(isset($params->depApId) && strlen($params->depApId)>0){
                $sqlWhere = " WHERE dep_ap_id = ".$params->depApId." ";
                $response->addData("depApId", $params->depApId);
            }
            if(isset($params->destId) && strlen($params->destId)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (dest_id = ".$params->destId." )";
                }
                else{
                    $sqlWhere .= " WHERE (dest_id = ".$params->destId." )";
                }
                $response->addData("destId", $params->destId);
            }
            if(isset($params->arrApId) && strlen($params->arrApId)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (arr_ap_id = ".$params->arrApId." )";
                }
                else{
                    $sqlWhere .= " WHERE (arr_ap_id = ".$params->arrApId." )";
                }
                $response->addData("arrApId", $params->arrApId);
            }
            
            $sqlOrder = " group by dep_ap_id, date(adate)
                            order by adate desc";
            
            $sql .= (isset($sqlWhere)?$sqlWhere:"").$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("payloads", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_wad.st_airport_destination_day ".
                    (isset($sqlWhere)?($sqlWhere." and 1=? group by dep_ap_id, date(adate) "):" where 1=? group by dep_ap_id, date(adate) ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            if(isset($ret_json_data) && count($ret_json_data)>0){
                $rowJson = json_decode(json_encode($ret_json_data[0]));
                $response->addData("rowsCount", $rowJson->totalRows);
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function destinationPcpdTable($params) {
        $mn = "AmsAirport::destinationPcpdTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT  d.dep_ap_id as depApId, dep.ap_lat as depLat, dep.ap_lon as depLon,
                if(dep.ap_iata, dep.ap_iata, dep.ap_icao) as depApCode, dep.ap_name as depApNme,
                d.arr_ap_id as arrApId, arr.ap_lat as arrLat, arr.ap_lon as arrLon,
                if(arr.ap_iata, arr.ap_iata, arr.ap_icao) as arrApCode, arr.ap_name as arrApNme,
                 d.distance_km as distanceKm, d.max_flight_h as maxFlightH,
                apd.dest_id destId, apd.ppd_e as ppdE, apd.ppd_b as ppdB, apd.ppd_f as ppdF, apd.cpd as cargoKg
                FROM ams_wad.st_airport_destination apd
                join ams_wad.cfg_airport_destination d on d.dest_id = apd.dest_id
                join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
                join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id  ";
            
            
            $sqlWhere  = "where (apd.ppd_e>0 OR apd.ppd_b>0 or apd.ppd_f>0 OR apd.cpd >0)";
            if(isset($params->depApId) && strlen($params->depApId)>0){
                 if(isset($sqlWhere)){
                    $sqlWhere .= " AND (d.dep_ap_id = ".$params->depApId." )";
                }
                else{
                    $sqlWhere .= " WHERE (d.dep_ap_id = ".$params->depApId." )";
                }
            }
            if(isset($params->arrApId) && strlen($params->arrApId)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (d.arr_ap_id = ".$params->arrApId." )";
                }
                else{
                    $sqlWhere .= " WHERE (d.arr_ap_id = ".$params->arrApId." )";
                }
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (dep.ap_name like '%".$params->qry_filter."%'  OR arr.ap_name like '%".$params->qry_filter."%')";
                }
                else{
                    $sqlWhere .= " WHERE (dep.ap_name like '%".$params->qry_filter."%'  OR arr.ap_name like '%".$params->qry_filter."%')";
                }
               
            }
            
            $sqlOrder = " ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by distanceKm ";
            }
            $sql .= (isset($sqlWhere)?$sqlWhere:"").$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("payloads", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_wad.st_airport_destination apd
                join ams_wad.cfg_airport_destination d on d.dest_id = apd.dest_id
                join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
                join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id ".
                    (isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function PayloadDemandSumByArrApTable($params) {
        $mn = "AmsAirport::PayloadDemandSumByArrApTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT d.arr_ap_id as arrApId, concat(arr.ap_name,' ',if(arr.ap_iata, arr.ap_iata, arr.ap_icao)) as arrApCode, 
                    d.distance_km as distanceKm, d.max_flight_h as maxFlightH, 
                    sum(ifnull(apd.cpd,0)) as cargoKg,
                    sum((ifnull(apd.ppd_e,0) + ifnull(apd.ppd_b,0) + ifnull(apd.ppd_f,0))) as ppd,
                    apd.dest_id destId, 
                    sum(ifnull(apd.ppd_e,0)) as ppdE, sum(ifnull(apd.ppd_b,0)) as ppdB, sum(ifnull(apd.ppd_f,0)) as ppdF,
                     d.dep_ap_id as depApId, if(dep.ap_iata, dep.ap_iata, dep.ap_icao) as depApCode
                    FROM ams_wad.st_airport_destination apd
                    join ams_wad.cfg_airport_destination d on d.dest_id = apd.dest_id
                    join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id";
            
            
            $sqlWhere  = " where (apd.ppd_e>0 OR apd.ppd_b>0 or apd.ppd_f>0 OR apd.cpd >0) ";
            if(isset($params->depApId) && strlen($params->depApId)>0){
                $sqlWhere .= " AND d.dep_ap_id = ".$params->depApId." ";
                $response->addData("depApId", $params->depApId);
            }
            if(isset($params->arrApId) && strlen($params->arrAptId)>1){
                $sqlWhere .= " AND (d.arr_ap_id = ".$params->arrAptId." )";
                $response->addData("arrAptId", $params->arrAptId);
            }
            
             if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (if(arr.ap_iata is null, arr.ap_icao, arr.ap_iata) like '%".$params->qry_filter."%' ) ";
                }
                else{
                    $sqlWhere .= " WHERE (if(arr.ap_iata is null, arr.ap_icao, arr.ap_iata) like '%".$params->qry_filter."%' ) ";
                }
               
            }
            
            $sqlOrder = " group by d.dep_ap_id, d.arr_ap_id, d.distance_km, d.max_flight_h, apd.dest_id  "
                    . " order by ppd desc, cargoKg desc, distance_km ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= ", apd.".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc")." ";
            }
            
            $sql .= (isset($sqlWhere)?$sqlWhere:"").$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("payloads", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows 
                    FROM (SELECT d.arr_ap_id from ams_wad.st_airport_destination apd
                    join ams_wad.cfg_airport_destination d on d.dest_id = apd.dest_id
                    join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id ".
                    (isset($sqlWhere)?($sqlWhere." and 1=? group by d.dep_ap_id, d.arr_ap_id, d.distance_km, d.max_flight_h, apd.dest_id) d "):" where 1=? group by d.dep_ap_id, d.arr_ap_id, d.distance_km, d.max_flight_h, apd.dest_id ) d ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
}
