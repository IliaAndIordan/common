<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Jan 7, 2021 
 *  Filename            : RunwayModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of Country
 *
 * @author IZIordanov
 */
class Runway {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $rwId;
    public $apId;
    
    public $rwType;
    public $rwDirection;
    public $serfaceId;
    public $width;
    public $lenght;
    public $mtow;
    public $rwOrder;
    public $rotation;
    public $radius;
    public $udate;
    public $svgLine;
    public $svgPath;
    
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($rw_id) {
        $mn = "Runway::LoadById(".$rw_id.")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = Runway::SelectJson($rw_id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = $objArrJ[0];
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($dataJson) {
        $mn = "Runway::Save()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        AmsAlLogger::log($mn, " dataJson = " . json_encode($dataJson));
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $rw_id = null;
            if(isset($dataJson->rwId)){
               AmsAlLogger::log($mn, "Update  rwId =" . $dataJson->rwId);
               $rw_id = $dataJson->rwId;
               $rw_id = Runway::Update($dataJson, $conn, $mn, $logModel);

            } else{
                AmsAlLogger::log($mn, "Create ap ");
                $rw_id = Runway::Create($dataJson, $conn, $mn, $logModel);
            }
            
            AmsAlLogger::log($mn, " rw_id =" . $rw_id);
            if(isset($rw_id)){
                 $bound_params_r = ["i",$rw_id];
                 $sql = "call ams_wad.calc_mtow_rwid(?)";
                $conn->runPreparedQuery($sql, $bound_params_r, $logModel);
                $objArrJ = Runway::SelectJson($rw_id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = $objArrJ[0];
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
       return $response;
    }
    
    public static function RwTable($params) {
        $mn = "Runway::AirportTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT rw_id as rwId, ap_id as apId, rw_type as rwType,
                    rw_direction as rwDirection, rw_serface_id as serfaceId,
                    rw_width_m as width, rw_lenght_m as lenght, mtow_kg as mtow,
                    rw_order as rwOrder, rw_rodation_degree as rotation,
                    rw_radius as radius, udate, rw_svg_line as svgLine, rw_svg_path_cercle as svgPath
                    FROM ams_wad.cfg_airport_runway ";
            
            
            
            if(isset($params->apId) && strlen($params->apId)>0){
                $sqlWhere = " WHERE ap_id = ".$params->apId." ";
            } 
           
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND ( rw_direction like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE ( rw_direction like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = " ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by rwOrder ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("runways", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_wad.cfg_airport_runway ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function LoadByAirportId($ap_id) {
        $mn = "Runway::LoadByAirportId(".$ap_id.")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
             //UNIX_TIMESTAMP
            $sql = "SELECT rw_id as rwId, ap_id as apId, rw_type as rwType,
                    rw_direction as rwDirection, rw_serface_id as serfaceId,
                    rw_width_m as width, rw_lenght_m as lenght, mtow_kg as mtow,
                    rw_order as rwOrder, rw_rodation_degree as rotation,
                    rw_radius as radius, rw_svg_line as svgLine, rw_svg_path_cercle as svgPath
                    FROM ams_wad.cfg_airport_runway 
                    WHERE ap_id = ?";
            
            
            
            
            $bound_params_r = ["i", $ap_id];
            $response = $conn->SelectJson($sql, $bound_params_r, $logModel);
           
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function SelectJson($rw_id, $conn, $mn, $logModel){
        
        $sql = " SELECT rw_id as rwId,  ap_id as apId, rw_type as rwType,
            rw_direction as rwDirection, rw_serface_id as serfaceId,
            rw_width_m as width, rw_lenght_m as lenght, mtow_kg as mtow,
            rw_order as rwOrder, rw_rodation_degree as rotation,
            rw_radius as radius, udate, rw_svg_line as svgLine, rw_svg_path_cercle as svgPath
            FROM ams_wad.cfg_airport_runway
            where rw_id = ? " ;

        $bound_params_r = ["i",$rw_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_wad.cfg_airport_runway
            (ap_id, rw_type, rw_direction, rw_serface_id, rw_width_m, rw_lenght_m,
            mtow_kg, rw_order, rw_rodation_degree, rw_radius)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["iisidddiii",
            ((!isset($dataJson->apId)) ? null : $dataJson->apId),
            ((!isset($dataJson->rwType)) ? null : $dataJson->rwType),
            ((!isset($dataJson->rwDirection)) ? null : $dataJson->rwDirection),
            ((!isset($dataJson->serfaceId)) ? null : $dataJson->serfaceId),
            ((!isset($dataJson->width)) ? null : $dataJson->width),
            ((!isset($dataJson->lenght)) ? null : $dataJson->lenght),
            ((!isset($dataJson->mtow)) ? null : $dataJson->mtow),
            ((!isset($dataJson->rwOrder)) ? 1 : $dataJson->rwOrder),
            ((!isset($dataJson->rotation)) ? null : $dataJson->rotation),
            ((!isset($dataJson->radius)) ? null : $dataJson->radius)
        ];
        
        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_wad.cfg_airport_runway
                    SET ap_id=?,  rw_type=?, rw_direction=?, 
                    rw_serface_id=?, rw_width_m=?, rw_lenght_m=?,
                    mtow_kg=?, rw_order=?, rw_rodation_degree=?, 
                    rw_radius=?
                     WHERE rw_id = ? " ;

        $bound_params_r = ["iisidddiiii",
            ((!isset($dataJson->apId)) ? null : $dataJson->apId),
            ((!isset($dataJson->rwType)) ? null : $dataJson->rwType),
            ((!isset($dataJson->rwDirection)) ? null : $dataJson->rwDirection),
            ((!isset($dataJson->serfaceId)) ? null : $dataJson->serfaceId),
            ((!isset($dataJson->width)) ? null : $dataJson->width),
            ((!isset($dataJson->lenght)) ? null : $dataJson->lenght),
            ((!isset($dataJson->mtow)) ? null : $dataJson->mtow),
            ((!isset($dataJson->rwOrder)) ? 1 : $dataJson->rwOrder),
            ((!isset($dataJson->rotation)) ? null : $dataJson->rotation),
            ((!isset($dataJson->radius)) ? null : $dataJson->radius),
            ($dataJson->rwId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->rwId;
    }
    
    static function Delete($rw_id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM ams_wad.cfg_airport_runway
                   WHERE rw_id = ? " ;

        $bound_params_r = ["i", $rw_id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted rw_id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}
