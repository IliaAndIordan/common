<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Dec 22, 2020 
 *  Filename            : CfgCharterGroup.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of CfgCharterGroup
 *
 * @author IZIordanov
 */
class CfgCharterGroup {
    
   // <editor-fold defaultstate="collapsed" desc="Fields">

    public $chGrpId;
    public $apId;
    public $chGrpCode;
    public $chGrpName; 
    public $chGrpNotes;
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($id) {
        $mn = "CfgCharterGroup::LoadById(".$id.")";
        AmsAlLogger::logBegin($mn);
        $response = new CfgCharterGroup();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = CfgCharterGroup::SelectJson($id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "CfgCharterGroup::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsAlLogger::log($mn, " flightName = " . $dataJson->flightName);
        $response = new CfgCharterGroup();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = null;
            if(isset($dataJson->chGrpId)){
               //AmsAlLogger::log($mn, "Update  chGrpId =" . $dataJson->chGrpId);
               $id = $dataJson->chGrpId;
               CfgCharterGroup::Update($dataJson, $conn, $mn, $logModel);

            } else{
                //AmsAlLogger::log($mn, "Create CfgCharterGroup");
                $id = CfgCharterGroup::Create($dataJson, $conn, $mn, $logModel);
            }
            
            //AmsAlLogger::log($mn, " id =" . $id);
           if(isset($id)){                
                $objArrJ = CfgCharterGroup::SelectJson($id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function CharterGroupTable($params) {
        $mn = "CfgCharterGroup::CharterGroupTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT g.ch_grp_id as chGrpId,
                        g.ap_id  as apId,
                        g.ch_grp_code as chGrpCode,
                        g.ch_grp_name as chGrpName,
                        g.ch_grp_notes as chGrpNotes
                        FROM ams_wad.cfg_charter_group g
                     ";
            
            
            $sqlWhere="";
            if(isset($params->chGrpId) && strlen($params->chGrpId)>0){
                $sqlWhere = " WHERE g.ch_grp_id = ".$params->chGrpId." ";
            }
            if(isset($params->apId) && strlen($params->apId)>1){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND g.ap_id = ".$params->apId." ";
                }
                else{
                    $sqlWhere = " WHERE g.ap_id ".$params->apId." ";
                }
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (g.ch_grp_code like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or g.ch_grp_name like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere = " WHERE (g.ch_grp_code '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or g.ch_grp_name like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = "";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by chGrpCode ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("groups", $ret_json_data);
            
            $sql = "SELECT count(*) as total_rows
                    FROM ams_wad.cfg_charter_group g ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->total_rows);
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
  

    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function SelectJson($id, $conn, $mn, $logModel){
        
        $sql = "SELECT g.ch_grp_id as chGrpId,
                g.ap_id  as apId,
                g.ch_grp_code as chGrpCode,
                g.ch_grp_name as chGrpName,
                g.ch_grp_notes as chGrpNotes
                FROM ams_wad.cfg_charter_group g
                WHERE g.ch_grp_id = ? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_wad.cfg_charter_group 
            ( ap_id, ch_grp_code, ch_grp_name, ch_grp_notes)
            VALUES(?, ?, ?, ?)" ;

        $bound_params_r = ["1sss",
             ((!isset($dataJson->apId)) ? null : $dataJson->apId),
            ((!isset($dataJson->chGrpCode)) ? null : $dataJson->chGrpCode),
            ((!isset($dataJson->chGrpName)) ? null : $dataJson->chGrpName),
            ((!isset($dataJson->chGrpNotes)) ? null : $dataJson->chGrpNotes)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_wad.cfg_charter_group 
                    SET ap_id=?,   ch_grp_code=?, 
                    ch_grp_name=?,  ch_grp_notes=?
                    WHERE ch_grp_id = ? " ;

        $bound_params_r = ["isss1",
             ((!isset($dataJson->apId)) ? null : $dataJson->apId),
            ((!isset($dataJson->chGrpCode)) ? null : $dataJson->chGrpCode),
            ((!isset($dataJson->chGrpName)) ? null : $dataJson->chGrpName),
            ((!isset($dataJson->chGrpNotes)) ? null : $dataJson->chGrpNotes),
            ((!isset($dataJson->chGrpId)) ? null : $dataJson->chGrpId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        
        $logModel->logDebug('Update', " affectedRows: " . $affectedRows);
        return $dataJson->chGrpId;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE ams_wad.cfg_charter_group 
                   WHERE ch_grp_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted charter_id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
    
}

