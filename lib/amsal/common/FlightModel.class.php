<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Oct 21, 2021 
 *  Filename          : FlightModel.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */
require_once("Functions.php");
require_once ("AircraftModel.class.php");

/**
 * Description of FlightModel
 *
 * @author IZIordanov
 */
class FlightModel {

    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $flId;
    public $flTypeId;
    public $flName;
    public $flDescription;
    public $fleId;
    public $flpId;
    public $routeId;
    public $alId;
    public $acId;
    public $dApId;
    public $aApId;
    public $distanceKm;
    public $fuelL;
    public $pax;
    public $payloadKg;
    public $etdtime;
    public $etatime;
    public $dtime;
    public $atime;
    public $delayMin;
    public $expences;
    public $income;
    public $revenue;
    public $flLogId;
    public $acfsId;
    public $flLogDescription;
    public $flTimeMin;
    public $flDistanceKm;
    public $flLogOpTimeSec;
    public $flLogDelayMin;
    public $adate;
    public $macId;
    public $speedKmPh;
    public $remainDistanceKm;
    public $remainTimeMin;

    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Flight Methods">

    public static function LoadById($id) {
        $mn = "FlightModel::LoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = FlightModel::SelectJson($id, $conn, $mn, $logModel);

            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
            
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function PayloadById($id) {
        $mn = "FlightModel::PayloadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = FlightModel::SelectPayloadJson($id, $conn, $mn, $logModel);

            if (isset($objArrJ)) {
                $response = json_decode(json_encode($objArrJ));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
     public static function FlighhtQueueLoadById($id) {
        $mn = "FlightModel::FlighhtQueueLoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = FlightModel::FlightQueueSelectJson($id, $conn, $mn, $logModel);

            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function FlighhtQueuePayloadById($id) {
        $mn = "FlightModel::FlighhtQueuePayloadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = FlightModel::FlightQueueSelectPayloadJson($id, $conn, $mn, $logModel);

            if (isset($objArrJ)) {
                $response = json_decode(json_encode($objArrJ));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function FlightTable($params) {
        $mn = "FlightModel::FlightTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT fl.fl_id as flId, fl.fl_type_id as flTypeId,
                    fl.fl_name as flName, fl.fl_description as flDescription, 
                    fl.fle_id as fleId, fl.flp_id as flpId, fl.route_id as routeId, fl.ch_id as chId,
                    fl.al_id as alId, fl.ac_id as acId, ac.registration as acRegNr, fl.d_ap_id as depApId, fl.a_ap_id as arrApId,
                    fl.distance_km as distanceKm, fl.oil_l as fuelL, fl.pax, fl.payload_kg as payloadKg,
                    fl.dtime as etdTime, fl.atime as etaTime, fl.dtime_real as dtime, fl.atime_real as atime,
                    fl.delay_min as delayMin, ifnull(fl.expences, 0) as expences, ifnull(fl.income, 0) as income, ifnull(fl.revenue, 0) as revenue,
                    l.fl_log_id as flLogId, l.acfs_id as acfsId, 
                    l.fl_log_description as flLogDescription, l.fl_time_min as flTimeMin,
                    l.fl_distance_km as flDistanceKm, l.op_time_sec as flLogOpTimeSec, ifnull(l.delay_min, 0) as flLogDelayMin, l.adate, l.udate,
                    ac.mac_id as macId, mac.ac_type_id as acTypeId, mac.cruise_speed_kmph as speedKmPh,
                    round((fl.distance_km - l.fl_distance_km),0) remainDistanceKm, 
                    round(((fl.distance_km - l.fl_distance_km)/mac.cruise_speed_kmph)*if(l.acfs_id>6,0,60),2) as remainTimeMin,
                    al.al_name as alName, al.iata as alIata,
                    ifnull(dep.ap_iata, dep.ap_icao) as  depApCode, ifnull(arr.ap_iata, arr.ap_icao) as arrApCode
                    FROM ams_ac.ams_al_flight fl
                    join ams_ac.ams_al_flight_log l on fl.fl_id = l.fl_id
                    join ams_ac.ams_aircraft ac on ac.ac_id = fl.ac_id
                    join ams_ac.cfg_mac mac on mac.mac_id = ac.mac_id
                    join ams_al.ams_airline al on al.al_id = fl.al_id 
                    join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id 
                    join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id ";

            $sqlWhere = " ";
            if (isset($params->alId) && strlen($params->alId) > 0) {
                $sqlWhere = " WHERE fl.al_id = " . $params->alId . " ";
            }

            if (isset($params->acId) && strlen($params->acId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and fl.ac_id = " . $params->acId . " ";
                } else {
                    $sqlWhere = " WHERE fl.ac_id = " . $params->acId . " ";
                }
            }

            if (isset($params->macId) && strlen($params->macId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and ac.mac_id = " . $params->macId . " ";
                } else {
                    $sqlWhere = " WHERE ac.mac_id = " . $params->macId . " ";
                }
            }

            if (isset($params->dApId) && strlen($params->dApId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and fl.d_ap_id = " . $params->dApId . " ";
                } else {
                    $sqlWhere = " WHERE fl.d_ap_id = " . $params->dApId . " ";
                }
            }

            if (isset($params->aApId) && strlen($params->aApId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and fl.a_ap_id = " . $params->aApId . " ";
                } else {
                    $sqlWhere = " WHERE fl.a_ap_id = " . $params->aApId . " ";
                }
            }

            if (isset($params->qry_filter) && strlen($params->qry_filter) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND ( ifnull(dep.ap_iata, dep.ap_icao) like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or ifnull(arr.ap_iata, arr.ap_icao) like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or fl.fl_description like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or al.al_name like '%" . $params->qry_filter . "%' )";
                } else {
                    $sqlWhere .= " WHERE ( ifnull(dep.ap_iata, dep.ap_icao) like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or ifnull(arr.ap_iata, arr.ap_icao) like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or fl.fl_description like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or al.al_name like '%" . $params->qry_filter . "%' )";
                }
            }
            $sqlOrder = "";
            if (isset($params->qry_orderCol)) {
                $sqlOrder .= " order by remainTimeMin, " . $params->qry_orderCol . " " . ($params->qry_isDesc ? "desc" : " asc");
            } else {
                $sqlOrder .= " order by remainTimeMin, flId ";
            }
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder) > 1 ? $sqlOrder : "");
            $sql .= " LIMIT ? OFFSET ? ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("flights", $ret_json_data);

            $sql = "SELECT flpl.flpl_id as flplId,
                        flpl.fl_id as flId,
                        flpl.ch_id as chId,
                        flpl.payload_type as payloadType,
                        flpl.pax,
                        flpl.cargo_kg as cargoKg,
                        flpl.payload_kg as payloadKg,
                        flpl.description, flpl.price, flpl.adate
                    FROM ams_ac.ams_al_flight_payload flpl
                    join (
                        SELECT fl.fl_id
                        FROM ams_ac.ams_al_flight fl
                            join ams_ac.ams_al_flight_log l on fl.fl_id = l.fl_id
                            join ams_ac.ams_aircraft ac on ac.ac_id = fl.ac_id
                            join ams_ac.cfg_mac mac on mac.mac_id = ac.mac_id
                            join ams_al.ams_airline al on al.al_id = fl.al_id 
                            join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id 
                            join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id ";
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            //$sql .= (isset($sqlOrder) && strlen($sqlOrder)>1?$sqlOrder:"");
            $sql .= " LIMIT ? OFFSET ? ) fl on fl.fl_id = flpl.fl_id ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            //$bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_payload = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("payloads", $ret_json_payload);

            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_al_flight fl
                    join ams_ac.ams_al_flight_log l on fl.fl_id = l.fl_id
                    join ams_ac.ams_aircraft ac on ac.ac_id = fl.ac_id
                    join ams_ac.cfg_mac mac on mac.mac_id = ac.mac_id
                    join ams_al.ams_airline al on al.al_id = fl.al_id 
                    join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id 
                    join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id " . (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } 
        catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function FlightQueueTable($params) {
        $mn = "FlightModel::FlightQueueTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT fl.fl_id as flId, fl.fl_type_id as flTypeId,
                    fl.fl_name as flName, fl.fl_description as flDescription, 
                    fl.fle_id as fleId, fl.flp_id as flpId, fl.route_id as routeId, fl.ch_id as chId,
                    fl.al_id as alId, fl.ac_id as acId, ac.registration as acRegNr, fl.d_ap_id as depApId, fl.a_ap_id as arrApId,
                    fl.distance_km as distanceKm, fl.oil_l as fuelL, fl.pax, fl.payload_kg as payloadKg,
                    fl.dtime as etdTime, fl.atime as etaTime, 
                    ac.mac_id as macId, mac.cruise_speed_kmph as speedKmPh,
                    round(fl.distance_km,0) remainDistanceKm, 
                    round((fl.distance_km /mac.cruise_speed_kmph)*60,2) as remainTimeMin,
                    al.al_name as alName, al.iata as alIata,
                    ifnull(dep.ap_iata, dep.ap_icao) as  depApCode, ifnull(arr.ap_iata, arr.ap_icao) as arrApCode, fl.processed
                    FROM ams_ac.ams_al_flight_queue fl
                    join ams_ac.ams_aircraft ac on ac.ac_id = fl.ac_id
                    join ams_ac.cfg_mac mac on mac.mac_id = ac.mac_id
                    join ams_al.ams_airline al on al.al_id = fl.al_id 
                    join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id 
                    join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id ";

            $sqlWhere = " WHERE fl.processed=0 ";

            if (isset($params->alId) && strlen($params->alId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and fl.al_id = " . $params->alId . " ";
                } else {
                    $sqlWhere = " WHERE fl.al_id = " . $params->alId . " ";
                }
            }

            if (isset($params->acId) && strlen($params->acId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and fl.ac_id = " . $params->acId . " ";
                } else {
                    $sqlWhere = " WHERE fl.ac_id = " . $params->acId . " ";
                }
            }

            if (isset($params->macId) && strlen($params->macId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and ac.mac_id = " . $params->macId . " ";
                } else {
                    $sqlWhere = " WHERE ac.mac_id = " . $params->macId . " ";
                }
            }

            if (isset($params->dApId) && strlen($params->dApId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and fl.d_ap_id = " . $params->dApId . " ";
                } else {
                    $sqlWhere = " WHERE fl.d_ap_id = " . $params->dApId . " ";
                }
            }

            if (isset($params->aApId) && strlen($params->aApId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and fl.a_ap_id = " . $params->aApId . " ";
                } else {
                    $sqlWhere = " WHERE fl.a_ap_id = " . $params->aApId . " ";
                }
            }

            if (isset($params->qry_filter) && strlen($params->qry_filter) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND ( ifnull(dep.ap_iata, dep.ap_icao) like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or ifnull(arr.ap_iata, arr.ap_icao) like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or fl.fl_description like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or al.al_name like '%" . $params->qry_filter . "%' )";
                } else {
                    $sqlWhere .= " WHERE ( ifnull(dep.ap_iata, dep.ap_icao) like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or ifnull(arr.ap_iata, arr.ap_icao) like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or fl.fl_description like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or al.al_name like '%" . $params->qry_filter . "%' )";
                }
            }
            $sqlOrder = "";
            if (isset($params->qry_orderCol)) {
                $sqlOrder .= " order by  fl.dtime, " . $params->qry_orderCol . " " . ($params->qry_isDesc ? "desc" : " asc");
            } else {
                $sqlOrder .= " order by fl.dtime, flId ";
            }
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder) > 1 ? $sqlOrder : "");
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("flights", $ret_json_data);

            $sql = "SELECT flpl.flpl_id as flplId,
                        flpl.fl_id as flId,
                        flpl.ch_id as chId,
                        flpl.payload_type as payloadType,
                        flpl.pax,
                        flpl.cargo_kg as cargoKg,
                        flpl.payload_kg as payloadKg,
                        flpl.description, flpl.price, flpl.adate
                    FROM ams_ac.ams_al_flight_queue_payload flpl
                    join (
                        SELECT fl.fl_id as flId, 
                        fl.dtime as etdTime, fl.atime as etaTime
                        FROM ams_ac.ams_al_flight_queue fl
                        join ams_ac.ams_aircraft ac on ac.ac_id = fl.ac_id
                        join ams_ac.cfg_mac mac on mac.mac_id = ac.mac_id
                        join ams_al.ams_airline al on al.al_id = fl.al_id 
                        join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id 
                        join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id ";
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder) > 1 ? $sqlOrder : "");
            $sql .= " LIMIT ? OFFSET ? ) fl on fl.flId = flpl.fl_id ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            //$bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_payload = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("payloads", $ret_json_payload);

            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_al_flight_queue fl
                    join ams_ac.ams_aircraft ac on ac.ac_id = fl.ac_id
                    join ams_ac.cfg_mac mac on mac.mac_id = ac.mac_id
                    join ams_al.ams_airline al on al.al_id = fl.al_id 
                    join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id 
                    join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id " . (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", isset($rowJson)?$rowJson->totalRows:0);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function LastArrTimeByAcId($id) {
        $mn = "FlightModel::LastArrTimeByAcId(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = FlightModel::SelectArrTimeByAcIdJson($id, $conn, $mn, $logModel);

            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Flight History Methods">

    public static function FlightHistoryTable($params) {
        $mn = "FlightModel::FlightHistoryTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT fl.fl_id as flId, fl.fl_type_id as flTypeId,
                    fl.fl_name as flName, fl.fl_description as flDescription, 
                    fl.fle_id as fleId, fl.flp_id as flpId, fl.route_id as routeId, fl.ch_id as chId,
                    fl.al_id as alId, fl.ac_id as acId, fl.d_ap_id as depApId, fl.a_ap_id as arrApId,
                    fl.distance_km as distanceKm, fl.oil_l as fuelL, fl.pax, fl.payload_kg as payloadKg,
                    fl.dtime as etdTime, fl.atime as etaTime, fl.dtime_real as dtime, fl.atime_real as atime,
                    fl.delay_min as delayMin, ifnull(fl.expences, 0) as expences, ifnull(fl.income, 0) as income, ifnull(fl.revenue, 0) as revenue,
                    al.al_name as alName, al.iata as alIata,
                    ifnull(dep.ap_iata, dep.ap_icao) as  depApCode, ifnull(arr.ap_iata, arr.ap_icao) as arrApCode
                    FROM ams_ac.ams_al_flight_h fl
                    join ams_al.ams_airline al on al.al_id = fl.al_id 
                    join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id 
                    join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id ";

            $sqlWhere = "";
            if (isset($params->alId) && strlen($params->alId) > 0) {
                $sqlWhere = " WHERE fl.al_id = " . $params->alId . " ";
            }

            if (isset($params->acId) && strlen($params->acId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and fl.ac_id = " . $params->acId . " ";
                } else {
                    $sqlWhere = " WHERE fl.ac_id = " . $params->acId . " ";
                }
            }

            if (isset($params->dApId) && strlen($params->dApId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and fl.d_ap_id = " . $params->dApId . " ";
                } else {
                    $sqlWhere = " WHERE fl.d_ap_id = " . $params->dApId . " ";
                }
            }

            if (isset($params->aApId) && strlen($params->aApId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and fl.a_ap_id = " . $params->aApId . " ";
                } else {
                    $sqlWhere = " WHERE fl.a_ap_id = " . $params->aApId . " ";
                }
            }

            if (isset($params->qry_filter) && strlen($params->qry_filter) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND ( ifnull(dep.ap_iata, dep.ap_icao) like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or ifnull(arr.ap_iata, arr.ap_icao) like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or fl.fl_description like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or al.al_name like '%" . $params->qry_filter . "%' )";
                } else {
                    $sqlWhere .= " WHERE ( ifnull(dep.ap_iata, dep.ap_icao) like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or ifnull(arr.ap_iata, arr.ap_icao) like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or fl.fl_description like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or al.al_name like '%" . $params->qry_filter . "%' )";
                }
            }
            $sqlOrder = " order by fl.dtime_real desc  ";
            /*
            if (isset($params->qry_orderCol)) {
                $sqlOrder .= " order by " . $params->qry_orderCol . " " . ($params->qry_isDesc ? "desc" : " asc");
            } else {
                $sqlOrder .= " order by fl.dtime desc  ";
            }
             */
             
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder) > 1 ? $sqlOrder : "");
            $sql .= " LIMIT ? OFFSET ? ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("flights", $ret_json_data);

            $sql = "SELECT flpl.flpl_id as flplId,
                        flpl.fl_id as flId,
                        flpl.ch_id as chId,
                        flpl.payload_type as payloadType,
                        flpl.pax,
                        flpl.cargo_kg as cargoKg,
                        flpl.payload_kg as payloadKg,
                        flpl.description, flpl.price, flpl.adate
                    FROM ams_ac.ams_al_flight_h_payload flpl 
                    join (
                        SELECT fl.fl_id
                        FROM ams_ac.ams_al_flight_h fl
                        join ams_al.ams_airline al on al.al_id = fl.al_id 
                        join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id 
                        join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id ";
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder)>1?$sqlOrder:"");
            $sql .= " LIMIT ? OFFSET ? ) fl on fl.fl_id = flpl.fl_id ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_payload = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("payloads", $ret_json_payload);

            $sql = "select 
                    flh.id as orderId,
                    flh.fl_id as flId,
                    flh.acfs_id as acfsId,
                    flh.fl_log_description as flLogDescription,
                    flh.fl_time_min as flLogTimeMin,
                    flh.fl_distance_km as flLogDistanceKm,
                    flh.op_time_sec as flLogOpTimeSec,
                    flh.delay_min as delayMin,
                    flh.tr_id as trId,
                    flh.adate,
                    t.tr_type_id as trTypeId,
                    if(b.bank_id = t.bank_id_from, -1, 1) * t.amount as amount,
                    t.description, t.ap_id as trApId, t.posted
                    from ams_ac.ams_al_flight_log_h flh
                    join ams_al.ams_bank_transaction t on t.tr_id = flh.tr_id
                    join (SELECT fl.fl_id, fl.al_id 
                    FROM ams_ac.ams_al_flight_h fl
                    join ams_al.ams_airline al on al.al_id = fl.al_id 
                    join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id 
                    join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id ";
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder)>1?$sqlOrder:"");
            $sql .= " LIMIT ? OFFSET ? ) fl on fl.fl_id = flh.fl_id
                    join ams_al.ams_bank b on b.al_id = fl.al_id  ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_payload = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("flightLogs", $ret_json_payload);

            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_al_flight_h fl
                    join ams_al.ams_airline al on al.al_id = fl.al_id 
                    join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id 
                    join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id  " . (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Flight Transfer Methods">

    public static function FlightEstimateTransfer($acId, $apId) {
        $mn = "FlightModel::FlightEstimateTransfer(" . $acId . ", " . $apId . ")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        $obj = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);

            $sql = "call ams_ac.sp_al_fl_transfer_estimate(?,?)";
            $bound_params_r = ["ii", $acId, $apId];
            $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            //AmsAlLogger::log($mn, "count(result): ".count($result_r));
            if (count($result_r) > 0) {
                $fleId = $result_r[0]["fle_id"];
            }
            //AmsAlLogger::log($mn, "fleId: ".$fleId);
            $objArrJ = FlightModel::FlightEstimateJson($fleId, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $obj = json_decode(json_encode($objArrJ[0]));
            }

            $response->addData("flight", $obj);
            //$response->addData("affectedRows", $affected_rows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function FlightsEstimateTransferCreate($fleId) {
        $mn = "FlightModel::FlightsEstimateTransferCreate(" . $fleId . ")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        $obj = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);

            $sql = "call ams_ac.sp_al_fl_transfer_create(?)";
            $bound_params_r = ["i", $fleId];
            $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            if (count($result_r) > 0) {
                $flId = $result_r[0]["fl_id"];
            }
            AmsAlLogger::log($mn, "flId: " . $flId);
            $response->addData("flId", $flId);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Flight Charter Methods">

    public static function FlightsEstimateCharter($acId, $apId, $dtime) {
        $mn = "FlightModel::FlightEstimateCharter(" . $acId . ", " . $apId . ", " . $dtime . ")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        $obj = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);

            if (!isset($dtime)) {
                $dtime = StrToDateObj(CurrenDateTime());
            }
            
            $sql = "call ams_ac.sp_al_fl_charter_estimate_time(?, ?, ?, @o_fl_num_nex)";
            $bound_params_r = ["iis", $acId, $apId, DateTimeDbStr($dtime)];
            $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);

            $flTypeId = 2; //Charter
            $objArrJ = FlightModel::FlightsEstimateCharterJsonByAc($acId, $flTypeId, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $obj = json_decode(json_encode($objArrJ));
            }
            $response->addData("flights", $obj);
            if (isset($obj)) {
                $objArrJB = FlightModel::FlightsEstimateCharterPayloadJsonByAc($acId, $flTypeId, $conn, $mn, $logModel);
                if (isset($objArrJB) && count($objArrJB) > 0) {
                    $objB = json_decode(json_encode($objArrJB));
                }
                $response->addData("payloads", $objB);
            }
            //$response->addData("affectedRows", $affected_rows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function FlightsEstimateCharterCreate($fleId) {
        $mn = "FlightModel::FlightsEstimateCharterCreate(" . $fleId . ")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        $obj = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);

            $sql = "call ams_ac.sp_al_fl_charter_create(?)";
            $bound_params_r = ["i", $fleId];
            $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            if (count($result_r) > 0) {
                $flId = $result_r[0]["fl_id"];
            }
            AmsAlLogger::log($mn, "flId: " . $flId);
            $response->addData("flId", $flId);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">

    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = " SELECT fl.fl_id as flId, fl.fl_type_id as flTypeId,
            fl.fl_name as flName, fl.fl_description as flDescription, 
            fl.fle_id as fleId, fl.flp_id as flpId, fl.route_id as routeId, fl.ch_id as chId,
            fl.al_id as alId, fl.ac_id as acId, ac.registration as acRegNr, fl.d_ap_id as depApId, fl.a_ap_id as arrApId,
            fl.distance_km as distanceKm, fl.oil_l as fuelL, fl.pax, fl.payload_kg as payloadKg,
            fl.dtime as etdTime, fl.atime as etaTime, fl.dtime_real as dtime, fl.atime_real as atime,
            fl.delay_min as delayMin, fl.expences, fl.income, fl.revenue,
            l.fl_log_id as flLogId, l.acfs_id as acfsId, 
            l.fl_log_description as flLogDescription, l.fl_time_min as flTimeMin,
            l.fl_distance_km as flDistanceKm, l.op_time_sec as flLogOpTimeSec, fl.delay_min as flLogDelayMin, l.adate, l.udate,
            ac.mac_id as macId, mac.ac_type_id as acTypeId, mac.cruise_speed_kmph as speedKmPh,
            round((fl.distance_km - l.fl_distance_km),0) remainDistanceKm, 
            round(((fl.distance_km - l.fl_distance_km)/mac.cruise_speed_kmph)*if(l.acfs_id>6,0,60),2) as remainTimeMin,
            al.al_name as alName, al.iata as alIata
            FROM ams_ac.ams_al_flight fl
            join ams_ac.ams_al_flight_log l on fl.fl_id = l.fl_id
            join ams_ac.ams_aircraft ac on ac.ac_id = fl.ac_id
            join ams_ac.cfg_mac mac on mac.mac_id = ac.mac_id
            join ams_al.ams_airline al on al.al_id = fl.al_id 
            where fl.fl_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }
    
    static function SelectPayloadJson($id, $conn, $mn, $logModel) {

        $sql = " SELECT flpl.flpl_id as flplId,
                        flpl.fl_id as flId,
                        flpl.ch_id as chId,
                        flpl.payload_type as payloadType,
                        flpl.pax,
                        flpl.cargo_kg as cargoKg,
                        flpl.payload_kg as payloadKg,
                        flpl.description, flpl.price, flpl.adate
                    FROM ams_ac.ams_al_flight_payload flpl
                    where flpl.fl_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }
    
    static function FlightQueueSelectJson($id, $conn, $mn, $logModel) {

        $sql = " SELECT fl.fl_id as flId, fl.fl_type_id as flTypeId,
                    fl.fl_name as flName, fl.fl_description as flDescription, 
                    fl.fle_id as fleId, fl.flp_id as flpId, fl.route_id as routeId, fl.ch_id as chId,
                    fl.al_id as alId, fl.ac_id as acId, ac.registration as acRegNr, fl.d_ap_id as depApId, fl.a_ap_id as arrApId,
                    fl.distance_km as distanceKm, fl.oil_l as fuelL, fl.pax, fl.payload_kg as payloadKg,
                    fl.dtime as etdTime, fl.atime as etaTime, 
                    ac.mac_id as macId, mac.cruise_speed_kmph as speedKmPh,
                    round(fl.distance_km,0) remainDistanceKm, 
                    round((fl.distance_km /mac.cruise_speed_kmph)*60,2) as remainTimeMin,
                    al.al_name as alName, al.iata as alIata,
                    ifnull(dep.ap_iata, dep.ap_icao) as  depApCode, ifnull(arr.ap_iata, arr.ap_icao) as arrApCode, fl.processed
                    FROM ams_ac.ams_al_flight_queue fl
                    join ams_ac.ams_aircraft ac on ac.ac_id = fl.ac_id
                    join ams_ac.cfg_mac mac on mac.mac_id = ac.mac_id
                    join ams_al.ams_airline al on al.al_id = fl.al_id 
                    join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id 
                    join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id 
                    where fl.fl_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }
    
    static function FlightQueueSelectPayloadJson($id, $conn, $mn, $logModel) {

        $sql = " SELECT flpl.flpl_id as flplId,
                        flpl.fl_id as flId,
                        flpl.ch_id as chId,
                        flpl.payload_type as payloadType,
                        flpl.pax,
                        flpl.cargo_kg as cargoKg,
                        flpl.payload_kg as payloadKg,
                        flpl.description, flpl.price, flpl.adate
                    FROM ams_ac.ams_al_flight_queue_payload flpl 
                    where flpl.fl_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function FlightEstimateJson($id, $conn, $mn, $logModel) {

        $sql = " SELECT fle.fle_id as fleId, fle.flp_id as flpId, fle.route_id as routeId, fle.fl_type_id as flTypeId,
                fle.fl_num as flNum, fle.fl_name as flName, fle.fl_description as flDescription,
                fle.al_id as alId, fle.ac_id as acId, fle.d_ap_id as depApId, fle.a_ap_id as arrApId,
                fle.distance_km as distanceKm, fle.duration_min as durationMin,
                fle.oil_l as oilL, fle.oli_price as oilPrice, fle.landin_fee as landingFee, fle.ground_crew_fee as groundCrewFee, fle.boarding_fee as boardingFee,
                fle.fl_price flPrice, fle.bank_id_from, fle.payload_kg as payloadKg, fle.mtow,
                fle.dtime, fle.atime, fle.expences, fle.income, fle.revenue, fle.adate,
                 round((ch.payload_kg_completed/ch.payload_kg)*100,2) as chCompletedPct, 
                (ch.pax - ch.pax_completed) as chPaxLeft,
                (ch.cargo_kg - ch.cargo_kg_compleated) as chCargoLeft
                 FROM ams_ac.ams_al_flight_estimate fle
                  left join ams_ac.ams_charter ch  on ch.ch_id = fle.ch_id
                 where fle.fle_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function FlightsEstimateCharterJsonByAc($id, $flTypeId, $conn, $mn, $logModel) {

        $sql = " SELECT fle.fle_id as fleId, fle.flp_id as flpId, fle.route_id as routeId, 
                fle.ch_id as chId, fle.fl_type_id as flTypeId,
                fle.fl_num as flNum, fle.fl_name as flName, fle.fl_description as flDescription,
                fle.al_id as alId, fle.ac_id as acId, fle.d_ap_id as depApId, fle.a_ap_id as arrApId,
                fle.distance_km as distanceKm, fle.duration_min as durationMin,
                fle.oil_l as oilL, fle.oli_price as oilPrice, fle.landin_fee as landingFee, 
                fle.ground_crew_fee as groundCrewFee, fle.boarding_fee as boardingFee,
                fle.fl_price flPrice, fle.bank_id_from as bankIdFrom, fle.pax, fle.payload_kg as payloadKg, fle.mtow,
                fle.dtime, fle.atime, fle.expences, fle.income, fle.revenue, fle.adate,
                 round((ch.payload_kg_completed/ch.payload_kg)*100,2) as chCompletedPct, 
                (ch.pax - ch.pax_completed) as chPaxLeft,
                (ch.cargo_kg - ch.cargo_kg_compleated) as chCargoLeft
                 FROM ams_ac.ams_al_flight_estimate fle
                 left join ams_ac.ams_charter ch  on ch.ch_id = fle.ch_id
                 where fle.ac_id = ? and fle.fl_type_id = ? ";

        $bound_params_r = ["ii", $id, $flTypeId];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function FlightsEstimateCharterPayloadJsonByAc($id, $flTypeId, $conn, $mn, $logModel) {

        $sql = " SELECT flep.flep_id as flepId,
                    flep.fle_id as fleId,
                    flep.ch_id as chId,
                    flep.payload_type as payloadType,
                    flep.pax,
                    flep.cargo_kg as cargoKg,
                    flep.payload_kg as payloadKg,
                    flep.description, flep.price, flep.adate
                 FROM ams_ac.ams_al_flight_estimate_payload flep
                 join ams_ac.ams_al_flight_estimate fle on fle.fle_id = flep.fle_id
                 where fle.ac_id = ? and fle.fl_type_id = ? ";

        $bound_params_r = ["ii", $id, $flTypeId];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function SelectArrTimeByAcIdJson($id, $conn, $mn, $logModel) {

        $sql = " SELECT ac.ac_id as acId, ac.ac_status_id acStatusId,
                    count(flq.fl_id) flqCount,count(fl.fl_id) flCount,
                    if( flq.fl_id is not null, flq.atime, 
                            if(fl.fl_id is not null, fl.atime, if(acmp.fl_id is not null, acmp.atime, now()) ) ) as arrTime,
                    if( flq.fl_id is not null, flq.a_ap_id, if(fl.fl_id is not null, fl.a_ap_id, if(acmp.fl_id is not null, acmp.a_ap_id, acc.curr_ap_id)) ) as apId,
                     acc.op_time_min as opTimeMin
                    FROM ams_ac.ams_aircraft ac
                    join ams_ac.ams_aircraft_curr acc on acc.ac_id = ac.ac_id
                    left join (
                                    select a_ap_id, ac_id, atime, fl_id from ams_ac.ams_al_flight_queue 
                                    where fl_id = (select max(fl_id) 
                                    from  ams_ac.ams_al_flight_queue where ac_id=? and processed = 0) ) flq on flq.ac_id = ac.ac_id 
                    left join (
                                    select a_ap_id, ac_id, atime, fl_id from ams_ac.ams_al_flight 
                                    where fl_id = (select max(fl_id) 
                            from  ams_ac.ams_al_flight where ac_id=?) ) fl on fl.ac_id = ac.ac_id 
                    left join (
                                    select ap_id as a_ap_id, ac_id, end_time as atime, acmp_id as fl_id from ams_ac.ams_aircraft_maintenance_plan 
                                    where acmp_id = (select max(acmp_id) 
                            from  ams_ac.ams_aircraft_maintenance_plan where ac_id=? ) ) acmp on acmp.ac_id = ac.ac_id 
                    where ac.ac_id = ?";

        $bound_params_r = ["iiii", $id, $id, $id, $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    // </editor-fold>
}
