<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Dec 22, 2020 
 *  Filename            : AirlineModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of UserModel
 *
 * @author IZIordanov
 */
class AirlineModel {
    
   // <editor-fold defaultstate="collapsed" desc="Fields">

    public $alId;
    public $alName;
    public $iata;
    public $logo;
    public $livery;
    public $slogan;
    
    public $userId;
    public $apId;
    public $adate;
    public $udate;
    
    public $countryId;
    public $stateId;
    public $subregionId;
    public $regionId;
    public $apLocation;
    public $iso2;
    public $apLabel;
    public $bankId;
    public $amount;
    public $bankUdate;
    
          
    
    public function toJSON() {
        return json_encode($this);
    }
    
    public static function fromJSON($dataJson) {
        $rv = new AirlineModel();
        $rv->alId = (!isset($dataJson->alId)) ? null : $dataJson->alId;
        $rv->alName = (!isset($dataJson->alName)) ? null : $dataJson->alName;
        $rv->iata = (!isset($dataJson->iata)) ? null : $dataJson->iata;
        $rv->logo = (!isset($dataJson->logo)) ? null : $dataJson->logo;
        $rv->livery = (!isset($dataJson->livery)) ? null : $dataJson->livery;
        $rv->slogan = (!isset($dataJson->slogan)) ? null : $dataJson->slogan;
        $rv->userId = (!isset($dataJson->userId)) ? null : $dataJson->apId;
        $rv->apId = (!isset($dataJson->userId)) ? null : $dataJson->userId;
        $rv->adate = (!isset($dataJson->adate)) ? null : $dataJson->adate;
        $rv->udate = (!isset($dataJson->udate)) ? null : $dataJson->udate;
        $rv->countryId = (!isset($dataJson->alId)) ? null : $dataJson->countryId;
        $rv->stateId = (!isset($dataJson->stateId)) ? null : $dataJson->stateId;
        $rv->subregionId = (!isset($dataJson->subregionId)) ? null : $dataJson->subregionId;
        $rv->apLocation = (!isset($dataJson->apLocation)) ? null : $dataJson->apLocation;
        $rv->iso2 = (!isset($dataJson->iso2)) ? null : $dataJson->iso2;
        $rv->apLabel = (!isset($dataJson->apLabel)) ? null : $dataJson->apLabel;
        return $rv;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($id) {
        $mn = "AirlineModel::LoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AirlineModel::SelectJson($id, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function LoadByUserId($id) {
        $mn = "AirlineModel::LoadByUserId(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AirlineModel::SelectByUserJson($id, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "AirlineModel::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        AmsAlLogger::log($mn, " alName = " . $dataJson->alName);
        $response;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $alId = null;
            if(isset($dataJson->alId)){
               AmsAlLogger::log($mn, "Update  alId =" . $dataJson->alId);
               $alId = $dataJson->alId;
               AirlineModel::Update($dataJson, $conn, $mn, $logModel);

            } else{
                AmsAlLogger::log($mn, "Create airline");
                $alId = AirlineModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            AmsAlLogger::log($mn, " alId =" . $alId);
           if(isset($alId)){
                $objArrJ = AirlineModel::SelectJson($alId, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function AirlineTable($params) {
        $mn = "AirlineModel::AirlineTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT al.al_id as alId,
                        al.al_name as alName,
                        al.iata, al.logo, al.slogan, 
                        al.user_id as userId,
                        al.ap_id as apId,
                        al.adate, al.udate, 
                        ap.countryId,
                        ap.stateId, 
                        ap.subregionId,
                        ap.regionId,
                        ap.lat_lon as apLocation, ap.iso2, ap.apLabel,
                        b.bank_id as bankId, b.amount, b.udate as bankUdate
                        FROM ams_al.ams_airline al
                        left join ams_wad.v_solr_airport ap on ap.apId = al.ap_id
                        left join ams_al.ams_bank b on b.al_id = al.al_id
                     ";
            
            
            $sqlWhere="";
            if(isset($params->apId) && strlen($params->apId)>0){
                $sqlWhere = " WHERE ap.ap_id = ".$params->apId." ";
            } else if(isset($params->countryId) && strlen($params->countryId)>0){
                $sqlWhere = " WHERE ap.countryId = ".$params->countryId." ";
            } else  if(isset($params->stateId) && strlen($params->stateId)>0){
                $sqlWhere = " WHERE ap.stateId = ".$params->stateId." ";
            } else  if(isset($params->subregionId) && strlen($params->subregionId)>0){
                $sqlWhere = " WHERE ap.subregionId = ".$params->subregionId." ";
            } else  if(isset($params->regionId) && strlen($params->regionId)>0){
                $sqlWhere = " WHERE ap.subregionId = ".$params->regionId." ";
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (al.al_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or al.iata like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere = " WHERE (al.al_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or al.iata like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = "";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by al.".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by ap.al_name, al.iata ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("airlines", $ret_json_data);
            
            $sql = "SELECT count(*) as total_rows
                    FROM ams_al.ams_airline al
                    left join ams_wad.v_solr_airport ap on ap.apId = al.ap_id ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    
     // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
     
     static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_al.ams_airline 
            (al_name, iata, logo, livery,
            slogan, user_id, ap_id)
            VALUES(?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["sssssii",
             ((!isset($dataJson->alName)) ? null : $dataJson->alName),
            ((!isset($dataJson->iata)) ? null : $dataJson->iata),
            ((!isset($dataJson->logo)) ? null : $dataJson->logo),
            ((!isset($dataJson->livery)) ? null : $dataJson->livery),
            ((!isset($dataJson->slogan)) ? null : $dataJson->slogan),
            ((!isset($dataJson->userId)) ? null : $dataJson->userId),
            ((!isset($dataJson->apId)) ? null : $dataJson->apId),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_al.ams_airline 
                    SET al_name=?, 
                    iata=?, 
                    logo=?,
                    livery=?
                    slogan=?, 
                    user_id=?, 
                    ap_id=?
                    WHERE al_id = ?  " ;

        $bound_params_r = ["sssssiii",
            ((!isset($dataJson->alName)) ? null : $dataJson->alName),
            ((!isset($dataJson->iata)) ? null : $dataJson->iata),
            ((!isset($dataJson->logo)) ? null : $dataJson->logo),
            ((!isset($dataJson->livery)) ? null : $dataJson->livery),
            ((!isset($dataJson->slogan)) ? null : $dataJson->slogan),
            ((!isset($dataJson->userId)) ? null : $dataJson->userId),
            ((!isset($dataJson->apId)) ? null : $dataJson->apId),
            ($dataJson->alId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->userId;
    }
    
    static function SelectJson($id, $conn, $mn, $logModel){
        
        $sql = "SELECT al.al_id as alId,
                        al.al_name as alName,
                        al.iata, al.logo, al.livery, al.slogan, 
                        al.user_id as userId,
                        al.ap_id as apId,
                        al.adate, al.udate, 
                        ap.countryId,
                        ap.stateId, 
                        ap.subregionId,
                        ap.regionId,
                        ap.lat_lon as apLocation, 
                        ap.iso2,
                        ap.apLabel,
                        b.bank_id as bankId, b.amount, b.udate as bankUdate,
                        h.hubsCount
                        FROM ams_al.ams_airline al
                        left join ams_wad.v_solr_airport ap on ap.apId = al.ap_id
                        left join ams_al.ams_bank b on b.al_id = al.al_id
                        left join (select count(*) as hubsCount, al_id from ams_al.ams_al_hub group by al_id) h on h.al_id = al.al_id
                    WHERE al.al_id = ? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function SelectByUserJson($userId, $conn, $mn, $logModel){
        
        $sql = "SELECT al.al_id as alId,
                        al.al_name as alName,
                        al.iata, al.logo,  al.livery, al.slogan, 
                        al.user_id as userId,
                        al.ap_id as apId,
                        al.adate, al.udate, 
                        ap.countryId,
                        ap.stateId, 
                        ap.subregionId,
                        ap.regionId,
                        ap.lat_lon as apLocation, 
                        ap.iso2, ap.apLabel,
                        b.bank_id as bankId, b.amount, b.udate as bankUdate,
                        h.hubsCount
                        FROM ams_al.ams_airline al
                        left join ams_wad.v_solr_airport ap on ap.apId = al.ap_id
                        left join ams_al.ams_bank b on b.al_id = al.al_id
                        left join (select count(*) as hubsCount, al_id from ams_al.ams_al_hub group by al_id) h on h.al_id = al.al_id
                    WHERE al.user_id = ?" ;

        $bound_params_r = ["i",$userId];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function SelectByAirportJson($apId, $conn, $mn, $logModel){
        
        $sql = "SELECT al.al_id as alId,
                        al.al_name as alName,
                        al.iata, al.logo, al.livery, al.slogan, 
                        al.user_id as userId,
                        al.ap_id as apId,
                        al.adate, al.udate, 
                        ap.countryId,
                        ap.stateId, 
                        ap.subregionId,
                        ap.regionId,
                        ap.lat_lon as apLocation, 
                        ap.iso2, ap.apLabel,
                        b.bank_id as bankId, b.amount, b.udate as bankUdate
                        FROM ams_al.ams_airline al
                        left join ams_wad.v_solr_airport ap on ap.apId = al.ap_id
                        left join ams_al.ams_bank b on b.al_id = al.al_id
                    WHERE al.apId = ?" ;

        $bound_params_r = ["i",$apId, $pwd];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function CheckNameJson($alName, $conn, $mn, $logModel){
        
        $sql = "SELECT count(*) AS rows, al.al_name as alName
                    FROM ams_al.ams_airline al
                    WHERE al.al_name like ? 
                    group by al.al_name " ;

        $bound_params_r = ["s","%".$alName."%"];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function CheckIataJson($iata, $conn, $mn, $logModel){
        
        $sql = "SELECT count(*) AS rows, al.iata
                    FROM ams_al.ams_airline al
                    WHERE al.iata = ? 
                    group by al.iata " ;

        $bound_params_r = ["s",$iata];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM ams_al.ams_airline
                   WHERE al_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted alId =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Logo and Livery">
    
    public function getLogoPath() {
        $mn = "AirlineModel->getLogoPath()";
        AmsAlLogger::logBegin($mn);

       
        $retValue = "/images/airline/".$this->getLogo();
        //$docRoot = $_SERVER['DOCUMENT_ROOT']."/images/";
        $docRoot = COMMON_ROOT;
        //AmsAlLogger::log($mn, " docRoot=" . $docRoot);
        //AmsAlLogger::log($mn, "Create logo=".$this->alId);  
        $this->createLogo("/images/airline/logo_0.png");
        /*
        if (!file_exists($docRoot.$retValue)) 
        {
            AmsAlLogger::log($mn, "Create logo=".$this->alId);  
            $this->createLogo("/images/airline/logo_0.png");
        }*/
        AmsAlLogger::logEnd($mn);
        return $retValue;
    }
    
    public function getLogo() {
        $retValue = "logo_".$this->alId.".png";
        return $retValue;
    }
    
    public function createLogo($src) {
        $mn = "ams:AirlineModel::createLogo()";
        AmsAlLogger::logBegin($mn);
        //AmsAlLogger::log($mn, "src = ".$src);
        $height = 300;
        $width = 300;
        $fontSize = 126;
        $ctFontSize= 16;
        $iata = "AA";
        //$docRoot = $_SERVER['DOCUMENT_ROOT']."/images/";
        $docRoot = COMMON_ROOT;
        //logDebug("createLogo", "docRoot=".$docRoot);  
        $src = $docRoot.$src;
        if (!file_exists($src)) 
        {
            AmsAlLogger::log($mn,"Can not find file: ".$src.": exist=".file_exists($src)?"true":"false"); 
            return;
        }
        
        $country = strtolower($this->iso2);
        $aname = $this->alName;
        //if(strlen($aname)>10)
        //    $aname = substr($aname, 0, 10);
        $slogan = $this->slogan;
        if(!isset($slogan) || strlen($slogan)===0)
                $slogan=$aname;
        if(strlen($slogan)>36){
            $slogan = substr($slogan, 0, 36);
        }
        
        if(strlen($slogan)<36){
            $maxIdx = (36-strlen($slogan))/2;
            for($idx=0; $idx<$maxIdx ; $idx++)
            {
                $slogan = ($idx===0?"":" ") .$slogan . " ";
            }
        }
        //AmsAlLogger::log($mn,"country: ".$country.", aname: ".$aname.", slogan=".$slogan." "); 
        $iata = strtoupper($this->iata);
        if (strlen($iata) > 2){
             $iata = substr($iata, 0, 2);
        }
        $flagSrc = $docRoot."/images/flags/" . $country . ".png";
        //AmsAlLogger::log($mn,"flagSrc: ".$flagSrc.", iata: ".$iata.", slogan=".$slogan." "); 
        $imageinfoB = getimagesize($src);
        $imageinfoF = getimagesize($flagSrc);
        
        $flagw = $imageinfoF[0];
        $flagh = $imageinfoF[1];
        $logow = $imageinfoB[0];
        $logoh = $imageinfoB[1];
        
        //AmsAlLogger::log($mn, "flagw=".$flagw);  
        //AmsAlLogger::log($mn, "logow=".$logow);  
        
        $hK = $height / $logoh;
        $fontSize = $fontSize * $hK;
        
        //define iata text size
        $font = $docRoot."/fonts/Champagne & Limousines Bold.ttf"; // "../fonts/Quincho.ttf";:
        $iataSize = imagettfbbox($fontSize, 0, $font, $iata);
        
        $fhK = $fontSize / $flagh;
        
        $imgh = $height-($ctFontSize*2);
        $imgw = $width-($ctFontSize*2);
        
        $logoK = $imgh/$logoh;
        $lh = $imgh*$logoK;
        $lw = $imgw*$logoK;
        $lx = round($ctFontSize);
        $ly = round($ctFontSize);
        
        //$width = $logow * $hK;
        $fw = ($imgw/2)-$fontSize;
        $fh = $fontSize/1.4;
        $fx = ($width/2) -($fw/2);//$width / 3.5;
        $fy = ($imgh-(1.6*$fh));//$height-($height / 4.5);
        $iatax =  $width / 2 - (($iataSize[4] - $iataSize[0] )/2);
        $iatay = ($height / 4)+($ctFontSize*2);
        //--- Create Image
        $rImg = imagecreatetruecolor($width, $height);

        //--- Load Images Logo and Country Flag 
        $srcimage = imagecreatefrompng($src);
        $srcimageFlag = imagecreatefrompng($flagSrc);
        $corIata = imagecolorallocate($rImg, 217, 19, 59);
        //--- paste logo full size and transparent + flag
        imagealphablending($rImg, false);
        imagesavealpha($rImg, true);
        $transparent = imagecolorallocatealpha($rImg, 255, 255, 255, 127);
        imagefilledrectangle($rImg, 0, 0, $width, $height, $transparent);
        imagecopyresampled($rImg, $srcimage, $lx, $ly, 0, 0, $imgw, $imgh, $logow, $logoh);
        
        imagecopyresampled($rImg,$srcimageFlag,$fx, $fy,0,0, $fw,$fh,$flagw,$flagh);
        //--- IATA CODE
        imagettftext($rImg, $fontSize, 0,$iatax, $iatay, $corIata, $font, $iata);
        
         //---- Drow text in cercle
        $degrees = (360/strlen($slogan));
        
        $colorSlogan = imagecolorallocate($rImg, 148, 148, 184);
        $fontSlogan = $docRoot."/fonts/RobotoCondensed-Bold.ttf";
        //--- Center
        $cx = $width/2;
        $cy = ($height/2);
        //--- radius
        $r = (($width/2)-($ctFontSize/2));// + $logoh/4);//-($ctFontSize/4);
        for($idx=0;$idx<strlen($slogan);$idx++) 
        {
            $leterIdx = strlen($slogan)-1-$idx;
            $angle = ($degrees*$idx)-90;
            $cos = cos(deg2rad($angle));
            $sin = sin(deg2rad($angle));
            
            //---
            $px = round($cx + ($r*$cos)); //round($cos*($x) - $sin*($y));
            $py = round($cy + ($r*$sin));//round($sin*($x) + $cos*($y));

          imagettftext($rImg,$ctFontSize,90-$angle,$px,$py,$colorSlogan,$fontSlogan,$slogan[$leterIdx]);
        }
        //--- Save the image to file
        imagepng($rImg, $docRoot."/images/airline/logo_".$this->alId.".png");
        // Destroy image 
        imagedestroy($rImg); 
    }
    
    public function LiveryPath() {
        $retValue = "/images/airline/".$this->getLivery();
        //$docRoot = $_SERVER['DOCUMENT_ROOT']."/images/";
        $docRoot = COMMON_ROOT;
        $this->createLivery();
        /*
        if (!file_exists($docRoot.$retValue)) 
        { 
            
        }*/
        return $retValue;
    }
    
    public function getLivery() {
        $retValue = "livery_".$this->alId.".png";
        return $retValue;
    }
    
    public function createLivery() {
        $mn = "ams:AirlineModel::createLivery()";
        AmsAlLogger::logBegin($mn);
        
        $height = 25;//75;
        $width=108;//326;
        $fontSize=10;//20;
        $iata = "AA";
        //$docRoot = $_SERVER['DOCUMENT_ROOT']."/images/";
        $docRoot = COMMON_ROOT;
        //logDebug("createLivery", "docRoot=".$docRoot);  
        $src = $docRoot."/images/airline/logo_".$this->alId.".png";
        AmsAlLogger::log($mn, " src = ".$src);
        if (!file_exists($src)) 
        {
            AmsAlLogger::log($mn, "Not found file".$src.": exist=".file_exists($src)?"true":"false"); 
            return;
        }
        
        $aname = $this->alName;
        if(strlen($aname)>10)
            $aname = substr($aname, 0, 10);
        $slogan = $this->slogan;
        if(strlen($slogan)>36){
            $slogan = substr($slogan, 0, 36);
        }

        if(strlen($slogan)<36){
            $maxIdx = (36-strlen($slogan))/2;
            for($idx=0; $idx<$maxIdx ; $idx++)
            {
                $slogan = " " .$slogan . " ";
            }
        }
        $country = strtolower($this->iso2);
        AmsAlLogger::log($mn,  " country=".$country);  
        $iata = strtoupper($this->iata);
        if (strlen($iata) > 2)
            $iata = substr($iata, 0, 2);
        
        $flagSrc = $docRoot."/images/flags/" . $country . ".png";
        
        $imageinfoB = getimagesize($src);
        $imageinfoF = getimagesize($flagSrc);
        
        $flagw = $imageinfoF[0];
        $flagh = $imageinfoF[1];
        $logow = $imageinfoB[0];
        $logoh = $imageinfoB[1];
        
        //logDebug("createLivery", "logow=".$logow);  
        //logo resize facktor
        $logoRes = $height / $logoh;
        //$fontSize = $fontSize * $hK;
        //define iata text size
        $font = $docRoot."/fonts/Champagne & Limousines Bold.ttf"; // "../fonts/Quincho.ttf";:
        $iataSize = imagettfbbox($fontSize, 0, $font, $iata);
        //$fhK = $fontSize / $flagh;
        $lw = $logow * $logoRes;
        $lh = $logoh * $logoRes;
        //--- Flag bottom horizontal
        $fw = $width/2.3;
        $fh = $fontSize/2;
        $fx = $logow / 3.5;
        $fy = $height- $fh-2;//($height / 4.5);
        $iatax =  $width / 2 - (($iataSize[4] - $iataSize[0] )/2);
        $iatay = ($height / 4);
        $flW=$width-($logow/2)-1;//67*$hK;
        $flH=$fh;
        
       
        //---- define text size
        $font = $docRoot."/fonts/Champagne & Limousines Bold.ttf"; 
        $fontSlogan = $docRoot."/fonts/RobotoCondensed-Bold.ttf";
        
        //--- Create Image
        $rImg = imagecreatetruecolor($width, $height);

        //--- Load Images Logo and Country Flag 
        $srcimage = imagecreatefrompng($src);
        $srcimageFlag = imagecreatefrompng($flagSrc);
        $corIata = imagecolorallocate($rImg, 217, 19, 59);
        $corName =imagecolorallocate($rImg, 0, 77, 0);
        $corSlogan = imagecolorallocate($rImg, 148, 148, 184);
        //--- paste logo full size and Transparent + flag
        //imagealphablending($rImg, false);
        //imagesavealpha($rImg, true);
        //$transparent = imagecolorallocatealpha($rImg, 255, 255, 255, 127);
        //imagefilledrectangle($rImg, 0, 0, $width, $height, $transparent);
        
        //-- Image full size and background color
        $bg_color = $lightyelow;
        $white = imagecolorallocate($rImg, 255, 255, 255);
        $lightyelow = imagecolorallocate($rImg, 255, 255, 204);
        $lightblue = imagecolorallocate($rImg, 102, 176, 229);
        $lightgreen = imagecolorallocate($rImg, 0, 255, 0);
        $colorNum = rand(1,3);
        switch ($colorNum) {
            case 1:
                $bg_color=$lightyelow;
                break;
            case 2:
                $bg_color=$lightblue;
                break;
            case 1:
                $bg_color=$lightgreen;
                break;
            default:
                $bg_color=$lightyelow;
                break;
        }
        imagefilledrectangle($rImg, 0, 0, $width, $height, $bg_color);
        
        //-- Insert Logo left full
        imagecopyresampled($rImg, $srcimage, 0, 0, 0, 0, $lw, $lh, $logow, $logoh);
        
        //---- Insert Country Flag right vertical
        //--- Flag coordinate bottom horizontal
        $fw = ($lw / 2.5);
        $fh = $height-2;
        $fx = $width-($fw+2);
        $fy=1;
        imagecopyresampled($rImg,$srcimageFlag,$fx, $fy,0,0, $fw,$fh,$flagw,$flagh);
        
        //--- Slogan and Name
        imagettftext($rImg, $fontSize, 0, $lw,(7+$fontSize), $corName, $font, $aname);
        //imagettftext($rImg, $fontSize, 0, $lw + 20,(14+$fontSize), $corName, $font, $aname);
        //imagettftext($rImg, $fontSize/2, 0, $lw + 20,((20+$fontSize)+($fontSize)), $corSlogan, $fontSlogan, $slogan);
       
        //--- Save the image to file
        imagepng($rImg, $docRoot."/images/airline/livery_".$this->alId.".png");
        // Destroy image 
        imagedestroy($rImg); 
    }
    
    // </editor-fold>
}


