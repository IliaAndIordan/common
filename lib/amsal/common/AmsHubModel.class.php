<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Oct 26, 2023 
 *  Filename            : AmsHubModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2023 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of AmsHubModel
 *
 * @author IZIordanov
 */
class AmsHubModel   {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $hubId;
    public $apId;
    public $alId;
    public $upkeepDay;
    public $adate;
          
    
    public function toJSON() {
        return json_encode($this);
    }
    
    public static function fromJSON($dataJson) {
        $rv = new AmsHubModel();
        $rv->hubId = (!isset($dataJson->hubId)) ? null : $dataJson->hubId;
        $rv->apId = (!isset($dataJson->apId)) ? null : $dataJson->apId;
        $rv->alId = (!isset($dataJson->alId)) ? null : $dataJson->alId;
        $rv->upkeepDay = (!isset($dataJson->upkeepDay)) ? null : $dataJson->upkeepDay;
        $rv->adate = (!isset($dataJson->adate)) ? null : $dataJson->adate;
        return $rv;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($id) {
        $mn = "AmsHubModel::LoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsHubModel::SelectJson($id, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "AmsHubModel::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        AmsAlLogger::log($mn, " alId = " . $dataJson->alId);
        $response;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $hubId = null;
            if(isset($dataJson->hubId)){
               AmsAlLogger::log($mn, "Update  hubId =" . $dataJson->hubId);
               $hubId = $dataJson->hubId;
               AmsHubModel::Update($dataJson, $conn, $mn, $logModel);

            } else{
                AmsAlLogger::log($mn, "Create airline");
                $hubId = AmsHubModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            AmsAlLogger::log($mn, " hubId =" . $hubId);
           if(isset($hubId)){
                $objArrJ = AmsHubModel::SelectJson($hubId, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function AmsHubsTable($params) {
        $mn = "AmsHubModel::AmsHubsTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT h.hub_id as hubId, h.ap_id as apId, h.al_id as alId, h.upkeep_day as upkeepDay, h.adate,
                IF(al.ap_id = h.ap_id, 1, 2) as hubTypeId,
                ifnull(h.hub_name, concat(IF(al.ap_id = h.ap_id, 'Base ', 'Hub '), ap.ap_name)) as hubName,
                ROUND((tt.amount*ap.ap_type_id)*IF(al.ap_id = h.ap_id,  ppb.param_value, pph.param_value),2) as taxPerWeek,
                IF(al.ap_id = h.ap_id,  ppb.param_value, pph.param_value) as discount,
                ifnull(r.flpnCount,0) as flpnCount
                FROM ams_al.ams_al_hub h
                join ams_al.ams_airline al on al.al_id = h.al_id
                join ams_wad.cfg_airport ap on ap.ap_id = h.ap_id
                join (select param_value FROM ams_al.cfg_callc_param where param_name = 'fbo_fee_discount_base' ) ppb on 1=1
                join (select param_value FROM ams_al.cfg_callc_param where param_name = 'fbo_fee_discount_hub' ) pph on 1=1
                join ams_al.cfg_transaction_type tt on tt.tr_type_id = 16
                left join (select count(*) as flpnCount, al_id, dep_ap_id from ams_al.ams_al_flp_number group by al_id, dep_ap_id) r on r.al_id = h.al_id and r.dep_ap_id = h.ap_id
                     ";
            
            
            $sqlWhere="";
            if(isset($params->alId) && strlen($params->alId)>0){
                $sqlWhere = " WHERE h.al_id = ".$params->alId." ";
            } else if(isset($params->apId) && strlen($params->apId)>0){
                $sqlWhere = " WHERE h.ap_id = ".$params->apId." ";
            } 
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (al.al_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or al.iata like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere = " WHERE (al.al_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or al.iata like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = "";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by hubTypeId, adate ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("hubs", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_al.ams_al_hub h
                    join ams_al.ams_airline al on al.al_id = h.al_id
                    join ams_wad.cfg_airport ap on ap.ap_id = h.ap_id ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
   // <editor-fold defaultstate="collapsed" desc="DB Methods">
     
     static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_al.ams_al_hub (ap_id, al_id, hub_name) 
                SELECT ap.ap_id, al.al_id,  
                concat(IF(al.ap_id = h.ap_id, 'Base ', 'Hub '), ' ', ifnull(ap.ap_icao, ap.ap_iata),' ', ap.ap_name) as hubName 
                FROM (select ? as ap_id, ? as al_id) h 
                join ams_al.ams_airline al on al.al_id = h.al_id 
                join ams_wad.cfg_airport ap on ap.ap_id = h.ap_id " ;

        $bound_params_r = ["ii",
             ((!isset($dataJson->apId)) ? null : $dataJson->apId),
            ((!isset($dataJson->alId)) ? null : $dataJson->alId)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_al.ams_al_hub 
                    SET hub_name=?, upkeep_day=from_unixtime(?)
                    WHERE hub_id = ?  " ;

        $bound_params_r = ["sii",
            ((!isset($dataJson->hubName)) ? null : $dataJson->hubName),
            ((!isset($dataJson->upkeepTimeMs)) ? null : $dataJson->upkeepTimeMs),
            ($dataJson->hubId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->hubId;
    }
    
    static function SelectJson($id, $conn, $mn, $logModel){
        
        $sql = "SELECT h.hub_id as hubId, h.ap_id as apId, h.al_id as alId,
                IF(al.ap_id = h.ap_id, 1, 2) as typeId,
                concat(IF(al.ap_id = h.ap_id, 'Base ', 'Hub '), ' ', if(ap.ap_icao, ap.ap_icao, ap.ap_iata),' ', ap.ap_name) as hubName,
                ROUND((tt.amount*ap.ap_type_id)*IF(al.ap_id = h.ap_id,  ppb.param_value, pph.param_value),2) as taxPerWeek,
                IF(al.ap_id = h.ap_id,  ppb.param_value, pph.param_value) as discount,
                ifnull(r.flpnCount,0) as flpnCount
                FROM ams_al.ams_al_hub h
                join ams_al.ams_airline al on al.al_id = h.al_id
                join ams_wad.cfg_airport ap on ap.ap_id = h.ap_id
                join (select param_value FROM ams_al.cfg_callc_param where param_name = 'fbo_fee_discount_base' ) ppb on 1=1
                join (select param_value FROM ams_al.cfg_callc_param where param_name = 'fbo_fee_discount_hub' ) pph on 1=1
                join ams_al.cfg_transaction_type tt on tt.tr_type_id = 16
                left join (select count(*) as flpnCount, al_id, dep_ap_id from ams_al.ams_al_flp_number group by al_id, dep_ap_id) r on r.al_id = h.al_id and r.dep_ap_id = h.ap_id
                WHERE h.hub_id = ? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM ams_al.ams_al_hub
                   WHERE hub_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted alId =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}
