<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Oct 8, 2021 
 *  Filename          : AmsTask.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of AmsTask
 *
 * @author IZIordanov
 */
class AmsTask {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
 
    public $taskId;
    public $taskType;
    public $taskName;
    public $spName;
    public $periodMin;
    public $startTime;
    public $endTime;
    public $isRunning;
    public $execTimeMs;
    public $nextRun;
    public $info;
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($id) {
        $mn = "AmsTask::LoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsTask::SelectJson($id, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "AmsTask::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        AmsAlLogger::log($mn, " spName = " . $dataJson->spName);
        $response;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $taskId = null;
            if(isset($dataJson->taskId)){
               AmsAlLogger::log($mn, "Update  taskId =" . $dataJson->taskId);
               $taskId = $dataJson->taskId;
               AmsTask::Update($dataJson, $conn, $mn, $logModel);

            } else{
                AmsAlLogger::log($mn, "Create airline");
                $bankId = AmsTask::Create($dataJson, $conn, $mn, $logModel);
            }
            
            AmsAlLogger::log($mn, " taskId =" . $taskId);
           if(isset($bankId)){
                $objArrJ = AmsTask::SelectJson($taskId, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
     
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_wad.cfg_task 
            (task_type, task_name, sp_name, period_min, is_running, next_run, info)
            VALUES(?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["issiids",
             ((!isset($dataJson->taskType)) ? null : $dataJson->taskType),
            ((!isset($dataJson->taskName)) ? null : $dataJson->taskName),
            ((!isset($dataJson->spName)) ? null : $dataJson->spName),
            ((!isset($dataJson->periodMin)) ? null : $dataJson->periodMin),
            ((!isset($dataJson->isRunning)) ? null : $dataJson->isRunning),
            ((!isset($dataJson->nextRun)) ? null : $dataJson->nextRun),
            ((!isset($dataJson->info)) ? null : $dataJson->info)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_wad.cfg_task 
                    SET task_type=?, 
                    task_name=?,
                    sp_name=?,
                    period_min=?,
                    is_running=?,
                    next_run=?,
                    info=?
                    WHERE task_id = ?  " ;

        $bound_params_r = ["issiidsi",
             ((!isset($dataJson->taskType)) ? null : $dataJson->taskType),
            ((!isset($dataJson->taskName)) ? null : $dataJson->taskName),
            ((!isset($dataJson->spName)) ? null : $dataJson->spName),
            ((!isset($dataJson->periodMin)) ? null : $dataJson->periodMin),
            ((!isset($dataJson->isRunning)) ? null : $dataJson->isRunning),
            ((!isset($dataJson->nextRun)) ? null : $dataJson->nextRun),
            ((!isset($dataJson->info)) ? null : $dataJson->info),
            ($dataJson->taskId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->taskId;
    }
    
    static function SelectJson($id, $conn, $mn, $logModel){
        
        $sql = "SELECT t.task_id as taskId,
                t.task_type as taskType,
                t.task_name as taskName,
                t.sp_name as spName,
                t.period_min as periodMin,
                t.start as startTime,
                t.finish as endTime,
                t.is_running as isRunning,
                t.exec_time as execTimeMs,
                t.next_run as nextRun,
                t.info as info
               FROM ams_wad.cfg_task t
                    WHERE t.task_id = ? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM ams_wad.cfg_task 
                   WHERE task_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted task_id =" . $id);
                    
        return $id;
    }
    
    public static function TaskTable($params) {
        $mn = "AmsTask::TaskTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT t.task_id as taskId,
                    t.task_type as taskType,
                    t.task_name as taskName,
                    t.sp_name as spName,
                    t.period_min as periodMin,
                    t.start as startTime,
                    t.finish as endTime,
                    t.is_running as isRunning,
                    t.exec_time as execTimeMs,
                    t.next_run as nextRun,
                    t.info as info,
                    t.skip_execution as skipExecution
                   FROM ams_wad.cfg_task t ";
            
            
            $sqlWhere  = null;
            if(isset($params->taskType) && strlen($params->taskType)>0){
                $sqlWhere = " WHERE t.task_type = ".$params->taskType." ";
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (t.task_name like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE (t.task_name like '%".$params->qry_filter."%' )";
                }
               
            }
            
            $sqlOrder = " ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by skip_execution, next_run, task_type, exec_time ";
            }
            $sqlOrder = " order by skip_execution, next_run, task_type, exec_time ";
            $sql .= (isset($sqlWhere)?$sqlWhere:"").$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("tasks", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_wad.cfg_task t ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    
    public static function EventsChart($params) {
        $mn = "AmsTask::EventsChart()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT t.id, start_time as startTime, 
                    FORMAT(t.runing_time/60000, 2) as execTimeMin
                    FROM ams_wad.st_ev_run_h t
                    where t.ev_name = ?
                    order by start_time desc limit ?; ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["si", "sp_ev_min_one", $params->qry_limit];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("evMinOne", $ret_json_data);
            $bound_params_r = ["si", "sp_ev_min_five", $params->qry_limit];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("evMinFive", $ret_json_data);
            $bound_params_r = ["si", "sp_ev_min_ten", $params->qry_limit];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("evMinTen", $ret_json_data);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    // </editor-fold>
}
