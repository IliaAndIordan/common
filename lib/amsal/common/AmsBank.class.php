<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Oct 4, 2023 
 *  Filename            : AmsTask.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2024 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of AmsBank
 *
 * @author IZIordanov
 */
class AmsBank {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    
    public $bankId;
    public $alId;
    public $amount;
    public $adate;
    public $udate;
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($id) {
        $mn = "AmsBank::LoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsBank::SelectJson($id, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function LoadByAirlineId($id) {
        $mn = "AmsBank::LoadByAirlineId(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsBank::SelectByAirlineJson($id, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "AmsBank::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        AmsAlLogger::log($mn, " alName = " . $dataJson->alName);
        $response;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $bankId = null;
            if(isset($dataJson->bankId)){
               AmsAlLogger::log($mn, "Update  bankId =" . $dataJson->bankId);
               $bankId = $dataJson->bankId;
               AmsBank::Update($dataJson, $conn, $mn, $logModel);

            } else{
                AmsAlLogger::log($mn, "Create airline");
                $bankId = AmsBank::Create($dataJson, $conn, $mn, $logModel);
            }
            
            AmsAlLogger::log($mn, " $bankId =" . $bankId);
           if(isset($bankId)){
                $objArrJ = AmsBank::SelectJson($bankId, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
     
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_al.ams_bank 
            (al_id, amount)
            VALUES(?, ?)" ;

        $bound_params_r = ["id",
             ((!isset($dataJson->alId)) ? null : $dataJson->alId),
            ((!isset($dataJson->amount)) ? null : $dataJson->amount)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_al.ams_bank 
                    SET amount=?, 
                    WHERE bank_id = ?  " ;

        $bound_params_r = ["di",
            ((!isset($dataJson->amount)) ? null : $dataJson->amount),
            ($dataJson->bankId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->userId;
    }
    
    static function SelectJson($id, $conn, $mn, $logModel){
        
        $sql = "SELECT b.bank_id as bankId,  b.al_id as alId,
                b.amount, b.adate, b.udate
                FROM ams_al.ams_bank b
                    WHERE b.bank_id = ? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function SelectByAirlineJson($id, $conn, $mn, $logModel){
        
        $sql = "SELECT b.bank_id as bankId,  b.al_id as alId,
                b.amount, b.adate, b.udate
                FROM ams_al.ams_bank b
                    WHERE b.al_id = ? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM ams_al.ams_bank
                   WHERE bank_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted alId =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}
