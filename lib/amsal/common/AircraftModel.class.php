<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Oct 21, 2021 
 *  Filename          : AircraftModel.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */
require_once("Functions.php");
/**
 * Description of AircraftModel
 *
 * @author IZIordanov
 */
class AircraftModel {
    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $acId;
    public $registration;
    public $macId;
    public $ownerAlId;
    public $homeApId;
    public $adate;
    public $udate;
    public $cabinId;
    public $pilots;
    public $crew;
    public $paxF;
    public $paxB;
    public $paxE;
    public $cargoKg;
    public $costPerFh;
    public $acStatusId;
    public $currApId;
    public $flightHours;
    public $distanceKm;
    public $state;
    public $price;
    public $opTimeMin;
    public $aCheckFh;
    public $bCheckFh;
    public $cCheckFh;
    public $dCheckFh;
    public $stars;
    public $points;
    public $currUdate;
    public $homeAp;
    public $currAp;
    public $forSheduleFlights;

    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($id) {
        $mn = "AircraftModel::LoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AircraftModel::SelectJson($id, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function BuyId($acId, $alId) {
        $mn = "AircraftModel::BuyId(" . $acId. ", ". $alId. ")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        $obj = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            
            $sql = " CALL ams_ac.sp_acat_purchase(?,?)";
            $bound_params_r = ["ii", $acId, $alId];
            $affected_rows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
            //AmsAlLogger::logDebug($mn, "affected_rows: ".$affected_rows);
            $objArrJ = AircraftModel::SelectJson($acId, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $obj = json_decode(json_encode($objArrJ[0]));
            }
             $response->addData("aircraft", $obj);
             //$response->addData("affectedRows", $affected_rows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function SellId($acId) {
        $mn = "AircraftModel::SellId(" . $acId. ")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        $obj = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            
            $sql = " CALL ams_ac.sp_acat_sell(?)";
            $bound_params_r = ["i", $acId];
            $affected_rows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
            AmsAlLogger::logDebug($mn, "affected_rows: ".$affected_rows);
            $objArrJ = AircraftModel::SelectJson($acId, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $obj = json_decode(json_encode($objArrJ[0]));
            }
            $response = new Response("success", "Aircraft Sold.");
            $response->addData("aircraft", $obj);
            //$response->addData("affectedRows", $affected_rows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "AircraftModel::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsAlLogger::log($mn, " flightName = " . $dataJson->flightName);
        $response = new AircraftModel();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = null;
            if (isset($dataJson->acId)) {
                //AmsAlLogger::log($mn, "Update  book_id =" . $dataJson->id);
                $id = $dataJson->acId;
                AircraftModel::Update($dataJson, $conn, $mn, $logModel);
            } 

            //AmsAlLogger::log($mn, " id =" . $id);
            if (isset($id)) {
                $objArrJ = AircraftModel::SelectJson($id, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = json_decode(json_encode($objArrJ[0]));
                }
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function AircraftTable($params) {
        $mn = "AircraftModel::AircraftTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT a.ac_id as acId, 
                    a.registration, 
                    a.mac_id as macId, 
                    a.owner_al_id as ownerAlId,
                    a.home_ap_id as homeApId, 
                    a.adate, a.udate, a.cabin_id as cabinId, 
                    a.pilots_count as pilots, cb.seat_crew_count as crew,
                    a.pax_f as paxF, a.pax_b as paxB, a.pax_e as paxE, a.cargo_kg as cargoKg,
                    a.cost_per_fh as costPerFh, a.ac_status_id as acStatusId, 
                    a.for_shedule_flights as forSheduleFlights,
                    grp.grp_id as grpId,
                    grp.grp_name as grpName,
                    grp.is_active as grpActive,
                    ac.curr_ap_id as currApId, ac.flight_hours as flightHours, ac.distance_km as distanceKm,
                    ac.state_value as state, ac.price_value as price, ac.op_time_min as opTimeMin,
                    ac.a_check_fh as aCheckFh, ac.b_check_fh as bCheckFh, ac.c_check_fh as cCheckFh, ac.d_check_fh as dCheckFh,
                    ac.stars, ac.points, ac.udate currUdate, 
                    h.apLabel as homeAp, c.apLabel as currAp,
                    if(h.iata is null or h.iata = '',h.icao, h.iata) as homeApCode, 
                    if(c.iata is null or c.iata = '',c.icao, c.iata) as currApCode,
                    cb.cabin_name as cabinName, cb.confort, m.ac_type_id as acTypeId, 
                    m.fuel_consumption_lp100km as fuelConsumptionLp100km,
                    m.max_range_km as maxRangeKm, IF(m.landing_m>m.take_off_m,m.landing_m, m.take_off_m) as minRunwayM,
                    m.cruise_speed_kmph as cruiseSpeedKmph, 
                    l.acat_id as acActionTypeId, l.fl_id as logFlId, l.adate as logAdate, l.duration_min as logDurationMin,
                    l.flight_h as logFlightH, l.distance_km as logDistanceKm, l.udate as logUdate,
                    ac.flight_in_queue as flInQueue, 
                    q.atime as lastFlqArrTime,
                    q.a_ap_id as lastFlqArrApId,
                    ac.flight_h_in_queue as flqSumFlightH,
                    qn.a_ap_id as nextFlqDepApId,
                    qn.dtime as nextFlqDepTime,
                    ac.next_mnt_id as nextMntId,
                    mp.st_time as nextMntStart,
                    ac.flight_h_to_next_amt_id as nextAmtIdFlightHTo, 
                    ac.next_amt_id as nextAmtId
                    FROM ams_ac.ams_aircraft a
                    -- join ams_ac.v_ams_ac av on av.acId = a.ac_id 
                    join ams_ac.cfg_mac m on m.mac_id = a.mac_id 
                    join ams_ac.ams_aircraft_curr ac on ac.ac_id = a.ac_id 
                    left join ams_ac.ams_al_flight_queue q on q.fl_id = ac.last_flq_id 
                    left join ams_ac.ams_al_flight_queue qn on qn.fl_id = ac.next_flq_id 
                    left join ams_ac.ams_aircraft_maintenance_plan mp on mp.acmp_id = ac.next_mnt_id
                    join ams_ac.ams_aircraft_log l on l.ac_id = a.ac_id 
                    join ams_ac.cfg_mac_cabin cb on cb.cabin_id = a.cabin_id 
                    join ams_wad.v_solr_airport h on h.apId = a.home_ap_id
                    join ams_wad.v_solr_airport c on c.apId = ac.curr_ap_id 
                    left join ams_al.ams_al_flp_group grp on grp.ac_id = a.ac_id  ";
            
            
            $sqlWhere="";
            if(isset($params->macId) && strlen($params->macId)>0){
                $sqlWhere = " WHERE a.mac_id = ".$params->macId." ";
            }
            
            if(isset($params->homeApId) && strlen($params->homeApId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " and a.home_ap_id = ".$params->homeApId." ";
                } else{
                    $sqlWhere = " WHERE a.home_ap_id = ".$params->homeApId." ";
                }
            } 
            
            if(isset($params->forSheduleFlights)){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " and a.for_shedule_flights = ".($params->forSheduleFlights?1:0)." ";
                } else{
                    $sqlWhere = " WHERE a.for_shedule_flights = ".($params->forSheduleFlights?1:0)." ";
                }
            } 
            
            
            if(isset($params->flInQueue) && strlen($params->flInQueue)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " and ac.flight_in_queue ".$params->flInQueue." ";
                } else{
                    $sqlWhere = " WHERE ac.flight_in_queue ".$params->flInQueue." ";
                }
            } 
            
            if(isset($params->forShaduleFlighs) && strlen($params->forShaduleFlighs)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " and ac.flight_in_queue = ".$params->forShaduleFlighs." ";
                } else{
                    $sqlWhere = " WHERE ac.flight_in_queue = ".$params->forShaduleFlighs." ";
                }
            } 
            
            if(isset($params->ownerAlId) && strlen($params->ownerAlId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " and a.owner_al_id = ".$params->ownerAlId." ";
                } else{
                    $sqlWhere = " WHERE a.owner_al_id = ".$params->ownerAlId." ";
                }
            } else if(isset($params->newAc) && strlen($params->newAc)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " and a.owner_al_id is null ";
                } else{
                    $sqlWhere = " WHERE a.owner_al_id is null ";
                }
            } 
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (a.registration like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or h.apLabel like '%".$params->qry_filter."%' ";
                     $sqlWhere .= " or c.apLabel like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE (a.registration like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or h.apLabel like '%".$params->qry_filter."%' ";
                     $sqlWhere .= " or c.apLabel like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = "";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by a.registration, a.adate ";
            }
            $sql .= (isset($sqlWhere) && strlen($sqlWhere)>1?$sqlWhere:"");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder)>1?$sqlOrder:"");
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("aircrafts", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_aircraft a
                    join ams_ac.ams_aircraft_curr ac on ac.ac_id = a.ac_id 
                    join ams_wad.v_solr_airport h on h.apId = a.home_ap_id
                    join ams_wad.v_solr_airport c on c.apId = ac.curr_ap_id ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
     // </editor-fold>
    
     // <editor-fold defaultstate="collapsed" desc="Maintenance Methods">

    public static function MaintenancePlanCreate($acId, $amtId, $stTime) {
        $mn = "AircraftModel::MaintenancePlanCreate(" . $acId. ", ". $amtId. ")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        $obj = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            
            $sql = "CALL ams_ac.sp_ac_mnt_plan_create(?, ?, ?)";
            $bound_params_r = ["iii", $acId, $amtId, $stTime];
            $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            //AmsAlLogger::log($mn, "count(result): ".count($result_r));
            if(count($result_r)>0)
            {
                $acmpId = $result_r[0]["acmp_id"];
            }
            //AmsAlLogger::log($mn, "fleId: ".$fleId);
            $objArrJ = AircraftModel::SelectMaintenancePlanByIdJson($acmpId, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $obj = json_decode(json_encode($objArrJ[0]));
            }
            
            $response->addData("maintenance", $obj);
            //$response->addData("affectedRows", $affected_rows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function MaintenancePlanLoadById($id) {
        $mn = "AircraftModel::MaintenancePlanLoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AircraftModel::SelectMaintenancePlanByIdJson($id, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    static function SelectMaintenancePlanByIdJson($id, $conn, $mn, $logModel) {

        $sql = " SELECT acmp_id as acmpId, ac_id as acId, amt_id as amtId,
                    description, ap_id as apId, is_home_ap as isHomeAp,
                    price, al_id as alId, st_time as stTime,
                    end_time as endTime, tr_id as trId, processed,
                    adate
                FROM ams_ac.ams_aircraft_maintenance_plan
                    WHERE acmp_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }
    
    public static function MaintenancePlanTable($params) {
        $mn = "AircraftModel::MaintenancePlanTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT acmp.acmp_id as acmpId, acmp.ac_id as acId, acmp.amt_id as amtId,
                        acmp.description, acmp.ap_id as apId, acmp.is_home_ap as isHomeAp,
                        acmp.price, acmp.al_id as alId, acmp.st_time as stTime,
                        acmp.end_time as endTime, acmp.tr_id as trId, acmp.processed,
                        acmp.adate, l.adate as logAdate, l.adate as logDurationMin
                    FROM ams_ac.ams_aircraft_maintenance_plan acmp
                    left join ams_ac.ams_aircraft_log l on l.acmp_id = acmp.acmp_id ";
            
            
            $sqlWhere="";
            if(isset($params->alId) && strlen($params->alId)>0){
                $sqlWhere = " WHERE acmp.al_id = ".$params->alId." ";
            }
            
            if(isset($params->acId) && strlen($params->acId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " and acmp.ac_id = ".$params->acId." ";
                } else{
                    $sqlWhere = " WHERE acmp.ac_id = ".$params->acId." ";
                }
            }
            
            if(isset($params->apId) && strlen($params->apId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " and acmp.ap_id = ".$params->apId." ";
                } else{
                    $sqlWhere = " WHERE acmp.ap_id = ".$params->apId." ";
                }
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (acmp.description like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE (acmp.description like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = "";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= " order by acmp.stTime ";
            }
            $sql .= (isset($sqlWhere) && strlen($sqlWhere)>1?$sqlWhere:"");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder)>1?$sqlOrder:"");
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("maintenances", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_aircraft_maintenance_plan acmp
                    left join ams_ac.ams_aircraft_log l on l.acmp_id = acmp.acmp_id ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
     // </editor-fold>
      
    // <editor-fold defaultstate="collapsed" desc="Flight Methods">
    
    public static function FlightEstimateTransfer($acId, $apId) {
        $mn = "AircraftModel::FlightEstimateTransfer(" . $acId. ", ". $apId. ")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        $obj = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            
            $sql = "call ams_ac.sp_al_fl_transfer_estimate(?,?)";
            $bound_params_r = ["ii", $acId, $apId];
            $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            //AmsAlLogger::log($mn, "count(result): ".count($result_r));
            if(count($result_r)>0)
            {
                $fleId = $result_r[0]["fle_id"];
            }
            //AmsAlLogger::log($mn, "fleId: ".$fleId);
            $objArrJ = AircraftModel::FlightEstimateJson($fleId, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $obj = json_decode(json_encode($objArrJ[0]));
            }
            
            $response->addData("flight", $obj);
            //$response->addData("affectedRows", $affected_rows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Statistics Methods">
    
    public static function AmsAlAcAircraftStDayTable($params) {
        $mn = "AmsAlSheduleRoute::AmsAlAcAircraftStDayTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //--
            $sql = "SELECT id,
                        ac_id as acId,
                        al_id as alId,
                        stdate,
                        fl_count as flightsCount,
                        charters_count as chartersCount,
                        distance_km as distanceKm,
                        oil_l as oilL,
                        pax, round(payload_kg,2) as payloadKg,
                        expences, income, revenue, flight_h as flightH, cost_per_flight_h as costPerFlightH
                    FROM ams_ac.st_aircraft_day ";

            $sqlWhere = "";

            if (isset($params->alId) && strlen($params->alId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND al_id =" . $params->alId . " ";
                } else {
                    $sqlWhere = " WHERE al_id =" . $params->alId . " ";
                }
            }
            if (isset($params->acId) && strlen($params->acId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND ac_id =" . $params->acId . " ";
                } else {
                    $sqlWhere = " WHERE ac_id =" . $params->acId . " ";
                }
            }

            
            $sqlOrder = "";
            if (isset($params->qry_orderCol)) {
                $sqlOrder .= " order by " . $params->qry_orderCol . " " . ($params->qry_isDesc ? "desc" : " asc");
            } else {
                $sqlOrder .= "order by id desc ";
            }
            $sql .= $sqlWhere . $sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("rows", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.st_aircraft_day 
                     " . (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
            
            if((isset($params->alId) && strlen($params->alId) > 0) ||
              (isset($params->acId) && strlen($params->acId) > 0)) {
                if(isset($params->alId) && strlen($params->alId) > 0) {
                   $sql = "SELECT sum(revenue) as revenue FROM ams_ac.st_aircraft_day
                        where ac_id =? and al_id = ? ";
                   $bound_params_r = ["ii", $params->acId, $params->alId ];
                } else {
                    $sql = "SELECT sum(revenue) as revenue FROM ams_ac.st_aircraft_day
                        where ac_id =? ";
                   $bound_params_r = ["i", $params->acId ];
                }
            
                $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                $rowJson = json_decode(json_encode($ret_json_data[0]));
                $response->addData("totalRevenue", $rowJson->revenue);
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
     // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">

    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = " SELECT a.ac_id as acId, 
                    a.registration, 
                    a.mac_id as macId, 
                    a.owner_al_id as ownerAlId,
                    a.home_ap_id as homeApId, 
                    a.adate, a.udate, a.cabin_id as cabinId, 
                    a.pilots_count as pilots, cb.seat_crew_count as crew,
                    a.pax_f as paxF, a.pax_b as paxB, a.pax_e as paxE, a.cargo_kg as cargoKg,
                    a.cost_per_fh as costPerFh, a.ac_status_id as acStatusId, 
                    a.for_shedule_flights as forSheduleFlights,
                    grp.grp_id as grpId,
                    grp.grp_name as grpName,
                    grp.is_active as grpActive,
                    ac.curr_ap_id as currApId, ac.flight_hours as flightHours, ac.distance_km as distanceKm,
                    ac.state_value as state, ac.price_value as price, ac.op_time_min as opTimeMin,
                    ac.a_check_fh as aCheckFh, ac.b_check_fh as bCheckFh, ac.c_check_fh as cCheckFh, ac.d_check_fh as dCheckFh,
                    ac.stars, ac.points, ac.udate currUdate, 
                    h.apLabel as homeAp, c.apLabel as currAp,
                    if(h.iata is null or h.iata = '',h.icao, h.iata) as homeApCode, 
                    if(c.iata is null or c.iata = '',c.icao, c.iata) as currApCode,
                    cb.cabin_name as cabinName, cb.confort, m.ac_type_id as acTypeId, 
                    m.fuel_consumption_lp100km as fuelConsumptionLp100km,
                    m.max_range_km as maxRangeKm, IF(m.landing_m>m.take_off_m,m.landing_m, m.take_off_m) as minRunwayM,
                    m.cruise_speed_kmph as cruiseSpeedKmph, 
                    l.acat_id as acActionTypeId, l.fl_id as logFlId, l.adate as logAdate, l.duration_min as logDurationMin,
                    l.flight_h as logFlightH, l.distance_km as logDistanceKm, l.udate as logUdate,
                    ac.flight_in_queue as flInQueue, 
                    q.atime as lastFlqArrTime,
                    q.a_ap_id as lastFlqArrApId,
                    ac.flight_h_in_queue as flqSumFlightH,
                    qn.a_ap_id as nextFlqDepApId,
                    qn.dtime as nextFlqDepTime,
                    ac.next_mnt_id as nextMntId,
                    mp.st_time as nextMntStart,
                    ac.flight_h_to_next_amt_id as nextAmtIdFlightHTo, 
                    ac.next_amt_id as nextAmtId
                    FROM ams_ac.ams_aircraft a
                    -- join ams_ac.v_ams_ac av on av.acId = a.ac_id 
                    join ams_ac.cfg_mac m on m.mac_id = a.mac_id 
                    join ams_ac.ams_aircraft_curr ac on ac.ac_id = a.ac_id 
                    left join ams_ac.ams_al_flight_queue q on q.fl_id = ac.last_flq_id 
                    left join ams_ac.ams_al_flight_queue qn on qn.fl_id = ac.next_flq_id 
                    left join ams_ac.ams_aircraft_maintenance_plan mp on mp.acmp_id = ac.next_mnt_id
                    join ams_ac.ams_aircraft_log l on l.ac_id = a.ac_id 
                    join ams_ac.cfg_mac_cabin cb on cb.cabin_id = a.cabin_id 
                    join ams_wad.v_solr_airport h on h.apId = a.home_ap_id
                    join ams_wad.v_solr_airport c on c.apId = ac.curr_ap_id 
                    left join ams_al.ams_al_flp_group grp on grp.ac_id = a.ac_id
                    WHERE a.ac_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE ams_ac.ams_aircraft
                    SET home_ap_id=?, cabin_id=?, 
                    for_shedule_flights=?
                    WHERE ac_id = ? ";

        $bound_params_r = ["iiii",
            ((!isset($dataJson->homeApId)) ? null : $dataJson->homeApId),
            ((!isset($dataJson->cabinId)) ? null : $dataJson->cabinId),
            ((!isset($dataJson->forSheduleFlights)) ? 0 : ($dataJson->forSheduleFlights?1:0)),
            ((!isset($dataJson->acId)) ? null : $dataJson->acId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //BwtLogger::log($mn, "affectedRows=" . $affectedRows);
        $strSQL = "update ams_ac.cfg_mac_cabin c
                    join ams_ac.cfg_mac m on m.mac_id = c.mac_id
                    set c.mac_load_kg = m.load_kg where 1=? ";
         $bound_params_r = ["i",1];
         $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
         
        return $dataJson->acId;
    }

    static function FlightEstimateJson($id, $conn, $mn, $logModel) {

        $sql = " SELECT fle.fle_id as fleId, fle.flp_id as flpId, fle.route_id as routeId, fle.fl_type_id as flTypeId,
                fle.fl_num as flNum, fle.fl_name as flName, fle.fl_description as flDescription,
                fle.al_id as alId, fle.ac_id as acId, fle.d_ap_id as depApId, fle.a_ap_id as arrApId,
                fle.distance_km as distanceKm, fle.duration_min as durationMin,
                fle.oil_l as oilL, fle.oli_price as oilPrice, fle.landin_fee as landingFee, fle.ground_crew_fee as groundCrewFee, fle.boarding_fee as boardingFee,
                fle.fl_price flPrice, fle.bank_id_from, fle.payload_kg, fle.mtow,
                fle.dtime, fle.atime, fle.expences, fle.income, fle.revenue, adate
                 FROM ams_ac.ams_al_flight_estimate fle
                 where fle.fle_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }
    

    // </editor-fold>
}
