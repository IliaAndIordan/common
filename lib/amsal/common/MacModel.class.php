<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Jan 7, 2021 
 *  Filename            : MacModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of Country
 *
 * @author IZIordanov
 */
class Mac {

    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $macId;
    public $mfrId;
    public $acTypeId;
    public $model;
    public $price;
    public $pilots;
    public $cruiseSpeedKmph;
    public $cruiseAltitudeM;
    public $takeOffM;
    public $maxTakeOffKg;
    public $landingM;
    public $maxLandingKg;
    public $maxRangeKm;
    public $emptyWeightKg;
    public $fuelVolL;
    public $loadKg;
    public $wikiUrl;
    public $amsStatus;
    public $notes;
    public $fuelConsumptionLp100km;
    public $costPerFh;
    public $operationTimeMin;
    public $popularity;
    public $maxSeating;
    public $asmiRate;
    public $powerplant;
    public $productionStart;
    public $productionRateH;
    public $lastProducedOn;
    public $numberBuild;
    public $apId;
    public $apName;
    public $udate;

    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($mac_id) {
        $mn = "Mac::LoadById(" . $mac_id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = Mac::SelectJson($mac_id, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function LoadByAlId($alId) {
        $mn = "Mac::LoadByAlId(" . $alId . ")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $ret_json_data = Mac::SelectByAlIdJson($alId, $conn, $mn, $logModel);
            $response->addData("macs", $ret_json_data);
           
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function Save($dataJson) {
        $mn = "Mac::Save()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        //AmsAlLogger::log($mn, " mac_id = " . $dataJson->macId);
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $mac_id = null;
            if (isset($dataJson->macId)) {
                //AmsAlLogger::log($mn, "Update  macId =" . $dataJson->macId);
                $mac_id = $dataJson->macId;
                $mac_id = Mac::Update($dataJson, $conn, $mn, $logModel);
            } else {
                AmsAlLogger::log($mn, "Create mac ");
                $mac_id = Mac::Create($dataJson, $conn, $mn, $logModel);
            }

            AmsAlLogger::log($mn, " mac_id =" . $mac_id);
            if (isset($mac_id)) {
                $objArrJ = Mac::SelectJson($mac_id, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = json_decode(json_encode($objArrJ[0]));
                }
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function MacTable($params) {
        $mn = "Mac::MacTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT m.mac_id as macId, mfr_id as mfrId,
                    m.ac_type_id as acTypeId,
                    m.model, price, pilots,
                    m.cruise_speed_kmph as cruiseSpeedKmph,
                    m.cruise_altitude_m as cruiseAltitudeM,
                    m.take_off_m as takeOffM,
                    m.max_take_off_kg as maxTakeOffKg,
                    m.landing_m as landingM, 
                    m.max_landing_kg as maxLandingKg,
                    m.max_range_km as maxRangeKm,
                    m.empty_w_kg as emptyWeightKg,
                    m.fuel_vol_l as fuelVolL,
                    m.load_kg as loadKg,
                    m.cargo_kg as cargoKg,
                    m.wiki_link as wikiUrl,
                    m.ams_status_id as amsStatus,
                    m.notes, m.fuel_consumption_lp100km as fuelConsumptionLp100km,
                    m.cost_per_fh as costPerFh,
                    m.operation_time_min as operationTimeMin,
                    m.popularity,
                    m.max_seating as maxSeating, m.asmi_rate as asmiRate,
                    m.powerplant, m.production_start as productionStart,
                    m.production_rate_h as productionRateH,
                    m.last_produced_on as lastProducedOn, 
                    m.number_build as numberBuild, 
                    m.ap_id as apId,
                    a.ap_name as apName,
                    m.udate
                    FROM ams_ac.cfg_mac m
                    left join ams_wad.cfg_airport a on a.ap_id = m.ap_id ";



            if (isset($params->mfrId) && strlen($params->mfrId) > 0) {
                $sqlWhere = " WHERE m.mfr_id = " . $params->mfrId . " ";
            }

            if (isset($params->qry_filter) && strlen($params->qry_filter) > 1) {
                if (isset($sqlWhere)) {
                    $sqlWhere .= " AND ( m.model like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or m.powerplant like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or m.notes like '%" . $params->qry_filter . "%' )";
                } else {
                    $sqlWhere .= " WHERE ( m.model like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or m.powerplant like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or m.notes like '%" . $params->qry_filter . "%' )";
                }
            }
            $sqlOrder = "group by m.mac_id ";
            if (isset($params->qry_orderCol)) {
                $sqlOrder .= " order by " . $params->qry_orderCol . " " . ($params->qry_isDesc ? "desc" : " asc") . ", model";
            } else {
                $sqlOrder .= " order by m.model, m.mac_id ";
            }
            $sql .= $sqlWhere . $sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("macs", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.cfg_mac m " . (isset($sqlWhere) ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function MacMarketTable($params) {
        $mn = "Mac::MacMarketTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT m.mac_id as macId, mfr_id as mfrId,
                    m.ac_type_id as acTypeId,
                    m.model, m.price, m.pilots,
                    m.max_seating as maxSeating,
                    m.load_kg as loadKg,
                    m.cargo_kg as cargoKg,
                    m.cruise_speed_kmph as cruiseSpeedKmph,
                    m.cruise_altitude_m as cruiseAltitudeM,
					m.max_range_km as maxRangeKm,
                    m.fuel_consumption_lp100km as fuelConsumptionLp100km,
                    m.cost_per_fh as costPerFh,
                    m.operation_time_min as operationTimeMin,
                    m.number_build as numberBuild, 
                    m.ap_id as apId,
                    a.ap_name as apName,
                    m.wiki_link as wikiUrl,
                    m.ams_status_id as amsStatus,
                    m.notes, 
                    m.popularity,
                    m.asmi_rate as asmiRate,
                    m.take_off_m as takeOffM,
                    m.max_take_off_kg as maxTakeOffKg,
                    m.landing_m as landingM, 
                    m.max_landing_kg as maxLandingKg,
                   count(ac.ac_id) as aircraftCount,
                   ifnull(acMarketCount,0) as acMarketCount,
                    m.empty_w_kg as emptyWeightKg,
                    m.fuel_vol_l as fuelVolL,
                    m.powerplant, 
                    m.production_start as productionStart,
                    m.production_rate_h as productionRateH,
                    m.last_produced_on as lastProducedOn, 
                    m.udate
                    FROM ams_ac.cfg_mac m
                    left join ams_wad.cfg_airport a on a.ap_id = m.ap_id
                    left join ams_ac.ams_aircraft ac on ac.mac_id = m.mac_id 
                    left join (
			select mac_id, count(distinct(ac_id)) acMarketCount 
                        from ams_ac.ams_aircraft 
                        where ac_status_id in (1, 4) 
                        group by mac_id) acm on acm.mac_id = m.mac_id  ";


            $sqlWhere="";
            if (isset($params->mfrId) && strlen($params->mfrId) > 0) {
                $sqlWhere = " WHERE m.mfr_id = " . $params->mfrId . " ";
            }

            if (isset($params->qry_filter) && strlen($params->qry_filter) > 1) {
                if (isset($sqlWhere)) {
                    $sqlWhere .= " AND ( m.model like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or m.powerplant like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or m.notes like '%" . $params->qry_filter . "%' )";
                } else {
                    $sqlWhere .= " WHERE ( m.model like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or m.powerplant like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or m.notes like '%" . $params->qry_filter . "%' )";
                }
            }
            $sqlOrder = " group by m.mac_id ";
            if (isset($params->qry_orderCol)) {
                $sqlOrder .= " order by " . $params->qry_orderCol . " " . ($params->qry_isDesc ? "desc" : " asc") . ", model";
            } else {
                $sqlOrder .= " order by price, maxRangeKm, maxSeating ";
            }
            $sql .= (isset($sqlWhere) && strlen($sqlWhere)>1?$sqlWhere:"");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder)>1?$sqlOrder:"");
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("macs", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.cfg_mac m " . (isset($sqlWhere) && strlen($sqlWhere)>1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">

    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = " SELECT m.mac_id as macId, m.mfr_id as mfrId,
                    m.ac_type_id as acTypeId,
                    m.model, price, pilots,
                    mfr.mfr_name as mfrName,
                    m.cruise_speed_kmph as cruiseSpeedKmph,
                    m.cruise_altitude_m as cruiseAltitudeM,
                    m.take_off_m as takeOffM,
                    m.max_take_off_kg as maxTakeOffKg,
                    m.landing_m as landingM, 
                    m.max_landing_kg as maxLandingKg,
                    m.max_range_km as maxRangeKm,
                    m.empty_w_kg as emptyWeightKg,
                    m.fuel_vol_l as fuelVolL,
                    m.load_kg as loadKg,
                    m.cargo_kg as cargoKg,
                    m.wiki_link as wikiUrl,
                    m.ams_status_id as amsStatus,
                    m.notes, m.fuel_consumption_lp100km as fuelConsumptionLp100km,
                    m.cost_per_fh as costPerFh,
                    m.operation_time_min as operationTimeMin,
                    m.popularity,
                    m.max_seating as maxSeating, m.asmi_rate as asmiRate,
                    m.powerplant, m.production_start as productionStart,
                    m.production_rate_h as productionRateH,
                    m.last_produced_on as lastProducedOn, 
                    m.number_build as numberBuild, 
                    m.ap_id as apId,
                    a.ap_name as apName,
                    m.udate
                    FROM ams_ac.cfg_mac m
                    left join ams_wad.cfg_airport a on a.ap_id = m.ap_id
                    join ams_ac.cfg_manufacturer mfr on mfr.mfr_id = m.mfr_id
                    WHERE m.mac_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }
    
    static function SelectByAlIdJson($id, $conn, $mn, $logModel) {

        $sql = " SELECT m.mac_id as macId, 
                m.mfr_id as mfrId,
                m.ac_type_id as acTypeId,
                m.model, price, pilots,
                mfr.mfr_name as mfrName,
                m.cruise_speed_kmph as cruiseSpeedKmph,
                m.cruise_altitude_m as cruiseAltitudeM,
                m.take_off_m as takeOffM,
                m.max_take_off_kg as maxTakeOffKg,
                m.landing_m as landingM, 
                m.max_landing_kg as maxLandingKg,
                m.max_range_km as maxRangeKm,
                m.empty_w_kg as emptyWeightKg,
                m.fuel_vol_l as fuelVolL,
                m.load_kg as loadKg,
                m.cargo_kg as cargoKg,
                m.wiki_link as wikiUrl,
                m.ams_status_id as amsStatus,
                m.notes, m.fuel_consumption_lp100km as fuelConsumptionLp100km,
                m.cost_per_fh as costPerFh,
                m.operation_time_min as operationTimeMin,
                m.popularity,
                m.max_seating as maxSeating, m.asmi_rate as asmiRate,
                m.powerplant, m.production_start as productionStart,
                m.production_rate_h as productionRateH,
                m.last_produced_on as lastProducedOn, 
                m.number_build as numberBuild, 
                m.ap_id as apId,
                a.ap_name as apName,
                m.udate, alc.ac_id_count as aircraftCount
                FROM ams_ac.cfg_mac m
                left join ams_wad.cfg_airport a on a.ap_id = m.ap_id
                join ams_ac.cfg_manufacturer mfr on mfr.mfr_id = m.mfr_id
                join 
                ( select ac.mac_id, count(*) as ac_id_count, ac.owner_al_id
                        from ams_ac.ams_aircraft ac 
                        where ac.owner_al_id = ?
                        group by ac.mac_id, ac.owner_al_id) as alc on alc.mac_id = m.mac_id
                order by maxSeating, cruiseSpeedKmph, loadKg ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function Create($dataJson, $conn, $mn, $logModel) {

        $strSQL = "INSERT INTO ams_ac.cfg_mac 
            (mfr_id, ac_type_id,
            model, price,
            pilots, cruise_speed_kmph,
            cruise_altitude_m, take_off_m,
            max_take_off_kg, landing_m,
            
            max_landing_kg, max_range_km,
            empty_w_kg, fuel_vol_l,
            wiki_link,
            ams_status_id, notes,
            fuel_consumption_lp100km, cost_per_fh,
            
            operation_time_min, popularity,
            max_seating, asmi_rate,
            powerplant, production_start,
            production_rate_h, ap_id)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 
            ?, ?, ?, ?, ?, ?, ?, ?, ?, 
            ?, ?, ?, ?, ?, ?, ?, ?)";

        $bound_params_r = ["iisdiiiiiiiiiisisdiiiiissii",
            ((!isset($dataJson->mfrId)) ? null : $dataJson->mfrId),
            ((!isset($dataJson->acTypeId)) ? 1 : $dataJson->acTypeId),
            ((!isset($dataJson->model)) ? null : $dataJson->model),
            ((!isset($dataJson->price)) ? null : $dataJson->price),
            ((!isset($dataJson->pilots)) ? null : $dataJson->pilots),
            ((!isset($dataJson->cruiseSpeedKmph)) ? null : $dataJson->cruiseSpeedKmph),
            ((!isset($dataJson->cruiseAltitudeM)) ? null : $dataJson->cruiseAltitudeM),
            ((!isset($dataJson->takeOffM)) ? null : $dataJson->takeOffM),
            ((!isset($dataJson->maxTakeOffKg)) ? null : $dataJson->maxTakeOffKg),
            ((!isset($dataJson->landingM)) ? null : $dataJson->landingM),
            
            ((!isset($dataJson->maxLandingKg)) ? null : $dataJson->maxLandingKg),
            ((!isset($dataJson->maxRangeKm)) ? null : $dataJson->maxRangeKm),
            ((!isset($dataJson->emptyWeightKg)) ? null : $dataJson->emptyWeightKg),
            ((!isset($dataJson->fuelVolL)) ? null : $dataJson->fuelVolL),
            //((!isset($dataJson->loadKg)) ? null : $dataJson->loadKg),
            ((!isset($dataJson->wikiUrl)) ? null : $dataJson->wikiUrl),
            ((!isset($dataJson->amsStatus)) ? null : $dataJson->amsStatus),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->fuelConsumptionLp100km)) ? null : $dataJson->fuelConsumptionLp100km),
            ((!isset($dataJson->costPerFh)) ? null : $dataJson->costPerFh),
            
            ((!isset($dataJson->operationTimeMin)) ? null : $dataJson->operationTimeMin),
            ((!isset($dataJson->popularity)) ? null : $dataJson->popularity),
            ((!isset($dataJson->maxSeating)) ? null : $dataJson->maxSeating),
            ((!isset($dataJson->asmiRate)) ? null : $dataJson->asmiRate),
            ((!isset($dataJson->powerplant)) ? null : $dataJson->powerplant),
            ((!isset($dataJson->productionStart)) ? null : $dataJson->productionStart),
            ((!isset($dataJson->productionRateH)) ? null : $dataJson->productionRateH),
            ((!isset($dataJson->apId)) ? null : $dataJson->apId)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);

        return $id;
    }

    static function Update($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE ams_ac.cfg_mac
                    SET mfr_id = ? , ac_type_id = ?,
                    model = ?, price = ?,
                    pilots = ?, cruise_speed_kmph = ?,
                    cruise_altitude_m = ?, take_off_m = ?,
                    max_take_off_kg = ?, landing_m = ?,
                    max_landing_kg = ?, max_range_km = ?,
                    empty_w_kg = ?, fuel_vol_l = ?, load_kg = ?,
                    wiki_link = ?,
                    ams_status_id = ?, notes = ?,
                    fuel_consumption_lp100km = ?, cost_per_fh = ?,
                    operation_time_min = ?, popularity = ?,
                    max_seating = ?, asmi_rate = ?,
                    powerplant = ?, production_start = ?,
                    production_rate_h = ?,
                    ap_id = ?
                     WHERE mac_id = ? ";

        $bound_params_r = [ "iisdiiiiiiiiiiisisdiiiiissiii",
            ((!isset($dataJson->mfrId)) ? null : $dataJson->mfrId),
            ((!isset($dataJson->acTypeId)) ? 1 : $dataJson->acTypeId),
            ((!isset($dataJson->model)) ? null : $dataJson->model),
            ((!isset($dataJson->price)) ? null : $dataJson->price),
            ((!isset($dataJson->pilots)) ? null : $dataJson->pilots),
            ((!isset($dataJson->cruiseSpeedKmph)) ? null : $dataJson->cruiseSpeedKmph),
            ((!isset($dataJson->cruiseAltitudeM)) ? null : $dataJson->cruiseAltitudeM),
            ((!isset($dataJson->takeOffM)) ? null : $dataJson->takeOffM),
            ((!isset($dataJson->maxTakeOffKg)) ? null : $dataJson->maxTakeOffKg),
            ((!isset($dataJson->landingM)) ? null : $dataJson->landingM),
            
            ((!isset($dataJson->maxLandingKg)) ? null : $dataJson->maxLandingKg),
            ((!isset($dataJson->maxRangeKm)) ? null : $dataJson->maxRangeKm),
            ((!isset($dataJson->emptyWeightKg)) ? null : $dataJson->emptyWeightKg),
            ((!isset($dataJson->fuelVolL)) ? null : $dataJson->fuelVolL),
            ((!isset($dataJson->loadKg)) ? null : $dataJson->loadKg),
            ((!isset($dataJson->wikiUrl)) ? null : $dataJson->wikiUrl),
            ((!isset($dataJson->amsStatus)) ? null : $dataJson->amsStatus),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->fuelConsumptionLp100km)) ? null : $dataJson->fuelConsumptionLp100km),
            ((!isset($dataJson->costPerFh)) ? null : $dataJson->costPerFh),
            
            ((!isset($dataJson->operationTimeMin)) ? null : $dataJson->operationTimeMin),
            ((!isset($dataJson->popularity)) ? null : $dataJson->popularity),
            ((!isset($dataJson->maxSeating)) ? null : $dataJson->maxSeating),
            ((!isset($dataJson->asmiRate)) ? null : $dataJson->asmiRate),
            ((!isset($dataJson->powerplant)) ? null : $dataJson->powerplant),
            ((!isset($dataJson->productionStart)) ? null : $dataJson->productionStart),
            ((!isset($dataJson->productionRateH)) ? null : $dataJson->productionRateH),
            ((!isset($dataJson->apId)) ? null : $dataJson->apId),
            ($dataJson->macId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->macId;
    }

    static function Delete($mac_id, $conn, $mn, $logModel) {

        $strSQL = "DELETE FROM ams_ac.cfg_mac
                   WHERE mac_id = ? ";

        $bound_params_r = ["i", $mac_id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted mac_id =" . $id);

        return $id;
    }

    // </editor-fold>
}
