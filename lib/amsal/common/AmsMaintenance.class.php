<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Oct 26, 2023 
 *  Filename            : AmsMaintenance.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2023 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of AmsMaintenance
 *
 * @author IZIordanov
 */
class AmsMaintenance {

    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $acmpId;
    public $acId;
    public $amtId;
    public $amtName;
    public $mtnTimeH;
    public $description;
    public $apId;
    public $isHomeAp;
    public $price;
    public $alId;
    public $startTime;
    public $endTime;
    public $trId;
    public $processed;
    public $adate;

    public function toJSON() {
        return json_encode($this);
    }

    public static function fromJSON($dataJson) {
        $rv = new AmsMaintenance();
        $rv->acmpId = (!isset($dataJson->acmpId)) ? null : $dataJson->acmpId;
        $rv->acId = (!isset($dataJson->acId)) ? null : $dataJson->acId;
        $rv->amtId = (!isset($dataJson->amtId)) ? null : $dataJson->amtId;
        $rv->amtName = (!isset($dataJson->amtName)) ? null : $dataJson->amtName;
        $rv->mtnTimeH = (!isset($dataJson->mtnTimeH)) ? null : $dataJson->mtnTimeH;
        $rv->description = (!isset($dataJson->description)) ? null : $dataJson->description;
        $rv->apId = (!isset($dataJson->apId)) ? null : $dataJson->apId;
        $rv->isHomeAp = (!isset($dataJson->isHomeAp)) ? null : $dataJson->isHomeAp;
        $rv->price = (!isset($dataJson->price)) ? null : $dataJson->price;
        $rv->alId = (!isset($dataJson->alId)) ? null : $dataJson->alId;
        $rv->startTime = (!isset($dataJson->startTime)) ? null : $dataJson->startTime;
        $rv->endTime = (!isset($dataJson->endTime)) ? null : $dataJson->endTime;
        $rv->trId = (!isset($dataJson->trId)) ? null : $dataJson->trId;
        $rv->processed = (!isset($dataJson->processed)) ? null : $dataJson->processed;
        $rv->adate = (!isset($dataJson->adate)) ? null : $dataJson->adate;
        return $rv;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($id) {
        $mn = "AmsMaintenance::LoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsMaintenance::SelectJson($id, $conn, $mn, $logModel);

            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function Save($data) {
        $mn = "AmsMaintenance::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        AmsAlLogger::log($mn, " isset acmpId = " . isset($dataJson->acmpId));
        $response;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objId = null;
            if (isset($dataJson->acmpId)) {
                AmsAlLogger::log($mn, "Update acmpId =" . $dataJson->acmpId);
                $objId = $dataJson->acmpId;
                // No update for maintenance will be supported, only delete and create new
                //AmsMaintenance::Update($dataJson, $conn, $mn, $logModel);
            } else {
                AmsAlLogger::log($mn, "Create acmpId ");
                $response = AmsMaintenance::Create($dataJson, $conn, $mn, $logModel);
                //$dataJson->acmpId = $objId;
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function Table($params) {
        $mn = "AmsMaintenance::Table()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //--
            $sql = "SELECT mp.acmp_id as acmpId,
                    mp.ac_id as acId,
                    mp.amt_id as amtId,
                    mt.amt_name as amtName,
                    mt.time_h as mtnTimeH,
                    mp.description,
                    mp.ap_id as apId,
                    mp.is_home_ap as isHomeAp,
                    mp.price,
                    mp.al_id as alId,
                    mp.st_time as startTime,
                    mp.end_time as endTime,
                    mp.tr_id as trId,
                    mp.processed,
                    mp.adate
                FROM ams_ac.ams_aircraft_maintenance_plan mp
                join ams_ac.cfg_aircraft_maintenance_type mt on mt.amt_id = mp.amt_id ";

            $sqlWhere = "";

            if (isset($params->alId) && strlen($params->alId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND mp.al_id =" . $params->alId . " ";
                } else {
                    $sqlWhere = " WHERE mp.al_id =" . $params->alId . " ";
                }
            }
            if (isset($params->apId) && strlen($params->apId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND mp.ap_id =" . $params->arrApId . " ";
                } else {
                    $sqlWhere = " WHERE mp.ap_id =" . $params->arrApId . " ";
                }
            }
            if (isset($params->acId) && strlen($params->acId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND mp.ac_id =" . $params->depApId . " ";
                } else {
                    $sqlWhere = " WHERE mp.ac_id =" . $params->depApId . " ";
                }
            }

            if (isset($params->qry_filter) && strlen($params->qry_filter) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND (mp.amt_name like '%" . $params->qry_filter . "%' OR ";
                    $sqlWhere .= " mp.description like '%" . $params->qry_filter . "%' ) ";
                } else {
                    $sqlWhere .= " WHERE (mp.amt_name like '%" . $params->qry_filter . "%' OR ";
                    $sqlWhere .= " mp.description like '%" . $params->qry_filter . "%' ) ";
                }
            }

            $sqlOrder = "";
            if (isset($params->qry_orderCol)) {
                $sqlOrder .= " order by " . $params->qry_orderCol . " " . ($params->qry_isDesc ? "desc" : " asc");
            } else {
                $sqlOrder .= "order by startTime desc ";
            }
            $sql .= $sqlWhere . $sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("flnrs", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_aircraft_maintenance_plan mp
                    join ams_ac.cfg_aircraft_maintenance_type mt on mt.amt_id = mp.amt_id 
                     " . (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function TableH($params) {
        $mn = "AmsMaintenance::Table()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //--
            $sql = "SELECT mp.acmp_id as acmpId,
                    mp.ac_id as acId,
                    mp.amt_id as amtId,
                    mt.amt_name as amtName,
                    mt.time_h as mtnTimeH,
                    mp.description,
                    mp.ap_id as apId,
                    mp.is_home_ap as isHomeAp,
                    mp.price,
                    mp.al_id as alId,
                    mp.st_time as startTime,
                    mp.end_time as endTime,
                    mp.tr_id as trId,
                    mp.processed,
                    mp.adate
                FROM ams_ac.ams_aircraft_maintenance_plan_h mp
                join ams_ac.cfg_aircraft_maintenance_type mt on mt.amt_id = mp.amt_id ";

            $sqlWhere = "";

            if (isset($params->alId) && strlen($params->alId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND mp.al_id =" . $params->alId . " ";
                } else {
                    $sqlWhere = " WHERE mp.al_id =" . $params->alId . " ";
                }
            }
            if (isset($params->apId) && strlen($params->apId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND mp.ap_id =" . $params->arrApId . " ";
                } else {
                    $sqlWhere = " WHERE mp.ap_id =" . $params->arrApId . " ";
                }
            }
            if (isset($params->acId) && strlen($params->acId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND mp.ac_id =" . $params->depApId . " ";
                } else {
                    $sqlWhere = " WHERE mp.ac_id =" . $params->depApId . " ";
                }
            }

            if (isset($params->qry_filter) && strlen($params->qry_filter) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND (mp.amt_name like '%" . $params->qry_filter . "%' OR ";
                    $sqlWhere .= " mp.description like '%" . $params->qry_filter . "%' ) ";
                } else {
                    $sqlWhere .= " WHERE (mp.amt_name like '%" . $params->qry_filter . "%' OR ";
                    $sqlWhere .= " mp.description like '%" . $params->qry_filter . "%' ) ";
                }
            }

            $sqlOrder = "";
            if (isset($params->qry_orderCol)) {
                $sqlOrder .= " order by " . $params->qry_orderCol . " " . ($params->qry_isDesc ? "desc" : " asc");
            } else {
                $sqlOrder .= "order by startTime desc ";
            }
            $sql .= $sqlWhere . $sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("flnrs", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_aircraft_maintenance_plan_h mp
                    join ams_ac.cfg_aircraft_maintenance_type mt on mt.amt_id = mp.amt_id 
                     " . (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">

    static function Create($dataJson, $conn, $mn, $logModel) {
        $mn = "AmsMaintenance::MaintenancePlanCreate(" . $dataJson->acId . ", " . $dataJson->amtId . ")";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        $obj = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            if(isset($dataJson->cabinId)){
                $sql = "CALL ams_ac.sp_ac_mnt_plan_create_cabin(?, ?, ?)";
                $bound_params_r = ["iii", $dataJson->acId, $dataJson->cabinId, $dataJson->stTime];
            }else{
                $sql = "CALL ams_ac.sp_ac_mnt_plan_create(?, ?, ?)";
                $bound_params_r = ["iii", $dataJson->acId, $dataJson->amtId, $dataJson->stTime];
            }
            $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            //AmsAlLogger::log($mn, "count(result): ".count($result_r));
            if (count($result_r) > 0) {
                $acmpId = $result_r[0]["acmp_id"];
            }
            //AmsAlLogger::log($mn, "fleId: ".$fleId);
            $objArrJ = AmsMaintenance::SelectJson($acmpId, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $obj = json_decode(json_encode($objArrJ[0]));
            }

            $response->addData("maintenance", $obj);
            //$response->addData("affectedRows", $affected_rows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    static function Update($dataJson, $conn, $mn, $logModel) {

        $dataJson->routeId = AmsMaintenance::GetRouteId($dataJson, $conn, $mn, $logModel);

        $strSQL = "UPDATE ams_ac.ams_aircraft_maintenance_plan 
                    SET st_time=?, end_time=?
                    WHERE acmp_id = ?  ";

        $bound_params_r = ["ddi",
            ((!isset($dataJson->startTime)) ? null : $dataJson->startTime),
            ((!isset($dataJson->endTime)) ? null : $dataJson->endTime),
            $dataJson->acmpId
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->acmpId;
    }

    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = "SELECT mp.acmp_id as acmpId,
                        mp.ac_id as acId,
                        mp.amt_id as amtId,
                        mt.amt_name as amtName,
                        mt.time_h as mtnTimeH,
                        mp.description,
                        mp.ap_id as apId,
                        mp.is_home_ap as isHomeAp,
                        mp.price,
                        mp.al_id as alId,
                        mp.st_time as startTime,
                        mp.end_time as endTime,
                        mp.tr_id as trId,
                        mp.processed,
                        mp.adate
                    FROM ams_ac.ams_aircraft_maintenance_plan mp
                    join ams_ac.cfg_aircraft_maintenance_type mt on mt.amt_id = mp.amt_id
                    WHERE mp.acmp_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function Delete($id, $conn, $mn, $logModel) {

        $strSQL = "DELETE FROM ams_ac.ams_aircraft_maintenance_plan
                   WHERE acmp_id = ? ";

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted acmp_id =" . $id);

        return $id;
    }

    // </editor-fold>
}

class AmsDestination {

    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadByAirports($dataJson) {
        $mn = "AmsDestination::LoadByAirports()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsDestination::SelectJson($dataJson, $conn, $mn, $logModel);

            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="DB Methods">

    static function SelectJson($dataJson, $conn, $mn, $logModel) {

        $sql = "call ams_wad.get_cfg_airport_destination_row(?, ?)";
        $bound_params_r = ["ii",
            ((!isset($dataJson->depApId)) ? null : $dataJson->depApId),
            ((!isset($dataJson->arrApId)) ? null : $dataJson->arrApId),
        ];

        $ret_json_data = $conn->preparedSelect($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    // </editor-fold>
}
