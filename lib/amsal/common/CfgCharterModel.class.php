<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Dec 22, 2020 
 *  Filename            : CfgCharterModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of CfgCharterModel
 *
 * @author IZIordanov
 */
class CfgCharterModel {
    
   // <editor-fold defaultstate="collapsed" desc="Fields">

    public $charterId;
    public $depApId;
    public $arrApId;
    public $flightName;
    public $flightDescription; 
    
    public $payloadType;//DEFAULT 'C' COMMENT 'available values are E,B,F for pax and C for cargo '
    public $pax;
    public $cargoKg;
    public $payloadKg;
    public $price;
    public $distanceKm;
    public $periodDays;
    public $lastRun;
    public $nextRun;
    public $adate;
    public $depApCode;
    public $arrApCode;
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($id) {
        $mn = "CfgCharterModel::LoadById(".$id.")";
        AmsAlLogger::logBegin($mn);
        $response = new CfgCharterModel();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = CfgCharterModel::SelectJson($id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "CfgCharterModel::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsAlLogger::log($mn, " flightName = " . $dataJson->flightName);
        $response = new CfgCharterModel();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $id = null;
            if(isset($dataJson->charterId)){
               //AmsAlLogger::log($mn, "Update  charterId =" . $dataJson->charterId);
               $id = $dataJson->charterId;
               CfgCharterModel::Update($dataJson, $conn, $mn, $logModel);

            } else{
                //AmsAlLogger::log($mn, "Create CfgCharterModel");
                $id = CfgCharterModel::Create($dataJson, $conn, $mn, $logModel);
            }
            
            //AmsAlLogger::log($mn, " id =" . $id);
           if(isset($id)){
               $sql = "call ams_wad.sp_cfg_charter_calculate(?)";
                $bound_params_r = ["i", $id];
                $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
                
                $objArrJ = CfgCharterModel::SelectJson($id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Precallculate() {
        $mn = "CfgCharterModel::Save()";
        AmsAlLogger::logBegin($mn);
        //AmsAlLogger::log($mn, " flightName = " . $dataJson->flightName);
        $response = new CfgCharterModel();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
           
            $sql = "call ams_wad.sp_cfg_charter_update(?)";
             $bound_params_r = ["i", 1];
             $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function CfgCharterTable($params) {
        $mn = "CfgCharterModel::CfgCharterTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT ch.charter_id as charterId,
                    ch.d_ap_id as depApId,
                    ch.a_ap_id as arrApId,
                    ch.ch_grp_id as chGrpId,
                    ch.fl_name as flightName,
                    ch.fl_description as flightDescription,
                    ch.payload_type as payloadType,
                    ch.pax,
                    ch.cargo_kg as cargoKg,
                    ch.payload_kg as payloadKg,
                    ch.price, 
                    ch.distance_km as distanceKm,
                    d.max_flight_h as flightH,
                    d.min_rw_lenght_m as minRwLen,
                    ch.period_days as periodDays,
                    ch.last_run as lastRun,
                    ch.next_run as nextRun,
                    ch.adate, ch.pstart, ch.pend,
                    IFNULL(dep.ap_iata, dep.ap_icao) as depApCode,
                    IFNULL(arr.ap_iata, arr.ap_icao) as arrApCode
                    FROM ams_wad.cfg_charter ch
                    join ams_wad.cfg_charter_group g on g.ch_grp_id = ch.ch_grp_id
                    join ams_wad.cfg_airport arr on arr.ap_id = ch.a_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = ch.d_ap_id 
                    left join ams_wad.cfg_airport_destination d on d.dep_ap_id = ch.d_ap_id and d.arr_ap_id = ch.a_ap_id ";
            
            
            $sqlWhere="";
            
            if(isset($params->depApId) && strlen($params->depApId)>0){
                $sqlWhere = " WHERE ch.d_ap_id = ".$params->depApId." ";
            } else if(isset($params->arrApId) && strlen($params->arrApId)>0){
                $sqlWhere = " WHERE ch.a_ap_id = ".$params->arrApId." ";
            } 
            
             if(isset($params->chGrpId) && strlen($params->chGrpId)>0){
                 if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere = " AND ch.ch_grp_id = ".$params->chGrpId." ";
                 } else{
                     $sqlWhere = " WHERE ch.ch_grp_id = ".$params->chGrpId." ";
                 }
            } 
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (ch.fl_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or IFNULL(arr.ap_iata, arr.ap_icao) like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or IFNULL(dep.ap_iata, dep.ap_icao) like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or ch.payload_type like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere = " WHERE (ch.fl_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or IFNULL(arr.ap_iata, arr.ap_icao) like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or IFNULL(dep.ap_iata, dep.ap_icao) like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or ch.payload_type like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = "";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by depApCode, ch.charter_id desc ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("charters", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_wad.cfg_charter ch
                    join ams_wad.cfg_airport arr on arr.ap_id = ch.a_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = ch.d_ap_id ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
            
            
            $sql = "SELECT fl_name as code, fl_description as note 
                    FROM ams_wad.cfg_charter
                    where 1=?
                    group by fl_name" ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("charterCodes", $ret_json_data);
            
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function AmsCharterTable($params) {
        $mn = "CfgCharterModel::AmsCharterTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT ch.ch_id as chId,
                    ch.charter_id as charterId,
                    ch.d_ap_id as depApId,
                    ch.a_ap_id as arrApId,
                    ch.fl_name as flightName,
                    ch.fl_description as flightDescription,
                    ch.payload_type as payloadType,
                    ch.pax,
                    ch.cargo_kg as cargoKg,
                    ch.payload_kg as payloadKg,
                    ch.price, 
                    ch.distance_km as distanceKm,
                    d.max_flight_h as flightH,
                    d.min_rw_lenght_m as minRwLen,
                    ch.completed,
                    ch.pax_completed as paxCompleated,
                    ch.cargo_kg_compleated as cargoKgCompleated,
                    ch.payload_kg_completed as payloadKgCompleated,
                    ch.price_completed as priceCompleted,
                    ch.expiration,
                    ch.adate,
                    IFNULL(dep.ap_iata, dep.ap_icao) as depApCode,
                    IFNULL(arr.ap_iata, arr.ap_icao) as arrApCode,
                    round(ch.payload_kg_completed/ch.payload_kg,2) as completedPct
                    FROM ams_ac.ams_charter ch 
                    join ams_wad.cfg_airport arr on arr.ap_id = ch.a_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = ch.d_ap_id
                    left join ams_wad.cfg_airport_destination d on d.dep_ap_id = ch.d_ap_id and d.arr_ap_id = ch.a_ap_id ";
            
            
            $sqlWhere="";
            if(isset($params->depApId) && strlen($params->depApId)>0){
                $sqlWhere = " WHERE ch.d_ap_id = ".$params->depApId." ";
            } else if(isset($params->arrApId) && strlen($params->arrApId)>0){
                $sqlWhere = " WHERE ch.a_ap_id = ".$params->arrApId." ";
            } 
            
            if(isset($params->completed) && strlen($params->completed)>0){
                 if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND ch.completed = '%".$params->completed." ";
                 } else{
                     $sqlWhere = " WHERE ch.completed = ".$params->completed." ";
                 }
            }
            
            if(isset($params->payloadType) && strlen($params->payloadType)>0){
                 if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND ch.payload_type = '%".$params->payloadType." ";
                 } else{
                     $sqlWhere = " WHERE ch.payload_type = ".$params->payloadType." ";
                 }
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (ch.fl_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or IFNULL(arr.ap_iata, arr.ap_icao) like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or ch.payload_type like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere = " WHERE (ch.fl_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or IFNULL(arr.ap_iata, arr.ap_icao) like '%".$params->qry_filter."%' )";
                    $sqlWhere .= " or ch.payload_type like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = "";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by depApCode, ch.charter_id desc ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("charters", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_charter ch
                    join ams_wad.cfg_airport arr on arr.ap_id = ch.a_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = ch.d_ap_id ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    // </editor-fold>
  

    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function SelectJson($id, $conn, $mn, $logModel){
        
        $sql = "SELECT ch.charter_id as charterId,
                ch.d_ap_id as depApId,
                ch.a_ap_id as arrApId,
                ch.ch_grp_id as chGrpId,
                ch.fl_name as flightName,
                ch.fl_description as flightDescription,
                ch.payload_type as payloadType,
                ch.pax,
                ch.cargo_kg as cargoKg,
                ch.payload_kg as payloadKg,
                ch.price, 
                ch.distance_km as distanceKm,
                d.max_flight_h as flightH,
                d.min_rw_lenght_m as minRwLen,
                ch.period_days as periodDays,
                ch.last_run as lastRun,
                ch.next_run as nextRun,
                ch.adate, ch.pstart, ch.pend,
                IFNULL(dep.ap_iata, dep.ap_icao) as depApCode,
                IFNULL(arr.ap_iata, arr.ap_icao) as arrApCode
                FROM ams_wad.cfg_charter ch
                join ams_wad.cfg_airport arr on arr.ap_id = ch.a_ap_id
                join ams_wad.cfg_airport dep on dep.ap_id = ch.d_ap_id
                left join ams_wad.cfg_airport_destination d on d.dep_ap_id = ch.d_ap_id and d.arr_ap_id = ch.a_ap_id
                WHERE ch.charter_id = ? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_wad.cfg_charter 
            ( d_ap_id, a_ap_id, ch_grp_id, fl_name, fl_description,
            payload_type, pax, cargo_kg, period_days, next_run, pstart, pend)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["iiisssiiisss",
             ((!isset($dataJson->depApId)) ? null : $dataJson->depApId),
            ((!isset($dataJson->arrApId)) ? null : $dataJson->arrApId),
            ((!isset($dataJson->chGrpId)) ? null : $dataJson->chGrpId),
            ((!isset($dataJson->flightName)) ? null : $dataJson->flightName),
            ((!isset($dataJson->flightDescription)) ? null : $dataJson->flightDescription),
            ((!isset($dataJson->payloadType)) ? null : $dataJson->payloadType),
            ((!isset($dataJson->pax)) ? null : $dataJson->pax),
            ((!isset($dataJson->cargoKg)) ? null : $dataJson->cargoKg),
            ((!isset($dataJson->periodDays)) ? null : $dataJson->periodDays),
            ((!isset($dataJson->nextRun)) ? null : DateTimeDbStr($dataJson->nextRun)),
            ((!isset($dataJson->pstart)) ? null : DateTimeDbStr($dataJson->pstart)),
            ((!isset($dataJson->pend)) ? null : DateTimeDbStr($dataJson->pend)),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_wad.cfg_charter
                    SET d_ap_id=?,   a_ap_id=?, ch_grp_id=?,
                    fl_name=?,  fl_description=?, 
                    payload_type=?,  pax=?,  cargo_kg=?, 
                    period_days=?, next_run=?, pstart=?, pend=?
                    WHERE charter_id = ? " ;

        $bound_params_r = ["iiisssiiisssi",
             ((!isset($dataJson->depApId)) ? null : $dataJson->depApId),
            ((!isset($dataJson->arrApId)) ? null : $dataJson->arrApId),
            ((!isset($dataJson->chGrpId)) ? null : $dataJson->chGrpId),
            ((!isset($dataJson->flightName)) ? null : $dataJson->flightName),
            ((!isset($dataJson->flightDescription)) ? null : $dataJson->flightDescription),
            ((!isset($dataJson->payloadType)) ? null : $dataJson->payloadType),
            ((!isset($dataJson->pax)) ? null : $dataJson->pax),
            ((!isset($dataJson->cargoKg)) ? null : $dataJson->cargoKg),
            ((!isset($dataJson->periodDays)) ? null : $dataJson->periodDays),
            ((!isset($dataJson->nextRun)) ? null :  DateTimeDbStr($dataJson->nextRun)),
            ((!isset($dataJson->pstart)) ? null : DateTimeDbStr($dataJson->pstart)),
            ((!isset($dataJson->pend)) ? null : DateTimeDbStr($dataJson->pend)),
            ((!isset($dataJson->charterId)) ? null : $dataJson->charterId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        
        $logModel->logDebug('Update', " affectedRows: " . $affectedRows);
        try{
            if(isset($dataJson->pstart) && isset($dataJson->pend) &&
               isset($dataJson->chGrpId) && isset($dataJson->updateGrpPeriod)){
                 $strSQL = "update ams_wad.cfg_charter
                        set pstart = ?, pend = ? 
                        where ch_grp_id=?;" ;

                $bound_params_r = ["ssi",
                     ((!isset($dataJson->pstart)) ? null : DateTimeDbStr($dataJson->pstart)),
                    ((!isset($dataJson->pend)) ? null : DateTimeDbStr($dataJson->pend)),
                    ((!isset($dataJson->chGrpId)) ? null : $dataJson->chGrpId),
                ];

                $updatedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
                $logModel->logDebug('Update', " updatedRows: " . $updatedRows);
            }
        }catch (Exception $ex) {
            $logModel->logError('Update', ex);
        }
        
        return $dataJson->charterId;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM ams_wad.cfg_charter
                   WHERE charter_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted charter_id =" . $id);
                    
        return $id;
    }
    
    public static function UsersTable($params) {
        $mn = "CfgCharterModel::UsersTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT u.user_id as userId, u.user_name as userName,
                    u.user_email as userEmail, u.user_pwd as userPwd, u.user_role as userRole, 
                    u.is_receive_emails as isReceiveEmails, u.ip_address as ipAddress,
                    u.adate, u.udate
                    FROM ams_al.ams_user u
                     ";
            
            
            $sqlWhere="";
            if(isset($params->userRole) && strlen($params->userRole)>0){
                $sqlWhere = " WHERE u.user_role = ".$params->userRole." ";
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (u.user_email like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or u.user_name like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere = " WHERE (u.user_email like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or u.user_name like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = "";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by u.".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by u.user_email, u.user_name, u.user_id ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("usersList", $ret_json_data);
            
            $sql = "SELECT count(*) as total_rows
                    FROM ams_al.ams_user u ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("totals", $ret_json_data);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
}

