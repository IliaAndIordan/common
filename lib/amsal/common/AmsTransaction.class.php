<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Oct 8, 2021 
 *  Filename          : AmsTransaction.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of AmsTransaction
 *
 * @author IZIordanov
 */
class AmsTransaction {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $trId;
    public $trTypeId;
    public $amount;
    public $description;
    public $paymentTime;
    public $posted;
    public $fromBankId;
    public $fromAlId;
    public $toBankId;
    public $apId;
    public $acId;
    public $flId;
   
    
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadByAirlineId($id) {
        $mn = "AmsTransaction::LoadByAirlineId(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsTransaction::SelectByAirlineJson($id, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "AmsTransaction::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        AmsAlLogger::log($mn, " alName = " . $dataJson->alName);
        $response;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $trId = null;
            if(isset($dataJson->trId)){
               AmsAlLogger::log($mn, "Update  trId =" . $dataJson->trId);
               $trId = $dataJson->trId;
               AmsTransaction::Update($dataJson, $conn, $mn, $logModel);

            } else{
                AmsAlLogger::log($mn, "Create airline");
                $trId = AmsTransaction::Create($dataJson, $conn, $mn, $logModel);
            }
            
            AmsAlLogger::log($mn, " trId =" . $trId);
            $response = $trId;
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function AmsTransactionTable($params) {
        $mn = "AmsTransaction::AmsTransactionTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT tre.tr_id as trId,  tre.tr_type_id as trTypeId,
                        Round(  ifnull(tre.amount*if(t.al_id = ?,1,-1), 0),2) as amount, 
                        tre.description, tre.payment_time as paymentTime, tre.posted,
                        tre.ap_id as apId, tre.aircraft_id as acId, tre.fl_id as flId,
                        f.bank_id as fromBankId, f.al_id as fromAlId, t.bank_id as toBankId, t.al_id as toAlId 
                    FROM  ams_al.ams_bank_transaction tre
                    join ams_al.ams_bank f on f.bank_id = tre.bank_id_from
                    join ams_al.ams_bank t on t.bank_id = tre.bank_id_to
                    where tre.posted = 1 and (f.al_id = ? or t.al_id = ?)
                    order by trId desc ";
            
            $sql .= " LIMIT ? OFFSET ? ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["iiiii", $params->alId,  $params->alId, $params->alId, $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("transactions", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM  ams_al.ams_bank_transaction tre
                    join ams_al.ams_bank f on f.bank_id = tre.bank_id_from
                    join ams_al.ams_bank t on t.bank_id = tre.bank_id_to
                    where tre.posted = 1 and (f.al_id = ? or t.al_id = ?) " ;
            $bound_params_r = ["ii", $params->alId,  $params->alId];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
     public static function AmsTransactionTableNoFlight($params) {
        $mn = "AmsTransaction::AmsTransactionTableNoFlight()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //--
            $sql = "SELECT tr.tr_id as trId, 
                    tr.tr_type_id as trTypeId, tr.payment_time as paymentTime,
                    ROUND(IF(tr.bank_id_to = 1, -1, 1) * ifnull(tr.amount,0),2) as amount,
                    tr.description, tr.posted,
                    tr.ap_id as apId, tr.aircraft_id as acId, tr.fl_id as flId,
                    IF(tr.bank_id_to = 1, bf.al_id, bt.al_id) as alId, 
                    IF(tr.bank_id_to = 1, 1, 0) as isExpenses, 
                    trt.tr_name as trName
                    FROM ams_al.ams_bank_transaction tr
                    JOIN ams_al.cfg_transaction_type trt on trt.tr_type_id = tr.tr_type_id
                    JOIN ams_al.ams_bank bf on bf.bank_id = tr.bank_id_from
                    JOIN ams_al.ams_bank bt on bt.bank_id = tr.bank_id_to
                        ";
            $sqlWhere=" where tr.posted = 1 and  trt.tr_group <> 'Flight' ";
            
            if(isset($params->alId) && strlen($params->alId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " and IF(tr.bank_id_to = 1, bf.al_id, bt.al_id) = ".$params->alId." ";
                }
                else{
                    $sqlWhere = " WHERE and IF(tr.bank_id_to = 1, bf.al_id, bt.al_id) = ".$params->alId." ";
                }
            }
            if(isset($params->acId) && strlen($params->acId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND tr.aircraft_id =".$params->acId." ";
                }
                else{
                    $sqlWhere = " WHERE tr.aircraft_id =".$params->acId." ";
                }
            }
            
            $sqlOrder = " order by tr.tr_id desc ";
            /*
            if(isset($params->qry_orderCol)){
                $sqlOrder .= ", ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }*/
            
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("transactions", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_al.ams_bank_transaction tr
                    JOIN ams_al.cfg_transaction_type trt on trt.tr_type_id = tr.tr_type_id
                    JOIN ams_al.ams_bank bf on bf.bank_id = tr.bank_id_from
                    JOIN ams_al.ams_bank bt on bt.bank_id = tr.bank_id_to
                     ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function AmsTransactionSumByDateLastWeek($alId) {
        $mn = "AmsTransaction::AmsTransactionSumByDateLastWeek()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT   
                        Round(  sum(ifnull(tre.amount*if(f.al_id = ?,-1,0), 0)),2) as expences, 
                        Round(  sum(ifnull(tre.amount*if(t.al_id = ?,1,0), 0)),2) as income, 
                        Round(  sum(ifnull(tre.amount*if(t.al_id = ?,1,0), 0)) + sum(ifnull(tre.amount*if(f.al_id = ?,-1,0), 0)),2) as revenue, 
                        max(DATE(tre.payment_time)) as trDate, 1 as alId, weekday(tre.payment_time) as weekday
                    FROM ams_al.ams_bank_transaction tre 
                        join ams_al.ams_bank f on f.bank_id = tre.bank_id_from
                        join ams_al.ams_bank t on t.bank_id = tre.bank_id_to
                    where tre.posted = 1 and (f.al_id = ? or t.al_id=?) and 
                    DATE(tre.payment_time) > DATE_SUB(CURDATE(), INTERVAL 8 day)
                    group by DATE(tre.payment_time), weekday(tre.payment_time)
                    order by trDate  ";
            
            
            $bound_params_r = ["iiiiii", $alId,  $alId, $alId, $alId, $alId, $alId];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("trSumList", $ret_json_data);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function AmsTransactionSumToday($alId) {
        $mn = "AmsTransaction::AmsTransactionSumToday()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT   tt.tr_type_id as trTypeId, tt.tr_name as trName,
                        Round(  sum(ifnull(tre.amount,0)*if(t.al_id = ?,1,if(f.al_id = ?,-1,0))),2) as amount, 
                        max(DATE(tre.payment_time)) as trDate, ? as alId
                    FROM  ams_al.cfg_transaction_type tt
                        left join ams_al.ams_bank_transaction tre on tre.tr_type_id = tt.tr_type_id
                        join ams_al.ams_bank f on f.bank_id = tre.bank_id_from
                        join ams_al.ams_bank t on t.bank_id = tre.bank_id_to
                    where tre.posted = 1 and (f.al_id = ? or t.al_id=?) and DATE(tre.payment_time) = CURDATE()
                    group by tt.tr_type_id, DATE(tre.payment_time)
                    order by tt.tr_group, tt.tr_type_id;  ";
            
            
            $bound_params_r = ["iiiii", $alId,  $alId, $alId, $alId, $alId];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("trTypeSum", $ret_json_data);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function AmsStTransactionSumByTypeAndDayTable($params) {
        $mn = "AmsTransaction::AmsStTransactionSumByTypeAndDayTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT tr_date as trDate, bank_id as bankId, al_id as alId,
                sum(if(tr_type_id = 1,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as AirlineCreate,
                sum(if(tr_type_id = 2,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as AircraftPurchase,
                sum(if(tr_type_id = 3,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as Boarding,
                sum(if(tr_type_id = 4,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as CargoLoad,
                sum(if(tr_type_id = 5,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as Disembarking,
                sum(if(tr_type_id = 6,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as CargoUnload,
                sum(if(tr_type_id = 7,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as Insurance,
                sum(if(tr_type_id = 8,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as LandingAndTaxyToGate,
                sum(if(tr_type_id = 9,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as Maintenance,
                sum(if(tr_type_id = 10,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as Refuling,
                sum(if(tr_type_id = 11,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as AirportTaxes,
                sum(if(tr_type_id = 12,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as TaxyAndTakeoff,
                sum(if(tr_type_id = 13,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as FlightCrew,
                sum(if(tr_type_id = 14,  (ifnull(income,0) - ifnull(expences,0)), 0 )) as FlightIncome
               FROM ams_al.st_bank_transaction_day
               where al_id = ?
               group by tr_date, bank_id, al_id
               order by tr_date desc";
            
            $sql .= " LIMIT ? OFFSET ? ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["iii", $params->alId, $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("transactionsday", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_al.st_bank_transaction_day
                    where al_id = ?
                    group by tr_date, bank_id, al_id
                    order by tr_date desc " ;
            $bound_params_r = ["i", $params->alId];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    } 
    
    public static function AmsStRevenuesDayTable($params) {
        $mn = "AmsAirport::AmsStRevenuwDayTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT id as rdId, bank_id as bankId, al_id as alId,  
                tr_date as rdDate,   expences, income, revenue
                FROM ams_al.st_bank_revenue_day ";
            
            
            $sqlWhere  = null;
            if(isset($params->alId) && strlen($params->alId)>0){
                $sqlWhere = " WHERE al_id = ".$params->alId." ";
            }
           
            
            $sqlOrder = " order by id desc ";
           
            $sql .= (isset($sqlWhere)?$sqlWhere:"").$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("revenues", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_al.st_bank_revenue_day ".
                    (isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Airline Stat">
    
    public static function AmsTransactionAlStWeekTable($params) {
        $mn = "AmsTransaction::AmsTransactionAlStWeekTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT trs.tr_week AS trWeek, sum(revenue) as revenue
                    from ams_ac.st_ac_revenue_day_tr trs 
                    group by trs.tr_week
                    order by trs.tr_week desc  limit ? ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["i", 8];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("weeks", $ret_json_data);
            //$date = date('Y-m-d H:i:s');
            if(isset($params->trweek)){
                $trweek = $params->trweek;
            } else{
                $trweek = date("YW", date('Y-m-d H:i:s'));
                if(isset($ret_json_data) && count($ret_json_data)>0){
                   $last_week = json_decode(json_encode($ret_json_data[0]));
                   if(isset($last_week) && isset($last_week->trWeek)){
                       $trweek = $last_week->trWeek;
                   }
                }
            }
            AmsAlLogger::log($mn, " trweek= " . $trweek . " ");
            $sql = "SELECT  tt.tr_type_id as trTypeId, tt.tr_name as trName, 
            sum(ifnull(mon, 0)) as mon, sum(ifnull(tue, 0)) as tue,
            sum(ifnull(wed, 0)) as wed, sum(ifnull(thu, 0)) as thu,
            sum(ifnull(fri, 0)) as fri, sum(ifnull(sat, 0)) as sat,
            sum(ifnull(sun, 0)) as sun
            from ams_al.cfg_transaction_type tt
            left join (
            select t.tr_type_id, 
            case when wd_id = 1 then price end as 'mon',
            case when wd_id = 2 then price end as 'tue',
            case when wd_id = 3 then price end as 'wed',
            case when wd_id = 4 then price end as 'thu',
            case when wd_id = 5 then price end as 'fri',
            case when wd_id = 6 then price end as 'sat',
            case when wd_id = 7 then price end as 'sun'
            from(
                    select DAYOFWEEK(trs.tr_date) as wd_id,  trs.tr_type_id, sum(ifnull(trs.revenue,0)) as price
                    from ams_ac.st_ac_revenue_day_tr trs 
                    where trs.al_id = ? and trs.tr_week = ?
                    group by trs.tr_type_id,  DAYOFWEEK(trs.tr_date)
                    order by wd_id, tr_type_id
            ) t
            ) b on b.tr_type_id = tt.tr_type_id
            group by tt.tr_type_id " ;
            $bound_params_r = ["ii", $params->alId,  $trweek];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("transStat", $ret_json_data);
            $response->addData("week", $trweek);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
     
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_al.ams_bank_transaction
            (bank_id_from, bank_id_to, tr_type_id,
            amount, payment_time, description, ap_id,
            aircraft_id, fl_id, posted)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["iiidssiiii",
             ((!isset($dataJson->fromBankId)) ? null : $dataJson->fromBankId),
            ((!isset($dataJson->toBankId)) ? null : $dataJson->toBankId),
            ((!isset($dataJson->trTypeId)) ? null : $dataJson->trTypeId),
            ((!isset($dataJson->amount)) ? null : $dataJson->amount),
            ((!isset($dataJson->paymentTime)) ? null : $dataJson->paymentTime),
            ((!isset($dataJson->description)) ? null : $dataJson->description),
            ((!isset($dataJson->apId)) ? null : $dataJson->apId),
            ((!isset($dataJson->acId)) ? null : $dataJson->acId),
            ((!isset($dataJson->flId)) ? null : $dataJson->flId),
            0
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_al.ams_bank_transaction 
                    SET bank_id_from=?, bank_id_to = ?, tr_type_id = ?,
                    amount = ?, payment_time = ?, description = ?, ap_id = ?,
                    aircraft_id = ?, fl_id = ?, posted = ? 
                    WHERE tr_id = ? " ;

        $bound_params_r = ["iiidssiiiii",
            ((!isset($dataJson->fromBankId)) ? null : $dataJson->fromBankId),
            ((!isset($dataJson->toBankId)) ? null : $dataJson->toBankId),
            ((!isset($dataJson->trTypeId)) ? null : $dataJson->trTypeId),
            ((!isset($dataJson->amount)) ? null : $dataJson->amount),
            ((!isset($dataJson->paymentTime)) ? null : $dataJson->paymentTime),
            ((!isset($dataJson->description)) ? null : $dataJson->description),
            ((!isset($dataJson->apId)) ? null : $dataJson->apId),
            ((!isset($dataJson->acId)) ? null : $dataJson->acId),
            ((!isset($dataJson->flId)) ? null : $dataJson->flId),
            ((!isset($dataJson->posted)) ? null : $dataJson->posted),
            ($dataJson->trId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->userId;
    }
    
    static function SelectByAirlineJson($id, $conn, $mn, $logModel){
        
        $sql = "SELECT tre.tr_id as trId,  tre.tr_type_id as trTypeId,
                    Round(  ifnull(tre.amount*if(t.al_id = ?,1,-1), 0),2) as amount, 
                    tre.description, tre.payment_time as paymentTime, tre.posted,
                    tre.ap_id as apId, tre.aircraft_id as acId, tre.fl_id as flId,
                    f.bank_id as fromBankId, f.al_id as fromAlId, t.bank_id as toBankId, t.al_id as toAlId 
                FROM  ams_al.ams_bank_transaction tre
                join ams_al.ams_bank f on f.bank_id = tre.bank_id_from
                join ams_al.ams_bank t on t.bank_id = tre.bank_id_to
                where tre.posted = 1 and (f.al_id = ? or t.al_id=?)
                order by trId desc " ;

        $bound_params_r = ["iii",$id, $id, $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE ams_al.ams_bank_transaction
                   WHERE tr_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, "deleted alId =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}
