<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Jan 7, 2021 
 *  Filename            : ManufacturerModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of Country
 *
 * @author IZIordanov
 */
class Manufacturer {

    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $mfrId;
    public $mfrName;
    public $headquartier;
    public $notes;
    public $apId;
    public $iata;
    public $logoImg;
    public $wikiUrl;
    public $homeUrl;
    public $amsStatus;
    public $udate;

    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($mfr_id) {
        $mn = "Manufacturer::LoadById(" . $mfr_id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = Manufacturer::SelectJson($mfr_id, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function Save($dataJson) {
        $mn = "Manufacturer::Save()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        //AmsAlLogger::log($mn, " mfr_id = " . $dataJson->mfrId);
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $mfr_id = null;
            if (isset($dataJson->mfrId)) {
                AmsAlLogger::log($mn, "Update  mfr_id =" . $dataJson->mfrId);
                $mfr_id = $dataJson->mfrId;
                $mfr_id = Manufacturer::Update($dataJson, $conn, $mn, $logModel);
            } else {
                AmsAlLogger::log($mn, "Create mfr ");
                $mfr_id = Manufacturer::Create($dataJson, $conn, $mn, $logModel);
            }

            AmsAlLogger::log($mn, " mfr_id =" . $mfr_id);
            if (isset($mfr_id)) {
                $objArrJ = Manufacturer::SelectJson($mfr_id, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = json_decode(json_encode($objArrJ[0]));
                }
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function ManufacturerTable($params) {
        $mn = "Manufacturer::ManufacturerTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT m.mfr_id as mfrId,
                    m.mfr_name as mfrName,
                    m.mfr_headquartier as headquartier,
                    m.mfr_notes as notes,
                    m.ap_id as apId,
                    m.mfr_iata as iata,
                    m.mfr_logo as logoImg,
                    m.mfr_wikilink as wikiUrl,
                    m.mfr_web as homeUrl,
                    m.ams_status_id as amsStatus,
                    m.udate, 
                    count(distinct(mac.mac_id)) as macCount,
                    count(distinct(a.ac_id)) as acCount
                    FROM ams_ac.cfg_manufacturer m
                    left join ams_ac.cfg_mac mac on mac.mfr_id = m.mfr_id
                    left join ams_ac.ams_aircraft a on a.mac_id = mac.mac_id
                     ";

            $sqlWhere = "";
            if (isset($params->apId) && strlen($params->apId) > 0) {
                $sqlWhere = " WHERE m.ap_id = " . $params->apId . " ";
            } else if (isset($params->amsStatus) && strlen($params->amsStatus) > 0) {
                $sqlWhere = " WHERE m.ams_status_id = " . $params->amsStatus . " ";
            }

            if (isset($params->qry_filter) && strlen($params->qry_filter) > 1) {
                if (isset($sqlWhere)) {
                    $sqlWhere .= " AND ( m.mfrName like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or m.mfr_notes like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or m.mfr_headquartier like '%" . $params->qry_filter . "%' )";
                } else {
                    $sqlWhere .= " WHERE ( m.mfrName like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or m.mfr_notes like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or m.mfr_headquartier like '%" . $params->qry_filter . "%' )";
                }
            }
            $sqlOrder = " group by m.mfr_id ";
            if (isset($params->qry_orderCol)) {
                $sqlOrder .= " order by " . $params->qry_orderCol . " " . ($params->qry_isDesc ? "desc" : " asc") . ", mfrName";
            } else {
                $sqlOrder .= " order by mfrName, m.mfr_id ";
            }
            $sql .= $sqlWhere . $sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("manufacturers", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.cfg_manufacturer m
                    left join ams_ac.cfg_mac mac on mac.mfr_id = m.mfr_id
                    left join ams_ac.ams_aircraft a on a.mac_id = mac.mac_id  " . (isset($sqlWhere) && strlen($sqlWhere) > 2 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="DB Methods">

    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = " SELECT m.mfr_id as mfrId,
                m.mfr_name as mfrName,
                m.mfr_headquartier as headquartier,
                m.mfr_notes as notes,
                m.ap_id as apId,
                m.mfr_iata as iata,
                m.mfr_logo as logoImg,
                m.mfr_wikilink as wikiUrl,
                m.mfr_web as homeUrl,
                m.ams_status_id as amsStatus,
                m.udate, ap_id as apId
                FROM ams_ac.cfg_manufacturer m
                where m.mfr_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function Create($dataJson, $conn, $mn, $logModel) {

        $strSQL = "INSERT INTO ams_ac.cfg_manufacturer 
            (mfr_name, mfr_headquartier, mfr_notes, ap_id, mfr_iata, mfr_logo,
            mfr_wikilink, mfr_web, ams_status_id)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $bound_params_r = ["sssissssi",
            ((!isset($dataJson->mfrName)) ? null : $dataJson->mfrName),
            ((!isset($dataJson->headquartier)) ? null : $dataJson->headquartier),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->apId)) ? null : $dataJson->apId),
            ((!isset($dataJson->iata)) ? null : $dataJson->iata),
            ((!isset($dataJson->logoImg)) ? null : $dataJson->logoImg),
            ((!isset($dataJson->wikiUrl)) ? null : $dataJson->wikiUrl),
            ((!isset($dataJson->homeUrl)) ? null : $dataJson->homeUrl),
            ((!isset($dataJson->amsStatus)) ? null : $dataJson->amsStatus),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);

        return $id;
    }

    static function Update($dataJson, $conn, $mn, $logModel) {

        $strSQL = " UPDATE ams_ac.cfg_manufacturer
                        SET mfr_name=?,  mfr_headquartier=?, mfr_notes=?, 
                        ap_id=?, mfr_iata=?, mfr_logo=?,
                        mfr_wikilink=?, mfr_web=?, ams_status_id=?
                    WHERE mfr_id = ? ";

        $bound_params_r = ["sssissssii",
            ((!isset($dataJson->mfrName)) ? null : $dataJson->mfrName),
            ((!isset($dataJson->headquartier)) ? null : $dataJson->headquartier),
            ((!isset($dataJson->notes)) ? null : $dataJson->notes),
            ((!isset($dataJson->apId)) ? null : $dataJson->apId),
            ((!isset($dataJson->iata)) ? null : $dataJson->iata),
            ((!isset($dataJson->logoImg)) ? null : $dataJson->logoImg),
            ((!isset($dataJson->wikiUrl)) ? null : $dataJson->wikiUrl),
            ((!isset($dataJson->homeUrl)) ? null : $dataJson->homeUrl),
            ((!isset($dataJson->amsStatus)) ? 1 : $dataJson->amsStatus),
            ($dataJson->mfrId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->mfrId;
    }

    static function Delete($mfr_id, $conn, $mn, $logModel) {

        $strSQL = "DELETE FROM ams_ac.cfg_manufacturer
                   WHERE mfr_id = ? ";

        $bound_params_r = ["i", $mfr_id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted mfr_id =" . $id);

        return $id;
    }

    // </editor-fold>
}
