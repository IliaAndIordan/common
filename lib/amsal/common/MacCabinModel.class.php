<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Jan 7, 2021 
 *  Filename            : MacCabinModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of Country
 *
 * @author IZIordanov
 */
class MacCabin {

    // <editor-fold defaultstate="collapsed" desc="Fields">
    public $cabinId;
    public $macId;
    
    public $cName;
    public $seatE;
    public $seatEtypeId;
    public $seatB;
    public $seatBtypeId;
    public $seatF;
    public $seatFtypeId;
    public $seatCrew;
    public $cargoKg;
    public $confort;
    public $price;
    public $maxIFEId;
    public $isDefault;

    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($id) {
        $mn = "MacCabin::LoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = MacCabin::SelectJson($id, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = $objArrJ[0];
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function Save($dataJson) {
        $mn = "MacCabin::Save()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        AmsAlLogger::log($mn, " data = " . json_encode($dataJson));
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $cabin_id = null;
            if (isset($dataJson->cabinId)) {
                AmsAlLogger::log($mn, "Update  cabinId =" . $dataJson->cabinId);
                $cabin_id = $dataJson->cabinId;
                $cabin_id = MacCabin::Update($dataJson, $conn, $mn, $logModel);
            } else {
                AmsAlLogger::log($mn, "Create cabin_id ");
                $cabin_id = MacCabin::Create($dataJson, $conn, $mn, $logModel);
            }

            AmsAlLogger::log($mn, " cabin_id =" . $cabin_id);
            if (isset($cabin_id)) {
                $objArrJ = MacCabin::SelectJson($cabin_id, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = $objArrJ[0];
                }
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function MacCabinTable($params) {
        $mn = "MacCabin::MacCabinTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT c.cabin_id as cabinId, 
                    c.mac_id as macId,
                    c.cabin_name as cName,
                    c.seat_e_count as seatE,
                    c.seat_e_type_id as seatEtypeId,
                    c.seat_b_count as seatB,
                    c.seat_b_type_id as seatBtypeId,
                    c.seat_f_count as seatF,
                    c.seat_f_type_id as seatFtypeId,
                    c.seat_crew_count as seatCrew,
                    c.confort, price,
                    c.cargo_kg as cargoKg,
                    c.max_ife_id as maxIFEId,
                    c.default  as isDefault, 
                    ifnull(b.acCount, 0) as acCount
                    FROM ams_ac.cfg_mac_cabin c 
                    left join (select count(*) as acCount, cabin_id from ams_ac.ams_aircraft group by cabin_id) b on b.cabin_id = c.cabin_id ";

            if (isset($params->macId) && strlen($params->macId) > 0) {
                $sqlWhere = " WHERE c.mac_id = " . $params->macId . " ";
            }
            if (isset($params->cabinId) && strlen($params->cabinId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " and c.cabin_id = " . $params->cabinId . " ";
                } else {
                    $sqlWhere = " WHERE c.cabin_id = " . $params->cabinId . " ";
                }
            }

            if (isset($params->qry_filter) && strlen($params->qry_filter) > 1) {
                if (isset($sqlWhere)) {
                    $sqlWhere .= " AND ( c.cabin_name like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or c.cabin_name like '%" . $params->qry_filter . "%' )";
                } else {
                    $sqlWhere .= " WHERE ( c.cabin_name like '%" . $params->qry_filter . "%' ";
                    $sqlWhere .= " or c.cabin_name like '%" . $params->qry_filter . "%' )";
                }
            }
            $sqlOrder = "group by c.cabin_id ";
            if (isset($params->qry_orderCol)) {
                $sqlOrder .= " order by " . $params->qry_orderCol . " " . ($params->qry_isDesc ? "desc" : " asc") . ", cabinId";
            } else {
                $sqlOrder .= " order by seatF desc, seatB desc, seatE desc, cabinId ";
            }
            $sql .= $sqlWhere . $sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("cabins", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.cfg_mac_cabin c  " . (isset($sqlWhere) ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">

    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = " SELECT c.cabin_id as cabinId, 
                c.mac_id as macId,
                c.cabin_name as cName,
                c.seat_e_count as seatE,
                c.seat_e_type_id as seatEtypeId,
                c.seat_b_count as seatB,
                c.seat_b_type_id as seatBtypeId,
                c.seat_f_count as seatF,
                c.seat_f_type_id as seatFtypeId,
                c.seat_crew_count as seatCrew,
                c.cargo_kg as cargoKg,
                c.confort, price,
                c.max_ife_id as maxIFEId,
                c.default  as isDefault
                FROM ams_ac.cfg_mac_cabin c WHERE c.cabin_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function Create($dataJson, $conn, $mn, $logModel) {

        $strSQL = "INSERT INTO `ams_ac`.`cfg_mac_cabin`
            (`mac_id`,
            `cabin_name`,
            `seat_e_count`,
            `seat_e_type_id`,
            `seat_b_count`,
            `seat_b_type_id`,
            `seat_f_count`,
            `seat_f_type_id`,
            `seat_crew_count`,
            `confort`,
            `price`,
            `max_ife_id`,
            `default`)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        $bound_params_r = ["isiiiiiiiddii",
            ((!isset($dataJson->macId)) ? null : $dataJson->macId),
            ((!isset($dataJson->cName)) ? null : $dataJson->cName),
            ((!isset($dataJson->seatE)) ? null : $dataJson->seatE),
            ((!isset($dataJson->seatEtypeId)) ? null : $dataJson->seatEtypeId),
            ((!isset($dataJson->seatB)) ? null : $dataJson->seatB),
            ((!isset($dataJson->seatBtypeId)) ? null : $dataJson->seatBtypeId),
            ((!isset($dataJson->seatF)) ? null : $dataJson->seatF),
            ((!isset($dataJson->seatFtypeId)) ? null : $dataJson->seatFtypeId),
            ((!isset($dataJson->seatCrew)) ? null : $dataJson->seatCrew),
            ((!isset($dataJson->confort)) ? null : $dataJson->confort),
            
            ((!isset($dataJson->price)) ? null : $dataJson->price),
            ((!isset($dataJson->maxIFEId)) ? null : $dataJson->maxIFEId),
            ((!isset($dataJson->isDefault) || $dataJson->isDefault == 0) ? 0 : 1)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);
        
        $sql = "update ams_ac.cfg_mac_cabin c
                join ams_ac.cfg_mac m on m.mac_id = c.mac_id
                set c.mac_load_kg = m.load_kg where 1=? ";
        $bound_params_r = ["i", 1];
        $affectedRows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);

        return $id;
    }

    static function Update($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE ams_ac.cfg_mac_cabin
                    SET mac_id = ? , cabin_name = ?,
                    seat_e_count = ?, seat_e_type_id = ?,
                    seat_b_count = ?, seat_b_type_id = ?,
                    seat_f_count = ?, seat_f_type_id = ?,
                    seat_crew_count = ?, confort = ?,
                    price = ?, max_ife_id = ?, `default` = ?
                   WHERE cabin_id = ? ";

        $bound_params_r = [ "isiiiiiiiddiii",
            ((!isset($dataJson->macId)) ? null : $dataJson->macId),
            ((!isset($dataJson->cName)) ? null : $dataJson->cName),
            ((!isset($dataJson->seatE)) ? null : $dataJson->seatE),
            ((!isset($dataJson->seatEtypeId)) ? null : $dataJson->seatEtypeId),
            ((!isset($dataJson->seatB)) ? null : $dataJson->seatB),
            ((!isset($dataJson->seatBtypeId)) ? null : $dataJson->seatBtypeId),
            ((!isset($dataJson->seatF)) ? null : $dataJson->seatF),
            ((!isset($dataJson->seatFtypeId)) ? null : $dataJson->seatFtypeId),
            ((!isset($dataJson->seatCrew)) ? null : $dataJson->seatCrew),
            ((!isset($dataJson->confort)) ? null : $dataJson->confort),
            
            ((!isset($dataJson->price)) ? null : $dataJson->price),
            ((!isset($dataJson->maxIFEId)) ? null : $dataJson->maxIFEId),
            ((!isset($dataJson->isDefault) || $dataJson->isDefault == 0) ? 0 : 1),
            ($dataJson->cabinId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);

        $sql = "update ams_ac.cfg_mac_cabin c
                join ams_ac.cfg_mac m on m.mac_id = c.mac_id
                set c.mac_load_kg = m.load_kg where 1=? ";
        $bound_params_r = ["i", 1];
        $affectedRows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
        return $dataJson->cabinId;
    }

    static function Delete($cabinId, $conn, $mn, $logModel) {

        $strSQL = "DELETE FROM ams_ac.cfg_mac_cabin
                   WHERE cabin_id = ? ";

        $bound_params_r = ["i", $cabinId];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, "deleted cabin_id =" . $cabinId);

        return $cabinId;
    }

    // </editor-fold>
}
