<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Jan 7, 2021 
 *  Filename          : StateModel.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of Country
 *
 * @author IZIordanov
 */
class AmsState {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $stateId;
    public $stName;
    public $iso;
    public $countryId;
    public $wikiUrl;
    public $amsStatus;
    public $wadStatus;
    public $udate;
    
    public $cities;
    public $airports;
    public $apActive;
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($state_id) {
        $mn = "AmsState::LoadById(".$state_id.")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsState::SelectJson($state_id, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($dataJson) {
        $mn = "AmsState::Save()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        AmsAlLogger::log($mn, " iso = " . $dataJson->iso);
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $state_id = null;
            if(isset($dataJson->stateId)){
               //AmsAlLogger::log($mn, "Update  state_id =" . $dataJson->stateId);
               $state_id = $dataJson->countryId;
               $state_id = AmsState::Update($dataJson, $conn, $mn, $logModel);

            } else{
                //AmsAlLogger::log($mn, "Create state");
                $state_id = AmsState::Create($dataJson, $conn, $mn, $logModel);
            }
            
            //AmsAlLogger::log($mn, " state_id =" . $state_id);
            if(isset($state_id)){
                $objArrJ = AmsState::SelectJson($state_id, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
       return $response;
    }
    
    public static function StateTable($params) {
        $mn = "AmsState::StateTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT st.state_id as stateId, st.state_name as stName,
                    st.state_iso as iso, st.country_id as countryId,
                    st.state_wikiurl as wikiUrl, st.ams_status_id as amsStatus, 
                    st.wad_status_id as wadStatus, st.udate,
                    count(distinct(if(c.city_id is null,null,c.city_id))) as cities,
                    count(distinct(if(a.ap_id is null,null,a.ap_id))) as airports,
                    sum(if(a.ams_status_id=4,1,0)) as apActive
                    FROM ams_wad.cfg_country_state st
                    left join ams_wad.cfg_city c on c.state_id = st.state_id
                    left join ams_wad.cfg_airport a on a.state_id = st.state_id ";
            
            
            
            if(isset($params->countryId) && strlen($params->countryId)>0){
                $sqlWhere = " WHERE st.country_id = ".$params->countryId." ";
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (st.state_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or st.state_iso like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE (st.state_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or st.state_iso like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = " group by st.state_id ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by st.state_name ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("states", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_wad.cfg_country_state st ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            if (isset($ret_json_data) && count($ret_json_data) > 0) {
                $obj = json_decode(json_encode($ret_json_data[0]));
            }
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function SelectJson($state_id, $conn, $mn, $logModel){
        
        $sql = " SELECT st.state_id as stateId, 
                st.state_name as stName,
                st.state_iso as iso, 
                st.country_id as countryId,
                st.state_wikiurl as wikiUrl, 
                st.ams_status_id as amsStatus, 
                st.wad_status_id as wadStatus, 
                st.udate,
                count(c.city_id) as cities, 
                count(a.ap_id) as airports, 
                sum(if(a.ams_status_id=4,1,0)) as apActive
                FROM ams_wad.cfg_country_state st
                left join ams_wad.cfg_city c on c.state_id = st.state_id
                left join ams_wad.cfg_airport a on a.state_id = st.state_id
                where st.state_id = ?" ;

        $bound_params_r = ["i",$state_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_wad.cfg_country_state
            (state_name, state_iso, country_id,
            state_wikiurl,  
            ams_status_id, wad_status_id)
            VALUES(?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["ssisii",
            ((!isset($dataJson->stName)) ? null : $dataJson->stName),
            ((!isset($dataJson->iso)) ? null : $dataJson->iso),
            ((!isset($dataJson->countryId)) ? null : $dataJson->countryId),
            ((!isset($dataJson->wikiUrl)) ? null : $dataJson->wikiUrl),
            ((!isset($dataJson->amsStatus)) ? 1 : $dataJson->amsStatus),
            ((!isset($dataJson->wadStatus)) ? 1 : $dataJson->wadStatus)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_wad.cfg_country_state
                    SET state_name=?, 
                    state_iso=?, 
                    country_id=?, 
                    state_wikiurl=?, 
                    ams_status_id=?, 
                    wad_status_id=?  WHERE state_id = ? " ;

        $bound_params_r = ["ssisiii",
            ((!isset($dataJson->stName)) ? null : $dataJson->stName),
            ((!isset($dataJson->iso)) ? null : $dataJson->iso),
            ((!isset($dataJson->countryId)) ? null : $dataJson->countryId),
            ((!isset($dataJson->wikiUrl)) ? null : $dataJson->wikiUrl),
            ((!isset($dataJson->amsStatus)) ? 1 : $dataJson->amsStatus),
            ((!isset($dataJson->wadStatus)) ? 1 : $dataJson->wadStatus),
            ($dataJson->stateId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->countryId;
    }
    
    static function Delete($state_id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM ams_wad.cfg_country_state
                   WHERE state_id = ? " ;

        $bound_params_r = ["i", $state_id];
        $rowsAffected = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, "deleted rowsAffected =" . $rowsAffected);
                    
        return (isset($rowsAffected) && $rowsAffected>0)? $state_id:$rowsAffected;
    }
    
    // </editor-fold>
}
