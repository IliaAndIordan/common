<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Dec 22, 2020 
 *  Filename            : UserModel.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of UserModel
 *
 * @author IZIordanov
 */
class AmsUser {
    
   // <editor-fold defaultstate="collapsed" desc="Fields">

    public $userId;
    public $userName;
    public $userEmail;
    public $userPwd;
    public $userRole = 1; //2 predefined roles 1-user, 3 admin, 2 editor
    
    public $isReceiveEmails;
    public $ipAddress;
    public $adate;
    public $udate;
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">

    public static function Login($email, $pwd) {
        $mn = "AmsUser::Login()";
        AmsAlLogger::logBegin($mn);
        AmsAlLogger::log($mn, " eMail: " . $email.", password".$pwd); 
        $response = new AmsUser();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $userJ = AmsUser::LoginJson($email, $pwd, $conn, $mn, $logModel);
            if(isset($userJ) && count($userJ)>0){
                $valJson = json_encode($userJ[0]);
                $user = json_decode($valJson);
                $response->userId = $user->userId;
                $response->userName = $user->userName;
                $response->userEmail = $user->userEmail;
                $response->userPwd = $user->userPwd;
                $response->userRole = $user->userRole;
                $response->ipAddress = $user->ipAddress;
                $response->isReceiveEmails = $user->isReceiveEmails;
                $response->adate = $user->adate;
                $response->udate = $user->udate;
            }else{
                $response = null;
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function LoadById($user_id) {
        $mn = "AmsUser::LoadById(".$user_id.")";
        AmsAlLogger::logBegin($mn);
        $response = new AmsUser();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $userJ = AmsUser::SelectJson($user_id, $conn, $mn, $logModel);
            if(isset($userJ) && count($userJ)>0){
                $valJson = json_encode($userJ[0]);
                $user = json_decode($valJson);
                $response->userId = $user->userId;
                $response->userName = $user->userName;
                $response->userEmail = $user->userEmail;
                $response->userPwd = $user->userPwd;
                $response->userRole = $user->userRole;
                $response->ipAddress = $user->ipAddress;
                $response->isReceiveEmails = $user->isReceiveEmails;
                $response->adate = $user->adate;
                $response->udate = $user->udate;
            }else{
                $response = null;
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "AmsUser::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        AmsAlLogger::log($mn, " userEmail = " . $dataJson->userEmail);
        $response;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $user_id = null;
            if(isset($dataJson->userId)){
               AmsAlLogger::log($mn, "Update  userId =" . $dataJson->userId);
               $user_id = $dataJson->userId;
               AmsUser::Update($dataJson, $conn, $mn, $logModel);

            } else{
                AmsAlLogger::log($mn, "Create user");
                $user_id = AmsUser::Create($dataJson, $conn, $mn, $logModel);
            }
            
            AmsAlLogger::log($mn, " user_id =" . $user_id);
            $response = AmsUser::LoadById($user_id);
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_al.ams_user
            (user_name, user_email, user_pwd,
            user_role, is_receive_emails, ip_address)
            VALUES(?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["sssiis",
             ((!isset($dataJson->userName)) ? null : $dataJson->userName),
            ((!isset($dataJson->userEmail)) ? null : $dataJson->userEmail),
            ((!isset($dataJson->userPwd)) ? null : $dataJson->userPwd),
            ((!isset($dataJson->userRole)) ? 3 : $dataJson->userRole),
            ((!isset($dataJson->isReceiveEmails)) ? 0 : $dataJson->isReceiveEmails),
            ((!isset($dataJson->ipAddress)) ? null : json_encode($dataJson->ipAddress)),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_al.ams_user
                    SET user_name=?, user_email=?, 
                    user_pwd=?,
                    user_role=?, 
                    is_receive_emails=?, 
                    ip_address=?
                    WHERE user_id = ? " ;

        $bound_params_r = ["sssiisi",
            ((!isset($dataJson->userName)) ? null : $dataJson->userName),
            ((!isset($dataJson->userEmail)) ? null : $dataJson->userEmail),
            ((!isset($dataJson->userPwd)) ? null : $dataJson->userPwd),
            ((!isset($dataJson->userRole)) ? 3 : $dataJson->userRole),
            ((!isset($dataJson->isReceiveEmails)) ? 0 : $dataJson->isReceiveEmails),
            ((!isset($dataJson->ipAddress)) ? null : json_encode($dataJson->ipAddress)),
            ($dataJson->userId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->userId;
    }
    
    static function SelectJson($uaer_id, $conn, $mn, $logModel){
        
        $sql = "SELECT u.user_id    as userId, 
                    u.user_name     as userName,
                    u.user_email    as userEmail, 
                    u.user_pwd      as userPwd, 
                    u.user_role     as userRole, 
                    u.is_receive_emails as isReceiveEmails, 
                    u.ip_address    as ipAddress, 
                    u.adate, u.udate
                    FROM ams_al.ams_user u
                    WHERE u.user_id = ? " ;

        $bound_params_r = ["i",$uaer_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function LoginJson($email, $pwd, $conn, $mn, $logModel){
        
        $sql = "SELECT u.user_id    as userId, 
                    u.user_name     as userName,
                    u.user_email    as userEmail, 
                    u.user_pwd      as userPwd, 
                    u.user_role     as userRole, 
                    u.is_receive_emails as isReceiveEmails, 
                    u.ip_address    as ipAddress, 
                    u.adate, u.udate
                    FROM ams_al.ams_user u
                    WHERE u.user_email = ? and u.user_pwd =?" ;

        $bound_params_r = ["ss",$email, $pwd];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function CheckEmailJson($email, $conn, $mn, $logModel){
        
        $sql = "SELECT count(*) AS rowCount
                    FROM ams_al.ams_user u
                    WHERE u.user_email = ? " ;

        $bound_params_r = ["s",$email];
        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
        return $ret_json_data;
    }
    
    public static function UsersTable($params) {
        $mn = "AmsUser::UsersTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT u.user_id as userId, u.user_name as userName,
                    u.user_email as userEmail, u.user_pwd as userPwd, u.user_role as userRole, 
                    u.is_receive_emails as isReceiveEmails, u.ip_address as ipAddress,
                    u.adate, u.udate
                    FROM ams_al.ams_user u
                     ";
            
            
            $sqlWhere="";
            if(isset($params->userRole) && strlen($params->userRole)>0){
                $sqlWhere = " WHERE u.user_role = ".$params->userRole." ";
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (u.user_email like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or u.user_name like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere = " WHERE (u.user_email like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or u.user_name like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = "";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by u.".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by u.user_email, u.user_name, u.user_id ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("usersList", $ret_json_data);
            
            $sql = "SELECT count(*) as total_rows
                    FROM ams_al.ams_user u ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("totals", $ret_json_data);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
}


class AmsUserSettings {
    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $userId;
    public $displayDistanceMl;
    public $displayWeightLb;
    public $ticketPriceFactor;

    public function toJSON() {
        return json_encode($this);
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($user_id) {
        $mn = "AmsUserSettings::LoadById()";
        AmsAlLogger::logBegin($mn);
        //AmsAlLogger::log($mn, " userId: " . $user_id); 
        $response = new AmsUserSettings();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $jsonArr = AmsUserSettings::SelectJson($user_id, $conn, $mn, $logModel);
           
            //AmsAlLogger::log($mn, " count jsonArr: " . count($jsonArr)); 
            if(!isset($jsonArr) || count($jsonArr) == 0){
                $response->userId = $user_id;
                $response->displayDistanceMl = 0;
                $response->displayWeightLb = 0;
                $response->ticketPriceFactor = 0.25;
                AmsUserSettings::Create($response, $conn, $mn, $logModel);
                $jsonArr = AmsUserSettings::SelectJson($user_id, $conn, $mn, $logModel);
                 $response = json_decode(json_encode($jsonArr[0]));
            }else{
               $response = json_decode(json_encode($jsonArr[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
           $response = new AmsUserSettings();
            $response->userId = $user_id;
            $response->displayDistanceMl = 0;
            $response->displayWeightLb = 0;
            $response->ticketPriceFactor = 0.25;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($dataJson) {
        $mn = "AmsUserSettings::Save()";
        AmsAlLogger::logBegin($mn);
        $settings = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $user_id = null;
            if(isset($dataJson->userId)){
               AmsAlLogger::log($mn, "Update  userId =" . $dataJson->userId);
               $user_id = $dataJson->userId;
               $user_id = AmsUserSettings::Update($dataJson, $conn, $mn, $logModel);

            } else{
                AmsAlLogger::log($mn, "Create user");
                $user_id = AmsUserSettings::Create($dataJson, $conn, $mn, $logModel);
            }
            
            AmsAlLogger::log($mn, " user_id =" . $user_id);
            $objJ = AmsUserSettings::SelectJson($user_id, $conn, $mn, $logModel);
            $valJson = json_encode($objJ[0]);
            $settings = json_decode($valJson);
            //$ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            //$response->addData("settings", $obj);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            //$response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        //AmsAlLogger::logEnd($mn);
        //$this->EncodeResponce($response);
        return $settings;
    }
    
    static function SelectJson($uaer_id, $conn, $mn, $logModel){
        
        $sql = "SELECT u.user_id as userId, 
                    u.display_distance_ml     as displayDistanceMl,
                    u.display_weight_lb    as displayWeightLb,
                    u.ticket_price_factor as ticketPriceFactor
                    FROM ams_al.ams_user_settings u
                    WHERE u.user_id = ? " ;

        $bound_params_r = ["i",$uaer_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_al.ams_user_settings
            (user_id, display_distance_ml, display_weight_lb)
            VALUES(?, ?, ?)" ;

        $bound_params_r = ["iii",
            ( $dataJson->userId),
            ((!isset($dataJson->displayDistanceMl)) ? 0 : $dataJson->displayDistanceMl),
            ((!isset($dataJson->displayWeightLb)) ? 0 : $dataJson->displayWeightLb)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_al.ams_user_settings
                    SET display_distance_ml=?, 
                    display_weight_lb=?, 
                    ticket_price_factor=?
                    WHERE user_id = ? " ;

        $bound_params_r = ["iisi",
            ((!isset($dataJson->displayDistanceMl)) ? 0 : $dataJson->displayDistanceMl),
            ((!isset($dataJson->displayWeightLb)) ? 0 : $dataJson->displayWeightLb),
            ((!isset($dataJson->ticketPriceFactor)) ? 0 : $dataJson->ticketPriceFactor),
            ($dataJson->userId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->userId;
    }
    
    // </editor-fold>
    
}
