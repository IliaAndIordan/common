<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Jan 7, 2021 
 *  Filename          : Country.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2021 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of Country
 *
 * @author IZIordanov
 */
class AmsCountry {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $countryId;
    public $cCode;
    public $cName;
    public $iso2;
    public $iso3;
    public $icao;
    
    public $regionId;
    public $subregionId;
    
    public $wikiUrl;
    public $imageUrl;
    public $amsStatus;
    public $wadStatus;
    public $udate;
    
    public $statesCount;
    
    
    public function toJSON() {
        return json_encode($this);
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($country_id) {
        $mn = "AmsCountry::LoadById(".$country_id.")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsCountry::SelectJson($country_id, $conn, $mn, $logModel);
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = $objArrJ[0];
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($dataJson) {
        $mn = "AmsCountry::Save()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        AmsAlLogger::log($mn, " icao = " . $dataJson->icao);
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $country_id = null;
            if(isset($dataJson->countryId)){
               AmsAlLogger::log($mn, "Update  countryId =" . $dataJson->countryId);
               $country_id = $dataJson->countryId;
               $country_id = AmsCountry::Update($dataJson, $conn, $mn, $logModel);

            } else{
                AmsAlLogger::log($mn, "Create country");
                $country_id = AmsCountry::Create($dataJson, $conn, $mn, $logModel);
            }
            
            AmsAlLogger::log($mn, " countryId =" . $country_id);
            if(isset($country_id)){
                $objArrJ = AmsCountry::SelectJson($country_id, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = $objArrJ[0];
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
       return $response;
    }
    
    public static function CountryTable($params) {
        $mn = "AmsCountry::CountryTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT c.country_id as countryId, c.country_code as cCode, c.country_name as cName, 
                c.country_iso2 as iso2, c.country_iso3 as iso3, c.country_icao as icao, 
                c.region_id as regionId, c.subregion_id as subregionId,
                c.wiki_link as wikiUrl, c.image_url as imageUrl, 
                c.ams_status_id as amsStatus, c.wad_status_id as wadStatus,
                c.udate, count(cs.state_id) as statesCount, 
                ifnull(v.airports, 0) as airports, ifnull(v.apActive, 0) as apActive
                FROM ams_wad.cfg_country c
                left join ams_wad.cfg_country_state cs on cs.country_id = c.country_id
                left join ams_wad.v_country_airports_count v on v.country_id = c.country_id ";
            
            
            
            if(isset($params->subregionId) && strlen($params->subregionId)>0){
                $sqlWhere = " WHERE c.subregion_id = ".$params->subregionId." ";
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere)){
                    $sqlWhere .= " AND (c.country_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or c.country_icao like '%".$params->qry_filter."%' )";
                }
                else{
                    $sqlWhere .= " WHERE (c.country_name like '%".$params->qry_filter."%' ";
                    $sqlWhere .= " or c.country_icao like '%".$params->qry_filter."%' )";
                }
               
            }
            $sqlOrder = " group by c.country_id ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by c.country_name ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("countries", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_wad.cfg_country c ".(isset($sqlWhere)?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = $ret_json_data[0];
            $response->addData("rowsCount", $ret_json_data[0]);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function SelectJson($country_id, $conn, $mn, $logModel){
        
        $sql = " SELECT c.country_id as countryId, 
            c.country_code as cCode, 
            c.country_name as cName, 
            c.country_iso2 as iso2, 
            c.country_iso3 as iso3, 
            c.country_icao as icao, 
            c.region_id as regionId, 
            c.subregion_id as subregionId,
            c.wiki_link as wikiUrl, 
            c.image_url as imageUrl, 
            c.ams_status_id as amsStatus, 
            c.wad_status_id as wadStatus,
            c.udate, 
            count(cs.state_id) as statesCount
            FROM ams_wad.cfg_country c
            left join ams_wad.cfg_country_state cs on cs.country_id = c.country_id
            where c.country_id=?" ;

        $bound_params_r = ["i",$country_id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_wad.cfg_country
            (country_code, country_name, country_iso2,
            country_iso3, country_icao, region_id, 
            subregion_id, wiki_link, image_url, 
            ams_status_id, wad_status_id)
            VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["sssssiissii",
            ((!isset($dataJson->cCode)) ? null : $dataJson->cCode),
            ((!isset($dataJson->cName)) ? null : $dataJson->cName),
            ((!isset($dataJson->iso2)) ? null : $dataJson->iso2),
            ((!isset($dataJson->iso3)) ? null : $dataJson->iso3),
            ((!isset($dataJson->icao)) ? null : $dataJson->icao),
            ((!isset($dataJson->regionId)) ? null : $dataJson->regionId),
            ((!isset($dataJson->subregionId)) ? null : $dataJson->subregionId),
            ((!isset($dataJson->wikiUrl)) ? null : $dataJson->wikiUrl),
            ((!isset($dataJson->imageUrl)) ? null : $dataJson->imageUrl),
            ((!isset($dataJson->amsStatus)) ? 1 : $dataJson->amsStatus),
            ((!isset($dataJson->wadStatus)) ? 1 : $dataJson->wadStatus)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
   
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_wad.cfg_country
                    SET country_code=?, 
                    country_name=?, 
                    country_iso2=?, 
                    country_iso3=?, 
                    country_icao=?, 
                    region_id=?, 
                    subregion_id=?, 
                    wiki_link=?, 
                    image_url=?, 
                    ams_status_id=?, 
                    wad_status_id=?  WHERE country_id = ? " ;

        $bound_params_r = ["sssssiissiii",
            ((!isset($dataJson->cCode)) ? null : $dataJson->cCode),
            ((!isset($dataJson->cName)) ? null : $dataJson->cName),
            ((!isset($dataJson->iso2)) ? null : $dataJson->iso2),
            ((!isset($dataJson->iso3)) ? null : $dataJson->iso3),
            ((!isset($dataJson->icao)) ? null : $dataJson->icao),
            ((!isset($dataJson->regionId)) ? null : $dataJson->regionId),
            ((!isset($dataJson->subregionId)) ? null : $dataJson->subregionId),
            ((!isset($dataJson->wikiUrl)) ? null : $dataJson->wikiUrl),
            ((!isset($dataJson->imageUrl)) ? null : $dataJson->imageUrl),
            ((!isset($dataJson->amsStatus)) ? 1 : $dataJson->amsStatus),
            ((!isset($dataJson->wadStatus)) ? 1 : $dataJson->wadStatus),
            ($dataJson->countryId)
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->countryId;
    }
    
    // </editor-fold>
}
