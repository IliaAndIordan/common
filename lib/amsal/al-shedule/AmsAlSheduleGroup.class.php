<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Oct 26, 2023 
 *  Filename            : AmsAlSheduleGroup.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2023 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of AmsAlSheduleGroup.class
 *
 * @author IZIordanov
 */
class AmsAlSheduleGroup {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $grpId;
    public $grpName;
    public $alId;
    public $apId;
    public $acIdPrototype;
    
    public $isActive;
    public $acId;
    public $startDate;
    
          
    
    public function toJSON() {
        return json_encode($this);
    }
    
    public static function fromJSON($dataJson) {
        $rv = new AmsAlSheduleGroup();
        $rv->grpId = (!isset($dataJson->grpId)) ? null : $dataJson->grpId;
        $rv->grpName = (!isset($dataJson->grpName)) ? null : $dataJson->grpName;
        $rv->alId = (!isset($dataJson->alId)) ? null : $dataJson->alId;
        $rv->apId = (!isset($dataJson->apId)) ? null : $dataJson->apId;
        $rv->acIdPrototype = (!isset($dataJson->acIdPrototype)) ? null : $dataJson->acIdPrototype;
        $rv->isActive = (!isset($dataJson->isActive)) ? null : $dataJson->isActive;
        $rv->acId = (!isset($dataJson->acId)) ? null : $dataJson->acId;
        $rv->startDate = (!isset($dataJson->startDate)) ? null : $dataJson->startDate;
        
        return $rv;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($id) {
        $mn = "AmsAlSheduleGroup::LoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsAlSheduleGroup::SelectJson($id, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "AmsAlSheduleGroup::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsAlLogger::log($mn, " isset grp_id = " . isset($dataJson->grpId));
        $response;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objId = null;
            if(isset($dataJson->grpId)){
               //AmsAlLogger::log($mn, "Update  grpId =" . $dataJson->grpId);
               $objId = $dataJson->grpId;
               AmsAlSheduleGroup::Update($dataJson, $conn, $mn, $logModel);
            } else{
                //AmsAlLogger::log($mn, "Create airline group");
                $objId = AmsAlSheduleGroup::Create($dataJson, $conn, $mn, $logModel);
            }
            
            //AmsAlLogger::log($mn, " grpId =" . $objId);
           if(isset($objId)){
                $objArrJ = AmsAlSheduleGroup::SelectJson($objId, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $response = json_decode(json_encode($objArrJ[0]));
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function AmsAlGroupTable($params) {
        $mn = "AmsAlSheduleGroup::AmsAlGroupTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            
            $sql = "SELECT g.grp_id as grpId,  g.grp_name as grpName, g.al_id as alId,
                    g.ac_id_prototype as acIdPrototype, g.ap_id as apId,
                    m.ac_type_id as acTypeId, m.take_off_m as minRunwayM, 
                    m.max_range_km as maxRangeKm, m.cruise_speed_kmph as cruiseSpeedKmph,  m.operation_time_min as opTimeMin,
                    m.fuel_consumption_lp100km as fuelConsumptionLp100km,
                    g.ac_id as acId, a.mac_id as macId, g.is_active as isActive, g.start_date as startDate, flp.flights
                        FROM ams_al.ams_al_flp_group g
                        left join ams_ac.ams_aircraft a on a.ac_id = g.ac_id_prototype
                        left join ams_ac.cfg_mac m on m.mac_id = a.mac_id
                        left join (
                            SELECT flpns.grp_id, count(flp.flp_id) as flights 
                            FROM  ams_al.ams_al_flp_number_schedule flpns 
                            left join ams_ac.ams_al_flight_plan flp on  flp.flpns_id = flpns.flpns_id 
                            group by flpns.grp_id
                        ) flp on flp.grp_id = g.grp_id ";
            
            $sqlWhere="";
            if(isset($params->alId) && strlen($params->alId)>0){
                $sqlWhere = " WHERE g.al_id = ".$params->alId." ";
            } 
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>1){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND g.grp_name like '%".$params->qry_filter."%' ";
                }
                else{
                    $sqlWhere = " WHERE g.grp_name like '%".$params->qry_filter."%' ";
                }
            }
            $sqlOrder = "";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by grp_name ";
            }
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("groups", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_al.ams_al_flp_group g ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
   // <editor-fold defaultstate="collapsed" desc="DB Methods">
     
     static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_al.ams_al_flp_group 
                (grp_name, al_id, ac_id_prototype, ap_id, ac_id, is_active, start_date) 
                VALUES(?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["siiiiis",
             ((!isset($dataJson->grpName)) ? null : $dataJson->grpName),
            ((!isset($dataJson->alId)) ? null : $dataJson->alId),
            ((!isset($dataJson->acIdPrototype)) ? null : $dataJson->acIdPrototype),
            ((!isset($dataJson->apId)) ? null : $dataJson->apId),
            ((!isset($dataJson->acId)) ? null : $dataJson->acId),
            ((!isset($dataJson->isActive)) ? null :$dataJson->isActive),
            ((!isset($dataJson->startDate)) ? null : $dataJson->startDate),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log("$mn", "id=" . $id);
                    
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_al.ams_al_flp_group 
                    SET grp_name=?, al_id=?, ap_id=?, ac_id_prototype=?, 
                    ac_id=?, is_active=?, start_date=?
                    WHERE grp_id = ?  " ;

        $bound_params_r = ["siiiiisi",
            ((!isset($dataJson->grpName)) ? null : $dataJson->grpName),
            ((!isset($dataJson->alId)) ? null : $dataJson->alId),
            ((!isset($dataJson->apId)) ? null : $dataJson->apId),
            ((!isset($dataJson->acIdPrototype)) ? null : $dataJson->acIdPrototype),
            ((!isset($dataJson->acId)) ? null : $dataJson->acId),
            ((!isset($dataJson->isActive)) ? null :$dataJson->isActive),
            ((!isset($dataJson->startDate)) ? null : $dataJson->startDate),
            ($dataJson->grpId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->grpId;
    }
    
    static function SelectJson($id, $conn, $mn, $logModel){
        
        $sql = "SELECT g.grp_id as grpId,  g.grp_name as grpName, g.al_id as alId,
                    g.ac_id_prototype as acIdPrototype, g.ap_id as apId,
                    m.ac_type_id as acTypeId, m.take_off_m as minRunwayM, 
                    m.max_range_km as maxRangeKm, m.cruise_speed_kmph as cruiseSpeedKmph,  m.operation_time_min as opTimeMin,
                    m.fuel_consumption_lp100km as fuelConsumptionLp100km,
                    g.ac_id as acId, a.mac_id as macId, g.is_active as isActive, g.start_date as startDate, flp.flights
                        FROM ams_al.ams_al_flp_group g
                        left join ams_ac.ams_aircraft a on a.ac_id = g.ac_id_prototype
                        left join ams_ac.cfg_mac m on m.mac_id = a.mac_id
                        left join (
                            SELECT flpns.grp_id, count(flp.flp_id) as flights 
                            FROM  ams_al.ams_al_flp_number_schedule flpns 
                            left join ams_ac.ams_al_flight_plan flp on  flp.flpns_id = flpns.flpns_id 
                            group by flpns.grp_id
                        ) flp on flp.grp_id = g.grp_id
                        where g.grp_id = ? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM ams_al.ams_al_flp_group
                   WHERE grp_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, "deleted grp_id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}
