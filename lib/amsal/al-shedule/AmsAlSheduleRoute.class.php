<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Oct 26, 2023 
 *  Filename            : AmsAlSheduleRoute.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2023 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of AmsAlSheduleRoute
 *
 * @author IZIordanov
 */
class AmsAlSheduleRoute {

    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $routeId;
    public $routeNr;
    public $routeName;
    public $destId;
    public $revDestId;
    public $depApId;
    public $arrApId;
    public $priceE;
    public $priceB;
    public $priceF;
    public $priceCPerKg;
    public $adate;
    public $udate;

    public function toJSON() {
        return json_encode($this);
    }

    public static function fromJSON($dataJson) {
        $rv = new AmsAlSheduleRoute();
        $rv->routeId = (!isset($dataJson->routeId)) ? null : $dataJson->routeId;
        $rv->routeNr = (!isset($dataJson->routeNr)) ? null : $dataJson->routeNr;
        $rv->routeName = (!isset($dataJson->routeName)) ? null : $dataJson->routeName;
        $rv->destId = (!isset($dataJson->destId)) ? null : $dataJson->destId;
        $rv->revDestId = (!isset($dataJson->revDestId)) ? null : $dataJson->revDestId;
        $rv->depApId = (!isset($dataJson->depApId)) ? null : $dataJson->depApId;
        $rv->arrApId = (!isset($dataJson->arrApId)) ? null : $dataJson->arrApId;
        $rv->priceE = (!isset($dataJson->priceE)) ? null : $dataJson->priceE;
        $rv->priceB = (!isset($dataJson->priceB)) ? null : $dataJson->priceB;
        $rv->priceF = (!isset($dataJson->priceF)) ? null : $dataJson->priceF;
        $rv->priceCPerKg = (!isset($dataJson->priceCPerKg)) ? null : $dataJson->priceCPerKg;
        $rv->adate = (!isset($dataJson->adate)) ? null : $dataJson->adate;
        $rv->udate = (!isset($dataJson->udate)) ? null : $dataJson->udate;
        return $rv;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($id) {
        $mn = "AmsAlSheduleRoute::LoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsAlSheduleRoute::SelectJson($id, $conn, $mn, $logModel);

            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function Save($data) {
        $mn = "AmsAlSheduleRoute::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsAlLogger::log($mn, " isset route_id = " . isset($dataJson->routeId));
        $response;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objId = null;
            if (isset($dataJson->routeId)) {
                //AmsAlLogger::log($mn, "Update  routeId =" . $dataJson->routeId);
                $objId = $dataJson->routeId;
                AmsAlSheduleRoute::Update($dataJson, $conn, $mn, $logModel);
            } else {
                //AmsAlLogger::log($mn, "Create route ");
                $objId = AmsAlSheduleRoute::Create($dataJson, $conn, $mn, $logModel);
            }

            //AmsAlLogger::log($mn, " routeId =" . $objId);
            if (isset($objId)) {
                $objArrJ = AmsAlSheduleRoute::SelectJson($objId, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = json_decode(json_encode($objArrJ[0]));
                }
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function ResetPrices($data) {
        $mn = "AmsAlSheduleRoute::ResetPrices()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsAlLogger::log($mn, " isset route_id = " . isset($dataJson->routeId));
        $conn = AmsAlConnection::dbConnect();
        $logModel = AmsAlLogger::currLogger()->getModule($mn);
        if (isset($dataJson->alId)) {
            //AmsAlLogger::log($mn, "Update  routeId =" . $dataJson->routeId);
            $response = AmsAlSheduleRoute::FlpnResetPrices($dataJson, $conn, $mn, $logModel);
        } else {
            $response = new Response("error", 'Missing required parameters.');
            $response->statusCode = 412;
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function UpdatePriceFromFlpn($data) {
        $mn = "AmsAlSheduleRoute::UpdatePriceFromFlpn()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data;
        //AmsAlLogger::log($mn, " isset route_id = " . isset($dataJson->routeId));
        $response = new Response("success", "No return found.");
        $response->addData("affectedRows", 0);
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            if (isset($dataJson->routeId)) {
                AmsAlLogger::log($mn, "Update  routeId =" . $dataJson->routeId);
                $affectedRows = AmsAlSheduleRoute::UpdateFromFlpn($dataJson, $conn, $mn, $logModel);
                $response = new Response("success", "Update FLPNS prices from FLPN.");
                $response->addData("affectedRows", $affectedRows);
            } else {
                $response = new Response("error", 'Missing required parameters.');
                $response->statusCode = 412;
            }
            //AmsAlLogger::log($mn, " flpnId =" . $objId);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function AmsAlRouteTable($params) {
        $mn = "AmsAlSheduleRoute::AmsAlRouteTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //--
            $sql = "SELECT r.route_id as routeId,
                    r.route_nr as routeNr,
                    r.route_name as routeName,
                    r.flight_h as flightH,
                    d.distance_km as distanceKm,
                    r.dest_id as destId,
                    r.rev_dest_id as revDestId,
                    r.dep_ap_id as depApId,
                    r.arr_ap_id as arrApId,
                    r.price_e as priceE,
                    r.price_b as priceB,
                    r.price_f as priceF,
                    r.price_c_p_kg as priceCPerKg,
                    r.adate, r.udate,
                    Round(flp.param_value * d.max_ticket_price_e, 2) as maxPriceE,
                    Round(flp.param_value * d.max_ticket_price_b, 2)  as maxPriceB,
                    Round(flp.param_value * d.max_ticket_price_f, 2)  as maxPriceF,
                    ROUND(flp.param_value * (d.distance_km * cpdmax.param_value), 4) as maxPriceCPerKg
                    FROM ams_al.ams_flp_route r
                    join ams_wad.cfg_airport_destination d on d.dest_id = r.dest_id
                    join ams_wad.cfg_airport dep on dep.ap_id = r.dep_ap_id
                    join ams_wad.cfg_airport arr on arr.ap_id = r.arr_ap_id
                    join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'max_ticket_price_cpd_kg_per_km' ) cpdmax on 1=1
                    join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'flight_charter_pct_from_max_price' ) flp on 1=1
                     ";

            $sqlWhere = "";

            if (isset($params->apId) && strlen($params->apId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND r.dep_ap_id =" . $params->apId . " or r.arr_ap_id=" . $params->apId . " ";
                } else {
                    $sqlWhere = " WHERE r.dep_ap_id =" . $params->apId . " or r.arr_ap_id=" . $params->apId . " ";
                }
            }
            if (isset($params->arrApId) && strlen($params->arrApId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND r.arr_ap_id =" . $params->arrApId . " ";
                } else {
                    $sqlWhere = " WHERE r.arr_ap_id =" . $params->arrApId . " ";
                }
            }
            if (isset($params->depApId) && strlen($params->depApId) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND r.dep_ap_id =" . $params->depApId . " ";
                } else {
                    $sqlWhere = " WHERE r.dep_ap_id =" . $params->depApId . " ";
                }
            }

            if (isset($params->qry_filter) && strlen($params->qry_filter) > 0) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND r.route_name like '%" . $params->qry_filter . "%' ";
                } else {
                    $sqlWhere = " WHERE r.route_name like '%" . $params->qry_filter . "%' ";
                }
                $sqlWhere .= " OR ifnull(dep.ap_iata, dep.ap_icao) like '%" . $params->qry_filter . "%' ";
                $sqlWhere .= " OR ifnull(arr.ap_iata, arr.ap_icao) like '%" . $params->qry_filter . "%' ";
            }
            $sqlOrder = "";
            if (isset($params->qry_orderCol)) {
                $sqlOrder .= " order by " . $params->qry_orderCol . " " . ($params->qry_isDesc ? "desc" : " asc");
            } else {
                $sqlOrder .= "order by routeNr ";
            }
            $sql .= $sqlWhere . $sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            //AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("routes", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM ams_al.ams_flp_route r
                    join ams_wad.cfg_airport_destination d on d.dest_id = r.dest_id
                    join ams_wad.cfg_airport dep on dep.ap_id = r.dep_ap_id
                    join ams_wad.cfg_airport arr on arr.ap_id = r.arr_ap_id
                     " . (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Transfer Methods">

    public static function AmsAlTransfersTablePopulate($params) {
        $mn = "AmsAlSheduleRoute::AmsAlTransfersTablePopulate()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $sql = "call ams_ac.sp_ams_al_flp_transfer_pcpd()";
            $ret_json_data = $conn->dbExecuteSQLJson($sql, $logModel);
            $response = new Response("success", 'Table sp_ams_al_flp_transfer_pcpd populated.');
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response("error", 'Error populating sp_ams_al_flp_transfer_pcpd table.');
            $response->statusCode = 412;
        }
        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function AmsAlTransfersTable($params) {
        $mn = "AmsAlSheduleRoute::AmsAlTransfersTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);

            $sql = "SELECT 
                    tr.transfer_id AS transferId,
                    tr.ppd_id AS ppdId,
                    tr.cpd_id AS cpdId,
                    tr.flp_id_1 AS flpId,
                    tr.flp_id_2 AS flpId2,
                    tr.flp_id_3 AS flpId3,
                    tr.flp_id_4 AS flpId4,
                    tr.d_ap_id AS depApId,
                    tr.a_ap_id AS arrApId,
                    tr.distance_km AS distanceKm,
                    tr.fl_name_1 AS flName,
                    tr.fl_name_2 AS flName2,
                    tr.fl_name_3 AS flName3,
                    tr.fl_name_4 AS flName4,
                    tr.free_payload_1 AS remainPax,
                    tr.free_payload_2 AS remainPax2,
                    tr.free_payload_3 AS remainPax3,
                    tr.free_payload_4 AS remainPax4,
                    round(tr.ppd_price - tr.flpns_price,2) AS diffPrice,
                    tr.flpns_price AS sumPrice,
                    tr.ppd_price AS pPrice,
                    tr.pax_class_id AS paxClassId,
                    tr.payload AS payload,
                    tr.ppd_note AS note
                    FROM ams_ac.v_ams_al_flight_plan_transfer tr ";
            $sqlWhere = "";
            if (isset($params->apId) && strlen($params->apId) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND tr.d_ap_id =" . $params->apId . " or tr.a_ap_id=" . $params->apId . " ";
                } else {
                    $sqlWhere = " WHERE tr.d_ap_id =" . $params->apId . " or tr.a_ap_id=" . $params->apId . " ";
                }
            }
            if (isset($params->qry_filter) && strlen($params->qry_filter) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND (tr.fl_name_1 like '%" . $params->qry_filter . "%' OR tr.fl_name_2 like '%" . $params->qry_filter . "%' OR tr.fl_name_3 like '%" . $params->qry_filter . "%' OR tr.fl_name_4 like '%" . $params->qry_filter . "%') ";
                } else {
                    $sqlWhere = " WHERE (tr.fl_name_1 like '%" . $params->qry_filter . "%' OR tr.fl_name_2 like '%" . $params->qry_filter . "%' OR tr.fl_name_3 like '%" . $params->qry_filter . "%' OR tr.fl_name_4 like '%" . $params->qry_filter . "%') ";
                }
            }
            $sqlOrder = ""; // ORDER BY transferId";
            $sql .= $sqlWhere . $sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response = new Response("success", "Get Flight Plan Transfers Table.");
            if (isset($ret_json_data)) {
                $rowJson = json_decode(json_encode($ret_json_data));
                $response->addData("routes", $rowJson);
            } else {
                $response->addData("routes", []);
            }

            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_al_flight_plan_transfer tr
                    left join ams_ac.v_ams_flight_plan_payload_sum_class flp1 on flp1.flp_id = tr.flp_id_1 "
                    . (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            if (isset($params->qry_offset) && $params->qry_offset > 0 && isset($params->rowsCount)) {
                AmsAlLogger::log($mn, " params->rowsCount= " . $params->rowsCount . " ");
                $response->addData("rowsCount", $params->rowsCount);
            } else {
                $bound_params_r = ["i", 1];
                AmsAlLogger::log($mn, " sql= " . $sql . " ");
                $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                if (isset($ret_json_data) && count($ret_json_data) > 0) {
                    $rowJson = json_decode(json_encode($ret_json_data[0]));
                    $response->addData("rowsCount", $rowJson->totalRows);
                } else {
                    //AmsAlLogger::log($mn, " sql count= " . $sql . " ");
                    $response->addData("rowsCount", 0);
                }
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " Finish ");
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function AmsAlTransfersPpdTable($params) {
        $mn = "AmsAlSheduleRoute::AmsAlTransfersPpdTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);

            $sql = " SELECT 
                    tr.transfer_id AS transferId,
                    tr.ppd_id AS ppdId,
                    tr.flp_id_1 AS flpId,
                    tr.flp_id_2 AS flpId2,
                    tr.flp_id_3 AS flpId3,
                    tr.flp_id_4 AS flpId4,
                    tr.dep_ap_id AS depApId,
                    tr.arr_ap_id AS arrApId,
                    tr.distance_km AS distanceKm,
                    tr.fl_name_1 AS flName,
                    tr.fl_name_2 AS flName2,
                    tr.fl_name_3 AS flName3,
                    tr.fl_name_4 AS flName4,
                    tr.free_payload_1 AS remainPax,
                    tr.free_payload_2 AS remainPax2,
                    tr.free_payload_3 AS remainPax3,
                    tr.free_payload_4 AS remainPax4,
                    round(tr.ppd_price - tr.flpns_price,2) AS diffPrice,
                    tr.flpns_price AS sumPrice,
                    tr.ppd_price AS pPrice,
                    tr.pax_class_id AS paxClassId,
                    tr.ppd_pax AS pax,
                    tr.ppd_payload_kg AS payloadKg,
                    tr.ppd_note AS note
                    FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v03 tr ";

            $sqlWhere = "";
            if (isset($params->apId) && strlen($params->apId) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND tr.d_ap_id =" . $params->apId . " or tr.a_ap_id=" . $params->apId . " ";
                } else {
                    $sqlWhere = " WHERE tr.d_ap_id =" . $params->apId . " or tr.a_ap_id=" . $params->apId . " ";
                }
            }
            if (isset($params->qry_filter) && strlen($params->qry_filter) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND (tr.fl_name_1 like '%" . $params->qry_filter . "%' OR tr.fl_name_2 like '%" . $params->qry_filter . "%' OR tr.fl_name_3 like '%" . $params->qry_filter . "%' OR tr.fl_name_4 like '%" . $params->qry_filter . "%') ";
                } else {
                    $sqlWhere = " WHERE (tr.fl_name_1 like '%" . $params->qry_filter . "%' OR tr.fl_name_2 like '%" . $params->qry_filter . "%' OR tr.fl_name_3 like '%" . $params->qry_filter . "%' OR tr.fl_name_4 like '%" . $params->qry_filter . "%') ";
                }
            }
            $sqlOrder = ""; // ORDER BY transferId"; // ORDER BY remain_pax desc, remain_pax2 desc, remain_pax3 desc, remain_pax4 desc";

            $sql .= $sqlWhere . $sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            //AmsAlLogger::log($mn, " sqlWhere= " . $sqlWhere . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response = new Response("success", "Pax Payload.");
            if (isset($ret_json_data)) {
                $rowJson = json_decode(json_encode($ret_json_data));
                $response->addData("routes", $rowJson);
            } else {
                $response->addData("routes", []);
            }


            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_al_flight_plan_transfer tr
                    join ams_ac.ams_airport_ppd p on p.dest_id = tr.dest_id  "
                    . (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=? FOR SHARE") : " where 1=? FOR SHARE");
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v03 tr  "
                    . (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=? ") : " where 1=? ");

            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            if (isset($ret_json_data) && count($ret_json_data) > 0) {
                $rowJson = json_decode(json_encode($ret_json_data[0]));
                $response->addData("rowsCount", $rowJson->totalRows);
            } else {
                AmsAlLogger::log($mn, " sql count= " . $sql . " ");
                $response->addData("rowsCount", 0);
            }
        } catch (Exception $ex) {
            AmsAlLogger::log($mn, " sql count= " . $sql . " ");
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function AmsAlTransfersCpdTable($params) {
        $mn = "AmsAlSheduleRoute::AmsAlTransfersCpdTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $sql = " SELECT 
                    tr.transfer_id AS transferId,
                    tr.cpd_id AS cpdId,
                    tr.flp_id_1 AS flpId,
                    tr.flp_id_2 AS flpId2,
                    tr.flp_id_3 AS flpId3,
                    tr.flp_id_4 AS flpId4,
                    tr.d_ap_id AS depApId,
                    tr.a_ap_id AS arrApId,
                    tr.distance_km AS distanceKm,
                    tr.fl_name_1 AS flName,
                    tr.fl_name_2 AS flName2,
                    tr.fl_name_3 AS flName3,
                    tr.fl_name_4 AS flName4,
                    ifnull(tr.free_payload_1,0) as remainPayloadKg,
                    ifnull(tr.free_payload_2,0) as remainPayloadKg2,
                    ifnull(tr.free_payload_3,0) as remainPayloadKg3,
                    ifnull(tr.free_payload_4,0) as remainPayloadKg4,
                    round((tr.cpd_price - tr.flpns_price),2)	 AS diffPrice,
                    tr.flpns_price AS sumPrice,
                    tr.cpd_price AS pPrice,
                    tr.pax_class_id AS paxClassId,
                    tr.cpd_payload_kg AS payloadKg,
                    tr.cpd_note AS note,
                    tr.adate AS adate
                FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui_v01 tr  ";

            $sqlWhere = " ";
            if (isset($params->apId) && strlen($params->apId) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND tr.d_ap_id =" . $params->apId . " or tr.a_ap_id=" . $params->apId . " ";
                } else {
                    $sqlWhere = " WHERE tr.d_ap_id =" . $params->apId . " or tr.a_ap_id=" . $params->apId . " ";
                }
            }
            if (isset($params->qry_filter) && strlen($params->qry_filter) > 1) {
                if (isset($sqlWhere) && strlen($sqlWhere) > 1) {
                    $sqlWhere .= " AND (tr.fl_name_1 like '%" . $params->qry_filter . "%' OR tr.fl_name_2 like '%" . $params->qry_filter . "%' OR tr.fl_name_3 like '%" . $params->qry_filter . "%' OR tr.fl_name_4 like '%" . $params->qry_filter . "%') ";
                } else {
                    $sqlWhere = " WHERE (tr.fl_name_1 like '%" . $params->qry_filter . "%' OR tr.fl_name_2 like '%" . $params->qry_filter . "%' OR tr.fl_name_3 like '%" . $params->qry_filter . "%' OR tr.fl_name_4 like '%" . $params->qry_filter . "%') ";
                }
            }
            $sqlOrder = " ";

            $sql .= $sqlWhere . $sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response = new Response("success", "Cargo Payload.");
            if (isset($ret_json_data)) {
                $rowJson = json_decode(json_encode($ret_json_data));
                $response->addData("routes", $rowJson);
            } else {
                $response->addData("routes", []);
            }

            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_al_flight_plan_transfer tr
                    join ams_ac.ams_airport_cpd p on p.dest_id = tr.dest_id  "
                    . (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            if (isset($ret_json_data) && count($ret_json_data) > 0) {
                $rowJson = json_decode(json_encode($ret_json_data[0]));
                $response->addData("rowsCount", $rowJson->totalRows);
            } else {
                //AmsAlLogger::log($mn, " sql count= " . $sql . " ");
                $response->addData("rowsCount", 0);
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function FlpAddPayloadTransferPpd($data) {
        $mn = "AmsAlSheduleRoute::FlpAddPayloadTransferPpd()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsAlLogger::log($mn, " isset flpId = " . isset($dataJson->flpId));
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);

            if (isset($dataJson->transferId)) {
                //AmsAlLogger::log($mn, "Add Payload Transfer  trId =" . $dataJson->trId);
                $transferId = AmsAlSheduleRoute::AddPayloadTransferPpd($dataJson, $conn, $mn, $logModel);

                $response = new Response("success", "Added Payload for selected transfer flight.");
                $response->addData("transferId", $transferId);
            } else {
                $response = new Response("error", 'Required parameters missing in request.');
                $response->statusCode = 412;
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    public static function FlpAddPayloadTransferCpd($data) {
        $mn = "AmsAlSheduleRoute::FlpAddPayloadTransferPpd()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsAlLogger::log($mn, " isset flpId = " . isset($dataJson->flpId));
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);

            if (isset($dataJson->transferId)) {
                //AmsAlLogger::log($mn, "Add Payload Transfer  trId =" . $dataJson->trId);
                $transferId = AmsAlSheduleRoute::AddPayloadTransferCpd($dataJson, $conn, $mn, $logModel);

                $response = new Response("success", "Added Payload for selected transfer flight.");
                $response->addData("transferId", $transferId);
            } else {
                $response = new Response("error", 'Required parameters missing in request.');
                $response->statusCode = 412;
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }

    static function AddPayloadTransferPpd($dataJson, $conn, $mn, $logModel) {

        $sql = "call ams_ac.sp_al_flight_plan_payload_add_d_ppd_by_transfer_id(?,?)";
        $bound_params_r = ["ii",
            ((!isset($dataJson->transferId)) ? null : $dataJson->transferId),
            ((!isset($dataJson->ppdId)) ? null : $dataJson->ppdId),
        ];
        $affected_rows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
        //$logModel->logDebug($mn, "affected_rows: ".$affected_rows);

        return $dataJson->transferId;
    }

    static function AddPayloadTransferCpd($dataJson, $conn, $mn, $logModel) {

        $sql = "call ams_ac.sp_al_flight_plan_payload_add_d_cpd_by_transfer_id(?,?)";
        $bound_params_r = ["ii",
            ((!isset($dataJson->transferId)) ? null : $dataJson->transferId),
            ((!isset($dataJson->cpdId)) ? null : $dataJson->cpdId),
        ];
        $affected_rows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
        //$logModel->logDebug($mn, "affected_rows: ".$affected_rows);

        return $dataJson->transferId;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="DB Methods">

    static function Create($dataJson, $conn, $mn, $logModel) {

        $sql = "call ams_al.sp_flp_route_create(?, ?, ?)";
        $bound_params_r = ["iii",
            ((!isset($dataJson->depApId)) ? null : $dataJson->depApId),
            ((!isset($dataJson->arrApId)) ? null : $dataJson->arrApId),
            ((!isset($dataJson->routeId)) ? null : $dataJson->routeId)
        ];

        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        AmsLogger::log($mn, "count(result_r)=" . count($result_r));
        $data = null;
        if (isset($result_r) && count($result_r) > 0) {
            $data = $result_r[0];
            //AmsLogger::log($mn, "count(data)=".count($data));
            $id = ($data['out_route_id:']);
        }
        AmsAlLogger::log("$mn", "id=" . $id);
        return $id;
    }

    static function Update($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE ams_al.ams_flp_route 
                    SET route_nr=?, route_name=?,
                    dep_ap_id=?, arr_ap_id=?, flight_h=?, 
                    price_e=?, price_b=?, price_f=?, price_c_p_kg=?
                    WHERE route_id = ?  ";

        $bound_params_r = ["isiidddddi",
            ((!isset($dataJson->routeNr)) ? null : $dataJson->routeNr),
            ((!isset($dataJson->routeName)) ? null : $dataJson->routeName),
            ((!isset($dataJson->depApId)) ? null : $dataJson->depApId),
            ((!isset($dataJson->arrApId)) ? null : $dataJson->arrApId),
            ((!isset($dataJson->flightH)) ? null : $dataJson->flightH),
            ((!isset($dataJson->priceE)) ? null : $dataJson->priceE),
            ((!isset($dataJson->priceB)) ? null : $dataJson->priceB),
            ((!isset($dataJson->priceF)) ? null : $dataJson->priceF),
            ((!isset($dataJson->priceCPerKg)) ? null : $dataJson->priceCPerKg),
            $dataJson->routeId
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->routeId;
    }

    static function UpdateFromFlpn($dataJson, $conn, $mn, $logModel) {

        $strSQL = "update ams_al.ams_al_flp_number_schedule flpns
                    join ams_al.ams_al_flp_number flpn on flpn.flpn_id = flpns.flpn_id
                    set flpns.price_e = flpn.price_e, 
                    flpns.price_b = flpn.price_b, 
                    flpns.price_f = flpn.price_f, 
                    flpns.price_c_p_kg = flpn.price_c_p_kg
                    where flpn.route_id = ? ";

        $bound_params_r = ["i", $dataJson->routeId];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);

        return $affectedRows;
    }

    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = "SELECT r.route_id as routeId,
                r.route_nr as routeNr,
                r.route_name as routeName,
                r.flight_h as flightH,
                d.distance_km as distanceKm,
                r.dest_id as destId,
                r.rev_dest_id as revDestId,
                r.dep_ap_id as depApId,
                r.arr_ap_id as arrApId,
                r.price_e as priceE,
                r.price_b as priceB,
                r.price_f as priceF,
                r.price_c_p_kg as priceCPerKg,
                r.adate, r.udate,
                Round(flp.param_value * d.max_ticket_price_e, 2) as maxPriceE,
                Round(flp.param_value * d.max_ticket_price_b, 2)  as maxPriceB,
                Round(flp.param_value * d.max_ticket_price_f, 2)  as maxPriceF,
                ROUND(flp.param_value * (d.distance_km * cpdmax.param_value), 4) as maxPriceCPerKg
                FROM ams_al.ams_flp_route r
                join ams_wad.cfg_airport_destination d on d.dest_id = r.dest_id
                join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'max_ticket_price_cpd_kg_per_km' ) cpdmax on 1=1
		join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'flight_charter_pct_from_max_price' ) flp on 1=1
                WHERE route_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function Delete($id, $conn, $mn, $logModel) {

        $strSQL = "DELETE FROM ams_al.ams_flp_route
                   WHERE route_id = ? ";

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted route_id =" . $id);

        return $id;
    }

    static function FlpnResetPrices($dataJson, $conn, $mn, $logModel) {

        $sql = "call ams_al.ams_al_flp_number_schedule_reset_prise(?)";
        $bound_params_r = ["i",
            ((!isset($dataJson->alId)) ? null : $dataJson->alId),
        ];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, " ret_json_data= " . json_encode($ret_json_data) . " ");
        $response = new Response("success", "Reset Prices.");
        if (isset($ret_json_data) && count($ret_json_data) > 0) {
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            AmsAlLogger::log($mn, " affected_rows= " . $rowJson->affected_rows . " ");
            $response->addData("affectedRows", $rowJson->affected_rows);
        } else {
            //AmsAlLogger::log($mn, " sql count= " . $sql . " ");
            $response->addData("affectedRows", 0);
        }

        //AmsAlLogger::log("$mn", "id=" . $id);
        return $response;
    }

    // </editor-fold>
}
