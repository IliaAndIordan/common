<?php
/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Oct 26, 2023 
 *  Filename            : AmsAlFlightNumberSchedule.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2023 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of AmsAlFlightNumberSchedule
 *
 * @author IZIordanov
 */
class AmsAlFlightNumberSchedule {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $flpnsId;
    public $flpnId;
    public $flpnNumber;
    public $flpnsName;
    public $grpId;
    public $alId;
    public $wdId;
    public $dtimeH;
    public $dtimeMin;
    public $atimeH;
    public $atimeMin;
    
    public $flightH;
    public $priceE;
    public $priceB;
    public $priceF;
    public $priceCpKg;
    public $adate;
    public $udate;
    
    public $depApId;
    public $arrApId;
          
    
    public function toJSON() {
        return json_encode($this);
    }
    
    public static function fromJSON($dataJson) {
        $rv = new AmsAlFlightNumberSchedule();
        $rv->flpnsId = (!isset($dataJson->flpnsId)) ? null : $dataJson->flpnsId;
        $rv->flpnId = (!isset($dataJson->flpnId)) ? null : $dataJson->flpnId;
        $rv->flpnNumber = (!isset($dataJson->flpnNumber)) ? null : $dataJson->flpnNumber;
        $rv->flpnsName = (!isset($dataJson->flpnsName)) ? null : $dataJson->flpnsName;
        $rv->grpId = (!isset($dataJson->grpId)) ? null : $dataJson->grpId;
        $rv->alId = (!isset($dataJson->alId)) ? null : $dataJson->alId;
        $rv->wdId = (!isset($dataJson->wdId)) ? null : $dataJson->wdId;
        $rv->dtimeH = (!isset($dataJson->dtimeH)) ? null : $dataJson->dtimeH;
        $rv->dtimeMin = (!isset($dataJson->dtimeMin)) ? null : $dataJson->dtimeMin;
        $rv->atimeH = (!isset($dataJson->atimeH)) ? null : $dataJson->atimeH;
        $rv->atimeMin = (!isset($dataJson->atimeMin)) ? null : $dataJson->atimeMin;
        $rv->flightH = (!isset($dataJson->flightH)) ? null : $dataJson->flightH;
        $rv->priceE = (!isset($dataJson->priceE)) ? null : $dataJson->priceE;
        $rv->priceB = (!isset($dataJson->priceB)) ? null : $dataJson->priceB;
        $rv->priceF = (!isset($dataJson->priceF)) ? null : $dataJson->priceF;
        $rv->priceCpKg = (!isset($dataJson->priceCpKg)) ? null : $dataJson->priceCpKg;
        $rv->adate = (!isset($dataJson->adate)) ? null : $dataJson->adate;
        $rv->udate = (!isset($dataJson->udate)) ? null : $dataJson->udate;
         $rv->depApId = (!isset($dataJson->depApId)) ? null : $dataJson->depApId;
        $rv->arrApId = (!isset($dataJson->arrApId)) ? null : $dataJson->arrApId;
        return $rv;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($id) {
        $mn = "AmsAlFlightNumberSchedule::LoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsAlFlightNumberSchedule::SelectJson($id, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "AmsAlFlightNumberSchedule::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsAlLogger::log($mn, " isset flpnsId = " . isset($dataJson->flpnsId));
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objId = null;
            if(isset($dataJson->flpnsId)){
               //AmsAlLogger::log($mn, "Update flpnsId =" . $dataJson->flpnsId);
               $objId = $dataJson->flpnsId;
               AmsAlFlightNumberSchedule::Update($dataJson, $conn, $mn, $logModel);
            } else{
                AmsAlLogger::log($mn, "Create flpnId ");
                $objId = AmsAlFlightNumberSchedule::Create($dataJson, $conn, $mn, $logModel);
            }
            
            AmsAlLogger::log($mn, " flpnsId =" . $objId);
           if(isset($objId)){
                $response = new Response("success", "Airline flight number scjedule saved.");
                $objArrJ = AmsAlFlightNumberSchedule::SelectJson($objId, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                   $obj = json_decode(json_encode($objArrJ[0]));
                   $response->addData("flnschedule", $obj);
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function CopyDay($data) {
        $mn = "AmsAlFlightNumberSchedule::CopyDay()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data;
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objId = null;
            if(isset($dataJson->grpId)){
              $objArrJ = AmsAlFlightNumberSchedule::Copy($dataJson, $conn, $mn, $logModel);
              if(isset($objArrJ) && count($objArrJ)>0){
                   $ret_json_data = json_decode(json_encode($objArrJ));
                   $response = json_decode(json_encode($objArrJ));
                   //$response->addData("flnschedules", $ret_json_data);
                   //$response->addData("rowsCount", count($objArrJ));
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Table($params) {
        $mn = "AmsAlFlightNumberSchedule::Table()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //--
            $sql = "SELECT s.flpns_id as flpnsId,
                        s.flpn_id as flpnId,
                        fn.flpn_number as flpnNumber,
                        concat(a.iata, '-', LPAD(fn.flpn_number, 4, '0')) as flpnName,
                        concat(LPAD(fn.flpn_number, 4, '0'), ' ', w.wd_abbr, ' ',
                        ifnull(dep.ap_iata, dep.ap_icao) ,' ',
                        ifnull(arr.ap_iata, arr.ap_icao) ) as flpnsName,
                        s.grp_id as grpId,
                        g.ac_id as acId,
                        s.al_id as alId,
                        s.wd_id as wdId,
                        w.wd_abbr as wdAbbr,
                        s.dtime_h as dtimeH,
                        s.dtime_min as dtimeMin,
                        s.atime_h as atimeH,
                        s.atime_min as atimeMin,
                        s.flight_h as flightH,
                        s.price_e as priceE,
                        s.price_b as priceB,
                        s.price_f as priceF,
                        s.price_c_p_kg as priceCpKg,
                        s.adate, s.udate,
                        fn.dep_ap_id as depApId,
                        fn.arr_ap_id as arrApId,
                        d.distance_km as distanceKm,
                        d.min_rw_lenght_m as minRunwayM,
                        d.min_mtow_kg as minMtowKg,
                        if(g.is_active and g.start_date <= current_date() , 1, 0) as isActive,
                        g.start_date as grpStDate,
                        g.is_active as grpIsActive,
                        st.fl_count as flCount, st.revenue, 
                        st.avg_delay_min as avgDelayMin, 
                        st.avg_pax_pct_full avgPaxPctFull, 
                        st.avg_payload_kg_pct_full avgCargoKgPctFull
                        FROM ams_al.ams_al_flp_number_schedule s
                        join ams_al.ams_al_flp_group g on g.grp_id = s.grp_id
                        join ams_al.ams_al_flp_number fn on fn.flpn_id = s.flpn_id
                        join ams_wad.cfg_airport_destination d on d.dest_id = fn.dest_id
                        join ams_wad.cfg_weekday w on w.wd_id = s.wd_id
                        join ams_wad.cfg_airport arr on arr.ap_id = fn.arr_ap_id
                        join ams_wad.cfg_airport dep on dep.ap_id = fn.dep_ap_id
                        join ams_al.ams_airline a on a.al_id = s.al_id
                        left join ams_al.ams_al_flp_number_schedule_st st on st.flpns_id = s.flpns_id
                        ";
            $sqlWhere="";
            
            if(isset($params->alId) && strlen($params->alId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND s.al_id =".$params->alId." ";
                }
                else{
                    $sqlWhere = " WHERE s.al_id =".$params->alId." ";
                }
            }
            if(isset($params->grpId) && strlen($params->grpId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND s.grp_id =".$params->grpId." ";
                }
                else{
                    $sqlWhere = " WHERE s.grp_id =".$params->grpId." ";
                }
            }
            if(isset($params->wdId) && strlen($params->wdId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND s.wd_id =".$params->wdId." ";
                }
                else{
                    $sqlWhere = " WHERE s.wd_id =".$params->wdId." ";
                }
            }
            
            if(isset($params->flpnId) && strlen($params->flpnId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND s.flpn_id =".$params->flpnId." ";
                }
                else{
                    $sqlWhere = " WHERE s.flpn_id =".$params->flpnId." ";
                }
            }
           
            if(isset($params->arrApId) && strlen($params->arrApId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND fn.arr_ap_id =".$params->arrApId." ";
                }
                else{
                    $sqlWhere = " WHERE fn.arr_ap_id =".$params->arrApId." ";
                }
            }
            if(isset($params->depApId) && strlen($params->depApId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND fn.dep_ap_id =".$params->depApId." ";
                }
                else{
                    $sqlWhere = " WHERE fn.dep_ap_id =".$params->depApId." ";
                }
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (ifnull(dep.ap_iata, dep.ap_icao) like '%".$params->qry_filter."%' OR ";
                    $sqlWhere .= " ifnull(arr.ap_iata, arr.ap_icao) like '%".$params->qry_filter."%' ) ";
                }
                else{
                    $sqlWhere .= " WHERE ((ifnull(dep.ap_iata, dep.ap_icao) like '%".$params->qry_filter."%' OR ";
                    $sqlWhere .= " ifnull(arr.ap_iata, arr.ap_icao) like '%".$params->qry_filter."%' ) ";
                }
            }
            $sqlOrder = " order by s.wd_id, s.dtime_h, s.dtime_min ";
            if(isset($params->arrApId) && strlen($params->arrApId)>0){
                $sqlOrder = " order by s.wd_id, s.atime_h, s.atime_min ";
            }
            
            if(isset($params->qry_orderCol)){
                $sqlOrder .= ", ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("flnschedules", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_al.ams_al_flp_number_schedule s
                    join ams_al.ams_al_flp_group g on g.grp_id = s.grp_id
                    join ams_al.ams_al_flp_number fn on fn.flpn_id = s.flpn_id
                    join ams_wad.cfg_airport_destination d on d.dest_id = fn.dest_id
                    join ams_wad.cfg_weekday w on w.wd_id = s.wd_id
                    join ams_wad.cfg_airport arr on arr.ap_id = fn.arr_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = fn.dep_ap_id
                    join ams_al.ams_airline a on a.al_id = s.al_id
                    left join ams_al.ams_al_flp_number_schedule_st st on st.flpns_id = s.flpns_id
                     ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
   // <editor-fold defaultstate="collapsed" desc="DB Methods">
     
    static function Create($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "INSERT INTO ams_al.ams_al_flp_number_schedule 
                (flpn_id, grp_id, al_id, 
                wd_id, dtime_h, dtime_min, atime_h, atime_min,
                flight_h, price_e, price_b, price_f, price_c_p_kg) 
                VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" ;

        $bound_params_r = ["iiiiiiiiddddd",
             ((!isset($dataJson->flpnId)) ? null : $dataJson->flpnId),
            ((!isset($dataJson->grpId)) ? null : $dataJson->grpId),
            ((!isset($dataJson->alId)) ? null : $dataJson->alId),
            ((!isset($dataJson->wdId)) ? null : $dataJson->wdId),
            ((!isset($dataJson->dtimeH)) ? null : $dataJson->dtimeH),
            ((!isset($dataJson->dtimeMin)) ? null : $dataJson->dtimeMin),
            ((!isset($dataJson->atimeH)) ? null : $dataJson->atimeH),
            ((!isset($dataJson->atimeMin)) ? null : $dataJson->atimeMin),
            ((!isset($dataJson->flightH)) ? null : $dataJson->flightH),
            ((!isset($dataJson->priceE)) ? null : $dataJson->priceE),
            ((!isset($dataJson->priceB)) ? null : $dataJson->priceB),
            ((!isset($dataJson->priceF)) ? null : $dataJson->priceF),
            ((!isset($dataJson->priceCpKg)) ? null : $dataJson->priceCpKg)
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log("$mn", "id=" . $id);       
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_al.ams_al_flp_number_schedule 
                    SET flpn_id =?, grp_id =?, al_id =?, 
                        wd_id =?, dtime_h =?, dtime_min =?, atime_h =?, atime_min =?,
                        flight_h =?, price_e =?, price_b =?, price_f =?, price_c_p_kg =?
                    WHERE flpns_id = ?  " ;

        $bound_params_r = ["iiiiiiiidddddi",
            ((!isset($dataJson->flpnId)) ? null : $dataJson->flpnId),
            ((!isset($dataJson->grpId)) ? null : $dataJson->grpId),
            ((!isset($dataJson->alId)) ? null : $dataJson->alId),
            ((!isset($dataJson->wdId)) ? null : $dataJson->wdId),
            ((!isset($dataJson->dtimeH)) ? null : $dataJson->dtimeH),
            ((!isset($dataJson->dtimeMin)) ? null : $dataJson->dtimeMin),
            ((!isset($dataJson->atimeH)) ? null : $dataJson->atimeH),
            ((!isset($dataJson->atimeMin)) ? null : $dataJson->atimeMin),
            ((!isset($dataJson->flightH)) ? null : $dataJson->flightH),
            ((!isset($dataJson->priceE)) ? null : $dataJson->priceE),
            ((!isset($dataJson->priceB)) ? null : $dataJson->priceB),
            ((!isset($dataJson->priceF)) ? null : $dataJson->priceF),
            ((!isset($dataJson->priceCpKg)) ? null : $dataJson->priceCpKg),
            $dataJson->flpnsId
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->flpnsId;
    }
    
    static function SelectJson($id, $conn, $mn, $logModel){
        
        /**
         * SELECT s.flpns_id as flpnsId,
                    s.flpn_id as flpnId,
                    fn.flpn_number as flpnNumber,
                    concat(LPAD(fn.flpn_number, 4, '0'), ' ', w.wd_abbr, ' ',
                    if(dep.ap_iata, dep.ap_iata, dep.ap_icao) ,' ',
                    if(arr.ap_iata, arr.ap_iata, arr.ap_icao) ) as flpnsName,
                    s.grp_id as grpId,
                    s.al_id as alId,
                    s.wd_id as wdId,
                    s.dtime_h as dtimeH,
                    s.dtime_min as dtimeMin,
                    s.atime_h as atimeH,
                    s.atime_min as atimeMin,
                    s.flight_h as flightH,
                    s.price_e as priceE,
                    s.price_b as priceB,
                    s.price_f as priceF,
                    s.price_c_p_kg as priceCpKg,
                    s.adate, s.udate,
                    fn.dep_ap_id as depApId,
                    fn.arr_ap_id as arrApId,
                    d.distance_km as distanceKm,
                    d.min_rw_lenght_m as minRunwayM,
                    d.min_mtow_kg as minMtowKg
                    FROM ams_al.ams_al_flp_number_schedule s
                    join ams_al.ams_al_flp_number fn on fn.flpn_id = s.flpn_id
                    join ams_wad.cfg_airport_destination d on d.dest_id = fn.dest_id
                    join ams_wad.cfg_weekday w on w.wd_id = s.wd_id
                    join ams_wad.cfg_airport arr on arr.ap_id = fn.arr_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = fn.dep_ap_id
         */
        
        $sql = "SELECT s.flpns_id as flpnsId,
                    s.flpn_id as flpnId,
                    fn.flpn_number as flpnNumber,
                    concat(a.iata, '-', LPAD(fn.flpn_number, 4, '0')) as flpnName,
                    concat(LPAD(fn.flpn_number, 4, '0'), ' ', w.wd_abbr, ' ',
                    ifnull(dep.ap_iata, dep.ap_icao) ,' ',
                    ifnull(arr.ap_iata, arr.ap_icao) ) as flpnsName,
                    s.grp_id as grpId,
                    g.ac_id as acId,
                    s.al_id as alId,
                    s.wd_id as wdId,
                    w.wd_abbr as wdAbbr,
                    s.dtime_h as dtimeH,
                    s.dtime_min as dtimeMin,
                    s.atime_h as atimeH,
                    s.atime_min as atimeMin,
                    s.flight_h as flightH,
                    s.price_e as priceE,
                    s.price_b as priceB,
                    s.price_f as priceF,
                    s.price_c_p_kg as priceCpKg,
                    s.adate, s.udate,
                    fn.dep_ap_id as depApId,
                    fn.arr_ap_id as arrApId,
                    d.distance_km as distanceKm,
                    d.min_rw_lenght_m as minRunwayM,
                    d.min_mtow_kg as minMtowKg,
                    if(g.is_active and g.start_date <= current_date() , 1, 0) as isActive,
                    g.start_date as grpStDate,
                    g.is_active as grpIsActive,
                    st.fl_count as flCount, st.revenue, 
                    st.avg_delay_min as avgDelayMin, 
                    st.avg_pax_pct_full avgPaxPctFull, 
                    st.avg_payload_kg_pct_full avgCargoKgPctFull
                    FROM ams_al.ams_al_flp_number_schedule s
                    join ams_al.ams_al_flp_group g on g.grp_id = s.grp_id
                    join ams_al.ams_al_flp_number fn on fn.flpn_id = s.flpn_id
                    join ams_wad.cfg_airport_destination d on d.dest_id = fn.dest_id
                    join ams_wad.cfg_weekday w on w.wd_id = s.wd_id
                    join ams_wad.cfg_airport arr on arr.ap_id = fn.arr_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = fn.dep_ap_id
                    join ams_al.ams_airline a on a.al_id = s.al_id
                    left join ams_al.ams_al_flp_number_schedule_st st on st.flpns_id = s.flpns_id
                WHERE s.flpns_id = ? " ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $sql = "DELETE FROM ams_al.ams_al_flp_number_schedule
                   WHERE flpns_id = ? " ;
        $sql = "call sp_ams_al_flp_number_schedule_delete(?)" ;
        $bound_params_r = ["i", $id];
        $affected_rows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
        $logModel->logDebug($mn, "deleted rows : ".$affected_rows);
        //$id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, "deleted flpns_id =" . $id);
                    
        return $id;
    }
    
    static function Copy($dataJson, $conn, $mn, $logModel){
        
        $sql = "CALL ams_al.ams_al_flp_number_schedule_copy_day(?, ?, ?)";
        $bound_params_r = ["iii",
            ((!isset($dataJson->grpId)) ? null : $dataJson->grpId),
            ((!isset($dataJson->wdIdFrom)) ? null : $dataJson->wdIdFrom),
            ((!isset($dataJson->wdIdTo)) ? null : $dataJson->wdIdTo),
        ];

        $result_r = $conn->preparedSelectJson($sql, $bound_params_r, $logModel);
        $payload_json = (object) [
            'grpId' => $dataJson->grpId,
            'wdId' => $dataJson->wdIdTo,
            'qry_limit' => 1000,
            'qry_offset' => 0,
        ];
        //$response = AmsAlFlightNumberSchedule::Table($payload_json);
        $logModel->logDebug($mn, "count(result_r)=".count($result_r));
        return $result_r;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Flight Plan Payload">
    
    public static function SaveFlpStatus($data) {
        $mn = "AmsAlFlightNumberSchedule::SaveFlpStatus()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsAlLogger::log($mn, " isset flpId = " . isset($dataJson->flpId));
         $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objId = null;
            if(isset($dataJson->flpId)){
               //AmsAlLogger::log($mn, "Update flpId =" . $dataJson->flpId);
               $objId = $dataJson->flpId;
               AmsAlFlightNumberSchedule::UpdateFlpStatus($dataJson, $conn, $mn, $logModel);
            } 
            
            //AmsAlLogger::log($mn, " flpId =" . $objId);
            if(isset($objId)){
                $response = AmsAlFlightNumberSchedule::TableFlightPlanPayload($dataJson);
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function FlpAddPayloadSingle($data) {
        $mn = "AmsAlFlightNumberSchedule::FlpAddPayloadSingle()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsAlLogger::log($mn, " isset flpId = " . isset($dataJson->flpId));
         $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            
            if(isset($dataJson->flpId)){
               //AmsAlLogger::log($mn, "Update flpId =" . $dataJson->flpId);
               $objId = $dataJson->flpId;
               AmsAlFlightNumberSchedule::AddPayloadSingle($dataJson, $conn, $mn, $logModel);
                
                $response = AmsAlFlightNumberSchedule::TableFlightPlanPayload($dataJson);
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function TableFlightPlanPayload($params) {
        $mn = "AmsAlFlightNumberSchedule::TableFlightPlanPayload()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //--
            $sql = "SELECT fpl.flp_id as flpId,
                    fpl.fl_type_id as flTypeId,
                    fpl.flpns_id as flpnsId,
                    fpl.week as yearweek,
                    flpns.wd_id  as wdId,
                    flpns.flpn_id as flpnId,
                    fpl.fl_number as flNumber,
                    fpl.fl_name as flName,
                    fpl.route_id as routeId,
                    fpl.dest_id as destId,
                    flpns.grp_id as grpId,
                    g.grp_name as grpName,
                    fpl.d_ap_id as dApId,
                    fpl.a_ap_id as aApId,
                    fpl.al_id as alId,
                    fpl.ac_id as acId,
                    ac.macId, ac.homeApId, ac.acStatusId, ac.opTimeMin,
                    ac.acTypeId, ac.flight_h_in_queue as flightHInQueue,
                    ac.flight_in_queue as flightInQueue,
                    ac.last_flq_atime as lastFlqAtime,
                    fpl.distance_km as distanceKm,
                    fpl.oil_l as oilL,
                    fpl.flight_h as flightH,
                    fpl.pax, 
                    fpl.payload_kg as payloadKg,
                    fpl.price, fpl.dtime, fpl.atime,
                    fpl.flp_status_id as flpStatusId,
                    fpl.available_pax_f as availablePaxF,
                    fpl.available_pax_b as availablePaxB,
                    fpl.available_pax_e as availablePaxE,
                    fpl.available_cargo_kg as availableCargoKg,
                    fpl.flppl_count as flpplCount,
                    fpl.flppl_pax_f as flpplPaxF,
                    fpl.flppl_pax_b as flpplPaxB,
                    fpl.flppl_pax_e as flpplPaxE,
                    fpl.flppl_payload_kg as flpplPayloadKg,
                    fpl.flppl_price as flpplPrice,
                    fpl.remain_pax_f as remainPaxF,
                    fpl.remain_pax_b as remainPaxB,
                    fpl.remain_pax_e as remainPaxE,
                    fpl.remain_payload_kg as remainPayloadKg,
                    fpl.pax_pct_full as paxPctFull,
                    fpl.payload_kg_pct_full as payloadKgPctFull,
                    fpl.payload_jobs as payloadJobsCount
                    FROM ams_ac.ams_al_flight_plan_payload_pct_full fpl
                    join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = fpl.flpns_id
                    join ams_al.ams_al_flp_group g on g.grp_id = flpns.grp_id
                    left join ams_ac.v_ams_ac ac on ac.acId = fpl.ac_id
                    join ams_wad.cfg_airport arr on arr.ap_id = fpl.a_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = fpl.d_ap_id ";
                        
            $sqlWhere="";
            
            if(isset($params->alId) && strlen($params->alId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND fpl.al_id =".$params->alId." ";
                }
                else{
                    $sqlWhere = " WHERE fpl.al_id =".$params->alId." ";
                }
            }
            
            if(isset($params->acId) && strlen($params->acId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND fpl.ac_id =".$params->acId." ";
                }
                else{
                    $sqlWhere = " WHERE fpl.ac_id =".$params->acId." ";
                }
            }
            
            if(isset($params->flpId) && strlen($params->flpId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND fpl.flp_id =".$params->flpId." ";
                }
                else{
                    $sqlWhere = " WHERE fpl.flp_id =".$params->flpId." ";
                }
            }
            
            if(isset($params->flpIdIn) && count($params->flpIdIn)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND fpl.flp_id in (".implode(",", ($params->flpIdIn)).") ";
                }
                else{
                    $sqlWhere = " WHERE fpl.flp_id in (".implode(",", ($params->flpIdIn)).") ";
                }
            }
            if(isset($params->grpId) && strlen($params->grpId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND flpns.grp_id =".$params->grpId." ";
                }
                else{
                    $sqlWhere = " WHERE flpns.grp_id =".$params->grpId." ";
                }
            }
            if(isset($params->wdId) && strlen($params->wdId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND flpns.wd_id =".$params->wdId." ";
                }
                else{
                    $sqlWhere = " WHERE flpns.wd_id =".$params->wdId." ";
                }
            }
            
            if(isset($params->flpnId) && strlen($params->flpnId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND fpl.flpn_id =".$params->flpnId." ";
                }
                else{
                    $sqlWhere = " WHERE fpl.flpn_id =".$params->flpnId." ";
                }
            }
           
            if(isset($params->arrApId) && strlen($params->arrApId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND fpl.a_ap_id =".$params->arrApId." ";
                }
                else{
                    $sqlWhere = " WHERE fpl.a_ap_id =".$params->arrApId." ";
                }
            }
            if(isset($params->depApId) && strlen($params->depApId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND fpl.d_ap_id =".$params->depApId." ";
                }
                else{
                    $sqlWhere = " WHERE fpl.d_ap_id =".$params->depApId." ";
                }
            }
            /*
            if(isset($params->qry_filter) && strlen($params->qry_filter)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (ifnull(dep.ap_iata, dep.ap_icao) like '%".$params->qry_filter."%' OR ";
                    $sqlWhere .= " ifnull(arr.ap_iata, arr.ap_icao) like '%".$params->qry_filter."%' ) ";
                }
                else{
                    $sqlWhere .= " WHERE (ifnull(dep.ap_iata, dep.ap_icao) like '%".$params->qry_filter."%' OR ";
                    $sqlWhere .= " ifnull(arr.ap_iata, arr.ap_icao) like '%".$params->qry_filter."%' ) ";
                }
            }*/
            if(isset($params->qry_filter) && strlen($params->qry_filter)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND ( fpl.fl_name like '%".$params->qry_filter."%' OR ";
                    $sqlWhere .= " fpl.fl_number like '%".$params->qry_filter."%' ) ";
                }
                else{
                    $sqlWhere .= " WHERE (fpl.fl_name like '%".$params->qry_filter."%' OR ";
                    $sqlWhere .= " fpl.fl_number like '%".$params->qry_filter."%' ) ";
                }
            }
            
            $sqlOrder = " order by fpl.dtime, fpl.atime ";
            if(isset($params->qry_orderCol)){
                //$sqlOrder .= ", fpl.".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            AmsAlLogger::log($mn, " sqlWhere= " . $sqlWhere . " ");
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("flightplans", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_al_flight_plan_payload_pct_full fpl
                    join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = fpl.flpns_id
                    join ams_al.ams_al_flp_group g on g.grp_id = flpns.grp_id
                    left join ams_ac.v_ams_ac ac on ac.acId = fpl.ac_id
                    join ams_wad.cfg_airport arr on arr.ap_id = fpl.a_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = fpl.d_ap_id
                     ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function TableFlightPlanPayloadH($params) {
        $mn = "AmsAlFlightNumberSchedule::TableFlightPlanPayloadH()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //--
            $sql = "SELECT fpl.flp_id as flpId,
                    fpl.fl_type_id as flTypeId,
                    fpl.flpns_id as flpnsId,
                    fpl.week as yearweek,
                    flpns.wd_id  as wdId,
                    fpl.fl_number as flNumber,
                    fpl.fl_name as flName,
                    fpl.route_id as routeId,
                    fpl.dest_id as destId,
                    flpns.grp_id as grpId,
                    g.grp_name as grpName,
                    fpl.d_ap_id as dApId,
                    fpl.a_ap_id as aApId,
                    fpl.al_id as alId,
                    fpl.ac_id as acId,
                    ac.macId, ac.homeApId, ac.acStatusId, ac.opTimeMin,
                    ac.flight_h_in_queue as flightHInQueue,
                    ac.flight_in_queue as flightInQueue,
                    ac.last_flq_atime as lastFlqAtime,
                    fpl.distance_km as distanceKm,
                    fpl.oil_l as oilL,
                    fpl.flight_h as flightH,
                    fpl.pax, 
                    fpl.payload_kg as payloadKg,
                    fpl.price, fpl.dtime, fpl.atime,
                    fpl.flp_status_id as flpStatusId,
                    fpl.available_pax_f as availablePaxF,
                    fpl.available_pax_b as availablePaxB,
                    fpl.available_pax_e as availablePaxE,
                    fpl.available_cargo_kg as availableCargoKg,
                    fpl.flppl_count as flpplCount,
                    fpl.flppl_pax_f as flpplPaxF,
                    fpl.flppl_pax_b as flpplPaxB,
                    fpl.flppl_pax_e as flpplPaxE,
                    fpl.flppl_payload_kg as flpplPayloadKg,
                    fpl.flppl_price as flpplPrice,
                    fpl.remain_pax_f as remainPaxF,
                    fpl.remain_pax_b as remainPaxB,
                    fpl.remain_pax_e as remainPaxE,
                    fpl.remain_payload_kg as remainPayloadKg,
                    fpl.pax_pct_full as paxPctFull,
                    fpl.payload_kg_pct_full as payloadKgPctFull,
                    fpl.payload_jobs as payloadJobsCount
                    FROM ams_ac.ams_al_flight_plan_payload_h_pct_full fpl
                    left join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = fpl.flpns_id
                    join ams_al.ams_al_flp_group g on g.grp_id = flpns.grp_id
                    left join ams_ac.v_ams_ac ac on ac.acId = fpl.ac_id
                    join ams_wad.cfg_airport arr on arr.ap_id = fpl.a_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = fpl.d_ap_id
                        ";
            $sqlWhere="";
            
            if(isset($params->alId) && strlen($params->alId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND fpl.al_id =".$params->alId." ";
                }
                else{
                    $sqlWhere = " WHERE fpl.al_id =".$params->alId." ";
                }
            }
            if(isset($params->grpId) && strlen($params->grpId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND flpns.grp_id =".$params->grpId." ";
                }
                else{
                    $sqlWhere = " WHERE flpns.grp_id =".$params->grpId." ";
                }
            }
            if(isset($params->wdId) && strlen($params->wdId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND flpns.wd_id =".$params->wdId." ";
                }
                else{
                    $sqlWhere = " WHERE flpns.wd_id =".$params->wdId." ";
                }
            }
            
            if(isset($params->flpnId) && strlen($params->flpnId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND fpl.flpn_id =".$params->flpnId." ";
                }
                else{
                    $sqlWhere = " WHERE fpl.flpn_id =".$params->flpnId." ";
                }
            }
           
            if(isset($params->arrApId) && strlen($params->arrApId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND fpl.a_ap_id =".$params->arrApId." ";
                }
                else{
                    $sqlWhere = " WHERE fpl.a_ap_id =".$params->arrApId." ";
                }
            }
            if(isset($params->depApId) && strlen($params->depApId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND fpl.d_ap_id =".$params->depApId." ";
                }
                else{
                    $sqlWhere = " WHERE fpl.d_ap_id =".$params->depApId." ";
                }
            }
            
            if(isset($params->qry_filter) && strlen($params->qry_filter)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (if(dep.ap_iata, dep.ap_iata, dep.ap_icao) like '%".$params->qry_filter."%' OR ";
                    $sqlWhere .= " if(arr.ap_iata, arr.ap_iata, arr.ap_icao) like '%".$params->qry_filter."%' ) ";
                }
                else{
                    $sqlWhere .= " WHERE (if(dep.ap_iata, dep.ap_iata, dep.ap_icao) like '%".$params->qry_filter."%' OR ";
                    $sqlWhere .= " if(arr.ap_iata, arr.ap_iata, arr.ap_icao) like '%".$params->qry_filter."%' ) ";
                }
            }
            $sqlOrder = " order by flpns.wd_id, fpl.dtime, fpl.atime ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= ", ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("flightplans", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_al_flight_plan_payload_h_pct_full fpl
                    left join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = fpl.flpns_id
                    join ams_al.ams_al_flp_group g on g.grp_id = flpns.grp_id
                    left join ams_ac.v_ams_ac ac on ac.acId = fpl.ac_id
                    join ams_wad.cfg_airport arr on arr.ap_id = fpl.a_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = fpl.d_ap_id
                     ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function TableFlpPayloads($params) {
        $mn = "AmsAlFlightNumberSchedule::TableFlpPayloads()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //--
            $sql = "SELECT pld.flppl_id as flpplId,
                pld.flp_id as flpId,
                pld.pcpd_id as pcpdId,
                pld.pax as pax,
                pld.payload_kg as payloadKg,
                pld.pax_class_id as payloadType,
                pld.ppd_type_id as ppdTypeId,
                pld.note as description,
                pld.price, pld.adate
                FROM ams_ac.ams_al_flight_plan_payload pld
                where pld.flp_id = ?
                order by pld.adate ";
            
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["i", $params->flpId];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("payloads", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_ac.ams_al_flight_plan_payload pld
                    where pld.flp_id = ? "  ;
            $bound_params_r = ["i", $params->flpId];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function FlpDelete($data) {
        $mn = "AmsAlFlightNumberSchedule::FlpDelete()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsAlLogger::log($mn, " isset flpId = " . isset($dataJson->flpId));
         $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            
            if(isset($dataJson->flpId)){
               //AmsAlLogger::log($mn, "Update flpId =" . $dataJson->flpId);
               $payload_json = AmsAlFlightNumberSchedule::DeleteFlp($dataJson, $conn, $mn, $logModel);
               $response = new Response("success", "Flight plan deleted.");
               $response->addData("flpId", $payload_json->flpId);
               $response->addData("totalRows", $payload_json->affected_rows);
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc=" Flight Plan Payload DB Methods">
    
    static function UpdateFlpStatus($dataJson, $conn, $mn, $logModel){
        
        $strSQL = "UPDATE ams_ac.ams_al_flight_plan 
                    SET flp_status_id = ?, payload_jobs = ?
                    WHERE flp_id = ?  " ;

        $bound_params_r = ["iii",
            ((!isset($dataJson->flpStatusId)) ? null : $dataJson->flpStatusId),
            ((!isset($dataJson->payloadJobs)) ? null : $dataJson->payloadJobs),
            $dataJson->flpId
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->flpId;
    }
    
    static function AddPayloadSingle($dataJson, $conn, $mn, $logModel){
        
        $sql = "call ams_ac.sp_al_flight_plan_payload_add_d_flp_ppd_v01(?)";
         $bound_params_r = ["i",
            ((!isset($dataJson->flpId)) ? null : $dataJson->flpId),
        ];
        $affected_rows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
        $sql = "call ams_ac.sp_al_flight_plan_payload_add_d_flp_cpd_v01(?)";
         $bound_params_r = ["i",
            ((!isset($dataJson->flpId)) ? null : $dataJson->flpId),
        ];
        $affected_rows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
        $sql = "call ams_ac.sp_al_flight_plan_payload_add_pcpd_single(?)";
        $bound_params_r = ["i",
            ((!isset($dataJson->flpId)) ? null : $dataJson->flpId),
        ];
        $affected_rows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
        //call ams_ac.sp_al_flight_plan_payload_add_d_flp_ppd_v01(i_flp_id);
	//call ams_ac.sp_al_flight_plan_payload_add_d_flp_cpd_v01(i_flp_id);
        AmsAlLogger::log($mn, "affected_rows: ".$affected_rows);
        //$result_r = $conn->preparedSelectJson($sql, $bound_params_r, $logModel);
        $payload_json = (object) [
            'flpId' => $dataJson->flpId,
        ];
        return $dataJson->flpId;
    }
    
    static function DeleteFlp($dataJson, $conn, $mn, $logModel){
        
        $sql = "call ams_ac.sp_al_flight_plan_delete_id(?)";
        $bound_params_r = ["i",
            ((!isset($dataJson->flpId)) ? null : $dataJson->flpId),
        ];
        $affected_rows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "affected_rows: ".$affected_rows);
        //$result_r = $conn->preparedSelectJson($sql, $bound_params_r, $logModel);
        $payload_json = (object) [
            'flpId' => $dataJson->flpId,
            'affected_rows' => $affected_rows
        ];
        return $payload_json;
    }
    
    // </editor-fold>
}

