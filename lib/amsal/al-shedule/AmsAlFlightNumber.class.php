<?php
/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation       : Oct 26, 2023 
 *  Filename            : AmsAlFlightNumber.class
 *  Author              : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2023 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of AmsAlFlightNumber
 *
 * @author IZIordanov
 */
class AmsAlFlightNumber {
    
    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $flpnId;
    public $flpnNumber;
    public $alId;
    public $depApId;
    public $arrApId;
    public $destId;
    public $routeId;
    public $flightH;
    public $priceE;
    public $priceB;
    public $priceF;
    public $priceCpKg;
    public $adate;
    public $udate;
          
    
    public function toJSON() {
        return json_encode($this);
    }
    
    public static function fromJSON($dataJson) {
        $rv = new AmsAlFlightNumber();
        $rv->flpnId = (!isset($dataJson->flpnId)) ? null : $dataJson->flpnId;
        $rv->flpnNumber = (!isset($dataJson->flpnNumber)) ? null : $dataJson->flpnNumber;
        $rv->alId = (!isset($dataJson->alId)) ? null : $dataJson->alId;
        $rv->depApId = (!isset($dataJson->depApId)) ? null : $dataJson->depApId;
        $rv->arrApId = (!isset($dataJson->arrApId)) ? null : $dataJson->arrApId;
        $rv->destId = (!isset($dataJson->destId)) ? null : $dataJson->destId;
        $rv->routeId = (!isset($dataJson->routeId)) ? null : $dataJson->routeId;
        $rv->flightH = (!isset($dataJson->flightH)) ? null : $dataJson->flightH;
        $rv->priceE = (!isset($dataJson->priceE)) ? null : $dataJson->priceE;
        $rv->priceB = (!isset($dataJson->priceB)) ? null : $dataJson->priceB;
        $rv->priceF = (!isset($dataJson->priceF)) ? null : $dataJson->priceF;
        $rv->priceCpKg = (!isset($dataJson->priceCpKg)) ? null : $dataJson->priceCpKg;
        $rv->adate = (!isset($dataJson->adate)) ? null : $dataJson->adate;
        $rv->udate = (!isset($dataJson->udate)) ? null : $dataJson->udate;
        return $rv;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadById($id) {
        $mn = "AmsAlFlightNumber::LoadById(" . $id . ")";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsAlFlightNumber::SelectJson($id, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function Save($data) {
        $mn = "AmsAlFlightNumber::Save()";
        AmsAlLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        AmsAlLogger::log($mn, " isset flpnId = " . isset($dataJson->flpnId));
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objId = null;
            if(isset($dataJson->flpnId)){
               AmsAlLogger::log($mn, "Update flpnId =" . $dataJson->flpnId);
               $objId = $dataJson->flpnId;
               AmsAlFlightNumber::Update($dataJson, $conn, $mn, $logModel);
               if(isset($dataJson->routeId) && isset($dataJson->alId)){
                    AmsAlFlightNumber::UpdateRoutPrices($dataJson, $conn, $mn, $logModel);
               }
            } else{
                AmsAlLogger::log($mn, "Create flpnId ");
                $objId = AmsAlFlightNumber::Create($dataJson, $conn, $mn, $logModel);
                $dataJson->flpnId = $objId;
                if(isset($dataJson->flpnId)){
                    AmsAlFlightNumber::Update($dataJson, $conn, $mn, $logModel);
                }
                
            }
            
            AmsAlLogger::log($mn, " flpnId =" . $objId);
           if(isset($objId)){
                $objArrJ = AmsAlFlightNumber::SelectJson($objId, $conn, $mn, $logModel);
                if(isset($objArrJ) && count($objArrJ)>0){
                    $response = new Response("success", "Airline flight number saved.");
                    $obj = json_decode(json_encode($objArrJ[0]));
                    $response->addData("flnr", $obj);
                }
            }
            
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . json_encode($response));
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function alFlightNumberTable($params) {
        $mn = "AmsAlFlightNumber::alFlightNumberTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //--
            $sql = "SELECT flpn.flpn_id as flpnId,
                        flpn.flpn_number as flpnNumber,
                        concat( LPAD(flpn.flpn_number, 4, '0'), ' ',
                            ifnull(dep.ap_iata,  dep.ap_icao) ,' ',
                            ifnull(arr.ap_iata, arr.ap_icao) )   as flpnName,
                        flpn.al_id as alId,
                        flpn.dep_ap_id as depApId,
                        flpn.arr_ap_id as arrApId,
                        flpn.dest_id as destId,
                        flpn.route_id as routeId,
                        flpn.flight_h as flightH,
                        flpn.price_e as priceE,
                        flpn.price_b as priceB,
                        flpn.price_f as priceF,
                        flpn.price_c_p_kg as priceCpKg,
                        flpn.adate, flpn.udate,
                        ifnull(flpn.flpn_schedules_count, 0) as flpnSchedulesCount,
                        Round(flp.param_value * d.max_ticket_price_e, 2) as maxPriceE,
                        Round(flp.param_value * d.max_ticket_price_b, 2)  as maxPriceB,
                        Round(flp.param_value * d.max_ticket_price_f, 2)  as maxPriceF,
                        ROUND(flp.param_value * (d.distance_km * cpdmax.param_value), 4) as maxPriceCPerKg,
                        d.distance_km as distanceKm,
                        d.min_rw_lenght_m as minRunwayM,
                        d.min_mtow_kg as minMtow,
                        sum(ifnull(st.fl_count,0)) as flCount, round(sum(ifnull(st.revenue, 0)),2) as revenue, 
                        round(avg(ifnull(st.avg_delay_min,0)),0) as avgDelayMin, 
                        round(avg(ifnull(st.avg_pax_pct_full,0)),0) as avgPaxPctFull, 
                        round(avg(ifnull(st.avg_payload_kg_pct_full,0)),0) as avgCargoKgPctFull
                    FROM ams_al.ams_al_flp_number flpn
                    left join ams_wad.cfg_airport_destination d on d.dest_id = flpn.dest_id
                    join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'max_ticket_price_cpd_kg_per_km' ) cpdmax on 1=1
                    join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'flight_charter_pct_from_max_price' ) flp on 1=1
                    join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id 
                    left join ams_al.ams_al_flp_number_schedule flpns on flpns.flpn_id = flpn.flpn_id
                    left join ams_al.ams_al_flp_number_schedule_st st on st.flpns_id = flpns.flpns_id 
                    ";
            
            
            $sqlWhere="";
           
            
            if(isset($params->alId) && strlen($params->alId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND flpn.al_id =".$params->alId." ";
                }
                else{
                    $sqlWhere = " WHERE flpn.al_id =".$params->alId." ";
                }
            }
            if(isset($params->arrApId) && strlen($params->arrApId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND flpn.arr_ap_id =".$params->arrApId." ";
                }
                else{
                    $sqlWhere = " WHERE flpn.arr_ap_id =".$params->arrApId." ";
                }
            }
            if(isset($params->depApId) && strlen($params->depApId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND flpn.dep_ap_id =".$params->depApId." ";
                }
                else{
                    $sqlWhere = " WHERE flpn.dep_ap_id =".$params->depApId." ";
                }
            }
            
             if(isset($params->qry_filter) && strlen($params->qry_filter)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND (ifnull(dep.ap_iata, dep.ap_icao) like '%".$params->qry_filter."%' OR ";
                    $sqlWhere .= " ifnull(arr.ap_iata, arr.ap_icao) like '%".$params->qry_filter."%' ) ";
                }
                else{
                    $sqlWhere .= " WHERE (ifnull(dep.ap_iata, dep.ap_icao) like '%".$params->qry_filter."%' OR ";
                    $sqlWhere .= " ifnull(arr.ap_iata, arr.ap_icao) like '%".$params->qry_filter."%' ) ";
                }
            }
           
            $sqlOrder = " group by flpn.flpn_id ";
            if(isset($params->qry_orderCol)){
                $sqlOrder .= " order by ".$params->qry_orderCol." ".($params->qry_isDesc?"desc":" asc");
            }
            else{
                $sqlOrder .= "order by flpnNumber ";
            }
            $sqlOrder .= " ";
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("flnrs", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows, max(flpn.flpn_number) as maxFlpnNumber
                    FROM ams_al.ams_al_flp_number flpn
                    left join ams_wad.cfg_airport_destination d on d.dest_id = flpn.dest_id
                    join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'max_ticket_price_cpd_kg_per_km' ) cpdmax on 1=1
                    join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'flight_charter_pct_from_max_price' ) flp on 1=1
                    join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
                     ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
            $response->addData("maxFlpnNumber", $rowJson->maxFlpnNumber);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
    public static function PriceHistoryTable($params) {
        $mn = "AmsAlFlightNumber::PriceHistoryTable()";
        AmsAlLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            //--
            $sql = "SELECT flpn_id as flpnId, price_f as priceF, price_b as priceB, 
                    price_e as priceE, price_c_p_kg as priceC, min(udate) as udate 
                    FROM ams_al.ams_al_flp_number_h ";
            
            
            $sqlWhere="";
            if(isset($params->flpnId) && strlen($params->flpnId)>0){
                if(isset($sqlWhere) && strlen($sqlWhere)>1){
                    $sqlWhere .= " AND flpn_id =".$params->flpnId." ";
                }
                else{
                    $sqlWhere = " WHERE flpn_id =".$params->flpnId." ";
                }
            }
            
           
            $sqlOrder = " group by flpn_id, price_f, price_b, price_e, price_c_p_kg
                            order by udate";
           
            $sql .= $sqlWhere.$sqlOrder;
            $sql .= " LIMIT ? OFFSET ? ";
            AmsAlLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->qry_limit, $params->qry_offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("prices", $ret_json_data);
            
            $sql = "SELECT count(*) as totalRows
                    FROM ams_al.ams_al_flp_number_h
                     ".(isset($sqlWhere) && strlen($sqlWhere)>1?($sqlWhere." and 1=?"):" where 1=? ")  ;
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $rowJson = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $rowJson->totalRows);
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsAlLogger::log($mn, " response = " . $response->toJSON());
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    // </editor-fold>
    
   // <editor-fold defaultstate="collapsed" desc="DB Methods">
     
     static function Create($dataJson, $conn, $mn, $logModel){
        
        $sql = "call ams_al.sp_al_flpn_create(?, ?, ?, ?, ?)";
        $bound_params_r = ["iiiii",
            ((!isset($dataJson->alId)) ? null : $dataJson->alId),
            ((!isset($dataJson->depApId)) ? null : $dataJson->depApId),
            ((!isset($dataJson->arrApId)) ? null : $dataJson->arrApId),
            ((!isset($dataJson->flpnNumber)) ? null : $dataJson->flpnNumber),
            ((!isset($dataJson->flpnId)) ? null : $dataJson->flpnId)
        ];

        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, "count(result_r)=".count($result_r));
        $data = null;
        if(isset($result_r) && count($result_r)>0){
            $data = $result_r[0];
            $json = json_decode(json_encode($result_r[0]));
            //AmsAlLogger::log($mn, " Create json=".json_encode($json));
            if(isset($json->out_flpn_id)){
                 $id = $json->out_flpn_id;    
            }else if(isset($json->out_route_id)){
                 $id = $json->out_route_id;
            }else{
                $id = $data[0];
            }
           
        }
        AmsAlLogger::log("$mn", "id=" . $id);         
        return $id;
    }
    
    static function GetRouteId($dataJson, $conn, $mn, $logModel){
        
        $sql = "call ams_al.sp_flp_route_create(?, ?, ?)";
        $bound_params_r = ["iii",
            ((!isset($dataJson->depApId)) ? null : $dataJson->depApId),
            ((!isset($dataJson->arrApId)) ? null : $dataJson->arrApId),
            ((!isset($dataJson->routeId)) ? null : $dataJson->routeId)
        ];

        
        $result_r = $conn->preparedSelect($sql, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, " count(result_r)=".count($result_r));
        $data = null;
        if(isset($result_r) && count($result_r)>0){
            $data = $result_r[0];
            //AmsAlLogger::log($mn, "count(data)=".count($data));
            $json = json_decode(json_encode($data));
            //AmsAlLogger::log($mn, " json=".json_encode($json));
            //$id = ($data['out_route_id:']);
            $id = $json->out_route_id;
        }
        AmsAlLogger::log("$mn", "id=" . $id);         
        return $id;
    }
    
    static function Update($dataJson, $conn, $mn, $logModel){
        
        $dataJson->routeId = AmsAlFlightNumber::GetRouteId($dataJson, $conn, $mn, $logModel);
        
        $strSQL = "UPDATE ams_al.ams_al_flp_number 
                    SET flpn_number=?, al_id=?,
                    dep_ap_id=?, arr_ap_id=?,
                    dest_id=?, route_id=?,
                    flight_h=?, price_e=?, price_b=?, 
                    price_f=?, price_c_p_kg=?
                    WHERE flpn_id = ?  " ;

        $bound_params_r = ["iiiiiidddddi",
            ((!isset($dataJson->flpnNumber)) ? null : $dataJson->flpnNumber),
            ((!isset($dataJson->alId)) ? null : $dataJson->alId),
             ((!isset($dataJson->depApId)) ? null : $dataJson->depApId),
             ((!isset($dataJson->arrApId)) ? null : $dataJson->arrApId),
             ((!isset($dataJson->destId)) ? null : $dataJson->destId),
             ((!isset($dataJson->routeId)) ? null : $dataJson->routeId),
            ((!isset($dataJson->flightH)) ? null : $dataJson->flightH),
            ((!isset($dataJson->priceE)) ? null : $dataJson->priceE),
            ((!isset($dataJson->priceB)) ? null : $dataJson->priceB),
            ((!isset($dataJson->priceF)) ? null : $dataJson->priceF),
            ((!isset($dataJson->priceCpKg)) ? null : $dataJson->priceCpKg),
            $dataJson->flpnId
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->flpnId;
    }
    
    static function UpdateRoutPrices($dataJson, $conn, $mn, $logModel){
        
        $dataJson->routeId = AmsAlFlightNumber::GetRouteId($dataJson, $conn, $mn, $logModel);
        
        $strSQL = "UPDATE ams_al.ams_al_flp_number 
                    SET price_e=?, price_b=?, 
                    price_f=?, price_c_p_kg=?
                    WHERE al_id=? and route_id = ?  " ;

        $bound_params_r = ["ddddii",
            ((!isset($dataJson->priceE)) ? null : $dataJson->priceE),
            ((!isset($dataJson->priceB)) ? null : $dataJson->priceB),
            ((!isset($dataJson->priceF)) ? null : $dataJson->priceF),
            ((!isset($dataJson->priceCpKg)) ? null : $dataJson->priceCpKg),
            ((!isset($dataJson->alId)) ? null : $dataJson->alId),
            ((!isset($dataJson->routeId)) ? null : $dataJson->routeId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //AmsAlLogger::log($mn, "affectedRows=" . $affectedRows);
                    
        return $dataJson->flpnId;
    }
    
    static function SelectJson($id, $conn, $mn, $logModel){
        
        $sql = "SELECT flpn.flpn_id as flpnId,
                        flpn.flpn_number as flpnNumber,
                        concat( LPAD(flpn.flpn_number, 4, '0'), ' ',
                            ifnull(dep.ap_iata,  dep.ap_icao) ,' ',
                            ifnull(arr.ap_iata, arr.ap_icao) )   as flpnName,
                        flpn.al_id as alId,
                        flpn.dep_ap_id as depApId,
                        flpn.arr_ap_id as arrApId,
                        flpn.dest_id as destId,
                        flpn.route_id as routeId,
                        flpn.flight_h as flightH,
                        flpn.price_e as priceE,
                        flpn.price_b as priceB,
                        flpn.price_f as priceF,
                        flpn.price_c_p_kg as priceCpKg,
                        flpn.adate, flpn.udate,
                        ifnull(flpn.flpn_schedules_count, 0) as flpnSchedulesCount,
                        Round(flp.param_value * d.max_ticket_price_e, 2) as maxPriceE,
                        Round(flp.param_value * d.max_ticket_price_b, 2)  as maxPriceB,
                        Round(flp.param_value * d.max_ticket_price_f, 2)  as maxPriceF,
                        ROUND((d.distance_km * cpdmax.param_value), 4) as maxPriceCPerKg,
                        d.distance_km as distanceKm,
                        d.min_rw_lenght_m as minRunwayM,
                        d.min_mtow_kg as minMtow,
                        sum(ifnull(st.fl_count,0)) as flCount, round(sum(ifnull(st.revenue, 0)),2) as revenue, 
                        round(avg(ifnull(st.avg_delay_min,0)),0) as avgDelayMin, 
                        round(avg(ifnull(st.avg_pax_pct_full,0)),0) as avgPaxPctFull, 
                        round(avg(ifnull(st.avg_payload_kg_pct_full,0)),0) as avgCargoKgPctFull
                    FROM ams_al.ams_al_flp_number flpn
                    left join ams_wad.cfg_airport_destination d on d.dest_id = flpn.dest_id
                    join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'max_ticket_price_cpd_kg_per_km' ) cpdmax on 1=1
                    join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'flight_charter_pct_from_max_price' ) flp on 1=1
                    join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
                    join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
                    left join ams_al.ams_al_flp_number_schedule flpns on flpns.flpn_id = flpn.flpn_id
                    left join ams_al.ams_al_flp_number_schedule_st st on st.flpns_id = flpns.flpns_id
                    WHERE flpn.flpn_id = ? group by flpn.flpn_id" ;

        $bound_params_r = ["i",$id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
                    
        return $ret_json_data;
    }
    
    static function Delete($id, $conn, $mn, $logModel){
        
        $strSQL = "DELETE FROM ams_al.ams_al_flp_number
                   WHERE flpn_id = ? " ;

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsAlLogger::log($mn, "deleted route_id =" . $id);
                    
        return $id;
    }
    
    // </editor-fold>
}

class AmsDestination {
    
    // <editor-fold defaultstate="collapsed" desc="Methods">
    
    public static function LoadByAirports($dataJson) {
        $mn = "AmsDestination::LoadByAirports()";
        AmsAlLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsAlConnection::dbConnect();
            $logModel = AmsAlLogger::currLogger()->getModule($mn);
            $objArrJ = AmsDestination::SelectJson($dataJson, $conn, $mn, $logModel);
            
            if(isset($objArrJ) && count($objArrJ)>0){
               $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsAlLogger::logError($mn, $ex);
            $response = null;
        }
        AmsAlLogger::logEnd($mn);
        return $response;
    }
    
     // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
     
    static function SelectJson($dataJson, $conn, $mn, $logModel){
        
        $sql = "call ams_wad.get_cfg_airport_destination_row(?, ?)";
        $bound_params_r = ["ii",
           ((!isset($dataJson->depApId)) ? null : $dataJson->depApId),
            ((!isset($dataJson->arrApId)) ? null : $dataJson->arrApId),
        ];

        $ret_json_data = $conn->preparedSelect($sql, $bound_params_r, $logModel);
              
        return $ret_json_data;
    }
    
    // </editor-fold>
}
