<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : AmsOne    
 *  Date Creation       : Feb 13, 2025 
 *  Filename            : AmsUser.class
 *  Author              : izior
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2025 izior
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 */

/**
 * Description of AmsUser
 *
 * @author izior
 */
class AmsUser {

    // <editor-fold defaultstate="collapsed" desc="Fields">

    public $userId;
    public $name;
    public $email;
    public $password;
    public $roleId;
    public $isReceiveEmails;
    public $ipAddress;
    public $adate;
    public $udate;

    public function toJSON() {
        return json_encode($this);
    }

    public static function nameFromEmail($email) {
        $retValue = $email;
        if (isset($email)) {
            $valueArr = explode('@', $email, 2);
            if (sizeof($valueArr) > 0)
                $retValue = $valueArr[0];
        }
        return $retValue;
    }

    public static function decryptPwd($mn, $encrypted) {
        $key = pack("H*", "0123456789abcdef0123456789abcdef");
        $iv = pack("H*", "abcdef9876543210abcdef9876543210");
        $password = null;
        try {
            $encrypted2 = base64_decode($encrypted);
            $password1 = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encrypted2, MCRYPT_MODE_CBC, $iv);
            //AmsOneLogger::log($mn, " password1 = " . $password1);
            $pad = substr($password1, -1);
            $password = rtrim($password1, $pad);
        } catch (Exception $ex) {
            BwtLogger::logError($mn, $ex);
        }
        return $password;
    }

    public static function fromJSON($dataJson) {
        $rv = new AmsUser();
        $rv->userId = (!isset($dataJson->userId)) ? null : $dataJson->userId;
        $rv->name = (!isset($dataJson->name)) ? null : $dataJson->name;
        $rv->email = (!isset($dataJson->email)) ? null : $dataJson->email;
        $rv->password = (!isset($dataJson->password)) ? null : $dataJson->password;
        $rv->roleId = (!isset($dataJson->roleId)) ? null : $dataJson->roleId;
        $rv->destId = (!isset($dataJson->destId)) ? null : $dataJson->destId;
        $rv->routeId = (!isset($dataJson->routeId)) ? null : $dataJson->routeId;
        $rv->isReceiveEmails = (!isset($dataJson->isReceiveEmails)) ? 0 : $dataJson->isReceiveEmails;
        $rv->ipAddress = (!isset($dataJson->ipAddress)) ? null : $dataJson->ipAddress;
        $rv->adate = (!isset($dataJson->adate)) ? null : $dataJson->adate;
        $rv->udate = (!isset($dataJson->udate)) ? null : $dataJson->udate;
        return $rv;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($id) {
        $mn = "AmsUser::LoadById(" . $id . ")";
        AmsOneLogger::logBegin($mn);
        $response = null;
        try {
            $conn = AmsOneConnection::dbConnect();
            $logModel = AmsOneLogger::currLogger()->getModule($mn);
            $arrJson = AmsUser::SelectJson($id, $conn, $mn, $logModel);

            if (isset($arrJson) && count($arrJson) > 0) {
                $response = json_decode(json_encode($arrJson[0]));
                $response->password = null;
            }
        } catch (Exception $ex) {
            AmsOneLogger::logError($mn, $ex);
            $response = null;
        }
        AmsOneLogger::logEnd($mn);
        return $response;
    }

    public static function Login($email, $encrypted) {
        $mn = "AmsUser::Login(" . $email . ")";
        AmsOneLogger::logBegin($mn);
        $response = null;
        try {
            $password = AmsUser::decryptPwd($mn, $encrypted);
            $conn = AmsOneConnection::dbConnect();
            $logModel = AmsOneLogger::currLogger()->getModule($mn);
            $arrJson = AmsUser::LoginJson($email, $password, $conn, $mn, $logModel);

            if (isset($arrJson) && count($arrJson) > 0) {
                $response = json_decode(json_encode($arrJson[0]));
                $response->password = null;
            }
        } catch (Exception $ex) {
            AmsOneLogger::logError($mn, $ex);
            $response = null;
        }
        AmsOneLogger::logEnd($mn);
        return $response;
    }

    public static function Save($data) {
        $mn = "AmsUser::Save()";
        AmsOneLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsOneLogger::log($mn, " isset route_id = " . isset($dataJson->routeId));
        $response;
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        try {
            $conn = AmsOneConnection::dbConnect();
            $logModel = AmsOneLogger::currLogger()->getModule($mn);
            $objId = null;
            if (isset($dataJson->userId)) {
                //AmsOneLogger::log($mn, "Update  userId =" . $dataJson->userId);
                $objId = $dataJson->userId;
                $dataJson->ipAddress = $ipAddress;
                AmsUser::Update($dataJson, $conn, $mn, $logModel);
            } else {
                //AmsOneLogger::log($mn, "Create route ");
                $dataJson->ipAddress = $ipAddress;
                $objId = AmsUser::Create($dataJson, $conn, $mn, $logModel);
            }

            //AmsOneLogger::log($mn, " routeId =" . $objId);
            if (isset($objId)) {
                $objArrJ = AmsUser::SelectJson($objId, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = json_decode(json_encode($objArrJ[0]));
                }
            }
        } catch (Exception $ex) {
            AmsOneLogger::logError($mn, $ex);
        }

        //AmsOneLogger::log($mn, " response = " . json_encode($response));
        AmsOneLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">

    static function Create($dataJson, $conn, $mn, $logModel) {

        $strSQL = "INSERT INTO iordanov_amsone_al.ams_user
            (user_name, user_email, user_password,
            user_role, is_receive_emails, ip_address)
            VALUES(?, ?, ?, ?, ?, ?)";

        $bound_params_r = ["sssiis",
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->email)) ? null : $dataJson->email),
            ((!isset($dataJson->password)) ? null : $dataJson->password),
            ((!isset($dataJson->roleId)) ? 3 : $dataJson->roleId),
            ((!isset($dataJson->isReceiveEmails)) ? 0 : $dataJson->isReceiveEmails),
            ((!isset($dataJson->ipAddress)) ? null : json_encode($dataJson->ipAddress)),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        AmsOneLogger::log("$mn", "id=" . $id);

        return $id;
    }

    static function Update($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE iordanov_amsone_al.ams_user
                    SET user_name=?, user_email=?, 
                    user_role=?, 
                    is_receive_emails=?, 
                    ip_address=?
                    WHERE user_id = ? ";

        $bound_params_r = ["ssiisi",
            ((!isset($dataJson->name)) ? null : $dataJson->name),
            ((!isset($dataJson->email)) ? null : $dataJson->email),
            ((!isset($dataJson->roleId)) ? 3 : $dataJson->roleId),
            ((!isset($dataJson->isReceiveEmails)) ? 0 : $dataJson->isReceiveEmails),
            ((!isset($dataJson->ipAddress)) ? null : json_encode($dataJson->ipAddress)),
            ($dataJson->userId),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        AmsOneLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->userId;
    }

    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = "SELECT u.user_id    as userId, 
                    u.user_name     as name,
                    u.user_email    as email, 
                    u.user_password      as password, 
                    u.user_role     as roleId, 
                    u.is_receive_emails as isReceiveEmails, 
                    u.ip_address    as ipAddress, 
                    u.adate, u.udate
                    FROM iordanov_amsone_al.ams_user u
                    WHERE u.user_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function Delete($id, $conn, $mn, $logModel) {
        $sql = "call iordanov_amsone_al.sp_user_delete(?)";
        $bound_params_r = ["i", $id];
        $affected_rows = $conn->preparedUpdate($sql, $bound_params_r, $logModel);
        $logModel->logDebug($mn, "deleted rows : " . $affected_rows);
        return $id;
    }

    static function LoginJson($email, $pwd, $conn, $mn, $logModel) {

        $sql = "SELECT u.user_id    as userId, 
                    u.user_name     as name,
                    u.user_email    as email, 
                    null as password, 
                    u.user_role     as roleId, 
                    u.is_receive_emails as isReceiveEmails, 
                    u.ip_address    as ipAddress, 
                    u.adate, u.udate
                    FROM iordanov_amsone_al.ams_user u
                    WHERE u.user_email = ? and u.user_password =?";

        $bound_params_r = ["ss", $email, $pwd];
        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
        return $ret_json_data;
    }

    static function CheckEmailJson($email, $conn, $mn, $logModel) {

        $sql = "SELECT count(*) AS rowCount
                    FROM iordanov_amsone_al.ams_user u
                    WHERE u.user_email = ? ";

        $bound_params_r = ["s", $email];
        $rv = -1;
        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
        if (isset($ret_json_data) && count($ret_json_data) > 0) {
            $obj = json_decode(json_encode($ret_json_data[0]));
            $rv = $obj->rowCount;
        }
        return $rv;
    }

    // </editor-fold>
}
