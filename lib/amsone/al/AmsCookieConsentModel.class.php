<?php

/*
 * -----------------------------------------------------------------------------
 *  Project             : common    
 *  Date Creation  : Sep 27, 2018 
 *  Filename          : AmsCookieConsentModel.class
 *  Author             : IZIordanov
 * -----------------------------------------------------------------------------
 *  Copyright(C) 2000-2018 IZIordanov
 *  
 *  This program is free software; you can redistribute it and/or modify it under 
 *  the terms of the GNU General Public License published by the Free Software Foundation.
 * -----------------------------------------------------------------------------
 **/

/**
 * Description of AmsCookieConsentModel
 *
 * @author IZIordanov
 **/
class AmsCookieConsentModel {

    public $id;
    public $ipaddress;
    public $userId;
    public $consentUseLocalStore;
    public $consentUseStatistics;
    public $consentAdvertisement;
    public $adate;
    public $udate;

    // <editor-fold defaultstate="collapsed" desc="Methods">

    public static function LoadById($id) {
        $mn = "AmsCookieConsentModel::LoadById(" . $id . ")";
        AmsOneLogger::logBegin($mn);
        $response = new AmsCookieConsentModel();
        try {
            $conn = AmsOneConnection::dbConnect();
            $logModel = AmsOneLogger::currLogger()->getModule($mn);
            $objArrJ = AmsCookieConsentModel::SelectJson($id, $conn, $mn, $logModel);
            if (isset($objArrJ) && count($objArrJ) > 0) {
                $response = json_decode(json_encode($objArrJ[0]));
            }
        } catch (Exception $ex) {
            AmsOneLogger::logError($mn, $ex);
            $response = null;
        }
        AmsOneLogger::logEnd($mn);
        return $response;
    }

    public static function Save($data) {
        $mn = "AmsCookieConsentModel::Save()";
        AmsOneLogger::logBegin($mn);
        $dataJson = $data; //json_decode($data);
        //AmsOneLogger::log($mn, " flightName = " . $dataJson->flightName);
        $response = new AmsCookieConsentModel();
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        try {
            $conn = AmsOneConnection::dbConnect();
            $logModel = AmsOneLogger::currLogger()->getModule($mn);
            $id = null;
            $dataJson->ipaddress = $ipAddress;
            if (isset($dataJson->id)) {
                //AmsOneLogger::log($mn, "Update  book_id =" . $dataJson->id);
                $id = $dataJson->id;
                AmsCookieConsentModel::Update($dataJson, $conn, $mn, $logModel);
            } else {
                //AmsOneLogger::log($mn, "Create CfgCharterModel");
                $id = AmsCookieConsentModel::Create($dataJson, $conn, $mn, $logModel);
            }

            //AmsOneLogger::log($mn, " id =" . $id);
            if (isset($id)) {
                $objArrJ = AmsCookieConsentModel::SelectJson($id, $conn, $mn, $logModel);
                if (isset($objArrJ) && count($objArrJ) > 0) {
                    $response = json_decode(json_encode($objArrJ[0]));
                }
            }
        } catch (Exception $ex) {
            AmsOneLogger::logError($mn, $ex);
        }

        //AmsOneLogger::log($mn, " response = " . json_encode($response));
        AmsOneLogger::logEnd($mn);
        return $response;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="DB Methods">
    
    static function SelectJson($id, $conn, $mn, $logModel) {

        $sql = "select consent_id as id, ipaddress, user_id as userId, 
                consent_local_store as consentUseLocalStore, 
                consent_statistics as consentUseStatistics, 
                consent_advertisement as consentAdvertisement,
                adate, udate
                FROM iordanov_amsone_al.ams_user_cookies_consent
                WHERE consent_id = ? ";

        $bound_params_r = ["i", $id];

        $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);

        return $ret_json_data;
    }

    static function Create($dataJson, $conn, $mn, $logModel) {

        $strSQL = "INSERT INTO iordanov_amsone_al.ams_user_cookies_consent 
            ( ipaddress, user_id, consent_local_store, consent_statistics, consent_advertisement)
            VALUES(?, ?, ?, ?, ?)";

        $bound_params_r = ["siiii",
            ((!isset($dataJson->ipaddress)) ? null : $dataJson->ipaddress),
            ((!isset($dataJson->userId)) ? null : $dataJson->userId),
            ((!isset($dataJson->consentUseLocalStore)) ? null : ($dataJson->consentUseLocalStore)),
            ((!isset($dataJson->consentUseStatistics)) ? null : ($dataJson->consentUseStatistics)),
            ((!isset($dataJson->consentAdvertisement)) ? null : $dataJson->consentAdvertisement),
        ];

        $id = $conn->preparedInsert($strSQL, $bound_params_r, $logModel);
        //AmsOneLogger::log("$mn", "id=" . $id);

        return $id;
    }

    static function Update($dataJson, $conn, $mn, $logModel) {

        $strSQL = "UPDATE iordanov_amsone_al.ams_user_cookies_consent
                    SET ipaddress=?,        user_id=?, 
                    consent_local_store=?,  consent_statistics=?,
                    consent_advertisement=?
                    WHERE consent_id = ? ";

        $bound_params_r = ["siiiii",
            ((!isset($dataJson->ipaddress)) ? null : $dataJson->ipaddress),
            ((!isset($dataJson->userId)) ? null : $dataJson->userId),
            ((!isset($dataJson->consentUseLocalStore)) ? null : $dataJson->consentUseLocalStore),
            ((!isset($dataJson->consentUseStatistics)) ? null : $dataJson->consentUseStatistics),
            ((!isset($dataJson->consentAdvertisement)) ? null : $dataJson->consentAdvertisement),
            ((!isset($dataJson->id)) ? null : $dataJson->id),
        ];

        $affectedRows = $conn->preparedUpdate($strSQL, $bound_params_r, $logModel);
        //AmsOneLogger::log($mn, "affectedRows=" . $affectedRows);

        return $dataJson->id;
    }

    static function Delete($id, $conn, $mn, $logModel) {

        $strSQL = "DELETE FROM iordanov_amsone_al.ams_user_cookies_consent
                   WHERE consent_id = ? ";

        $bound_params_r = ["i", $id];
        $id = $conn->preparedDelete($strSQL, $bound_params_r, $logModel);
        AmsOneLogger::log($mn, "deleted consent_id =" . $id);

        return $id;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Table Methods">

    public static function ConsenTable($params, $actorId) {
        $mn = "AmsCookieConsentModel::ConsenTable()";
        AmsOneLogger::logBegin($mn);
        $response = new Response();
        try {
            $conn = AmsOneConnection::dbConnect();
            $logModel = AmsOneLogger::currLogger()->getModule($mn);
            //UNIX_TIMESTAMP
            $sql = "SELECT select c.consent_id as id, c.ipaddress, c.user_id as userId, 
                    c.consent_local_store as consentUseLocalStore, 
                    c.consent_statistics as consentUseStatistics, 
                    c.consent_advertisement as consentAdvertisement,
                    c.adate, c.udate,
                    u.user_name userName, u.e_mail as email
                    FROM iordanov_amsone_al.ams_user_cookies_consent c
                    left join iordanov_amsone_al.bwt_user u on u.user_id = c.user_id";

            $sqlWhere = "";
            if (isset($params->consentId) && strlen($params->consentId) > 0) {
                $sqlWhere = " WHERE c.consent_id = " . $params->consentId . " ";
            }
            $sqlOrder = " order by c.adate desc ";
           
            $sql .= (isset($sqlWhere) && strlen($sqlWhere) > 1 ? $sqlWhere : "");
            $sql .= (isset($sqlOrder) && strlen($sqlOrder) > 1 ? $sqlOrder : "");
            $sql .= " LIMIT ? OFFSET ? ";
            AmsOneLogger::log($mn, " sql= " . $sql . " ");
            $bound_params_r = ["ii", $params->limit, $params->offset];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $response->addData("consents", $ret_json_data);

            $sql = "SELECT count(*) as totalRows
                    FROM iordanov_amsone_al.ams_user_cookies_consent c
                    left join iordanov_amsone_al.bwt_user u on u.user_id = c.user_id " .
                    (isset($sqlWhere) && strlen($sqlWhere) > 1 ? ($sqlWhere . " and 1=?") : " where 1=? ");
            $bound_params_r = ["i", 1];
            $ret_json_data = $conn->SelectJson($sql, $bound_params_r, $logModel);
            $obj = json_decode(json_encode($ret_json_data[0]));
            $response->addData("rowsCount", $obj->totalRows);
        } catch (Exception $ex) {
            AmsOneLogger::logError($mn, $ex);
            $response = new Response($ex);
        }

        //AmsOneLogger::log($mn, " response = " . $response->toJSON());
        AmsOneLogger::logEnd($mn);
        return $response;
    }
    
    // </editor-fold>
}